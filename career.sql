-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `career`;
CREATE TABLE `career` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `contact_no` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `career` (`id`, `name`, `contact_no`, `email`, `location`, `position`) VALUES
(7,	'Tanima Chowdhury',	'01622094043',	'tanimaspayel@gmail.com',	'Dhaka',	'Call Center'),
(17,	'Mostafizur Rahman',	'01600230358',	'notfound@gmail.com',	'Chattogram',	'IT Department- Any'),
(16,	'Md Alek',	'01300277290',	'notfound@gmail.com',	'Dhaka',	'Any');

-- 2023-01-22 11:02:52
