-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `career` (`id`, `name`, `contact_no`, `email`, `location`, `position`) VALUES
(7,	'Tanima Chowdhury',	'01622094043',	'tanimaspayel@gmail.com',	'Dhaka',	'Call Center'),
(17,	'Mostafizur Rahman',	'01600230358',	'notfound@gmail.com',	'Chattogram',	'IT Department- Any'),
(16,	'Md Alek',	'01300277290',	'notfound@gmail.com',	'Dhaka',	'Any');

DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `phone2` varchar(50) NOT NULL,
  `phone_int` varchar(50) NOT NULL,
  `neighborhood` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `mothers_name` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `source` varchar(50) NOT NULL,
  `lead_forword` varchar(50) NOT NULL,
  `interested_project` varchar(50) NOT NULL,
  `interested_project_type` varchar(50) NOT NULL,
  `note` text NOT NULL,
  `birthdate` varchar(40) NOT NULL,
  `occupation` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `religion` varchar(50) NOT NULL,
  `call_type` varchar(50) NOT NULL,
  `purpose` varchar(50) NOT NULL,
  `preferd` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- 2023-01-22 11:03:02
