<?php
	class phone_number_manager_model extends CI_Model
	{
            //public $generalQuery="select * from icx";
            public function __construct() {
                    parent::__construct();
                    $this->load->model('utility_model');
                    $this->load->model('prime_model');

            }

            public function get_by_query($query){
                return $this->prime_model->getByQuery($query);
            }
            public function get_phone_numbers_by_client($client_id)
            {
                //CREATE USER 'monty'@'%' IDENTIFIED BY 'some_pass';
                //GRANT ALL PRIVILEGES ON *.* TO 'monty'@'%'     WITH GRANT OPTION;

                /*$dbc= mysqli_connect('202.164.208.212','monty','some_pass','csmsc') or die("error connecting $dbName");
                $query="select * from sms_from_user";
                $result=  mysqli_query($dbc, $query)or die("error executing query:$query");
                $objectsArray=array();
                while($row = mysqli_fetch_array($result)) {
                        $objectsArray[]=  $row ; //change here
                }
                mysqli_close($dbc);
                print_r($objectsArray);*/
                
                /*$dbc= mysqli_connect('172.16.252.174','test123','1234','number_management') or die("error connecting number_management database");
                $query="select * from number where client_id=$client_id";
                $result=  mysqli_query($dbc, $query)or die("error executing query:$query");
                $objectsArray=array();
                while($row = mysqli_fetch_array($result)) {
                        $objectsArray[]=  $row ; //change here
                }
                mysqli_close($dbc);
                return $objectsArray;*/
                
                $phone_numbers= $this->prime_model->getByQuery("select * from number where client_id=$client_id");
                return $phone_numbers;
            }
                
	}
?>