<?php
class Timer
{
    public   function getCurrentTime()
    {
        echo $date = date('m/d/Y h:i:s a', time());
        return date("D M d, Y G:i a"); 
    }

    public static   function getDifference($startTime,$endTime)
    {
        //echo $startTime;
        $startDate= date( 'Y-m-d H:i:s',$startTime);
        //echo $startDate;
        $endDate= date( 'Y-m-d H:i:s',$endTime);
        $startDateTime=new DateTime($startDate);
        $endDateTime=new DateTime($endDate);
        $interval = $startDateTime->diff($endDateTime);
        $strDifference= $interval->y . " years, " . $interval->m." months, ".$interval->d." days".$interval->h." hours ".$interval->i." minutes ".$interval->s." seconds";
        return $strDifference;

    }

}
?>
