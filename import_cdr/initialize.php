<?php
date_default_timezone_set('Asia/Dhaka');
ini_set('memory_limit', '10240M');
ini_set('max_execution_time', -1);
spl_autoload_register(function ($class) {
    include 'Classes/' . $class . '.php';
});
//session must be loaded after loading class or it will cause problem
/*if (session_status() == PHP_SESSION_NONE) {
    // server should keep session data for AT LEAST 1 hour
    ini_set('session.gc_maxlifetime', 36000); //3600=1 hour //36000=10 hour

    // each client should remember their session id for EXACTLY 1 hour
    session_set_cookie_params(36000);

    session_start(); //session must be loaded after loading class or it will cause problem
}*/
//For versions of PHP < 5.4.0
if(session_id() == '') {
    session_start();
}

?>