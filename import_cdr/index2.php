<?php
	date_default_timezone_set('Asia/Dhaka');
	ini_set('memory_limit', '-1');
	ini_set('max_execution_time', -1);
	ignore_user_abort(true);

	function insert_cdr_from_ip($ip,$from_date,$to_date){
		try
        {
			
			echo "ip : $ip";
			$db_name='iptsp_billing';
			$user='root';
			$password='nopass1234';

			$switch_ip=$ip;
			$switch_db="asteriskcdrdb";
			$switch_user="test123";
			$switch_pass="test123";

			$dbc_billing= mysqli_connect('localhost',$user,$password,$db_name) or die("error connecting $db_name database");
			
			$dbc_switch= mysqli_connect($switch_ip,$switch_user,$switch_pass,$switch_db) or die("error connecting $switch_db database");
			$query="select * from cdr where calldate between '$from_date' and '$to_date'";
			echo $query;
			$result=  mysqli_query($dbc_switch, $query)or die("error executing query:$query");
			if(mysqli_num_rows($result)>0){
				$insert_query="insert into cdr(calldate,clid,src,dst,dcontext,channel,dstchannel,lastapp,lastdata,duration,billsec,disposition,amaflags,accountcode,userfield,uniqueid) values ";
				$values='';
				while($row = mysqli_fetch_array($result)) {
						$calldate=mysqli_real_escape_string($dbc_billing,$row['calldate']) ; //change here
						$clid=mysqli_real_escape_string($dbc_billing,$row['clid']);
						$src=mysqli_real_escape_string($dbc_billing,$row['src']);
						$dst=mysqli_real_escape_string($dbc_billing,$row['dst']);
						$dcontext=mysqli_real_escape_string($dbc_billing,$row['dcontext']);
						$channel=mysqli_real_escape_string($dbc_billing,$row['channel']);
						$dstchannel=mysqli_real_escape_string($dbc_billing,$row['dstchannel']);
						$lastapp=mysqli_real_escape_string($dbc_billing,$row['lastapp']);
						$lastdata=mysqli_real_escape_string($dbc_billing,$row['lastdata']);
						$duration=mysqli_real_escape_string($dbc_billing,$row['duration']);
						$billsec=mysqli_real_escape_string($dbc_billing,$row['billsec']);
						$disposition=mysqli_real_escape_string($dbc_billing,$row['disposition']);
						$amaflags=mysqli_real_escape_string($dbc_billing,$row['amaflags']);
						$accountcode=mysqli_real_escape_string($dbc_billing,$row['accountcode']);
						$userfield=mysqli_real_escape_string($dbc_billing,$row['userfield']);
						$uniqueid=mysqli_real_escape_string($dbc_billing,$row['uniqueid']);
						
						$values=$values.",('$calldate','$clid','$src','$dst','$dcontext','$channel','$dstchannel','$lastapp','$lastdata',$duration,$billsec,'$disposition',$amaflags,'$accountcode','$userfield','$uniqueid')";
				}
            
				$values=trim($values,',');
				//echo 'after trim'.$values.'</br>';
				$insert_query=$insert_query.$values;
				$result=  mysqli_query($dbc_billing, $insert_query)or die("error executing query:$insert_query");
				
				$file = '/var/www/html/iptspbilling/import_cdr/cdr.log';	//Open the file to get existing content
				//$current = file_get_contents($file); // Append a new data to the file
				$msg='';
				if($result){
					$msg= "#\n $from_date $ip data imported successfully.\n";
					
				}
				else{
					$msg= "#\n $from_date $ip  error in data import.\n";
				}			
				file_put_contents($file, $msg,FILE_APPEND); // Append to the file
			}
			
			mysqli_close($dbc_switch);    
			mysqli_close($dbc_billing);
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage(),"\n";
        }		
	}
	function delete_cdr($from_date,$to_date){
		try
        {
			$db_name='iptsp_billing';
			$user='root';
			$password='nopass1234';

			$dbc_billing= mysqli_connect('localhost',$user,$password,$db_name) or die("error connecting $db_name database");			
            $delete_query="Delete from cdr where calldate between '$from_date' and  '$to_date'";			
			$result=  mysqli_query($dbc_billing, $delete_query)or die("error executing query:$delete_query");
			echo $delete_query;
			
			$file = '/var/www/html/iptspbilling/import_cdr/cdr.log';	// Open the file to get existing content
			//$current = file_get_contents($file); // Append a new data to the file
			$msg="";
			if($result){
				$msg= "#\n $from_date :  delete query executed successfully."."\n";
				
			}
			else{
				$msg = "#\n $from_date: error executing delete query.\n";
			}			
			file_put_contents($file, $msg,FILE_APPEND);  // Append the contents  to the file
			mysqli_close($dbc_billing);
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage(),"\n";
        }		
	}
	
	

	
	
	$time_stamp = strtotime("-1 day");
	$date= date('Y-m-d', $time_stamp) ;
	$from_date=$date." 00:00:00";
	$to_date=$date." 23:59:59";
	
	/*echo $from_date;
	echo $to_date;*/
	
	
	$from_date='2017-06-09 00:00:00';
	$to_date  ='2017-06-09 23:59:59';
	

	delete_cdr($from_date,$to_date);
	
 	//insert_cdr_from_ip('172.16.252.157',$from_date,$to_date);
	insert_cdr_from_ip('172.16.252.174',$from_date,$to_date);
	insert_cdr_from_ip('172.16.252.113',$from_date,$to_date);
	insert_cdr_from_ip('172.25.0.2',$from_date,$to_date);
	insert_cdr_from_ip('172.16.252.3',$from_date,$to_date);
	insert_cdr_from_ip('172.16.252.173',$from_date,$to_date);
	/*insert_cdr_from_ip('172.20.13.78',$from_date,$to_date);
	insert_cdr_from_ip('172.20.13.66',$from_date,$to_date);*/
	
	


?>
