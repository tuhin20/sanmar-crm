<?php
	date_default_timezone_set('Asia/Dhaka');
	ini_set('memory_limit', '-1');
	ini_set('max_execution_time', -1);
	ignore_user_abort(true);

	function insert_scb_monem_from_ip($ip,$from_date,$to_date){
		try
        {
			
			echo "ip : $ip";
			$db_name='iptsp_billing';
			$user='root';
			$password='nopass1234';

			$switch_ip=$ip;
			$switch_db="scbmonem";
			$switch_user="test123";
			$switch_pass="test123";

			$dbc_billing= mysqli_connect('localhost',$user,$password,$db_name) or die("error connecting $db_name database");
			
			$dbc_switch= mysqli_connect($switch_ip,$switch_user,$switch_pass,$switch_db) or die("error connecting $switch_db database");
			$query="select * from survey where STR_TO_DATE(`start_time`,'%Y-%m-%d-%H:%i:%s') between '$from_date' and '$to_date'";
			echo $query;
			$result=  mysqli_query($dbc_switch, $query)or die("error executing query:$query");
			if(mysqli_num_rows($result)>0){
				$insert_query="insert into scb_monem(id	,start_time,	uniqueid,	number) values ";
				$values='';
				while($row = mysqli_fetch_array($result)) {
						$id=mysqli_real_escape_string($dbc_billing,$row['id']) ; //change here
						$start_time=mysqli_real_escape_string($dbc_billing,$row['start_time']);
						$uniqueid=mysqli_real_escape_string($dbc_billing,$row['uniqueid']);
						$number=mysqli_real_escape_string($dbc_billing,$row['number']);
						
						$values=$values.",($id,'$start_time','$uniqueid','$number')";
				}
            
				$values=trim($values,',');
				//echo 'after trim'.$values.'</br>';
				$insert_query=$insert_query.$values;
				$result=  mysqli_query($dbc_billing, $insert_query)or die("error executing query:$insert_query");
				
				$file = '/var/www/html/iptspbilling/import_cdr/monem_cdr.log';	//Open the file to get existing content
				//$current = file_get_contents($file); // Append a new data to the file
				$msg='';
				if($result){
					$msg= "#\n $from_date $ip data imported successfully.\n";
					
				}
				else{
					$msg= "#\n $from_date $ip  error in data import.\n";
				}			
				file_put_contents($file, $msg,FILE_APPEND); // Append to the file
			}
			
			mysqli_close($dbc_switch);    
			mysqli_close($dbc_billing);
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage(),"\n";
        }		
	}
	function duplicate_scb_monem_cdr($from_date,$to_date){
		try
        {
			$db_name='iptsp_billing';
			$user='root';
			$password='nopass1234';

			

			$dbc_billing= mysqli_connect('localhost',$user,$password,$db_name) or die("error connecting $db_name database");
			
			$insert_query="insert into scb_monem_cdr select * from cdr where calldate between '$from_date' and '$to_date' and src='09612351300' order by calldate";				
			$result=  mysqli_query($dbc_billing, $insert_query)or die("error executing query:$insert_query");
				
			$file = '/var/www/html/iptspbilling/import_cdr/monem_cdr.log';	//Open the file to get existing content
			$msg='';
			if($result){
				$msg= "#\n $from_date  data duplicated successfully.\n";
				
			}
			else{
				$msg= "#\n $from_date   error in data duplication.\n";
			}			
			file_put_contents($file, $msg,FILE_APPEND); // Append to the file			
			    
			mysqli_close($dbc_billing);
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage(),"\n";
        }		
	}
	function update_scb_monem_cdr($from_date,$to_date){
		try
        {
			$db_name='iptsp_billing';
			$user='root';
			$password='nopass1234';			

			$dbc_billing= mysqli_connect('localhost',$user,$password,$db_name) or die("error connecting $db_name database");
			
			$insert_query="update scb_monem_cdr set src=IFNULL((select number from scb_monem where uniqueid=scb_monem_cdr.uniqueid),src) where calldate between '$from_date' and '$to_date'";				
			$result=  mysqli_query($dbc_billing, $insert_query)or die("error executing query:$insert_query");
				
			$file = '/var/www/html/iptspbilling/import_cdr/monem_cdr.log';	//Open the file to get existing content
			$msg='';
			if($result){
				$msg= "#\n $from_date  data updated successfully.\n";
				
			}
			else{
				$msg= "#\n $from_date   error in data duplication.\n";
			}			
			file_put_contents($file, $msg,FILE_APPEND); // Append to the file			
			    
			mysqli_close($dbc_billing);
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage(),"\n";
        }		
	}
	function delete_cdr($from_date,$to_date){
		try
        {
			$db_name='iptsp_billing';
			$user='root';
			$password='nopass1234';

			$dbc_billing= mysqli_connect('localhost',$user,$password,$db_name) or die("error connecting $db_name database");
			
			$delete_query="Delete from scb_monem where STR_TO_DATE(`start_time`,'%Y-%m-%d-%H:%i:%s') between  '$from_date' and  '$to_date'";			
			$result=  mysqli_query($dbc_billing, $delete_query)or die("error executing query:$delete_query");
			
            $delete_query="Delete from scb_monem_cdr where calldate between '$from_date' and  '$to_date'";			
			$result=  mysqli_query($dbc_billing, $delete_query)or die("error executing query:$delete_query");
			echo $delete_query;
			
			$file = '/var/www/html/iptspbilling/import_cdr/monem_cdr.log';	// Open the file to get existing content
			//$current = file_get_contents($file); // Append a new data to the file
			$msg="";
			if($result){
				$msg= "#\n $from_date :  delete query executed successfully."."\n";
				
			}
			else{
				$msg = "#\n $from_date: error executing delete query.\n";
			}			
			file_put_contents($file, $msg,FILE_APPEND);  // Append the contents  to the file
			mysqli_close($dbc_billing);
        }
        catch(Exception $ex)
        {
            echo $ex->getMessage(),"\n";
        }		
	}
	
	

	
	
	$time_stamp = strtotime("-1 day");
	$date= date('Y-m-d', $time_stamp) ;
	$from_date=$date." 00:00:00";
	$to_date=$date." 23:59:59";
	
	/*echo $from_date;
	echo $to_date;*/
	
	
	/*$from_date='2017-10-09 00:00:00';
	$to_date  ='2017-10-14 23:59:59';*/
	

	delete_cdr($from_date,$to_date);
	
	insert_scb_monem_from_ip('172.16.252.174',$from_date,$to_date);
	duplicate_scb_monem_cdr($from_date,$to_date);
	update_scb_monem_cdr($from_date,$to_date);


?>
