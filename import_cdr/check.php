<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/x-icon" href="images/phoneBook.ico" />
		<title>Check CDR</title>

		<script src="javaScript/jquery-1.10.2.js"></script>
		<script src="javaScript/jquery-ui.js"></script>
		<!-- Bootstrap js -->
		<script src="javaScript/bootstrap.min.js"></script>
		<!-- Bootstrap core CSS -->
		<!--<link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">-->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Bootstrap theme -->
		<!--<link href="http://getbootstrap.com/dist/css/bootstrap-theme.min.css" rel="stylesheet">-->
		<link href="css/bootstrap-theme.min.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<!--<link href="http://getbootstrap.com/examples/theme/theme.css" rel="stylesheet">-->
		<!--<link rel="stylesheet" href="css/jquery-ui.css">-->
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="css/jquery-ui-timepicker-addon.css">
		<link rel="stylesheet" href="css/style2.css?v3"/>
		<!--<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>-->
		<script src="javaScript/jquery-ui.js"></script>
		<script type="text/javascript" language="javascript" src="javaScript/jquery-ui-timepicker-addon.js"></script>
		
	</head>

	<body>
		
		
		
		<nav class="navbar  navbar-inverse navbar-default">
		  <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="#">Check CDR</a>
			</div>
			
		  </div><!-- /.container-fluid -->
		</nav>
			
		<div class="container-fluid" id="wrapper">
			<?php require 'ctrl_loading_modal.php'; ?>
			<form class="form-inline" role="form"  method='post' id="create_user_form" action="check.php" >
						
				<div class="form-group">
					<label>From</label>
					<input class="form-control" type="text" id="from_date" name="from_date" value="<?php echo isset($_POST['from_date'])?$_POST['from_date']: '' ; ?>"  required>
					<?php //echo form_error('name','<label class="error">','</label>');?>
				</div>
				
				<div class="form-group">
					<label>To</label>
					<input class="form-control" type="text" id="to_date" name="to_date" value="<?php echo isset($_POST['to_date'])?$_POST['to_date']: '' ; ?>"  required>
					<?php //echo form_error('name','<label class="error">','</label>');?>
				</div>
				<div class="form-group">
					<button type="submit" id="generate_report" name="generate_report" class="btn btn-success" >View CDR Log</button> <!--onclick="return is_number_checked();"-->
				</div>
					
			</form>
			<style>
				#ui-datepicker-div { font-size: 12px; } 
			</style>
			<script>
				$('#from_date').datetimepicker({showSecond: true,timeFormat: 'HH:mm:ss',dateFormat: "yy-mm-dd "});
				$('#to_date').datetimepicker({showSecond: true,timeFormat: 'HH:mm:ss',dateFormat: "yy-mm-dd "});
				$('form').submit(function() {
					$('#loading_modal').modal('toggle');
					return true;
				});
			</script>
			<br>
			
			<?php
			require 'initialize.php';
			if(!isset($_POST['from_date'])||!isset($_POST['to_date'])){
				exit();
			}
			$startTime=time();
			
			$from_date='';
			$to_date='';
			if(isset($_POST['from_date']))
				$from_date=$_POST['from_date'];
			if(isset($_POST['to_date']))
				$to_date=$_POST['to_date'];
				
			$array_137=Billing::getCDRCount('localhost','root','nopass1234',$from_date,$to_date,'iptsp_billing');
			$array_174=Billing::getCDRCount('172.16.252.174','test123','test123',$from_date,$to_date);
			$array_113=Billing::getCDRCount('172.16.252.113','test123','test123',$from_date,$to_date);
			$array_25_0_2=Billing::getCDRCount('172.25.0.2','test123','test123',$from_date,$to_date);
			$array_3=Billing::getCDRCount('172.16.252.3','test123','test123',$from_date,$to_date);
			$array_173=Billing::getCDRCount('172.16.252.173','test123','test123',$from_date,$to_date);
			
			
			$endTime=time();
			$difference=Timer::getDifference($startTime,$endTime);	
			echo "<div class='alert alert-success' role='alert'> Total Time taken to fetch Data: $difference<br/> </div>";
			?>
			
			<table class="table table-bordered table-striped">
				<thead>
				  <tr>
					<th>DATE</th>
					<th>172.16.252.137</th>
					<th>Total</th>
					<th>172.16.252.174</th>
					<th>172.16.252.113</th>
					<th>172.25.0.2</th>
					<th>172.16.252.3</th>
					<th>172.16.252.173</th>
				  </tr>
				</thead>
				<tbody>
				<?php
				$begin = new DateTime( $from_date );
				$end = new DateTime( $to_date );

				$interval = DateInterval::createFromDateString('1 day');
				$period = new DatePeriod($begin, $interval, $end);				
				
				$total_137=0;
				$total_others=0;
				foreach ( $period as $dt ){
					//echo $dt->format( "l Y-m-d H:i:s\n" );
					
					$var=$dt->format( " Y-m-d " );
					
					$count_137=0;
					$count_total=0;
					$count_174=0;
					$count_113=0;
					$count_25_0_2=0;
					$count_3=0;
					$count_173=0;
					
					foreach ( $array_137 as $item ){
						if(trim($item['my_date'])==trim($var)){
						  $count_137=$item['total'];
						}
					}
					
					foreach ( $array_174 as $item ){
						if(trim($item['my_date'])==trim($var)){
						  $count_174=$item['total'];
						}
					}
					
					foreach ( $array_113 as $item ){
						if(trim($item['my_date'])==trim($var)){
						  $count_113=$item['total'];
						}
					}
					
					foreach ( $array_25_0_2 as $item ){
						if(trim($item['my_date'])==trim($var)){
						  $count_25_0_2=$item['total'];
						}
					}
					
					foreach ( $array_3 as $item ){
						if(trim($item['my_date'])==trim($var)){
						  $count_3=$item['total'];
						}
					}
					
					foreach ( $array_173 as $item ){
						if(trim($item['my_date'])==trim($var)){
						  $count_173=$item['total'];
						}
					}
					
					$count_total=$count_174+$count_113+	$count_25_0_2 +$count_3+$count_173;
					if($count_total==$count_137){
						$tr_class="success";
					}
					else $tr_class="danger";
					
					$total_137 +=$count_137;
					$total_others +=$count_total;
					
					echo " <tr class=\"$tr_class\"><td>$var</td><td>$count_137</td><td>$count_total</td><td>$count_174</td><td>$count_113</td><td>$count_25_0_2</td><td>$count_3</td><td>$count_173</td></tr>";	
					
				}
				echo " <tr class=\"active\"><td>Total</td><td>$total_137</td><td>$total_others</td><td></td><td></td><td></td><td></td><td></td></tr>";	
						
				?>
				  
				</tbody>
			</table>
			
		</div>
	</body>
	
</html>
        
            