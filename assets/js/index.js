window.onload = function () {
    $('label.tree-toggler').click(function () {
        $(this).parent().children('ul.tree').slideToggle(250);
    });
    $("#navBarLeft").resizable({
        handles: 'e, w'
    });

    getActionLink();
    $('.toggle_menu').click(function(){$('#navBarLeft').toggle('slow'); return false;});


};

function formToPostWhenEnterIsPressed(formID){
    $('#searchDiv .form-control').keypress(function(e){ //getting enter key press event while typing in form-control field within search div
        if(e.which == 13){//Enter key pressed
            doSearch(formID);
            return false;
        }
    });
}
function getActionLink(){

    $(document).on('click','.modalLink',loadModal);
    $(document).on('click','.deleteLink',deleteData);
    $(document).on('click','.linksDeleteSelected',deleteSelected);
    $(document).on('click','.linksSMSSelected',smsSelected);
    $(document).on('change','.selectAll',function(){
        if($(this).is(":checked")) {
            $('.resultRow').prop('checked', true);
        }
        else
            $('.resultRow').prop('checked', false);
    });
    //search bar options
    $(document).on('click','.search',function(){
        $("#searchDiv").slideToggle('slow');
        return false;
    });
    /*$(document).on('click','.downloadCSV',function(){
        var url=$('#siteUrl').val()+'/report/downloadCSV';
        $.post(
            url,
            { data: 'select * from sms'},
            function(data) {
                alert(data);
                $('#divForLoadingModal').html(data);
                //$('#myModal').modal('toggle');
            }
        );
        return false;
    });*/
    /*var modalLinks = document.querySelectorAll(".modalLink");
    for (var i = 0; i < modalLinks.length; i++) {
        modalLinks[i].onclick = loadModal;
    }

    var deleteLinks = document.querySelectorAll(".deleteLink");
    for (var i = 0; i < deleteLinks.length; i++) {
        deleteLinks[i].onclick = deleteData;
    }
    //$('.deleteLink').click(deleteData);

    $('.selectAll').change(function() {
        if($(this).is(":checked")) {
            $('.resultRow').prop('checked', true);
        }
        else
            $('.resultRow').prop('checked', false);
    });

    var linksDeleteSelected = document.querySelectorAll(".linksDeleteSelected");
    for (var i = 0; i < linksDeleteSelected.length; i++) {
        linksDeleteSelected[i].onclick = deleteSelected;
    }
    */
}
/*function is_session_expired(){
    var url=$('#siteUrl').val()+'/'+'expired/is_session_expired'; //getting from hidden text file at view_template
    alert(url);
    $result=false;
    $.post(
        url,
        { data: 'no data'},
        function(data) {
            alert(data);
            alert('session expired. Please reload');
            return ;
           if(data==='1'){
               return true;
           }
            else return false;
        }
    );
}*/
function loadModal(e) {
    //alert(e.target.getAttribute("is_logging"));
    var controllerMethod = e.target.getAttribute("controllerMethod");
    var url=$('#siteUrl').val()+'/'+controllerMethod; //getting from hidden text file at view_template

    var is_logging=0;
    var attr = $(this).attr('is_logging');

    // For some browsers, `attr` is undefined; for others,
    // `attr` is false.  Check for both.
    if (typeof attr !== typeof undefined && attr !== false) {
        is_logging=parseInt(attr);
    }

    var url_to_check=$('#siteUrl').val()+'/'+'expired/is_session_expired'; //getting from hidden text file at view_template

    $.ajax({
        url: url_to_check,
        type: 'POST',
        data: 'no data',
        success: function(data) {
            //alert(data);
            if((parseInt(data)===1) && (is_logging===0)){
                //window.stop();
                //window.location.assign("http://localhost/transport_management/index.php/schedule");  // https://172.16.252.158/Survey/dismiss.php
                alert('session expired. Please reload');
                return false;
            }
            else{
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: 'no data',
                    success: function(data) {
                        //alert(data);
                        $('#divForLoadingModal').html(data);
                        $('#myModal').modal('toggle');
                    },
                    async:false
                });

            }


        },
        async:false
    });
    return false;
}
function smsSelected(e) {
    if($('.resultRow:checked').size()<1)
    {
        alert('please select at least one record');
        return false;
    }
    var r = confirm(e.target.getAttribute("confirmationMsg"));
    if(r==false)
        return false;
    var controllerMethod = e.target.getAttribute("controllerMethod");
    var url=$('#siteUrl').val()+'/'+controllerMethod; //getting from hidden text file at view_template
    /*var obj = {
     "flammable": "inflammable",
     "duh": "no duh"
     };
     $.each( obj, function( key, value ) {
     alert( key + ": " + value );
     });*/

    var selectedIDs = [];
    $('.resultRow:checked').each(function() {
        selectedIDs.push($(this).val());
    });
    var controllerMethod = e.target.getAttribute("controllerMethod");
    var url=$('#siteUrl').val()+'/'+controllerMethod; //getting from hidden text file at view_template
    //alert(url);
    $.post(
        url,
        { ids: selectedIDs.join(", ")},
        function(data) {
            //alert(data);
            $('#divForLoadingModal').html(data);
            $('#myModal').modal('toggle');
        }
    );
    return false;
}
function deleteData(e) {
    var r = confirm(e.target.getAttribute("confirmationMsg"));
    if(r==false)
        return false;
    var controllerMethod = e.target.getAttribute("controllerMethod");
    var url=$('#siteUrl').val()+'/'+controllerMethod; //getting from hidden text file at view_template
    //alert(url);
    $.post(
        url,
        function(data) {
            var obj = $.parseJSON(data);
            if(obj.success){
                showStatusModal('Success','modal-info',obj.content);
                reloadResult(e.target.getAttribute("controllerReloadMethod"));
            }
            else showStatusModal('Error','modal-warning',obj.content);

        }
    );
    return false;
}
function deleteSelected(e) {
    if($('.resultRow:checked').size()<1)
    {
        alert('please select at least one record');
        return false;
    }
    var r = confirm(e.target.getAttribute("confirmationMsg"));
    if(r==false)
        return false;
    var controllerMethod = e.target.getAttribute("controllerMethod");
    var url=$('#siteUrl').val()+'/'+controllerMethod; //getting from hidden text file at view_template
    /*var obj = {
        "flammable": "inflammable",
        "duh": "no duh"
    };
    $.each( obj, function( key, value ) {
        alert( key + ": " + value );
    });*/

    var selectedIDs = [];
    $('.resultRow:checked').each(function() {
        selectedIDs.push($(this).val());
    });

    $.post(
        url,
        { selectedIDs: selectedIDs.join(", ")},
        function(data) {
            var obj = $.parseJSON(data);
            //console.log(obj);
            if(obj.success){
                showStatusModal('Success','modal-info',obj.content);
                reloadResult(e.target.getAttribute("controllerReloadMethod"));
            }
            else showStatusModal('Error','modal-warning',obj.content);
        }
    );
    return false;
}
function reloadResult(controllerReloadMethod){

    var url=$('#siteUrl').val()+'/'+controllerReloadMethod; //getting from hidden text file at view_template
    $.post(
        url,
        { data: 'no data'},
        function(data) {
            //alert(data);
            var obj = $.parseJSON(data);
            //console.log(obj);
            $('#result').html(obj.content);
        }
    );
}
function changeNavigationBackground(receivedID)
{
    //gNavID=receivedID;
    $(".basicLink").removeClass('active');
    var id='#'+receivedID;
    $(id).addClass('active');
}
function showStatusModal(modalTitle,modalHeaderClass,msg){
    $('#myModalLabel').html(modalTitle);
    $('#statusModalHeader').removeClass( "modal-info modal-warning" )
    $('#statusModalHeader').addClass(modalHeaderClass);
    $('#parentStatus').html(msg);
    $('#parentStatusModal').modal('toggle');
}

var gModal;

function slowAlert() {
    alert("That was really slow!");
}

function submitForm(formId,reloadUrl,goToUrl) //for save  r
{
    console.log($(formId).serialize());
    //reload url for reload partial div and goToUrl for go in to new url like when we log in we need to go in another page
    if($('#childStatus').offset().top<0)
        $('#myModal').scrollTop($('#childStatus').offset().top);
    $("#childStatus").show();

    $.ajax({ // create an AJAX call...
        data: $(formId).serialize(), // get the form data

        type: $(formId).attr('method'), // GET or POST
        url: $(formId).attr('action'), // the file to call
        success: function(response) { // on success..
            //alert(response);
            var obj = $.parseJSON(response);
            console.log(obj);
            //alert('wait');
            if(obj.success)
            {
                gModal.modal('toggle');

                if(typeof obj.all_sms != "undefined"){
                    //console.log( obj.all_sms);
                    gResponse=[];
                    smsCount=obj.all_sms.length;
                    corRequestCount=0;
                    hasError=false;
                    errorMsg='';
                    for(var j=0;j<obj.all_sms.length;j++){
                        //gResponse.push('now');
                        makeCorsRequest( obj.all_sms[j] );
                    }

                    //console.log(gResponse);
                }
                showStatusModal('Success','modal-info',obj.content);
                if(reloadUrl!=''){
                    var url=$('#siteUrl').val()+'/'+reloadUrl;
                    //alert(url);

                    $.post(
                        url,
                        { data: 'no data'},
                        function(data) {
                            //alert(data);
                            var obj = $.parseJSON(data);
                            //console.log(obj);
                            $('#result').html(obj.content);
                        }
                    );
                }
                else if(goToUrl!=''){
                    var url=$('#siteUrl').val()+'/'+goToUrl;
                    window.location = url;
                }
                else{
                    //var pathname = window.location.pathname; // Returns path only
                    var url      = window.location.href;     // Returns full URL
                    window.location = url;
                }
            }
            else
                gModal.find('.modal-body').html(obj.content);

        }
    });
    return false;
}


function submitFormSinglePage(formId) //for save  r
{
    //alert('submitted');
    console.log($(formId).serialize());
    //reload url for reload partial div and goToUrl for go in to new url like when we log in we need to go in another page
    /*if($('#childStatus').offset().top<0)
        $(formId+' #childStatus').scrollTop($('#childStatus').offset().top);
    $(formId+' #childStatus').show();*/

    $.ajax({ // create an AJAX call...
        data: $(formId).serialize(), // get the form data

        type: $(formId).attr('method'), // GET or POST
        url: $(formId).attr('action'), // the file to call
        success: function(response) { // on success..

            //alert(response);
            var obj = $.parseJSON(response);
            console.log(obj);
            if(obj.success)
            {
                showStatusModal('Success','modal-info',obj.content);
            }
        }
    });
    return false;
}

var gResponse=[];
var smsCount=0;
var corRequestCount=0;
var hasError=false;
var errorMsg='';
function submitSMSForm(formId) //for save  r
{
    //reload url for reload partial div and goToUrl for go in to new url like when we log in we need to go in another page
    if($('#childStatus').offset().top<0)
        $('#myModal').scrollTop($('#childStatus').offset().top);
    $("#childStatus").show();

    $.ajax({ // create an AJAX call...
        data: $(formId).serialize(), // get the form data

        type: $(formId).attr('method'), // GET or POST
        url: $(formId).attr('action'), // the file to call
        success: function(response) { // on success..
            //alert(response);
            var obj = $.parseJSON(response);
            //console.log(obj);
            if(obj.success)
            {
                gModal.modal('toggle');
                console.log(obj.contents);
                console.log(obj.allSms);
                console.log(obj.allReports);
                //showStatusModal('Success','modal-info',obj.content);
                /*$.post(
                    obj.content,
                    { data: 'no data'},
                    function(data) {
                        //alert(data);
                        console.log(data);
                        showStatusModal('Success','modal-info',data);
                    }
                );*/
                /*$.getJSON( obj.content, function( json ) {
                    console.log( "JSON Data: " + json );
                });*/
                gResponse=[];
                smsCount=obj.contents.length;
                corRequestCount=0;
                hasError=false;
                errorMsg='';
                for(var j=0;j<obj.contents.length;j++){
                    //gResponse.push('now');
                    makeCorsRequest( obj.contents[j] );
                }

                console.log(gResponse);
                /*for(var i=0;i<gResponse.length;i++){
                    if(gResponse[i].toUpperCase()!='SUCCESS'){
                        hasError=true;
                        errorMsg +=gResponse[i]+'<br>'; //for javascript we have to use \n but
                    }
                }

                if(!hasError){
                    showStatusModal('Success','modal-info','All SMS(s) send successfully');
                }
                else {
                    showStatusModal('Error','modal-warning',errorMsg);
                    console.log(errorMsg);
                }*/

            }
            else
                gModal.find('.modal-body').html(obj.content);
        }
    });
    return false;
}

function doSearch(formId) //for search
{
    $("div#divLoading").addClass('show');
    $.ajax({ // create an AJAX call...
        data: $(formId).serialize(), // get the form data
        type: $(formId).attr('method'), // GET or POST
        url: $(formId).attr('action'), // the file to call
        success: function(response) { // on success..
            var obj = $.parseJSON(response);
            console.log(obj);
            if(obj.success)
            {
                $('#result').html(obj.content);
            }
            else{
                showStatusModal('Error','modal-warning',obj.content);
            }
            $("div#divLoading").removeClass('show');
        }
    });
    return false;
}
function initializeModal(){
    $('#myModal').on('show.bs.modal', function (event) {
        //alert('initializing modal');
        gModal = $(this); //gModal is a global variable declared in index.js
    });
}

//cross domain request

// Create the XHR object.
function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
        // XHR for Chrome/Firefox/Opera/Safari.
        xhr.open(method, url, true); //synchronus so false, to make asynchronus make it true
    } else if (typeof XDomainRequest != "undefined") {
        // XDomainRequest for IE.
        xhr = new XDomainRequest();
        xhr.open(method, url,true);
    } else {
        // CORS not supported.
        xhr = null;
    }
    return xhr;
}

// Helper method to parse the title tag from the response.
function getTitle(text) {
    return text.match('<title>(.*)?</title>')[1];
}

// Make the actual CORS request.
function makeCorsRequest(url) {
    // All HTML5 Rocks properties support CORS.
    //var url = 'http://updates.html5rocks.com';

    var xhr = createCORSRequest('GET', url);
    if (!xhr) {
        alert('CORS not supported');
        return;
    }

    // Response handlers.
    xhr.onload = function() {
        var text = xhr.responseText;
        corRequestCount++;
        //var title = getTitle(text);
        //alert('Response from CORS request to ' + url + ': ' + title);
        console.log(text);
        //alert(text);
        gResponse.push(text);
        /*if(corRequestCount==smsCount)
        {
            for(var i=0;i<gResponse.length;i++){
                if(gResponse[i].toUpperCase()!='SUCCESS'){
                    hasError=true;
                    errorMsg +=gResponse[i]+'<br>'; //for javascript we have to use \n but
                }
            }

            if(!hasError){
                showStatusModal('Success','modal-info','All SMS(s) send successfully');
            }
            else {
                showStatusModal('Error','modal-warning',errorMsg);
                console.log(errorMsg);
            }
        }
        if(text.toUpperCase()=='SUCCESS'){
            //showStatusModal('Success','modal-info','SMS send successfully');
        }*/
        //console.log(gResponse);
        //else showStatusModal('Error','modal-warning',text);
    };

    xhr.onerror = function() {
        //alert('Woops, there was an error making the request.');
        showStatusModal('Error','modal-warning','Woops, there was an error making the request.');
    };

    xhr.send();
}