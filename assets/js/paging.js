$(document).ready(function(){
    $(document).on('keypress','.pageSearch',function(e){
        if(e.which == 13){//Enter key pressed
            postData($(this).val(),$('#hiddenItemPerPage').val() );
        }
    });
    $(document).on('keypress','.itemPerPage',function(e){
        if(e.which == 13){//Enter key pressed
            postData(1,$(this).val() );
        }
    });
    $(document).on('click','.paging',handlePaging);

});
/*
$('.pageSearch').keypress(function(e){
    if(e.which == 13){//Enter key pressed
        postData($(this).val(),$('#hiddenItemPerPage').val() );
    }
});
$('.itemPerPage').keypress(function(e){
    if(e.which == 13){//Enter key pressed
        postData(1,$(this).val() );
    }
});
var pagingButtons = document.querySelectorAll(".paging");
for (var i = 0; i < pagingButtons.length; i++) {
    pagingButtons[i].onclick = handlePaging;
}
*/

function handlePaging() //e
{
    postData($(this).val(),$('#hiddenItemPerPage').val());
}
function postData(currentPageNo,itemPerPage)
{
    var is_logging=0;
    var attr = $(this).attr('is_logging');

    // For some browsers, `attr` is undefined; for others,
    // `attr` is false.  Check for both.
    if (typeof attr !== typeof undefined && attr !== false) {
        is_logging=parseInt(attr);
    }
    var url_to_check=$('#siteUrl').val()+'/'+'expired/is_session_expired'; //getting from hidden text file at view_template
    $.ajax({
        url: url_to_check,
        type: 'POST',
        data: 'no data',
        async:false,
        success: function(data) {
            if((parseInt(data)===1) && (is_logging===0)){
                //window.stop();
                //window.location.assign("http://localhost/transport_management/index.php/schedule");  // https://172.16.252.158/Survey/dismiss.php
                alert('session expired. Please reload');
                return false ;
            }
            else do_paging(currentPageNo,itemPerPage);
        }

    });
}
function do_paging(currentPageNo,itemPerPage){
    $("div#divLoading").addClass('show');
    var totalItem=$('#totalItem').val();
    var query= $('#queryWithoutLimit').val();
    //alert(query);
    var controllerUrl=$('#controllerUrl').val();
    var lastPage=parseInt($('#lastPage').val());
    //alert('totalItem: '+totalItem+' query: '+query+' controllerUrl: '+controllerUrl+' last page: ' + lastPage);
    if(isNaN(currentPageNo)||isNaN(itemPerPage))
    {
        showStatusModal('Invalid','modal-warning','provide valid page');

        $("div#divLoading").removeClass('show');
        return ;
    }
    if(itemPerPage<1)
    {
        itemPerPage=1;
    }
    currentPageNo=parseInt(currentPageNo);
    itemPerPage=parseInt(itemPerPage);

    if((currentPageNo>lastPage) || (currentPageNo<1))
    {
        var msg="page "+currentPageNo+" does not exist.";
        showStatusModal('Invalid','modal-warning',msg);
        $("div#divLoading").removeClass('show');
        return ;

        /*$('#status').html("<div class='alert alert-warning' role='alert'>page "+currentPageNo+" does not exist.</div>");
         $("div#divLoading").removeClass('show');
         if($('#status').offset().top<0)
         $('#content2').scrollTop($('#status').offset().top);
         return ;*/
    }
    //console.log(query);
    $.ajax({
        url: controllerUrl,
        type: 'POST',
        data: { currentPage: currentPageNo, itemPerPage:itemPerPage, totalItem: totalItem, queryWithoutLimit : query ,controllerUrl:controllerUrl },
        success: function(data) {
            var obj = $.parseJSON(data);
            //console.log(obj);
            $('#result').html(obj.content);
            $("div#divLoading").removeClass('show');
        },
        async:false
    });
}