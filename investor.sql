-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `investor`;
CREATE TABLE `investor` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `contact_no` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `investor` (`id`, `name`, `contact_no`, `email`, `city`) VALUES
(3,	'Andalib Rahman Rossy',	'01755656459',	'andalib.rahman@yahoo.com',	'Dhaka'),
(4,	'Masudur Rahman',	'01755644645',	'masudur.rahman@mysanmar.com',	'Dhaka'),
(5,	'Maksuda Khatun',	'01685283115',	'maksuda.khatun@mysanmar.com',	'Dhaka'),
(8,	'Mohua',	'01755644572',	'mohua74@gmail.com',	'Chittagong'),
(10,	'Masudur Rahman',	'01755644645',	'masudur.rahman@gmail.com',	'Dhaka'),
(11,	'Khondakar Latif Ali',	'01755656459',	'latif.khondakar@yahoo.com',	'Chattogram'),
(12,	'Humayun Kabir',	'01711323600',	'humayun.kabir@gmail.com',	'Dhaka');

-- 2023-01-22 11:03:58
