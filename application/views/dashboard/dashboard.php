<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<?php  
	$this->load->view('common/header.php'); 
	$this->load->view('common/navbar.php');
?>

  

    <br>
    
    <div class="col-sm-12 maindiv">
      <div class="well">
        
        <h4 align="center">Welcome To Metro CRM</h4>
      </div>
      </div>

			<?php
		  if ( isset($dashboard_info)) {
			
          
          
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-danger\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">Sales</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$dashboard_info['total_sale']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-primary\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">Tickets</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$dashboard_info['total_ticket']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-info\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">FAQ</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$dashboard_info['total_faq']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  
		  
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-success\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">Sales Feedback</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$dashboard_info['total_sales_feedback']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-warning\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">Total Feedback On Ticket</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$dashboard_info['total_ticket_feedback']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-info\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">Total Opened Ticket</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$dashboard_info['total_open_ticket']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-danger\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">Total Closed Ticket</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$dashboard_info['total_close_ticket']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  }
		  
		  if ( isset($pie_chart_info)) {
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-success\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">Product Complaints Ticket</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$pie_chart_info['ticket_product_complaints']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-success\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">Product Sales Query Ticket</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$pie_chart_info['ticket_product_sales_query']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-primary\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">Service Complaints Ticket</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$pie_chart_info['ticket_service_complaints']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-info\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">Spare Parts Sales Ticket</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$pie_chart_info['ticket_spare_parts_sales_query']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  
		  echo "<div class=\"col-lg-3 col-md-6\">";
          echo "<div class=\"panel panel-danger\">";
          echo "<div class=\"panel-heading\">";
          echo "<div class=\"row\">";
          echo "<div class=\"col-xs-3\">";
          echo "<i class=\"fa fa-comments fa-5x\"></i>";
          echo "</div>";
          echo "<div class=\"col-xs-9 text-right\">";
          echo "<div><span id=\"status\">Service Sales Query Ticket</span></div>";
          echo "</div>";
		  echo "</div>";
          echo "</div>";
          echo "<a href=\"#\">";
		  echo "<div class=\"panel-footer\">";
          echo "<span  class=\"pull-left\">".$pie_chart_info['ticket_service_sales_query']."</span>";
		  echo "<span class=\"pull-right\"><i class=\"fa fa-arrow-circle-right\"></i></span>";
		  echo "<div class=\"clearfix\"></div>";
		  echo "</div>";
		  echo "</a>";
		  echo "</div>";
	      echo "</div>";
		  
		  
		  }
			             ?>
						 
						 
        
    <!--  <div class="row">
        <div class="col-sm-6">
          <div class="well">
            <div align="center" id="piechart"></div> 
          </div>
        </div>
      <div class="col-sm-6">
          <div class="well">
            <div align="center" id="piechart_second"></div> 
          </div>
        </div>
      </div>-->
    </div>
	

<?php $this->load->view ('common/footer.php');	?>
<script type="text/javascript">	
	jQuery(document.body).on('click', '.delete', function (e) {
		var this_holder = this;
		var delete_url = $(this).attr('href');
		e.preventDefault();
		
		bootbox.confirm("Are you sure you want to delete this entry?", function (response) {        
			if(response) {
				$.ajax({ 
					url: delete_url,
					dataType: 'text', 
					type: 'post', 
					contentType: 'application/x-www-form-urlencoded',  
					success: function( data, textStatus, jQxhr ){ 	
						if(data == true){
							$(this_holder).closest('td').closest('tr').hide(1000);
						}else{
							bootbox.alert('Unable to Delete this entry....');
						}						
					}, 
					error: function( jqXhr, textStatus, errorThrown ){ 
						alert(errorThrown); 
					} 
				});

			}
		});
	});
        $(document).on('click','.linksDeleteSelected',deleteSelected);
        $(document).on('change','.selectAll',function(){
            if($(this).is(":checked")) {
                $('.resultRow').prop('checked', true);
            }
            else
                $('.resultRow').prop('checked', false);
        });
        function deleteSelected(e) {
            if($('.resultRow:checked').size()<1)
            {
                alert('please select at least one record');
                return false;
            }
            var r = confirm(e.target.getAttribute("confirmationMsg"));
            if(r==false)
                return false;
            var controllerMethod = e.target.getAttribute("controllerMethod");
            var url=controllerMethod; 
            /*var obj = {
                "flammable": "inflammable",
                "duh": "no duh"
            };
            $.each( obj, function( key, value ) {
                alert( key + ": " + value );
            });*/

            var selectedIDs = [];
            $('.resultRow:checked').each(function() {
                selectedIDs.push($(this).val());
            });
            console.log(selectedIDs);
            $.post(
                url,
                { selectedIDs: selectedIDs.join(", ")},
                function(data) {
                    /*var obj = $.parseJSON(data);
                    if(obj.success){
                        showStatusModal('Success','modal-info',obj.content);
                        reloadResult(e.target.getAttribute("controllerReloadMethod"));
                    }
                    else showStatusModal('Error','modal-warning',obj.content);*/
                    window.location.replace(e.target.getAttribute('controllerReloadMethod'));
                }
            );
            return false;
        }

var pie_obj_query;
var pie_obj_division;		
	
		
		// Load google charts
google.charts.load('current', {'packages':['corechart']});
//google.charts.setOnLoadCallback(drawChart);
//google.charts.setOnLoadCallback(drawSecondChart);


// Draw the chart and set the chart values
function drawChart() {
	
	
	$.ajax({
		 async:false,
		 type: "POST",
		 url: "<?php echo site_url("login/get_pie_chart_query_type");?>",
		 data: {jm:7},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   console.log(data);
			pie_obj_query = $.parseJSON(data);
			//console.log(obj);
			//$('#piechart').html(obj.content);
			
			
		
	   });
	   
	  
	console.log(pie_obj_query);
	var temp= pie_obj_query.ticket_product_complaints;
	
	
	console.log('temp '+temp);
  var data = google.visualization.arrayToDataTable([
  ['Query Type', 'Ticket Number'],
  ['Product Complaints', parseInt(pie_obj_query.ticket_product_complaints)],
  ['Product Sales Query', parseInt(pie_obj_query.ticket_product_sales_query)],
  ['Service Complaints', parseInt(pie_obj_query.ticket_service_complaints)],
  ['Spare Parts Sales Query', parseInt(pie_obj_query.ticket_spare_parts_sales_query)],
  ['Service Sales Query', parseInt(pie_obj_query.ticket_service_sales_query)],
  ['Others',parseInt(pie_obj_query.ticket_others)]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Ticket Number Based on Query ', 'width':550, 'height':400};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}

function drawSecondChart() {
	
	
	$.ajax({
		 async:false,
		 type: "POST",
		 url: "<?php echo site_url("login/get_pie_chart_division_type");?>",
		 data: {jm:8},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   console.log(data);
			pie_obj_division = $.parseJSON(data);
			//console.log(obj);
			//$('#piechart').html(obj.content);
			
			
		
	   });
	   
	  
	console.log(pie_obj_division);
	
	
	
	
  var data = google.visualization.arrayToDataTable([
  ['Division Type', 'Ticket Number'],
  ['Power & Energy', parseInt(pie_obj_division.ticket_power_energy)],
  ['Motor Vehicle', parseInt(pie_obj_division.ticket_motor_vehicle)],
  ['Agro Machinaries', parseInt(pie_obj_division.ticket_agro_machineries)],
  ['LPG', parseInt(pie_obj_division.ticket_lpg)],
  ['Building Materials', parseInt(pie_obj_division.ticket_building_materials)],
  ['Central Service',parseInt(pie_obj_division.ticket_central_service)]
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Ticket Number Based on Division ', 'width':550, 'height':400};

  // Display the chart inside the <div> element with id="piechart_second"
  var chart = new google.visualization.PieChart(document.getElementById('piechart_second'));
  chart.draw(data, options);
}


</script>