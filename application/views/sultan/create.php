<?php $this->load->view('common/header.php');
//print_r($params);
?>
<!-- <head>
  <title>Username availability check using Codeigniter, AJAX and MySQL</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 </head> -->
<div class=" col-md-12"> 	<!-- this is alert size-->
	<?php 
		$return_value=$this->session->flashdata('return_value');
		if(isset($return_value)) 
		{
			//print_r($return_value);
			$msg=$return_value['msg'];
			if($return_value['success'] == true)
			{

				echo "<div class='alert alert-success' role='alert'>$msg</div>";

			}
			else if($msg!='') //if not success and msg not empty
			{
				echo "<div class='alert alert-danger' role='alert'>$msg</div>";
			}
		}
	?>
</div>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Sultan  / <small>Create<?php //echo $ui['title']?> </small> </h1>
    </div>
</div>



<div class="row" class="table-responsive" >
  <div class="">
	<form class="form-horizontal"  method="post" action="<?php echo site_url() ?>/contact/save_sultan">
	<div class="col-md-8">
		<!-- <input type="hidden" name="id" value="<?php //echo isset ($params['id'])? $params['id']:-1;  ?>"> -->
  
		<!--  <h4 class="text-center">Information About the Contact</h4>-->
		  
		  
		<div class="form-group">

				<label class="control-label col-sm-5">Mobile</label>
			<div class="col-sm-7">
				<input type="number" onchange="myFunction()" autocomplete="off" class="form-control input-sm" id="mobile" placeholder="Enter Mobile Number" name="mobile" value="" required="">
				<label id="msg"></label>
			</div>
		</div>

<div class="form-group">
				<label class="control-label col-sm-5">Name</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="name" placeholder="Enter Name" name="name" value="" required="">
				
			</div>
		</div>
		
		<div class="form-group">
				<label class="control-label col-sm-5">Address</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="address" placeholder="Enter Address" name="address" value="">
			</div>
		</div>
		<div class="form-group">
				<label class="control-label col-sm-5">Sales</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="sales" placeholder="Sales" name="sales" value="">
			</div>
		</div>
		
		<div class="form-group">
				<label class="control-label col-sm-5">Target</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="target" placeholder="Enter Target" name="target" value="">
			</div>
		</div>

<div class="form-group">
				<label class="control-label col-sm-5"></label>
			<div class="col-sm-7" style="padding-left: 0px;">
				   <div class="col-xs-3">
        <label for="ex2">Category</label>
       <input type="text" autocomplete="off" class="form-control input-sm" id="category" placeholder="Category.." name="category" value="">
      </div>
      <div class="col-xs-3">
        <label for="ex2">SKU</label>
       <input type="text" autocomplete="off" class="form-control input-sm" id="sku" placeholder="Select SKU" name="sku" value="">
      </div>
      <div class="col-xs-6">
        <label for="ex3">Rate</label>
        <input type="text" autocomplete="off" class="form-control input-sm" id="rate" placeholder="Enter rate" name="rate" value="">
      </div>
			</div>
		</div>


		
		<div class="form-group">
			<label class="control-label col-sm-5">Start Date</label>
		<div class="col-sm-7">
			<input type="Date" autocomplete="off" class="form-control input-sm" id="start_date" placeholder="Enter Start Date" name="start_date" value="">
		</div>
	</div>

	<div class="form-group">
			<label class="control-label col-sm-5">Created By</label>
	<div class="col-sm-5">
				<?php
					
						$names=$this->prime_model->getByQuery('select distinct name from user');
						?>
						
					  
						<select class="form-control input-sm" type="text" name="created_by" id="created_by">
							<option value='' >--</option>
						<?php
							foreach($names as $item)
						  {
							$selected='';
							if(isset($params['created_by']) && $item['name']==$params['created_by'])
								$selected='selected';
							$text=str_replace('_',' ',$item['name']);
							$value=$item['name'];
							echo "<option value='$value' $selected>$text</option>";
						  }

						 ?>
						</select>
			</div>
	</div>
	
	<div class="form-group">
			<label class="control-label col-sm-5">Remarks</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="remarks" placeholder="Remarks..." name="remarks" value="">
		</div>
	</div>
	
	
	

				
			</div>

	<div class="col-md-8">
	
	
	
	<div class="form-group">
      <label class="control-label col-sm-5">District</label>
      <div class="col-sm-5">
        <?php
		
			$names=$this->prime_model->getByQuery('select name from district');
			?>
			
		  


			<select class="form-control input-sm" type="text" name="district" id="district">
				<option value='' >-Select District-</option>
			<?php
				foreach($names as $item)
			  {
				$selected='';
				if(isset($params['district']) && $item['name']==$params['district'])
					$selected='selected';
				$text=str_replace('_',' ',$item['name']);
				$value=$item['name'];
				echo "<option value=\"$value\" $selected>$text</option>";
              }

             ?>
			</select>
      </div>

    </div>
	
	<?php echo $current_user['name'];?>
	</div>
	<div class="col-md-9">
	 <div class="form-group">        
    	<div class="col-sm-offset-9 col-sm-10">
        <button type="submit" class="btn btn-success">Submit</button>
      </div>
    </div>
</div>
  </form>
  </div>
 	
  
</div>  <!-- end of menu2 tab -->
    
      


                <?php $this->load->view('common/footer.php'); ?>
<script type="text/javascript">
 $(function () {
    $('input').on('click', function () {
        var sultan = $(this).val();
        $.ajax({
            url: 'api/dropdowntest',
            data: {
                text: $("option[name=installno]").val(),
                Status: installno
            },
            dataType : 'json'
        });
    });
});

</script>
<script type="text/javascript">
 $('#search').click(function() {

  var id_no = $('#id_no').val();

  $.ajax({
   url: '<?php echo site_url('service/inputdata'); ?>',
   type: 'POST',
    data: { //data pass to controller
    	id_no: id_no,

    },

    dataType: 'json',
    success: function(data) {
    	console.log(data);
    	if(data==){
    		
    	}
    }

});
//return false;
});
</script>

<script type="text/javascript">
	
	function getNewVal()
{
document.getElementById('text').value = option.text;
 document.getElementById('demo').value = "test";
}
</script>

<script type="text/javascript">
// Ajax post
$(document).ready(function() 
{
$("#mobile").blur(function() 
{
var mobile = $('#mobile').val();

	if(mobile!="")
	{
		jQuery.ajax({
		type: "POST",
		url: "<?php echo base_url('/index.php/contact/checkmobile_duplicate'); ?>",
		dataType: 'html',
		data: {mobile: mobile},
		success: function(res) 
		{
			if(res==1)
			{
			$("#msg").css({"color":"red"});
			$("#msg").html("This mobile number already exists, Please input another number");
			}
			else
			{
			$("#msg").css({"color":"green"});
			$("#msg").html("Congrates mobile number available !");	
			}
			
		},
		error:function()
		{
		alert('some error');	
		}
		});
	}
	else
	{
	alert("Please enter mobile number ");
	}

});
});
</script>
                  