<?php $this->load->view('common/header.php');
//print_r($params);
?>

<div class=" col-md-12"> 	<!-- this is alert size-->
	<?php 
		$return_value=$this->session->flashdata('return_value');
		if(isset($return_value)) 
		{
			//print_r($return_value);
			$msg=$return_value['msg'];
			if($return_value['success'] == true)
			{

				echo "<div class='alert alert-success' role='alert'>$msg</div>";

			}
			else if($msg!='') //if not success and msg not empty
			{
				echo "<div class='alert alert-danger' role='alert'>$msg</div>";
			}
		}
	?>
</div>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Sultan  / <small>Update<?php //echo $ui['title']?> </small> </h1>
    </div>
</div>

<div class="row" >
  
	<form class="form-horizontal"  method="post" action="<?php echo site_url() ?>/contact/update_sultan">
	<div class="col-md-6">
		<!-- <input type="hidden" name="id" value="<?php //echo isset ($params['id'])? $params['id']:-1;  ?>"> -->
  
		<!--  <h4 class="text-center">Information About the Contact</h4>-->
		  
		  
		<div class="form-group">
				
			<div class="col-sm-7">
				<input type="hidden" autocomplete="off" class="form-control input-sm" id="id" placeholder="Enter Number" name="id" value="<?php echo $sultaninfo[0]['id'] ?>">
				
			</div>
		</div><div class="form-group">
				<label class="control-label col-sm-5">Mobile</label>
			<div class="col-sm-7">
				<input type="number" autocomplete="off" class="form-control input-sm" id="mobile" placeholder="Enter Number" name="mobile" value="<?php echo $sultaninfo[0]['mobile'] ?>">
				
			</div>
		</div>
		<div class="form-group">
				<label class="control-label col-sm-5">Name</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="name" placeholder="Enter Name" name="name" value="<?php echo $sultaninfo[0]['name'] ?>">
				
			</div>
		</div>
		
		<div class="form-group">
				<label class="control-label col-sm-5">Address</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="address" placeholder="Enter Address" name="address" value="<?php echo $sultaninfo[0]['address'] ?>">
			</div>
		</div>
		<div class="form-group">
				<label class="control-label col-sm-5">Sales</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="sales" placeholder="Sales" name="sales" value="<?php echo $sultaninfo[0]['sales'] ?>">
			</div>
		</div>
		
		<div class="form-group">
				<label class="control-label col-sm-5">Target</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="target" placeholder="Enter Target" name="target" value="<?php echo $sultaninfo[0]['target'] ?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-sm-5">Category</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="category" placeholder="Select Category" name="category" value="<?php echo $sultaninfo[0]['category'] ?>">
		</div>
	    </div>
	    <div class="form-group">
			<label class="control-label col-sm-5">SKU</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="sku" placeholder="Select SKU" name="sku" value="<?php echo $sultaninfo[0]['sku'] ?>">
		</div>
	    </div>
	    <div class="form-group">
				<label class="control-label col-sm-5">Rate</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="rate" placeholder="Enter rate" name="rate" value="<?php echo $sultaninfo[0]['rate'] ?>">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5">Start Date</label>
		<div class="col-sm-7">
			<input type="Date" autocomplete="off" class="form-control input-sm" id="start_date" placeholder="Enter Start Date" name="start_date" value="<?php echo $sultaninfo[0]['start_date'] ?>">
		</div>
	</div>
	
	<div class="form-group">
			<label class="control-label col-sm-5">Remarks</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="remarks" placeholder="Select Issue Date" name="remarks" value="<?php echo $sultaninfo[0]['remarks'] ?>">
		</div>
	</div>
	
	
	</div>
	<div class="col-md-6">
	
	
	
	<div class="form-group">
      <label class="control-label col-sm-5">District</label>
      <div class="col-sm-7">
        <?php
		
			$names=$this->prime_model->getByQuery('select name from district');
			?>
			
		  


			<select class="form-control input-sm" type="text" name="district" id="district">
				<option value='' >-Select District-</option>
			<?php
				foreach($names as $item)
			  {
				$selected='';
				if(isset($params['district']) && $item['name']==$params['district'])
					$selected='selected';
				$text=str_replace('_',' ',$item['name']);
				$value=$item['name'];
				echo "<option value=\"$value\" $selected>$text</option>";
              }

             ?>
			</select>
      </div>
    </div>
	
	
	</div>
	
	 <div class="form-group">        
      <div class="col-sm-offset-10 col-sm-10">
        <button type="submit" class="btn btn-default">Update</button>
      </div>
    </div>

  </form>
  
 	
  
</div>  <!-- end of menu2 tab -->
    
      


                <?php $this->load->view('common/footer.php'); ?>
                  