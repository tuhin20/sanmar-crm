<?php  $this->load->view('common/newheader.php'); 
?>

<body style="background-image: linear-gradient(to bottom right, #783d3d85, #fffcdf) ; 
		background-size: cover; 
		background-repeat: no-repeat;
		background-position: center center;">
		
		
	<div class="container col-md-8" style=" margin-top:7%; margin-bottom: 7%;
		background-color: white; padding: 40px;left:16%; right:16%;">
		
		
		<form action="<?php echo site_url() ?>/contact/create_landowner" method="post" enctype="multipart/form-data" >
		 <h2 style="text-align: center; color: #6F3218">Add Land Owner Information</h2>
			<div class="col-md-12" style=" margin-top: 20px;">
				<div class="col-md-12">      
					<!-- Table -->
				
				
					<table class="table  table-hover">
								
							  <tr>
								  
									<th class="row col-md-3">
									<label for="name">Landowner Name<span class="required">*</span></label>
									</th>
									<td class="row col-md-9">
									<input class="form-control" placeholder="Landowner Name" name="name" type="text"  required>
									</td>
								  
							  </tr>
							  
							  <tr>
								   <th class="row col-md-3">
									<label for="name">Contact Name</label>
									</th>
									<td class="row col-md-9">
									<input class="form-control" placeholder="Contact Name" name="contact_name" type="text"  required>
									</td>
							  </tr>
							  <tr>
								   <th class="row col-md-3">
									<label for="name">Contact Number</label>
									</th>
									<td class="row col-md-9">
									<input class="form-control" placeholder="Contact Number" name="contact_number" type="Number"  >
									</td>
							  </tr>
							  
							  <tr>
								   <th class="row col-md-3">
									<label for="name">Registered Land Address</label>
									</th>
									<td class="row col-md-9">
									<input class="form-control" placeholder="Registered Land Address" name="registered_address" type="text"  required>
									</td>
							  </tr>
							  
							  <tr>
								  <th class="row col-md-3">
									<label for="name">Land Measurement</label>
									</th>
									<td class="row col-md-9">
									<input class="form-control" placeholder="Client Name" name="measurement" type="text"  required>
									</td>
							  </tr>
							  
							  <tr>
								    <th class="row col-md-3">
									<label for="name">Front Road Width</label>
									</th>
									<td class="row col-md-9">
								<input class="form-control" placeholder="Project facing" name="front_road_width" type="text"  required>
									</td>
								   
							  </tr>
							  
							  <tr>
									<th class="row col-md-3">
									<label for="name">Side Road Width</label>
									</th>
									<td class="row col-md-9">
									<input class="form-control" placeholder="Side Road Width" name="side_road_width" type="text"  required>
									</td>
									 
							  </tr>
					          
							  <tr>
									<th class="row col-md-3">
									<label for="name">Land Status</label>
									</th>
									<td class="row col-md-9">
									<input class="form-control" placeholder="Land Status" name="land_status" type="text"  required>
									</td>
									  
							  
							  </tr>
							  
							   <tr>
									<th class="row col-md-3">
									<label for="name">Types of Land</label>
									</th>
									<td class="row col-md-9">
									<input class="form-control" placeholder="Sold Unit" name="types_of_land" type="text" required>
									</td>
									  
							  
							  </tr>
							  
							</table>
					
					<div class="col-md-2" style=" margin-top:7%; margin-bottom: 7%;
		               padding: 0px;left:46%; right:46%;">
						<input class="btn btn-primary" onclick="show_alert();" type="submit" value="Submit">
					</div>
			
				</div>
				
			  
			</div>
			

		
		</form>
		
	</div>
	
		
</body>