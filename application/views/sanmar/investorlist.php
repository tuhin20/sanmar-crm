<?php  $this->load->view('common/newheader.php'); 
?>

<script>
  $( function() {
    $( "#start_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+10",
      dateFormat: "yy-mm-dd"
    });
    $( "#end_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+10",
      dateFormat: "yy-mm-dd"
    });
  } );
</script>
<body style="background-image: linear-gradient(to bottom right, #783d3d85, #fffcdf) ; 
		min-height: 100%;
   
   background-size: cover;
   background-repeat: no-repeat;
   background-position: center center;">
<div class="container col-md-8" style=" margin-top:7%; margin-bottom: 7%;
		background-color: white; padding: 40px;left:16%; right:16%;">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Investor Details
            
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-sm-3 col-md-3 col-lg-3">
    <?php
    //if(isset($it_user)&&$it_user==true){
        ?>
   
  <a STYLE="max-width:160px; max-height:70px;" id="downloadCSV" class="btn btn-success glyphicon glyphicon-download-alt" href="<?php echo site_url('/contact/download_investor_chart'); ?>"> Download CSV</a>
<?php 
  // }
   ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?php if(isset($search_info)) echo "<h3>$search_info</h3>"?>
        <div > <h3 id="search_info"></h3>
            <!-- <form class="form-inline" method="post" action="<?php echo site_url('contact/search_by_sultan_group');?>">
              <div class="form-group">
                <label for="email">Project Name</label>
                <?php 
                    
                        $names=$this->prime_model->getByQuery('select distinct project_name from project');
                        ?>
                        
                      
                        <select class="form-control input-sm" type="text" name="project_name" id="project_name">
                            <option value='' >--</option>
                        <?php
                            foreach($names as $item)
                          {
                            $selected='';
                            if(isset($params['project']) && $item['project_name']==$params['project'])
                                $selected='selected';
                            $text=str_replace('_',' ',$item['project_name']);
                            $value=$item['project_name'];
                            echo "<option value='$value' $selected>$text</option>";
                          }

                         ?>
                        </select>
              </div>
              
            <div class="form-group">
                <label for="email">Location</label>
                <?php
                    
                        $names=$this->prime_model->getByQuery('select distinct location from project');
                        ?>
                        
                      
                        <select class="form-control input-sm" type="text" name="location" id="location">
                            <option value='' >--</option>
                        <?php
                            foreach($names as $item)
                          {
                            $selected='';
                            if(isset($params['project']) && $item['location']==$params['location'])
                                $selected='selected';
                            $text=str_replace('_',' ',$item['location']);
                            $value=$item['location'];
                            echo "<option value='$value' $selected>$text</option>";
                          }

                         ?>
                        </select>
              </div>
              
              <!-- <div class="form-group">
                <label for="email">Start Date</label>
                
                    <input type="text" autocomplete="off" class="form-control input-sm" id="start_date" placeholder="Select Start Date" name="start_date">
              </div>
              <div class="form-group">
                <label for="email">End Date</label>
                
                    <input type="text" autocomplete="off" class="form-control input-sm" id="end_date" placeholder="Select End Date" name="end_date">
              </div> -->
             <!--  <button type="submit" class="btn btn-sm btn-success">Search</button>
            </form>  -->
            <br>


        
        </div>
        <div class="table-responsive">
            <!--<input type="checkbox" class="selectAll" /> <span role='button' data-toggle='tooltip' data-placement='top' title='Delete selected' class='glyphicon glyphicon-trash linksDeleteSelected ' controllerMethod="<?php echo site_url('group_manager/deleteSelected') ?>" controllerReloadMethod="<?php echo site_url('group_manager/group_list') ?>" confirmationMsg='are you sure to delete selected group(s)' aria-hidden='true' onclick=''></span>-->

            <table class="table table-bordered table-hover table-striped" id="my_datatable">
                <thead>
                <tr>
                    <!--<th style="width:10%;"></th>-->
                    <th>Sl</th>
                    <th>Name</th>
                    <th>Contact No</th>
                    <th>email</th>
                    <th>City</th>
                   
                     <?php  $current_user=$this->user_model->get_current_user(); 
if($current_user['group_id']==1){
                    ?>
                    <th class="col-md-1">Action</th>
<?php } else echo "string"; ?>

                </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</body>
<!--<script src='<?php //echo base_url("assets/js/$paging_js_name?v3");?>'></script>-->
<?php //$this->load->view('common/footer.php'); ?>

<script type="text/javascript">
    <?php 
    if(isset($search_contact)&&$search_contact==true){
    ?>
        $(document).ready(function() {
            var primary_number="<?php echo $primary_number;?>";
            $.ajax({        
                type: "POST" 
                ,url: "<?php echo site_url('contact/search');?>" 
                ,data:{primary_number:'<?php echo $primary_number;?>'}
                ,async: false
                ,success: function(response) { // on success..
                    var obj=JSON.parse(response);
                    if(obj.record_count==0){
                        $('#search_info').html("Number "+primary_number+" is not in your contact list. <a role='button' class='btn btn-info' href='<?php echo site_url("contact/create/$primary_number");?>'> Add to Contact</a>");
                    }
                    else {
                        $('#search_info').html("Search result of primary number: "+primary_number);
                    }
                    var query_id=obj.query_id;
                    console.log(response);
                    $('#my_datatable').dataTable({
                        destroy: true, //use this to reinitiate the table, other wise problem will occur
                        processing: true,
                        serverSide: true,
                        ajax: {
                            url: "<?php echo site_url('contact/process_paging_investor');?>"
                            ,type: 'POST'
                            ,data:{query_id: query_id}
                        }
                    });
                }
            });
        } );
    
    <?  
    }
    else{
    ?>
        $(document).ready(function() {
            $('#my_datatable').dataTable({
                destroy: true, //use this to reinitiate the table, other wise problem will occur
                processing: true,
                serverSide: true,
                ajax: {
                    url: "<?php echo site_url('contact/process_paging_investor');?>"
                    ,type: 'POST'
                    ,data:{query_id: '0'}
                }
            });
        } );
            jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
        e.preventDefault();
        var delete_url= $(this).attr('href');
        

        bootbox.confirm("Are you sure you want to delete this entry?", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
                            $(this_holder).closest('td').closest('tr').hide(1000);
                        } else {

                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            }
        });
    });
        
    <?
    }
    ?>
        var table = $('#my_datatable').DataTable({}) ;

        table.on('page.dt', function() {
          $('html, body').animate({
            scrollTop: $(".dataTables_wrapper").offset().top
           }, 'slow');
        });
    $( "form" ).submit(function( e ) {
        e.preventDefault();
        e.stopImmediatePropagation(); 
        //alert('form submitted');
        $.ajax({ // create an AJAX call...
            data: $(this).serialize(), // get the form data
            type: $(this).attr('method'), // GET or POST
            url: $(this).attr('action'), // the file to call
            success: function(response) { // on success..
                var obj=JSON.parse(response);
                var query_id=obj.query_id;
                console.log(response);
                $('#my_datatable').dataTable({
                    destroy: true, //use this to reinitiate the table, other wise problem will occur
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "<?php echo site_url('contact/process_paging_investor');?>"
                        ,type: 'POST'
                        ,data:{query_id: query_id}
                    }
                });
                
            }
        });
        return false;
    });
   

</script>