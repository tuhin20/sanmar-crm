<?php  $this->load->view('common/newheader.php'); 
?>
<script>
	$( function() {
		$( "#date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});
		$( "#birthdate" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});
		$( "#deadline" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});
		$( "#solution_timeline" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});
	} );
</script>
<style>
	.myDiv {
		border: 5px outset red;
		background-color: #85cf84;    
		text-align: center;
	}
</style>
<body style="background-image: linear-gradient(to bottom right, #783d3d85, #fffcdf) ; 
background-size: cover; 
background-repeat: no-repeat;
background-position: center center;">

<div class="container col-md-8" style=" margin-top:7%; margin-bottom: 7%;
background-color: white; padding: 40px;left:16%; right:16%;">


<form action="<?php echo site_url('contact/update_ticket');?>" method="post">
	<h2 style="text-align: center; color: #6F3218">Ticket Raising</h2>
	<div class="col-md-12" style=" margin-top: 20px;">
		
		<div class=" row">
			<div class="col-md-12 row">
				<div class="row " style="margin:5px">
					<div class="col-md-12 row">
						<div class="col-md-6 row" style="padding:0px; margin:0px;">
							<div class="col-md-3" style=" margin:0px;" >
								<label for="name" style="padding-right:0px">Customer Name:</label>
							</div>

							<div class="col-md-9" style="padding-left:5px; padding-right:9px; margin:0px;" >
								<input class="form-control" readonly placeholder="Customer Name" name="name" type="text" value="<?php echo $ticket_info[0]['name'] ?>"style="margin-left:10px" required>
							</div>
						</div>
						

					</div>
				</div>
				<div class="col-md-6 row" style="padding:0px; margin:0px;">
					
					<div class="col-md-2">
						<label for="name">Project Name:<span class="required">*</span></label>
					</div>
					<div class="col-md-9">
						<input type="hidden" class="form-control input-sm" id="id" value="<?php echo $ticket_info[0]['id'] ?>" name="id">
						<input class="form-control" placeholder="Project Name" name="project" value="<?php echo $ticket_info[0]['project'] ?>" type="text"  readonly>
					</div>
					
				</div>
				
				<div class="row col-md-6" style="padding:0px; margin:0px;" >
					<div  class="col-md-5" style="padding-left:20px; margin:0px;" >
						<label for="phone">Contact Number:</label>
					</div>
					<div class=" col-md-7" style="padding:0px; margin:0px;">
						<input class=" form-control"  placeholder="Contact Number" name="phone" value="<?php echo $ticket_info[0]['phone'] ?>" type="Number" readonly>
					</div>
					
				</div>
				
			</div>
		</div>	
		
		
		<div class=" row" style="margin-top:20px">
			<div class="col-md-12 row">
				
				<div class="col-md-6 row" style="padding:0px; margin:0px;">
					
					<div class="col-md-2">
						<label for="location">Date</label>
					</div>
					<div class="col-md-9">
						<input class="form-control" id="date"  value="<?php echo $ticket_info[0]['date'] ?>" name="date" readonly>
								<!-- <select class="form-control"id="location" name="location">
										
								</select> -->
							</div>
							
						</div>
						
						
						<div class="row col-md-6" style="padding:0px; margin:0px;" >
							<div  class="col-md-5" style="padding-left:20px; margin:0px;" >
								<label for="phone">Shop/Apt./Plot No</label>
							</div>
							<div class=" col-md-7" style="padding:0px; margin:0px;">
								<input class=" form-control"  value="<?php echo $ticket_info[0]['shop_no'] ?>" placeholder="Shop No" name="shop_no" type="text" readonly>
							</div>
							
						</div>
						
					</div>	
					
				</div>	
				
				
				<div class=" row" style="margin-top:20px">
					<div class="col-md-12 row">
						
						<div class="row col-md-6" style="padding:0px; margin:0px;" >
							<div  class="col-md-5" style="padding-left:15px; margin:0px;" >
								<label for="phone">Level /Floor/Road</label>
							</div>
							<div class=" col-md-6" style="padding:0px; margin:0px; width: 46%;">
								<input class=" form-control" value="<?php echo $ticket_info[0]['level_no'] ?>"  placeholder="Contact Number" name="level_no" type="text" readonly>
							</div>
							
						</div>
						
						<div class="row col-md-6" style="padding:0px; margin:0px;" >
							<div  class="col-md-5" style="padding-left:20px; margin:0px;" >
								<label for="phone">Car Park No</label>
							</div>
							<div class=" col-md-7" style="padding:0px; margin:0px;">
								<input class=" form-control" value="<?php echo $ticket_info[0]['car_park'] ?>"  placeholder="Contact Number" name="car_park" type="text" readonly>
							</div>
							
						</div>
						
					</div>
				</div>	
				
				<div class=" row" style="margin-top:20px">
					<div class="col-md-12 row">
						
						<div class="col-md-6 row" style="padding:0px; margin:0px;">
							
							<div class="col-md-4">
								<label for="location">Date of Birth:</label>
							</div>
							<div class="col-md-7">
								<input class="form-control" id="birthdate" value="<?php echo $ticket_info[0]['birthdate'] ?>" name="birthdate" readonly>
								<!-- <select class="form-control"id="location" name="location">
										
								</select> -->
							</div>
							
						</div>
						
						<div class="row col-md-6" style="padding:0px; margin:0px;" >
							<div  class="col-md-5" style="padding-left:22px; margin:0px;" >
								<label  for="source" >Occupation</label>
							</div>


							<div class=" col-md-7" style="padding:0px; margin:0px;">




								<input list="occupation" class=" form-control" id="occupation" name="occupation" value="<?php echo $ticket_info[0]['occupation'] ?>" readonly>
								<datalist id="occupation">
									<option value="Business">Business</option>
									<option value="Service_Holders">Service Holder</option>
									<option value="others">Others</option>

								</datalist>
								<div class="form-group occ" style="display: none;" >
									<label class="control-label col-sm-10" for="category">Please input if others:</label>
									<div class="col-sm-10" >          
										<input type="text" class="form-control" id="category" placeholder="" name="occu" readonly>
									</div>
								</div>
							</div>
							
						</div>
						
					</div>
					<!-- <div class="col-md-7" style="padding:0px; margin:0px;" >
								<select class="form-control"id="lead_forword" name="duration" value="<?php //echo $ticket_info[0]['duration'] ?>">
										<option value="ExistingProblem">Existing Problem</option>
										<option value="PossibleProblem">Possible Problem</option>
										<option value="Timeline">Solution Timeline</option>
										
								</select>
							</div> -->
						</div>	
						
						
						<div class=" row" style="margin-top:20px">
							<div class="col-md-12 row">
								
								
								<div class="row col-md-6" style="padding:0px; margin:0px;" >
									<div  class="col-md-5" style="padding-left:15px; margin:0px;" >
										<label  for="source" >Purpose of Call</label>
									</div>
									<div class=" col-md-6" style="padding:0px; margin:0px;width: 46%; ">
										<input type="text" class=" form-control" value="<?php echo $ticket_info[0]['purpose_call'] ?>" id="purpose_call" name="purpose_call" list="pur"  readonly>
										<datalist id="pur">
											
											<option value="SL">Sales</option>
											<option value="CS">After sales service</option>
											<option value="BD">Registration</option>
											<option value="others1">Others</option>
										</datalist>
										
										<div class="form-group purpose" style="display: none;" >
											<label class="control-label col-sm-10" for="category">Please input if others:</label>
											<div class="col-sm-12" >          
												<input type="text" class="form-control" id="category" placeholder="" name="others" readonly>
											</div>
										</div>
									</div>
									
									
								</div>
								
								<div class="col-md-6 row" style="padding:0px; margin:0px;">
									
									<div class="col-md-6" style="padding-left:23px;">
										<label for="location">Deadline of issue solve </label>
									</div>
									<div class="col-md-6">
										<input class="form-control" id="deadline"  name="deadline" value="<?php echo $ticket_info[0]['deadline'] ?>" style="width: 109%;" readonly>
									<!-- <select class="form-control"id="location" name="location">
											
									</select> -->
								</div>
								
							</div>
							
						</div>
					</div>	
					
					<div class="row " style="margin-top:20px">
						<div class="col-md-12 row">
							
							<div class="col-md-3" style=" margin:0px;" >
								<label for="name" style="padding-right:0px">Present Address:</label>
							</div>
							
							<div class="col-md-9" style="padding-left:5px; padding-right:9px; margin:0px;" >
								<input class="form-control" value="<?php echo $ticket_info[0]['present_address'] ?>" placeholder="Address" name="present_address" type="text" style="margin-left:10px" readonly>
							</div>
							
						</div>
					</div>
					
					<div class="row " style="margin-top:20px">
						<div class="col-md-12 row">
							
							<div class="col-md-3" style=" margin:0px;" >
								<label for="name" style="padding-right:0px">Permanent Address:</label>
							</div>
							
							<div class="col-md-9" style="padding-left:5px; padding-right:9px; margin:0px;" >
								<input class="form-control" value="<?php echo $ticket_info[0]['permanent_address'] ?>"  placeholder="Address" name="permanent_address" type="text" style="margin-left:10px" readonly>
							</div>
							
						</div>
					</div>
					
					
					<div class="row " style="margin-top:20px">
						<div class="col-md-12 row">

							<div class="col-md-5" style=" margin:0px;" >
								<label for="name" style="padding-right:0px">Product Category (Construction Wise)</label>
							</div>

							<div class="col-md-7" style="padding:0px; margin:0px;" >
								<input class="form-control" value="<?php echo $ticket_info[0]['pd_category'] ?>" id="construction" name="pd_category" list="all_project1" readonly>
								<datalist id="all_project1">
									<option value="BeforeHandover">Before Handover</option>
									<option value="WarrentyPeriods">Warrenty Periods</option>
									<option value="AfterHandover">After Handover/Warrenty Periods</option>
									<option value="others">Others</option>

								</datalist>

								
								
								<div class="form-group construction" style="display: none;" >
									<label class="control-label col-sm-10" for="category">Please input if others:</label>
									<div class="col-sm-12" >          
										<input type="text" class="form-control" id="category" placeholder="" name="others_category" readonly>
									</div>
								</div>
							</div>

						</div>
					</div>
					
					<div class=" row" style="margin-top:20px">
						<div class="col-md-12 row">
							
							<!-- <div class="row col-md-5" style="padding:0px; margin:0px;" >
								<div  class="col-md-6" style="padding-left:15px; margin:0px; " >
									<label  for="source" >Problem Details</label>
								</div>
								<div class=" col-md-6" style="padding:0px; margin:0px; width: 46%;">
									<input class=" form-control" value="<?php echo $ticket_info[0]['details'] ?>" id="problem" name="details" list="problem">
									<datalist id="problem">
										<option value="Tiles">Tiles Work</option>
										<option value="Painting">Painting Work</option>
										<option value="Sanitary">Sanitary Work</option>
										<option value="Plumbing">Plumbing Work</option>
										<option value="Aluminium">Aluminium Work</option>
										<option value="Carpentry">Carpentry Work</option>
										<option value="Electrical">Electrical Work</option>
										<option value="others">Others</option>

									</datalist>
									<div class="form-group problem" style="display: none;" >
										<label class="control-label col-sm-10" for="category">Please input if others:</label>
										<div class="col-sm-12" >          
											<input type="text" class="form-control" id="category" placeholder="" name="others_details">
										</div>
									</div>
								</div>
							</div> -->
							
							<div class="row col-md-7" style="padding:0px; margin:0px; padding-left:10px;" >
								<div  class="col-md-5" style="padding:0px; margin:0px;padding-left:15px;" >
									<label  for="source" >Respective Department</label>
								</div>
								<div class=" col-md-7" style="padding:0px; margin:0px;">
									<input class="form-control" value="<?php echo $ticket_info[0]['respective_dept'] ?>" id="" name="respective_dept" list="respective_dept" readonly>
									<datalist id="respective_dept">
										<option value="Lifestyle">Lifestyle Department</option>
										<option value="Maintenance">Maintenance Departmant</option>
										<option value="Customer_Service">Customer Service Department</option>
										
									</datalist>
								</div>
								
							</div>
							
								<div class="row " style="margin-top:20px">
									<div class="col-md-12 row">

										<div class="col-md-2" style=" margin:0px;" >
											<label for="name" style="padding-right:0px">Query Details</label>
										</div>

										<div class="col-md-10" style="padding-left:5px; padding-right:9px; margin:0px;" >
											<input class="form-control" value="<?php echo $ticket_info[0]['query_details'] ?>"  placeholder="Notes" name="query_details" type="text" style="margin-left:10px" readonly>
										</div>

									</div>
								</div>
						</div>
					</div>	
					<div class="row"><h2 style="text-align:center;">Feed back of Concern Department</h2></div>
					<div class="myDiv">
						<div class="row " style="margin-top:20px">
							<div class="col-md-12 row">

								<div class="col-md-2" style=" margin:0px;" >
									<label for="name" style="padding-right:0px">Feedback 1</label>
								</div>

								<div class="col-md-10" style="padding-left:5px; padding-right:9px; margin:0px;" >
									<input class="form-control" value="<?php echo $ticket_info[0]['feedback1'] ?>"  placeholder="Feedback 1" name="feedback1" type="text" style="margin-left:10px" >
								</div>

							</div>
						</div>
						<div class="row " style="margin-top:20px">
							<div class="col-md-12 row">

								<div class="col-md-2" style=" margin:0px;" >
									<label for="name" style="padding-right:0px">Feedback 2</label>
								</div>

								<div class="col-md-10" style="padding-left:5px; padding-right:9px; margin:0px;" >
									<input class="form-control" value="<?php echo $ticket_info[0]['feedback2'] ?>"  placeholder="Feedback 2" name="feedback2" type="text" style="margin-left:10px" >
								</div>

							</div>
						</div>
						<div class="row " style="margin-top:20px">
							<div class="col-md-12 row">

								<div class="col-md-2" style=" margin:0px;" >
									<label for="name" style="padding-right:0px">Feedback 3</label>
								</div>

								<div class="col-md-10" style="padding-left:5px; padding-right:9px; margin:0px;" >
									<input class="form-control" value="<?php echo $ticket_info[0]['feedback3'] ?>"  placeholder="Feedback 3" name="feedback3" type="text" style="margin-left:10px" >
								</div>

							</div>
						</div>

						<div class="row " style="margin-top:20px">
							<div class="col-md-12 row">

								<div class="col-md-5" style=" margin:0px;" >
									<label for="name" style="padding-right:0px">Required time For Completion (Days)</label>
								</div>

								<div class="col-md-2" style="padding:0px; margin:0px;" >
									<input type="date" class="form-control" value="<?php echo $ticket_info[0]['required_time'] ?>" name="required_time">
									
								</div>

							</div>


						</div>
						<div class="row">
							<div class="col-md-12 row" style="padding:0px; margin:0px;">
							<div class="col-md-3" style=" margin:0px;" >
								<label for="name" style="padding-right:0px">Status:</label>
							</div>

							<div class="col-md-3" style="padding-left:5px; padding-right:5px; margin:5px;" >
								<select class=" form-control" id="status" value="<?php echo $ticket_info[0]['status'] ?>" name="status" >

									
									<option value="OnProcess">On Process</option>
									<option value="Pending">Pending</option>
									<option value="Solved">Solved</option>

								</select>
							</div>
						</div>
						
						</div>
					</div>


					
					
					<div class="col-md-2" style=" margin-top:7%; margin-bottom: 7%;
					padding: 0px;left:46%; right:46%;">
					<input class="btn btn-primary" onclick="show_alert();" type="submit" value="Submit">
				</div>
				
			</div>
			
		</form>
		
	</div>
	

</div>

<script type="text/javascript">
	$('#duration').on('change',function(){
		duration = $(this).val();
		if(duration == 'timeline'){
			$('.abcd').show();
		}else{
			$('.abcd').hide();
		}
	});
	$('#occupation').on('change',function(){
  	//alert("gdshhg");
		occupation= $(this).val();
		if(occupation == 'others'){
			$('.occ').show();
		}else{
			$('.occ').hide();
		}
	});
	$('#purpose_call').on('change',function(){
  	//alert("gdshhg");
		purpose_call= $(this).val();

		if(purpose_call == 'others1'){
  			//alert('ahgjhh');
			$('.purpose').show();
		}else{
			$('.purpose').hide();
		}
	}); 
	$('#construction').on('change',function(){
  	//alert("gdshhg");
		category= $(this).val();
		if(category == 'others'){
			$('.construction').show();
		}else{
			$('.construction').hide();
		}
	}); 
	$('#problem').on('change',function(){
  	//alert("gdshhg");
		details= $(this).val();
		if(details == 'others'){
			$('.problem').show();
		}else{
			$('.problem').hide();
		}
	});

	function show_alert(){
		alert("Data Update Sucessfull!")
	}

</script>

</body>		