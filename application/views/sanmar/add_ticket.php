<?php  $this->load->view('common/newheader.php'); 
?>
<script>
	$( function() {
		$( "#date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "yy-mm-dd"
		});
		$( "#birthdate" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});
		$( "#deadline" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});
		$( "#solution_timeline" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});

	} );
</script>
<script type="text/javascript">	$(function() {
		//$("#date").datepicker({dateFormat: "yy-mm-dd"});
		//$("#end_date").datepicker({dateFormat: "yy-mm-dd"});
	});
</script>
<style type="text/css">
	
	#dt {
		text-indent: -500px;
		height: 25px;
		width: 200px;
	}
</style>

<body style="background-image: linear-gradient(to bottom right, #783d3d85, #fffcdf) ; 
background-size: cover; 
background-repeat: no-repeat;
background-position: center center;">

<div class="container col-md-8" style=" margin-top:7%; margin-bottom: 7%;
background-color: white; padding: 40px;left:16%; right:16%;">
<!-- <div>
	<form class="form-horizontal" method="Post" action="customer">
		<div align="center">
			<h3>Please input Mobile Number</h3>
			<input type="number" name="phone" id="phone_search">
			<button type="button" class="btn btn-primary button-loading" id="search">Submit</button>
		</div>
	</form>
</div> -->

<form action="<?php echo site_url('contact/create_ticket');?>" method="post">
	<h2 style="text-align: center; color: #6F3218">Ticket Raising</h2>
	
	<div class="col-md-12" style=" margin-top: 20px;">

		<div class="row ">
			<div class="col-md-12 row">
				<div class="col-md-6 row" style="padding:0px; margin:0px;">
					<div class="col-md-3" style=" margin:0px;" >
						<label for="name" style="padding-right:0px">Customer Name:</label>
					</div>

					<div class="col-md-9" style="padding-left:5px; padding-right:9px; margin:0px;" >
						<input class="form-control"  placeholder="Customer Name" id="name" name="name" type="text" style="margin-left:10px" required>
					</div>
				</div>
				<!-- <div class="col-md-6 row" style="padding:0px; margin:0px;">
					<div class="col-md-3" style=" margin:0px;" >
						<label for="name" style="padding-right:0px">email:</label>
					</div>

					<div class="col-md-9" style="padding-left:5px; padding-right:9px; margin:0px;" >
						<input class="form-control"  placeholder="email" id="email" name="email" type="email" style="margin-left:10px" >
					</div>
				</div>
				<div class="col-md-6 row" style="padding:0px; margin:0px;">
					<div class="col-md-3" style=" margin:0px;" >
						<label for="name" style="padding-right:0px">CC:</label>
					</div>

					<div class="col-md-9" style="padding-left:5px; padding-right:9px; margin:0px;" >
						<input class="form-control"  placeholder="cc" id="cc" name="cc" type="email" style="margin-left:10px" >
					</div>
				</div> -->
				<div class="col-md-6 row" style="padding:0px; margin:0px;">
					<div class="col-md-3" style=" margin:0px;" >
						<label for="name" style="padding-right:0px">Status:</label>
					</div>

					<div class="col-md-9" style="padding-left:5px; padding-right:9px; margin:0px;" >
						<select class=" form-control" id="status" name="status" >

							<option>--Select--</option>
							<option value="OnProcess">On Process</option>
							<option value="Pending">Pending</option>
							<option value="Solved">Solved</option>

						</select>
					</div>
				</div>
			</div>
		</div>
		<div class=" row">
			<div class="col-md-12 row">

				<div class="col-md-6 row" style="padding:0px; margin:0px;">

					<div class="col-md-2">
						<label for="name">Project Name:<span class="required">*</span></label>
					</div>
					<div class="col-md-9">
						<input class="form-control" placeholder="Project Name" name="project" type="text"  required>
					</div>

				</div>

				<div class="row col-md-6" style="padding:0px; margin:0px;" >
					<div  class="col-md-5" style="padding-left:20px; margin:0px;" >
						<label for="phone">Contact Number:</label>
					</div>
					<div class=" col-md-7" style="padding:0px; margin:0px;">
						<input class=" form-control"  placeholder="Contact Number" name="phone" id="phone" type="Number">
					</div>

				</div>

			</div>
		</div>	


		<div class=" row" style="margin-top:20px">
			<div class="col-md-12 row">

				<div class="col-md-6 row" style="padding:0px; margin:0px;">

					<div class="col-md-4">
						<label for="location">Date of Call</label>
					</div>
					<div class="col-md-8">

						<input type="text" id="date" placeholder="" name="date" />
					</div>

				</div>


				<div class="row col-md-6" style="padding:0px; margin:0px;" >
					<div  class="col-md-5" style="padding-left:20px; margin:0px;" >
						<label for="phone">Shop/Apt./Plot No</label>
					</div>
					<div class=" col-md-7" style="padding:0px; margin:0px;">
						<input class=" form-control"  placeholder="Shop No" name="shop_no" type="text">
					</div>

				</div>

			</div>	

		</div>	


		<div class=" row" style="margin-top:20px">
			<div class="col-md-12 row">

				<div class="row col-md-6" style="padding:0px; margin:0px;" >
					<div  class="col-md-5" style="padding-left:15px; margin:0px;" >
						<label for="phone">Level /Floor/Road</label>
					</div>
					<div class=" col-md-6" style="padding:0px; margin:0px; width: 46%;">
						<input class=" form-control"  placeholder="Contact Number" name="level_no" type="text">
					</div>

				</div>

				<div class="row col-md-6" style="padding:0px; margin:0px;" >
					<div  class="col-md-5" style="padding-left:20px; margin:0px;" >
						<label for="phone">Car Park No</label>
					</div>
					<div class=" col-md-7" style="padding:0px; margin:0px;">
						<input class=" form-control"  placeholder="Car Park Number" name="car_park" type="text">
					</div>

				</div>

			</div>
		</div>	

		<div class=" row" style="margin-top:20px">
			<div class="col-md-12 row">
				<div class="col-md-6 row" style="padding:0px; margin:0px;">

					<div class="col-md-6">
						<label for="location">Date of Birth</label>
					</div>
					<div class="col-md-6">

						<input type="text" placeholder="dd-mm-yy" id="birthdate" Value="" name="birthdate" onclick="" />
					</div>

				</div>




				<div class="row col-md-6" style="padding:0px; margin:0px;" >
					<div  class="col-md-5" style="padding-left:22px; margin:0px;" >
						<label  for="source" >Occupation</label>
					</div>
					<div class=" col-md-7" style="padding:0px; margin:0px;">
						<select class=" form-control" id="occupation" name="occupation" >

							<option>--Select--</option>
							<option value="Business">Business</option>
							<option value="Service_Holders">Service Holder</option>
							<option value="others">Others</option>

						</select>
						<div class="form-group occ" style="display: none;" >
							<label class="control-label col-sm-10" for="category">Please input if others:</label>
							<div class="col-sm-10" >          
								<input type="text" class="form-control" id="category" placeholder="" name="occu">
							</div>
						</div>
					</div>


				</div>

			</div>
		</div>	


		<div class=" row" style="margin-top:20px">
			<div class="col-md-12 row">


				<div class="row col-md-6" style="padding:0px; margin:0px;" >
					<div  class="col-md-5" style="padding-left:15px; margin:0px;" >
						<label  for="source" >Purpose of Call</label>
					</div>
					<div class=" col-md-6" style="padding:0px; margin:0px;width: 46%; ">
						<select class=" form-control" id="purpose_call" name="purpose_call" >

							<option value="">----Select----</option>
							<option value="Sales">Sales</option>
							<option value="After_Sales_Service">After sales service</option>
							<option value="Registration">Registration</option>
							<option value="others1">Others</option>

						</select>
						<div class="form-group purpose" style="display: none;" >
							<label class="control-label col-sm-10" for="category">Please input if others:</label>
							<div class="col-sm-12" >          
								<input type="text" class="form-control" id="category" placeholder="" name="others">
							</div>
						</div>
					</div>

				</div>

				<div class="col-md-6 row" style="padding:0px; margin:0px;">

					<div class="col-md-6" style="padding-left:23px;"> 

						<label for="location">Deadline of issue solve </label></div>
						<div class="col-md-6">
							
							<input type="text" placeholder="dd-mm-yy" id="deadline" name="deadline" Value="">

						</div>
						<!-- 	</div> -->
					</div>



						
					</div>
				</div>	
				
				<div class="row " style="margin-top:20px">
					<div class="col-md-12 row">

						<div class="col-md-3" style=" margin:0px;" >
							<label for="name" style="padding-right:0px">Present Address:</label>
						</div>

						<div class="col-md-9" style="padding-left:5px; padding-right:9px; margin:0px;" >
							<input class="form-control"  placeholder="Address" id="address" name="present_address" type="text" style="margin-left:10px" required>
						</div>

					</div>
				</div>
				
				<div class="row " style="margin-top:20px">
					<div class="col-md-12 row">

						<div class="col-md-3" style=" margin:0px;" >
							<label for="name" style="padding-right:0px">Permanent Address:</label>
						</div>

						<div class="col-md-9" style="padding-left:5px; padding-right:9px; margin:0px;" >
							<input class="form-control"  placeholder="Address" name="permanent_address" type="text" style="margin-left:10px" required>
						</div>

					</div>
				</div>
				
				
				<div class="row " style="margin-top:20px">
					<div class="col-md-12 row">

						<div class="col-md-5" style=" margin:0px;" >
							<label for="name" style="padding-right:0px">Product Category (Construction Wise)</label>
						</div>

						<div class="col-md-7" style="padding:0px; margin:0px;" >
							<select class="form-control" id="construction" name="pd_category">
								<option>--Select--</option>
								<option value="BeforeHandover">Before Handover</option>
								<option value="WarrentyPeriods">Warrenty Periods</option>
								<option value="AfterHandover">After Handover/Warrenty Periods</option>
								<option value="others">Others</option>
							</select>
							<div class="form-group construction" style="display: none;" >
								<label class="control-label col-sm-10" for="category">Please input if others:</label>
								<div class="col-sm-12" >          
									<input type="text" class="form-control" id="category" placeholder="" name="others_category">
								</div>
							</div>
						</div>

					</div>
				</div>
				
				<div class=" row" style="margin-top:20px">
					<div class="col-md-12 row">
						
						<!-- <div class="row col-md-5" style="padding:0px; margin:0px;" >
							<div  class="col-md-6" style="padding-left:15px; margin:0px; " >
								<label  for="source" >Problem Details</label>
							</div>
							<div class=" col-md-6" style="padding:0px; margin:0px; width: 46%;">
								<select class=" form-control" id="problem" name="details" >
									
									<option>--Select--</option>
									<option value="Tiles">Tiles Work</option>
									<option value="Painting">Painting Work</option>
									<option value="Sanitary">Sanitary Work</option>
									<option value="Plumbing">Plumbing Work</option>
									<option value="Aluminium">Aluminium Work</option>
									<option value="Carpentry">Carpentry Work</option>
									<option value="Electrical">Electrical Work</option>
									<option value="others">Others</option>

								</select>
								<div class="form-group problem" style="display: none;" >
									<label class="control-label col-sm-10" for="category">Please input if others:</label>
									<div class="col-sm-12" >          
										<input type="text" class="form-control" id="category" placeholder="" name="others_details">
									</div>
								</div>
							</div>

						</div> -->

						<div class="row col-md-7" style="padding:0px; margin:0px; padding-left:10px;" >
							<div  class="col-md-5" style="padding:0px; margin:0px;padding-left:15px;" >
								<label  for="source" >Respective Department</label>
							</div>
							<div class=" col-md-7" style="padding:0px; margin:0px;">
								<select class=" form-control" id="" name="respective_dept" >
									
									<option>--Select--</option>
									<option value="Lifestyle">Lifestyle Department</option>
									<option value="Maintenance">Maintenance Departmant</option>
									<option value="Customer_Service">Customer Service Department</option>

								</select>
							</div>

						</div>
						
					</div>
				</div>	
				
				<div class="row " style="margin-top:20px">
					<div class="col-md-12 row">

						<div class="col-md-2" style=" margin:0px;" >
							<label for="name" style="padding-right:0px">Query Details</label>
						</div>

						<div class="col-md-10"  >
							<textarea name="query_details" style="width: 600px; height: 80px;"></textarea>

						</div>

					</div>
				</div>
				
				<div class="row " style="margin-top:20px">
					<!-- <div class="col-md-6 row">

						<div class="col-md-5" style=" margin:0px;" >
							<label for="name" style="padding-right:0px">Required time For Completion (Days)</label>
						</div>

						<div class="col-md-7" style="padding:0px; margin:0px;" >
							
								<input class="form-control"  placeholder="Required time" name="required_time" type="date" style="margin-left:10px" >
							
						</div>

					</div> -->
					<!-- <div class="row " style="margin-top:20px"> -->

						<!-- <div class="col-md-6 row">

							<div class="col-md-5" style=" margin:0px;" >
								<label for="name" style="padding-right:0px">Service Duration</label>
							</div>

							<div class="col-md-7 " style="padding:0px; margin:0px;" >
								<select class="form-control"id="duration" name="duration">
									<option value="ExistingProblem">Existing Problem</option>
									<option value="PossibleProblem">Possible Problem</option>
									<option value="timeline">Solution Timeline</option>

								</select>
								<div class="form-group abcd" style="">

									<input class="" placeholder="dd-mm-yy" id="solution_timeline" placeholder="Address" name="solution_timeline" type="text" style="" >


								</div>	
							</div>


						</div> -->
					</div>


					<div class="col-md-2" style=" margin-top:7%; margin-bottom: 7%;
					padding: 0px;left:46%; right:46%;">
					<input class="btn btn-primary" onclick="show_alert();" type="submit" value="Submit">
				</div>
				
			</div>

		</form>

	</div>
	
	
	<script type="text/javascript">
		$('#duration').on('change',function(){
			duration = $(this).val();
			if(duration == 'timeline'){
				$('.abcd').show();
			}else{
				$('.abcd').hide();
			}
		});
		$('#occupation').on('change',function(){
  	//alert("gdshhg");
			occupation= $(this).val();
			if(occupation == 'others'){
				$('.occ').show();
			}else{
				$('.occ').hide();
			}
		});
		$('#purpose_call').on('change',function(){
  	//alert("gdshhg");
			purpose_call= $(this).val();

			if(purpose_call == 'others1'){
  			//alert('ahgjhh');
				$('.purpose').show();
			}else{
				$('.purpose').hide();
			}
		}); 
		$('#construction').on('change',function(){
  	//alert("gdshhg");
			category= $(this).val();
			if(category == 'others'){
				$('.construction').show();
			}else{
				$('.construction').hide();
			}
		}); 
		$('#problem').on('change',function(){
  	//alert("gdshhg");
			details= $(this).val();
			if(details == 'others'){
				$('.problem').show();
			}else{
				$('.problem').hide();
			}
		});

		function show_alert(){
			alert("Data Insert Sucessfull!")
		}

	</script>
	<script type="text/javascript">
		$('#search').click(function() {

			var phone = $('#phone_search').val();
  //var mobile = $('#mobile').val();

			$.ajax({
				url: '<?php echo site_url('contact/input_mobile'); ?>',
				type: 'POST',
    data: { //data pass to controller
    	phone: phone,

    },

    dataType: 'json',
    success: function(data) {
    	console.log(data);
    	if(data){
    		$('#phone').val(data.phone);
    		$('#name').val(data.name);

    		$('#birthdate').val(data.birthdate);
    		$('#occupation').val(data.occupation);
    		$('#address').val(data.address);

    		$('#other_issue').val(data.other_issue);
    		$('#note').val(data.note);
    	}
    }

 });
//return false;
		});
	</script>
</body>		
