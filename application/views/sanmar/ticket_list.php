<?php   $this->load->view('common/newheader.php');  
?>

<script>
/*	$( function() {
		$( "#start_date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});
		$( "#end_date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});
	} );*/
	$(function() {
		$("#start_date").datepicker({dateFormat: "yy-mm-dd"});
		$("#end_date").datepicker({dateFormat: "yy-mm-dd"});
	});
</script>
<body style="background-image: linear-gradient(to bottom right, #783d3d85, #fffcdf) ; 
min-height: 100%;

background-size: cover;
background-repeat: no-repeat;
background-position: center center;">

<div class="container col-md-8" style=" margin-top:7%; margin-bottom: 7%;
background-color: white; padding: 40px;left:16%; right:16%;">
<div class="row">
	<div class="col-md-12">
		<h1 class="page-header"> Ticket Details /
			<small>List</small>
		</h1>
	</div>
</div>

<div class="row">

</div>
<div class="row">
	<div class="col-md-12">
		<?php if(isset($search_info)) echo "<h3>$search_info</h3>"?>
		<div > <h3 id="search_info"></h3>
			<form class="form-inline" method="post" action="<?php echo site_url('contact/search_by_sanmar_ticket');?>">
				
				<br><br/>

				<div class="form-group">
					<label for="">Status</label>
					<!-- <?php

					//$names=$this->prime_model->getByQuery('select distinct status from ticket');
						//print_r($names);
				?> -->


				<select class="form-control input-sm" type="text" name="status" id="status">
					<option value='' >-Select Status-</option>
					<option value='Pending' >Pending</option>
					<option value='Solved' >Solved</option>
					<option value='OnProcess' >OnProcess</option>

				</select>
			</div>
			<div class="form-group">
				<label for="">Department</label>
					<!-- <?php

					//$names=$this->prime_model->getByQuery('select distinct status from ticket');
						//print_r($names);
				?> -->


				<select class="form-control input-sm" type="text" name="respective_dept" id="respective_dept">
					<option value='' >-Select Status-</option>
					<option value='Customer_Service' >Customer_Service</option>
					<option value='Maintenance' >Maintenance</option>
					<option value='Lifestyle' >Lifestyle</option>

				</select>
			</div>
			<br><br/>
			<div class="form-group">
				<label for="email">Start Date</label>

				<input type="text" autocomplete="off" class="form-control input-sm" id="start_date" placeholder="Select Start Date" name="start_date">
			</div>
			<div class="form-group">
				<label for="email">End Date</label>

				<input type="text" autocomplete="off" class="form-control input-sm" id="end_date" placeholder="Select End Date" name="end_date">
			</div> 
			<button type="submit" class="btn btn-sm btn-success">Search</button>
		</form>
		<br>



	</div>
	<div class="table-responsive">


		<table class="table table-bordered table-hover table-striped" id="my_datatable">
			<thead>
				<tr>
					<!--<th style="width:10%;"></th>-->
					<th>Sl</th>
					<th>Customer Name</th>
					<th>Project Name</th>
					<th>Date</th>
					<th>Created By</th>
					<th>Status</th>


					<th class="col-md-3" align="center">Action</th>

				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<div id="result" class="col-sm-3 col-md-3 col-lg-3">


		<button type="button" STYLE="max-width:160px; max-height:70px;" id="downloadCSV" class="btn btn-success glyphicon glyphicon-download-alt" >Download CSV</button>

	</div> </div>
</div>
</div>

</body>
<!--<script src='<?php //echo base_url("assets/js/$paging_js_name?v3");?>'></script>-->
<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">
	$(document).on('click', '#downloadCSV', function () {
		
		
		var url="<?php echo site_url('/contact/download_cdr_chart'); ?>";
		var status=$("#status").val();
		var respective_dept=$("#respective_dept").val();
		//alert(status);
		var start_date=$("#start_date").val();
		
		var end_date=$("#end_date").val();
//alert(start_date,end_date);

		$.ajax({
			url: url,
			type: 'post',
			data:{status:status,respective_dept:respective_dept,start_date:start_date,end_date:end_date},
			
			/*contentType: false,
			processData: false,*/
			success: function (data) 
			{
				
				var isHTML = RegExp.prototype.test.bind(/(<([^>]+)>)/i);                    
				if(!isHTML(data)){  
					var downloadLink = document.createElement("a");
					var fileData = ['\ufeff'+data];
					
					var blobObject = new Blob(fileData,{
						type: "text/csv;charset=utf-8;"
					});
					
					var url = URL.createObjectURL(blobObject);
					downloadLink.href = url;
					downloadLink.download = "data.csv";
					
                  /*
                   * Actually download CSV
                   */
					document.body.appendChild(downloadLink);
					downloadLink.click();
					document.body.removeChild(downloadLink);
					
					
				}                    
				
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) { 
				TRENDIN.ShowError("Status: " + textStatus); alert("Error: " + errorThrown); 
			}
		});
	});
	


	<?php 
	  if(isset($search_contact)&&$search_contact==true){
        ?>
        $(document).ready(function() {
            $("#result").hide();
            
            var primary_number="<?php echo $primary_number;?>";
            $.ajax({        
                type: "POST" 
                ,url: "<?php echo site_url('contact/search_by_sanmar_ticket');?>" 
                ,data:{primary_number:'<?php echo $primary_number;?>'}
                ,async: false
                ,success: function(response) { // on success..
                    var obj=JSON.parse(response);
                    if(obj.record_count==0){
                        //$('#search_info').html("Number "+primary_number+" is not in your Sales list. <a role='button' class='btn btn-info' href='<?php echo site_url("sale_details/create_sale/$primary_number");?>'> Add to Sale</a>");
                    }
                    else {
                        $('#search_info').html("Search result of primary number: "+primary_number);
                    }
                    var query_id=obj.query_id;
                    console.log(response);
                    $('#my_datatable').dataTable({
                        destroy: true, //use this to reinitiate the table, other wise problem will occur
                        processing: true,
                        serverSide: true,
                        ajax: {
                            url: "<?php echo site_url('contact/process_paging_ticket');?>"
                            ,type: 'POST'
                            ,data:{query_id: query_id}
                        }
                    });
                }
            });
        } );
        
        <?  
    }
	else{
		?>
		$(document).ready(function() {
			$("#result").hide();
			$('#my_datatable').dataTable({
				destroy: true, //use this to reinitiate the table, other wise problem will occur
				processing: true,
				serverSide: true,
				ajax: {
					url: "<?php echo site_url('contact/process_paging_ticket');?>"
					,type: 'POST'
					,data:{query_id: '0'}
				}
			});
		} );
		
		<?
	}
	?>		
	

	$( "form" ).submit(function( e ) {
		e.preventDefault();
		e.stopImmediatePropagation();
		$("#result").show();		
		//alert('form submitted');
		$.ajax({ // create an AJAX call...
			data: $(this).serialize(), // get the form data
			type: $(this).attr('method'), // GET or POST
			url: $(this).attr('action'), // the file to call
			success: function(response) { // on success..
				var obj=JSON.parse(response);
				var query_id=obj.query_id;
				console.log(response);
				$('#my_datatable').dataTable({
					destroy: true, //use this to reinitiate the table, other wise problem will occur
					processing: true,
					serverSide: true,
					ajax: {
						url: "<?php echo site_url('contact/process_paging_ticket');?>"
						,type: 'POST'
						,data:{query_id: query_id}
					}
				});
				
			}
		});
		return false;
	});
	jQuery(document.body).on('click', '.delete', function (e) {
		var this_holder = this;
		e.preventDefault();
		var delete_url= $(this).attr('href');
		

		bootbox.confirm("Are you sure you want to delete this entry?", function (response) {
			if (response) {
				$.ajax({
					url: delete_url,
					dataType: 'text',
					type: 'post',
					contentType: 'application/x-www-form-urlencoded',
					success: function (data, textStatus, jQxhr) {
						if (data == 1) {
							$(this_holder).closest('td').closest('tr').hide(1000);
						} else {

						}
					},
					error: function (jqXhr, textStatus, errorThrown) {
						alert(errorThrown);
					}
				});

			}
		});
	});
	
	
	$(document).on('click', '.linksDeleteSelected', deleteSelected);
	$(document).on('change', '.selectAll', function () {
		if ($(this).is(":checked")) {
			$('.resultRow').prop('checked', true);
		}
		else
			$('.resultRow').prop('checked', false);
	});

	function deleteSelected(e) {
		if ($('.resultRow:checked').size() < 1) {
			alert('please select at least one record');
			return false;
		}
		var r = confirm(e.target.getAttribute("confirmationMsg"));
		if (r == false)
			return false;
		var controllerMethod = e.target.getAttribute("controllerMethod");
		var url = controllerMethod;
        /*var obj = {
            "flammable": "inflammable",
            "duh": "no duh"
        };
        $.each( obj, function( key, value ) {
            alert( key + ": " + value );
        });*/

		var selectedIDs = [];
		$('.resultRow:checked').each(function () {
			selectedIDs.push($(this).val());
		});
		console.log(selectedIDs);
		$.post(
			url,
			{selectedIDs: selectedIDs.join(", ")},
			function (data) {
                /*var obj = $.parseJSON(data);
                if(obj.success){
                    showStatusModal('Success','modal-info',obj.content);
                    reloadResult(e.target.getAttribute("controllerReloadMethod"));
                }
                else showStatusModal('Error','modal-warning',obj.content);*/
				window.location.replace(e.target.getAttribute('controllerReloadMethod'));
			}
			);
		return false;
	}
</script>