<?php  $this->load->view('common/newheader.php'); 
?>

<body style="background-image: linear-gradient(to bottom right, #783d3d85, #fffcdf) ; 
		background-size: cover; 
		background-repeat: no-repeat;
		background-position: center center;">
		
		
	<div class="container col-md-8" style=" margin-top:7%; margin-bottom: 7%;
		background-color: white; padding: 40px;left:16%; right:16%;">
		
		
		<form action="<?php echo site_url() ?>/contact/add_investor" method="post" enctype="multipart/form-data" >
		 <h2 style="text-align: center; color: #6F3218">Add Investor Information</h2>
			<div class="col-md-12" style=" margin-top: 20px;">
				<div class="col-md-12">      
					<!-- Table -->
				
				
					<table class="table  table-hover">
								
							  <tr>
								  
									<th class="row col-md-3">
									<label for="name">Name<span class="required">*</span></label>
									</th>
									<td class="row col-md-9">
									<input class="form-control" placeholder="Please input Name" name="name" type="text"  required>
									</td>
								  
							  </tr>
							  
							  <tr>
								   <th class="row col-md-3">
									<label for="name">Contact No</label>
									</th>
									<td class="row col-md-9">
									<input class="form-control" placeholder="Contact No" name="contact_no" type="text"  required>
									</td>
							  </tr>
							  
							  <tr>
								   <th class="row col-md-3">
									<label for="name">email</label>
									</th>
									<td class="row col-md-9">
									<input class="form-control" placeholder="Please input email address" name="email" type="email"  required>
									</td>
							  </tr>
							  
							  <tr>
								  <th class="row col-md-3">
									<label for="name">City</label>
									</th>
									<td class="row col-md-6">
									<input class="form-control" placeholder="Please input city" name="city" type="text"  required>
									</td>
							  </tr>
							  
							 
							
							  
							</table>
					
					<div class="col-md-2" style=" margin-top:7%; margin-bottom: 7%;
		               padding: 0px;left:46%; right:46%;">
						<input class="btn btn-primary" onclick="show_alert();" type="submit" value="Submit">
					</div>
			
				</div>
				
			  
			</div>
			

		
		</form>
		
	</div>
	
		
</body>