<?php  $this->load->view('common/newheader.php'); 
?>
<script>
  $( function() {
    $( "#handover_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+10",
      dateFormat: "dd-mm-yy"
  });
    
} );
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<body style="background-image: linear-gradient(to bottom right, #783d3d85, #fffcdf) ; 
background-size: cover; 
background-repeat: no-repeat;
background-position: center center;">


<div class="container col-md-8" style=" margin-top:7%; margin-bottom: 7%;
background-color: white; padding: 40px;left:16%; right:16%;">


<form action="<?php echo site_url() ?>/contact/update_project" method="post" enctype='multipart/form-data' >
   <h2 style="text-align: center; color: #6F3218">Add Project Information</h2>
   <div class="col-md-12" style=" margin-top: 20px;">
    <div class="col-md-12">      
        <!-- Table -->
        
        
        <table class="table  table-hover">
            
          <tr>
              
            <th class="row col-md-3">
                <label for="name">Project Name<span class="required">*</span></label>
            </th>
            <td class="row col-md-9">
                <input class="form-control" placeholder="Client Name" name="id" value="<?php echo $projectinfo[0]['id'] ?>" type="hidden"  >

                <input class="form-control" placeholder="Client Name" name="project_name" value="<?php echo $projectinfo[0]['project_name'] ?>" type="text"  required>
            </td>
            
        </tr>
        
        <tr>
         <th class="row col-md-3">
            <label for="name">Project Type</label>
        </th>
        <td class="row col-md-9">
            <select class="form-control"id="lead_forword" name="project_type" value="">


             
              
               <?php 
               $project_dropdown=array('Residential'=>'Residential','Commercial'=>'Commercial','Mixuse'=>'Mix Use');
               foreach ($project_dropdown as $key => $value) {
                $selected="";
                if($projectinfo[0]['project_type']==$key){
                 $selected="selected";
             }
             echo "<option value='$key' $selected >$value</option>";
                            # code...
         }
         ?>
         
     </select>

 </td>
</tr>

<tr>
 <th class="row col-md-3">
    <label for="name">Location</label>
</th>
<td class="row col-md-3">
    <input list="all_project1" name="location" autocomplete="off" value="<?php echo $projectinfo[0]['location'] ?>">
    <datalist id="all_project1">
        <option value="Dhaka">Dhaka</option>
        <option value="Chattogram">Chattogram</option>

    </datalist>
</td>
</tr>

<tr>
  <th class="row col-md-3">
    <label for="name">Storied</label>
</th>
<td class="row col-md-9">
    <input class="form-control" placeholder="Storied " name="storied" type="text"  value="<?php echo $projectinfo[0]['storied'] ?>">
</td>
</tr>

<tr>
    <th class="row col-md-3">
        <label for="name">Project Facing</label>
    </th>
    <td class="row col-md-9">
        <input class="form-control" placeholder="Project facing" name="project_facing" type="text"  value="<?php echo $projectinfo[0]['project_facing'] ?>">
    </td>
    
</tr>

<tr>
    <th class="row col-md-3">
        <label for="name">Total Units</label>
    </th>
    <td class="row col-md-9">
        <input class="form-control" placeholder="Total Units" name="total_units" type="text"  value="<?php echo $projectinfo[0]['total_units'] ?>">
    </td>
    
</tr>

<tr>
    <th class="row col-md-3">
        <label for="name">Total Salesable Unit</label>
    </th>
    <td class="row col-md-9">
        <input class="form-control" placeholder="Total Salesable Unit" name="total_saleable_unit" type="text" id="firstNumber" value="<?php echo $projectinfo[0]['total_saleable_unit'] ?>">
    </td>
    
    
</tr>

<tr>
    <th class="row col-md-3">
        <label for="name">Sold Unit</label>
    </th>
    <td class="row col-md-9">
        <input class="form-control"  placeholder="Sold Unit" name="sold_unit" type="text"  id="secondNumber" value="<?php echo $projectinfo[0]['sold_unit'] ?>">
    </td>
    
    
</tr>

<tr>
    <th class="row col-md-3">
        <label for="name">Unsold Unit</label>
    </th>
    <td class="row col-md-9">
        <input class="form-control" id="result" placeholder="Unsold Unit" name="unsold" type="text" value="<?php echo $projectinfo[0]['unsold'] ?>">
    </td>
    
    
</tr>

<tr>
    <th class="row col-md-3">
        <label for="name">Apartment / Shop Size details </label>
    </th>
    <td class="row col-md-9">
        <textarea name="apartment" style="margin-left:10px; width: 500px; height: 100px;"><?php echo $projectinfo[0]['apartment'] ?></textarea>
        
        
    </td>
    
</tr>

<tr>
  
    <th class="row col-md-6">
        <label for="location">Handover Date (approximately)</label>
    </th>
    <td class="row col-md-6">
        <input class="form-control" id="handover_date" type="text" name="handover_date" value="<?php echo $projectinfo[0]['handover_date'] ?>">
        
    </td>
    
</tr>

<tr>
 <th class="row col-md-3">
    <label for="name">Total Land Area (Katha)</label>
</th>
<td class="row col-md-9">
    <input class="form-control" placeholder="Total Land Area" name="total_land_area" type="text"  value="<?php echo $projectinfo[0]['total_land_area'] ?>">
</td>
</tr>

<tr>
  
 <th class="row col-md-3">
    <label for="name">Project USP</label>
</th>
<td class="row col-md-9">
    
    <textarea name="project_usp" style="margin-left:10px; width: 500px; height: 100px;"><?php echo $projectinfo[0]['project_usp'] ?></textarea>
</td>


</tr>

<tr>
 <th class="row col-md-3">
    <label for="name">Adjacent Landmark</label>
</th>
<td class="row col-md-9">
  <textarea name="landmark" style="margin-left:10px; width: 500px; height: 100px;"><?php echo $projectinfo[0]['landmark'] ?></textarea>
  
</td>   

</tr>

<tr>
  
  <th class="row col-md-3">
    <label for="name">Parking Per Unit</label>
</th>
<td class="row col-md-9">
    <input class="form-control" placeholder="Parking Per Unit" name="parking_per_unit" type="text"  value="<?php echo $projectinfo[0]['parking_per_unit'] ?>">
</td>   

</tr>

<tr>
 
 <th class="row col-md-3">
    <label for="name">Parking Space</label>
</th>
<td class="row col-md-9">
    <input class="form-control" placeholder="Parking Space" name="parking_space" type="text"  value="<?php echo $projectinfo[0]['parking_space'] ?>">
</td>   

</tr>
                              <!--   <tr>
                   
                   <th class="row col-md-3">
                  <label for="name">Project Image</label>
                  </th>
                  <td class="row col-md-9">
                  <input class="form-control" name="image" type="file" value="<?php echo $projectinfo[0]['image'] ?>">
                  </td> 
                   
              </tr> -->
              
                              <!--   <tr>
                                   
                                   <th class="row col-md-3">
                                    <label for="name">Project Image</label>
                                    </th>
                                    <td class="row col-md-9">
                                    <input class="form-control" name="file" type="file" value="upload">
                                    </td>   
                                   
                                </tr> -->
                                
                                
                                
                                


                            </table>
                            
                            <div class="col-md-2" style=" margin-top:7%; margin-bottom: 7%;
                            padding: 0px;left:46%; right:46%;">
                            <input class="btn btn-primary" onclick="show_alert();" type="submit" value="Submit">
                        </div>
                        
                    </div>
                    
                    
                </div>
                

                
            </form>
            
        </div>
        
        <script type="text/javascript">

           $(document).on("change keyup blur", "#secondNumber", function() {
            var first = $('#firstNumber').val();
            var second = $('#secondNumber').val();
            var rest = first-second; //its convert 10 into 0.10
            //var mult = main * dec; // gives the value for subtract from main value
           // var discont = main - mult;
            $('#result').val(rest);
        });
           
           function unsold()
           {
        /*num1 = document.getElementById("firstNumber").value;
        num2 = document.getElementById("secondNumber").value;
        //document.getElementById("result").innerHTML = num1 * num2;
        var result=num1-num2;
        var divobj = document.getElementById('unsold');
                divobj.value = result;*/

           }
       </script>
   </body>