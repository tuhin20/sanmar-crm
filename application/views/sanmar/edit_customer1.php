<?php   $this->load->view('common/newheader.php');  
?>

<?php // print_r($customer_info); ?>

<!DOCTYPE html>
<html>

<script>
	$( function() {
		$( "#date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});
  /*  $( "#date" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-100:+10",
      dateFormat: "dd-mm-yy"
  });*/
} );
</script>

<body style="background-image: linear-gradient(to bottom right, #783d3d85, #fffcdf) ; 
background-size: cover; 
background-repeat: no-repeat;
background-position: center center;">




<div class="container col-md-8" style=" margin-top:7%; margin-bottom: 7%;
background-color: white; padding: 40px;left:16%; right:16%;">

<h2 style="text-align: center; color: #6F3218 ">Edit Customer Information</h2>
<?php if ($this->session->flashdata('success')) { ?>
	<div class="alert alert-success fade in" aria-label="close">
	<h3>
		<?php echo $this->session->flashdata('success'); ?>
	</h3>
	</div>
<?php } ?>
<form action="<?php echo site_url() ?>/contact/update_customer" method="post" >
	<div class="col-md-12" style=" margin-top: 20px;">
		
					

		<div class=" row">
			<div class="col-md-12 row">

				<div class="col-md-6 row" style="padding:0px; margin:0px;">

					<div class="col-md-4">
						<label for="name">Call Type:<span class="required">*</span></label>
					</div>
					<div class="col-md-9">
<!-- <select name="call_type" id="calltype" class="col-md-6 form-control" value="<?php echo $customerinfo[0]['call_type'] ?>"> -->
	<input list="call_type" value="<?php echo $customerinfo[0]['call_type'] ?>" name="call_type" autocomplete="off">
	<datalist id="call_type">
					<option>--Select--</option>
					<option value="Incoming">Incoming</option>
					<option value="Outgoing">Outgoing</option>
			<!-- 	</select> -->
			</datalist>
					</div>

				</div>

				<div class="row col-md-6" style="padding:0px; margin:0px;" >
					<div  class="col-md-5" style="padding-left:20px; margin:0px;" >
						<label for="phone">Name:</label>
					</div>
					<div class=" col-md-7" style="padding:0px; margin:0px;">
						<input class="form-control" value="<?php echo $customerinfo[0]['name'] ?>" placeholder="Client Name" name="name" type="text"  >
						<input class="form-control" value="<?php echo $customerinfo[0]['id'] ?>" placeholder="Client Name" name="id" type="hidden"  >
					</div>

				</div>

			</div>
		</div>
		<div class=" row mt-2" style="margin-top: 1em;">
			<div class="col-md-12 row ">

				<div class="col-md-4 row" style="padding:0px; margin:0px;">

					<div class="col-md-4">
						<label for="name">Mobile(Local)-1:<span class="required">*</span></label>
					</div>
					<div class="col-md-9">
						<input class="form-control" value="<?php echo $customerinfo[0]['phone'] ?>" placeholder="Contact Number" name="phone" type="text"  required>
					</div>

				</div>

				<div class="row col-md-4" style="padding:0px; margin:0px;" >
					<div class="col-md-4">
						<label for="name">Mobile(Local)-2:<span class="required">*</span></label>
					</div>
					<div class=" col-md-9" style="padding:0px; margin:0px;">
						<input class=" form-control" value="<?php echo $customerinfo[0]['phone2'] ?>"  placeholder="Contact Number" name="phone2" type="Number">
					</div>

				</div>
				<div class="row col-md-4" style="padding:0px; margin:0px;" >
					<div class="col-md-4">
						<label for="name">Mobile(International):<span class="required">*</span></label>
					</div>
					<div class=" col-md-9" style="padding:0px; margin:0px;">
						<input class=" form-control" value="<?php echo $customerinfo[0]['phone_int'] ?>"   placeholder="Contact Number" name="phone_int" type="Number">
					</div>

				</div>

			</div>
		</div>
			<div class=" row mt-2" style="margin-top: 1em; border-style: ridge;border-width: .01px .01px;">
			<div class="col-md-12 row">

				<div class="col-md-6 row" style="padding:0px; margin:0px;">

					<div class="col-md-6">
						<label for="name">Preferred Neighborhood:<span class="required">*</span></label>
					</div>
					<div class="col-md-9">
						<input list="all_project" name="neighborhood" value="<?php echo $customerinfo[0]['neighborhood'] ?>"   id="neighborhood" class="col-md-6 form-control">
						<datalist id="all_project">
							<option>--Select--</option>
					<option value="ctg">Chattogram</option>
					<option value="dhk">Dhaka</option>
					<option value="rest">Others</option>
			</datalist>
					</div>

				</div>

			<div class="col-md-6 ">
					<div class="form-group neighborhood" style="display: none; margin-top:20px">
						<label class="control-label col-sm-4" for="category">Chittagong:</label>
						<select name="ctg_city" >
						<!-- <select class="form-control" name="ext"> -->
							<option>--Select--</option>
								<option value="South_Khulshi">South Khulshi</option>
								<option value="Kulshi_Hills">Khulshi Hills R/A OR Nizam Road</option>
								<option value="Panchlaish">Panchlaish</option>
								<option value="Nasirabad_Housing">Nasirabad Housing</option>
								<option value="Mehedibag">Mehedibag</option>
								<option value="Jamal_Khan">Jamal Khan</option>
								<option value="AndarKilla">AndarKilla</option>
								<option value="Firingi_Bazar">Firingi Bazar</option>
								<option value="Amirbag">Amirbag</option>
								<option value="Hill_View">Hill View</option>
								<option value="Lalkhan_Bazar">Lalkhan Bazar</option>
								<option value="Kazir_Deuri">Kazir Deuri</option>
</select> 
							<!-- </select> -->
						</div>
					</div>
					<div class="col-md-6 ">
					<div class="form-group neighborhood_dhaka" style="display: none; margin-top:20px">
						<label class="control-label col-sm-3" for="category">Dhaka:</label>
							<select name="dhk_city">
						<!-- <select class="form-control" name="ext"> -->
							<option>--Select--</option>
								<option value="Gulshan1">Gulshan-1</option>
								<option value="Gulshan2">Gulshan-2</option>
								<option value="Niketan">Niketan</option>
								<option value="Bashundara R/A">Bashundara R/A</option>
								<option value="Uttara">Uttara</option>
								<option value="Mirpur">Mirpur</option>
								<option value="Dhanmondi">Dhanmondi</option>
								<option value="Mohammadpur">Mohammadpur</option>
								<option value="Banani">Banani</option>
								<option value="Mogbazar">Mogbazar</option>
						
</select> 
							<!-- </select> -->
						</div>
					</div>
			
		<div class="col-md-6 ">
					<div class="form-group rest" style="display: none; margin-top:20px">
						<label class="control-label col-sm-4" for="category">Others:</label>
				<input class="form-control"  placeholder="Others" id="rest" name="rest" type="text" style="margin-left:10px">
						</div>
					</div>
			</div>
		</div>	


		<div class="row " style="margin-top:20px">
			<div class="col-md-12 row">

				<div class="col-md-1" style=" margin:0px;" >
					<label for="name" style="padding-right:0px">email:<span class="required">*</span></label>
				</div>

				<div class="col-md-11" style="padding-left:5px; padding-right:9px; margin:0px;" >
					<input class="form-control" value="<?php echo $customerinfo[0]['email'] ?>"   placeholder="email" name="email" type="email" style="margin-left:10px" required>
				</div>

			</div>
		</div>	
		<div class="row " style="margin-top:20px">
			<div class="col-md-12 row">

				<div class="col-md-4" style=" margin:0px;" >
					<label for="name" style="padding-right:0px">Preferred Size of Apt/Shop:<span class="required">*</span></label>
				</div>

				<div class="col-md-6" style="padding-left:5px; padding-right:9px; margin:0px;" >
					<input class="form-control" value="<?php echo $customerinfo[0]['preferd'] ?>"  placeholder="Prefered Size of Apt/Shop" name="preferd" type="preferd" style="margin-left:0px" >
				</div>

			</div>
		</div>
<!-- 		<div class="row " style="margin-top:20px">
			<div class="col-md-12 row">

				<div class="col-md-4" style=" margin:0px;" >
					<label for="name" style="padding-right:0px">Prefered Project Location:<span class="required">*</span></label>
				</div>

				<div class="col-md-6" style="padding-left:5px; padding-right:9px; margin:0px;" >
					<input class="form-control"  placeholder="" name="email" type="email" style="margin-left:0px" >
				</div>

			</div>
		</div> -->
		
			<div class="col-md-12 ">
			<div class="col-md-6 row" style="margin-top:20px">
				<label class="col-md-6" for="category">Type of client:</label>

				<input list="TypeProject" name="type" id="type" class="col-md-6 form-control" value="<?php echo $customerinfo[0]['type'] ?>">
				<datalist id="TypeProject">
					<option>--Select--</option>
					<option value="New">New</option>
					<option value="Existing">Existing</option>
				</datalist>
			</div>

				<!--  <div class="form-group abcd" style="display: none;">
				  <label class="control-label col-sm-2" for="category">Existing:</label>
				  <div class="col-sm-6">          
					<input type="text" class="form-control" id="category" placeholder="Enter Category" name="ext">
				  </div>
				</div> -->
				<div class="col-md-6 ">
					<div class="form-group abcd" style="display: none; margin-top:20px">
						<label class="control-label col-sm-3" for="category">Existing:</label>
						<input list="all_project"  name="ext" autocomplete="off" >
						<!-- <select class="form-control" name="ext"> -->
							<datalist id="all_project">
								<?php foreach ($project_info as $value) { ?>
									<option value="<?php echo $value["project_name"]; ?>"><?php echo $value["project_name"]; ?></option>';
								<?php } ?>

							</datalist>
							<!-- </select> -->
						</div>
					</div>
				</div>
					<div class="row" style="margin-top:20px">
						<div class="col-md-12 row" >
							<div class="col-md-6 ">
								<label for="name" >Security Check Question - Mother's Name:</label>
							</div>
							<div class="col-md-6" style="padding:0px; margin:0 px;">
								<input class="form-control" value="<?php echo $customerinfo[0]['mothers_name'] ?>" placeholder="Mother's name" name="mothers_name" type="text">
							</div>

						</div>

					</div>



					<div class=" row" style="margin-top:20px">
						<div class="col-md-12 row">

							<div class="col-md-6 row" style="padding:0px; margin:0px;">

								<div class="col-md-6">
									<label for="location">Preferred Project Location:</label>
								</div>
								<div class="col-md-6">


									<!-- <select class="form-control" name="ext"> -->
								
											<input list="Prefered" class=" form-control" value="<?php echo $customerinfo[0]['location'] ?>" id="location1" name="location" >
												<datalist id="Prefered">
													<option>--Select--</option>
											<option value="Dhaka">Dhaka</option>
											<option value="Chattogram">Chattogram</option>
											<option value="Cox's_bazar">Cox's Bazar</option>
											<option value="Rajshahi">Rajshahi</option>
											<option value="Sylhet">Sylhet</option>
											<option value="Rangpur">Rangpur</option>
											<option value="Khulna">Khulna</option>
											<option value="Barishal">Barishal</option>
											<option value="Mymansingh">Mymansingh</option>
											<option value="otherprd">Others</option>
												
										</datalist>
										
								<!-- <select class="form-control"id="location" name="location">
										<option value="Dhaka">Dhaka</option>
										<option value="Chattogram">Chattogram</option>
									</select> -->
									
										<div class="col-sm-12" >  
											
										  	<div class="form-group prd" style="display: none;" >
										<label class="control-label col-sm-12" for="category">Please input if others:</label>
										<div class="col-sm-12" style="margin: 0px">          
											<input type="text" class="form-control" id="cd" placeholder="Enter Others" name="pre">
										</div>      
										
											<!-- <input type="text" class="form-control" id="category" placeholder="" name="Prefered"> -->
										</div>
									</div>
									<!-- <div class="form-group others" style="display: none;">
										<label class="control-label col-sm-2" for="category">Others:</label>
										<div class="col-sm-8" style="margin: 2px">          
											<input type="text" class="form-control" id="cd" placeholder="Enter Others" name="pre">
										</div>
									</div> -->
								</div>

							</div>

							<div class="row col-md-6" style="padding:0px; margin:0px;" >
								<div  class="col-md-3" style="padding-left:20px; margin:0px;" >
									<label  for="source" >Source:</label>
								</div>

								<div class=" col-md-9" style="padding:0px; margin:0px;">
									<input list="SocialMedia" class=" form-control" id="source" name="source" value="<?php echo $customerinfo[0]['source'] ?>" >
									<datalist id="SocialMedia">
										<option>--Select--</option>
										<option value="Facebook">Facebook</option>
										<option value="Instagram">Instagram</option>
										<option value="Tweeter">Tweeter</option>
										<option value="Tiktok">Tiktok</option>
										<option value="Linkedin">Linkedin</option>
										<option value="News">News Paper</option>
										<option value="TV">TV</option>
										<option value="Hoarding">Hoarding</option>
										<option value="Paper_Insertion">Paper Insertion</option>
										<option value="Caravan">Caravan</option>
										<option value="Bill_Board">Bill Board</option>
										<option value="Website">Website</option>
										<option value="Staff">Site Staff</option>
										<option value="Employee_Referal"> Employee Referal</option>
										<option value="Socialmedia">Social Media</option>
										<option value="Lifestyle_executive">Lifestyle Executive</option>
										<option value="SMS">SMS</option>
										<option value="email">email Invitation</option>
										<option value="Telemarketing">Telemarketing</option>
                    <option value="Others">Others</option>
									</datalist>
									<div class="form-group scmedia" style="display: none;" >
										<label class="control-label col-sm-10" for="category">Please input if others:</label>
										<div class="col-sm-6" >          
											<input type="text" class="form-control" id="category" placeholder="" name="scmedia">
										</div>
									</div>
									<div class="form-group others" style="display: none;">
										<label class="control-label col-sm-2" for="category">Others:</label>
										<div class="col-sm-8" style="margin: 2px">          
											<input type="text" class="form-control" id="cd" placeholder="Enter Others" name="abc">
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>	

					<div class="row " style="margin-top:20px">
						<div class="col-md-12 row">

							<div class="col-md-4" style=" margin:0px;" >
								<label for="name" style="padding-right:0px">Interested in Project Type:</label>
							</div>

							<div class="col-md-8" style="padding:0px; margin:0px;" >
								<input list="InterestedProject" class="form-control" value="<?php echo $customerinfo[0]['interested_project_type'] ?>" id="lead_forword" name="interested_project_type">
								<datalist id="InterestedProject">
									<option>--Select--</option>
									<option value="Residential">Residential</option>
									<option value="Commercial">Commercial</option>
									<option value="Shop">Shop</option>
									<option value="Plot">Plot</option>
									<option value="Land">Land</option>
								</datalist>
							</div>

						</div>
					</div>
					<div class="row " style="margin-top:20px">
						<div class="col-md-12 row">

							<div class="col-md-4" style=" margin:0px;" >
								<label for="name" style="padding-right:0px">Purpose of Call:</label>
							</div>

							<div class="col-md-8" style="padding:0px; margin:0px;" >
								<input list="Purpose" class="form-control" value="<?php echo $customerinfo[0]['purpose'] ?>" id="" name="purpose">
								<datalist id="Purpose">
									<option>--Select--</option>
									<option value="Rental">Rental</option>
									<option value="Re_Sale">Re-Sale</option>
									<option value="New_Apt">New Apt.</option>
									<option value="New_Commercial">New Commercial</option>
									<option value="New_Shop">New Shop</option>
									<option value="New_Plot">New Plot</option>
									<option value="New_Land">New Land</option>
									<option value="DHK_General_Service">DHK General Service</option>
									<option value="CTG_General_Service">CTG General Service</option>
									
							</datalist>
							</div>

						</div>
					</div>

					<div class="row " style="margin-top:20px">
						<div class="col-md-12 row">

							<div class="col-md-3" style=" margin:0px;" >
								<label for="name" style="padding-right:0px">Lead forwarded to:</label>
							</div>

							<div class="col-md-9" style="padding:0px; margin:0px;" >
								<input list="LeadForward" class="form-control" value="<?php echo $customerinfo[0]['lead_forword'] ?>" id="lead_forword" name="lead_forword">
								<datalist id="LeadForward">
									<option>--Select--</option>
									<option value="Dhaka">Dhaka Sales Team - Sales Concern Name</option>
									<option value="Chattogram">Chattogram Sales Team - - Sales Concern Name</option>
								</datalist>
							</div>

						</div>
					</div>
					

					<div class="row " style="margin-top:20px"> 
						<div class="col-md-12 row">

							<div class="col-md-3" style=" margin:0px;" >
								<label for="name" style="padding-right:0px">Interested project</label>
							</div>

							<div class="col-md-9" style="padding:0px; margin:0px;" >
								<input list="IntProject" class="form-control"id="interested" value="<?php echo $customerinfo[0]['interested_project'] ?>" name="interested_project">
								<datalist id="IntProject">
									<option>--Select--</option>
									<option value="GreenPark">Green Park</option>
									<option value="OrchardPreen">Orchard Garden</option>
									<option value="EdaniaAyub">Edania Ayub</option>
									<option value="others">Others</option>
							</datalist>
							</div>
							<div class="form-group interested_pr" style="display: none;" >
										<label class="control-label col-sm-10" for="category">Please input if others:</label>
										<div class="col-sm-6" >          
											<input type="text" class="form-control" id="category" placeholder="" name="others_interested">
										</div>
									</div>

						</div>
					</div>


					<div class="row " style="margin-top:20px">
						<div class="col-md-12 row">

							<div class="col-md-1" style=" margin:0px;" >
								<label for="name" style="padding-right:0px">Notes:</label>
							</div>

							<div class="col-md-11" style="padding-left:5px; padding-right:9px; margin:0px;" >
								<textarea name="note" value="" style="margin-left:10px; width: 700px; height: 100px;"><?php echo $customerinfo[0]['note'] ?></textarea>

							</div>

						</div>
					</div>

					<div class=" row" style="margin-top:20px">
						<div class="col-md-12 row">

							<div class="col-md-6 row" style="padding:0px; margin:0px;">

								<div class="col-md-4">
									<label for="location">Date of Birth:</label>
								</div>
								<div class="col-md-8">
							<!-- <input type="date" id="dt" name="birthdate" onchange="mydate1();" hidden/>
								<input type="text" id="ndt" name="birthdate" onclick="mydate();" hidden/> -->
								<input type="text" autocomplete="off" class="form-control input-sm" name="birthdate" id="date" Value="<?php echo $customerinfo[0]['birthdate'] ?>"/>
							</div>

						</div>

						<div class="row col-md-6" style="padding:0px; margin:0px;" >
							<div  class="col-md-4" style="padding-left:20px; margin:0px;" >
								<label  for="source" >Occupation:</label>
							</div>
							<div class=" col-md-8" style="padding:0px; margin:0px;">
								<input list="Occupation" class=" form-control" value="<?php echo $customerinfo[0]['occupation'] ?>" id="occupation" name="occupation" >
									<datalist id="Occupation">
									<option value="Business">Business</option>
									<option value="Service_Holders">Service Holder</option>
									<option value="Others">Others</option>
                  </datalist>
								<div class="form-group occ" style="display: none;" >
										<label class="control-label col-sm-10" for="category">Please input if others:</label>
										<div class="col-sm-10" >          
											<input type="text"  class="form-control" id="category" placeholder="" name="occ_others">
										</div>
									</div>
							</div>

						</div>
						
					</div>
				</div>		


				<div class="row " style="margin-top:20px">
					<div class="col-md-12 row">

						<div class="col-md-3" style=" margin:0px;" >
							<label for="name" style="padding-right:0px">Present Address:</label>
						</div>

						<div class="col-md-9" style="padding-left:5px; padding-right:9px; margin:0px;" >
							<input class="form-control" value="<?php echo $customerinfo[0]['address'] ?>" placeholder="Address" name="address" type="text" style="margin-left:10px" required>
						</div>

					</div>
				</div>

				<div class="row " style="margin-top:20px">
					<div class="col-md-12 row">

						<div class="col-md-2" style=" margin:0px;" >
							<label for="name" style="padding-right:0px">Religion :</label>
						</div>

						<div class="col-md-10" style="padding:0px; margin:0px;" >
							<input list="Religion" class="form-control"id="location" name="religion" value="<?php echo $customerinfo[0]['religion'] ?>">
							<datalist id="Religion">
								<option>--Select--</option>
								<option value="Islam">Islam</option>
								<option value="Hindu">Hindu</option>
								<option value="Christian">Christian</option>
								<!-- <option value="Others">Others</option> -->
							</datalist>
						</div>

					</div>
				</div>

				<div class="col-md-2" style=" margin-top:7%; margin-bottom: 7%;
				padding: 0px;left:46%; right:46%;">
				<input class="btn btn-primary" onclick="show_alert();" type="submit" value="Submit">
			</div>

		</div>
	</form>
	
</div>

<script>
	function myFunction() {
		var x = document.getElementById("myTopnav");
		if (x.className === "topnav") {
			x.className += " responsive";
		} else {
			x.className = "topnav";
		}
	}
</script>

</body>
<script type="text/javascript">
	
	function show_alert() {
		alert("Update Customer Successfully!");
	}


</script>
<script type="text/javascript">
	$('#type').on('change',function(){
		type = $(this).val();
		if(type == 'Existing'){
			$('.abcd').show();
		}else{
			$('.abcd').hide();
		}
	});
	$('#neighborhood').on('change',function(){
		neighborhood = $(this).val();
		if(neighborhood == 'ctg'){
			$('.neighborhood').show();
			$('.neighborhood_dhaka').hide();
			$('.rest').hide();
		}if (neighborhood=='dhk') {
				$('.neighborhood_dhaka').show();
				$('.neighborhood').hide();
				$('.rest').hide();
		}if (neighborhood=='rest') {
				$('.rest').show();
				$('.neighborhood_dhaka').hide();
				$('.neighborhood').hide();
			
		}
		
	});


</script>
<script type="text/javascript">
	


</script>
<script type="text/javascript">
	$('#occupation').on('change',function(){
		//alert("gshagfdga");
		occupation = $(this).val();
		if(occupation == 'Others'){
			$('.occ').show();
		}else{
			$('.occ').hide();
		}
	});
	$('#source').on('change',function(){
		source = $(this).val();
		if(source == 'Others'){
			$('.others').show();
		}else{
			$('.others').hide();
		}
	});


</script>
<script type="text/javascript">
	
	$('#location1').on('change',function(){
		location1 = $(this).val();
		if(location1 == 'otherprd'){
			$('.prd').show();
       //alert('dhfjsf');

   }else{
      //$('.socialmedia').hide();
      $('.prd').hide();
    /*    $('.Invitation').hide();
        $('.Telemarketing').hide();
        $('.Hoarding').hide();
        $('.Others').hide();*/
    }
});
	$('#source').on('change',function(){
		source = $(this).val();
		if(source == 'Socialmedia'){
			$('.scmedia').show();
       //alert('dhfjsf');

   }else{
      //$('.socialmedia').hide();
      $('.scmedia').hide();
    /*    $('.Invitation').hide();
        $('.Telemarketing').hide();
        $('.Hoarding').hide();
        $('.Others').hide();*/
    }
});
	$('#interested').on('change',function(){
		interested_project = $(this).val();
		if(interested_project == 'others'){
			$('.interested_pr').show();
      // alert('dhfjsf');

   }else{
      //$('.socialmedia').hide();
      $('.interested_pr').hide();
 
    }
});
</script>
<script type="text/javascript">
	
	function mydate() {
  //alert("");
  //document.getElementById("dt").hidden = false;
  //document.getElementById("ndt").hidden = true;

}

function mydate1() {
	d = new Date(document.getElementById("dt").value);
	dt = d.getDate();
	mn = d.getMonth();
	mn++;
	yy = d.getFullYear();
	document.getElementById("ndt").value = dt + "/" + mn + "/" + yy
	document.getElementById("ndt").hidden = false;
	document.getElementById("dt").hidden = true;
}

</script>
</html>



