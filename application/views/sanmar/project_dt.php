<?php  $this->load->view('common/newheader.php'); ?>


<body  style="background-image: linear-gradient(to bottom right, #783d3d85, #fffcdf) ; 
		min-height: 100%;
   
   background-size: cover;
   background-repeat: no-repeat;
   background-position: center center;">
   
<div class="container col-md-8" style=" margin-top:7%; margin-bottom: 7%;
		background-color: white; padding: 40px;left:16%; right:16%;">
<div class="row">

    <div class="col-lg-12">
        <h4 > Project /
            <small>View Details</small>
        </h4>
<?php // print_r($project_info); ?>

    </div>
</div>
<table class="table table-bordered table-hover" border="2">
    <tbody>
  
    <tr>
        <td style="font-style: oblique;"> Project Name</td>
        <td><?php echo $project_info['project_name']; ?>
    </tr>
    <tr>
        <td style="font-style: oblique;">Project Type</td>
        <td><?php echo $project_info['project_type']; ?></td>
    </tr><tr>
        <td style="font-style: oblique;">Storied</td>
        <td><?php echo $project_info['storied']; ?></td>
    </tr><tr>
        <td style="font-style: oblique;">Project Facing</td>
        <td><?php echo $project_info['project_facing']; ?></td>
    </tr><tr>
        <td style="font-style: oblique;">Total Unit</td>
        <td><?php echo $project_info['total_units']; ?></td>
    </tr><tr>
        <td style="font-style: oblique;">Total Salesable Unit </td>
        <td><?php echo $project_info['total_saleable_unit']; ?></td>
    </tr><tr>
        <td style="font-style: oblique;">Apartment</td>
        <td><?php echo $project_info['apartment']; ?></td>
    </tr><tr>
        <td style="font-style: oblique;">Sold Unit </td>
        <td><?php echo $project_info['sold_unit']; ?></td>
    </tr><tr>
        <td style="font-style: oblique;">Unsold</td>
        <td><?php echo $project_info['unsold']; ?></td>
    </tr><tr>
        <td style="font-style: oblique;">Handover Date</td>
        <td><?php echo $project_info['handover_date']; ?></td>
    </tr><tr>
        <td style="font-style: oblique;">Total Land Area</td>
        <td><?php echo $project_info['total_land_area']; ?></td>
    </tr><tr>
        <td style="font-style: oblique;">Project USP</td>
        <td><?php echo $project_info['project_usp']; ?></td>
    </tr><tr>
        <td style="font-style: oblique;">Landmark</td>
        <td><?php echo $project_info['landmark']; ?></td>
    </tr>
    <tr>
        <td style="font-style: oblique;">Parking Per Unit</td>
        <td><?php echo $project_info['parking_per_unit']; ?></td>
    </tr>
      <tr>
        <td style="font-style: oblique;">Parking Space</td>
        <td><?php echo $project_info['parking_space']; ?></td>
    </tr> 
    <tr>
        <td style="font-style: oblique;">Image</td>
      
       <td style="alignment-baseline:center; "> <img src="<?= base_url('uploads/images/'.$project_info['image'])?>" class="img img-responsive" width="350px" height="300px" target="_blank"></td>
    </tr>

  </tbody>
</table>

<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">


    /*$(document).ready(function() {
        $('#create_user_form').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

                            name: {
                                validators: {
                                    notEmpty: {
                                                    message: 'name is empty...'
                                            }
                                }
                            },
                            email: {
                                validators: {
                                    email:  {
                                                    message: 'provide valid email'
                                            }
                                }
                            }
            }
        });

    });*/
</script>