<?php $this->load->view('common/header.php'); 



?>
<style>
    body {
        font-size: 12px;
		/*margin-left:30px;*/
    }
	/*.modal-backdrop.fade.in {
    z-index: 0;*/
	
	</style>
	
	<script>
$(function() {
    $("#start_date").datepicker({dateFormat: "yy-mm-dd"});
	$("#end_date").datepicker({dateFormat: "yy-mm-dd"});
});
    
</script>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Ticket Details /
            <small>List</small>
        </h1>
    </div>
</div>

<form role="form" method='post' id="create_user_form" action="<?php echo site_url('contact/search_ticket');?>">
	

   <div class="row">
       <!-- <div class="col-md-1" style="width:130px">
            <select class="form-control input-sm" name="icx" id="icx" >
                <option value=''>ICX</option>
                <option value='mnh'>MNH</option>
                <option value='getco'>Getco</option>
                <option value='crossworld'>Crossworld</option>
                <option value='sheba'>Sheba</option>
                <option value='others'>Others</option>

            </select>
        </div>-->
	<!-- 	 <div class="col-md-2">
						<?php
            				$collection=$this->prime_model->getByQuery("select * from assigned_to order by name");
						?>
						<select  class="form-control input-sm"  name="received_by" id="received_by" >
							<option value='' >Received By</option>
							<?php	
							foreach($collection as $item){
								$selected='';
								if(isset($params['received_by']) && (trim($item['name'])==trim($params['received_by'])))
									$selected='selected';
								$text=$item['name'];//str_replace('_',' ',$item['name']);
								echo "<option value='$text' $selected>$text</option>";
							}
							?>
						</select>	
        </div> -->
				<!--  <div class="col-md-2">
						<?php
            				$collection=$this->prime_model->getByQuery("select * from assigned_to order by name");
						?>
						<select  class="form-control input-sm"  name="solved_by" id="solved_by" >
							<option value='' >Solved By</option>
							<?php	
							foreach($collection as $item){
								$selected='';
								if(isset($params['solved_by']) && (trim($item['name'])==trim($params['solved_by'])))
									$selected='selected';
								$text=$item['name'];//str_replace('_',' ',$item['name']);
								echo "<option value='$text' $selected>$text</option>";
							}
							?>
						</select>	
        </div> -->
  
     <!--   <div class="col-md-1" style="width:130px">
            <select class="form-control input-sm" name="call_type" id="call_type" >
                <option value=''>Call Type</option>
                <option value='incoming'>Incoming</option>
                <option value='outgoing'>Outgoing</option>
            </select>
        </div>-->
		   
  
       
   

        <div class="col-md-2">
            <input type="text" autocomplete="off" class="form-control input-sm" id="start_date" placeholder="Start Date" name="start_date" >

        </div>

        <div class="col-md-2">
            <input type="text" autocomplete="off" class="form-control input-sm" id="end_date" placeholder="End Date" name="end_date" >

        </div>
        <div class="col-md-2">
            <button type="submit" id="save_customer_details"  name="save_customer_details" class="btn btn-warning glyphicon glyphicon-search"></button>
        </div>
    </div>
	

 
</form>
<div class="row">
    <div class="col-sm-3 col-md-3 col-lg-3">
	<?php
	//if(isset($it_user)&&$it_user==true){
		?>
   
  <!--<a STYLE="max-width:160px; max-height:70px;" id="downloadCSV" class="btn btn-success glyphicon glyphicon-download-alt" href="<?php echo site_url('/contact/download_contact_chart'); ?>"> Download CSV</a>-->
<?php 
  // }
   ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
		<?php if(isset($search_info)) echo "<h3>$search_info</h3>"?>
		<div > <h3 id="search_info"></h3>
		
<br>


		
		</div>
        <div class="table-responsive">
            <!--<input type="checkbox" class="selectAll" /> <span role='button' data-toggle='tooltip' data-placement='top' title='Delete selected' class='glyphicon glyphicon-trash linksDeleteSelected ' controllerMethod="<?php echo site_url('group_manager/deleteSelected') ?>" controllerReloadMethod="<?php echo site_url('group_manager/group_list') ?>" confirmationMsg='are you sure to delete selected group(s)' aria-hidden='true' onclick=''></span>-->

            <table class="table table-bordered table-hover table-striped" id="my_datatable">
                <thead>
                <tr>
                    <!--<th style="width:10%;"></th>-->
                    <th>Sl</th>
                    <th>Ticket Number</th>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>PF Number</th>
					<th>Telephone</th>
					<th>Ticket Status</th>
					<th>Problem Type</th>

					<th>Remarks</th>
					<th>Received By</th>
					<th>Assigned To</th>
					<th>Solved By</th>
					<th>Created Time</th>
					<th>Updated Time</th>
                    <th class="col-md-2">Action</th>

                </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>
	<div class="row">
    <div id="result" class="col-sm-3 col-md-3 col-lg-3">
	
   
  <a STYLE="max-width:160px; max-height:70px;" id="downloadCSV" class="btn btn-success glyphicon glyphicon-download-alt" href="<?php echo site_url('/contact/download_cdr_chart'); ?>"> Download CSV</a>

    </div>
</div>
	</div>
<!--<script src='<?php echo base_url("assets/js/$paging_js_name?v3");?>'></script>-->
<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">




		<?php 
	if(isset($search_contact)&&$search_contact==true){
	?>
		$(document).ready(function() {
			$("#result").hide();
        
			var primary_number="<?php echo $primary_number;?>";
			$.ajax({ 		
				type: "POST" 
				,url: "<?php echo site_url('contact/search_ticket');?>" 
				,data:{primary_number:'<?php echo $primary_number;?>'}
				,async: false
				,success: function(response) { // on success..
					var obj=JSON.parse(response);
					if(obj.record_count==0){
					    //$('#search_info').html("Number "+primary_number+" is not in your Sales list. <a role='button' class='btn btn-info' href='<?php echo site_url("sale_details/create_sale/$primary_number");?>'> Add to Sale</a>");
					}
					else {
						$('#search_info').html("Search result of primary number: "+primary_number);
					}
					var query_id=obj.query_id;
					console.log(response);
					$('#my_datatable').dataTable({
						destroy: true, //use this to reinitiate the table, other wise problem will occur
						processing: true,
						serverSide: true,
						ajax: {
							url: "<?php echo site_url('contact/process_paging_ticket');?>"
							,type: 'POST'
							,data:{query_id: query_id}
						}
					});
				}
			});
		} );
	
	<?	
	}
	else{
	?>
$(document).ready(function() {
	        $("#result").hide();
			$('#my_datatable').dataTable({
				destroy: true, //use this to reinitiate the table, other wise problem will occur
				processing: true,
				serverSide: true,
				ajax: {
					url: "<?php echo site_url('contact/process_paging_ticket');?>"
					,type: 'POST'
					,data:{query_id: '0'}
				}
			});
		} );
		
 <?
	}
 ?>		
		

	$( "form" ).submit(function( e ) {
		e.preventDefault();
		e.stopImmediatePropagation();
		$("#result").show();		
		//alert('form submitted');
		$.ajax({ // create an AJAX call...
			data: $(this).serialize(), // get the form data
			type: $(this).attr('method'), // GET or POST
			url: $(this).attr('action'), // the file to call
			success: function(response) { // on success..
				var obj=JSON.parse(response);
				var query_id=obj.query_id;
				console.log(response);
				$('#my_datatable').dataTable({
					destroy: true, //use this to reinitiate the table, other wise problem will occur
					processing: true,
					serverSide: true,
					ajax: {
						url: "<?php echo site_url('contact/process_paging_ticket');?>"
						,type: 'POST'
						,data:{query_id: query_id}
					}
				});
				
			}
		});
		return false;
	});
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
        e.preventDefault();
		var delete_url= $(this).attr('href');
		

        bootbox.confirm("Are you sure you want to delete this entry?", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
                            $(this_holder).closest('td').closest('tr').hide(1000);
                        } else {

                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            }
        });
    });
	
		
    $(document).on('click', '.linksDeleteSelected', deleteSelected);
    $(document).on('change', '.selectAll', function () {
        if ($(this).is(":checked")) {
            $('.resultRow').prop('checked', true);
        }
        else
            $('.resultRow').prop('checked', false);
    });

    function deleteSelected(e) {
        if ($('.resultRow:checked').size() < 1) {
            alert('please select at least one record');
            return false;
        }
        var r = confirm(e.target.getAttribute("confirmationMsg"));
        if (r == false)
            return false;
        var controllerMethod = e.target.getAttribute("controllerMethod");
        var url = controllerMethod;
        /*var obj = {
            "flammable": "inflammable",
            "duh": "no duh"
        };
        $.each( obj, function( key, value ) {
            alert( key + ": " + value );
        });*/

        var selectedIDs = [];
        $('.resultRow:checked').each(function () {
            selectedIDs.push($(this).val());
        });
        console.log(selectedIDs);
        $.post(
            url,
            {selectedIDs: selectedIDs.join(", ")},
            function (data) {
                /*var obj = $.parseJSON(data);
                if(obj.success){
                    showStatusModal('Success','modal-info',obj.content);
                    reloadResult(e.target.getAttribute("controllerReloadMethod"));
                }
                else showStatusModal('Error','modal-warning',obj.content);*/
                window.location.replace(e.target.getAttribute('controllerReloadMethod'));
            }
        );
        return false;
    }

</script>