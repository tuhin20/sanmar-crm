<?php   $this->load->view('common/newheader.php');  
?>

<script>
	$( function() {
		$( "#start_date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});
		$( "#end_date" ).datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: "-100:+10",
			dateFormat: "dd-mm-yy"
		});
	} );
</script>
<body style="background-image: linear-gradient(to bottom right, #783d3d85, #fffcdf) ; 
min-height: 100%;

background-size: cover;
background-repeat: no-repeat;
background-position: center center;">
<!-- 	<div class="col-md-11" style=" margin-top:3%; margin-bottom: 3%;
	background-color: white; padding: 20px; left:4%; right:4%;"> -->
	<div class="container col-md-8" style=" margin-top:7%; margin-bottom: 7%;
	background-color: white; padding: 40px;left:16%; right:16%;">
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header"> LifeStyle Ticket Details /
				<small>List</small>
			</h1>
		</div>
	</div>


	<div class="row">
		<div class="col-md-12">
			<?php if(isset($search_info)) echo "<h3>$search_info</h3>"?>
			<div > <h3 id="search_info"></h3>
				<form class="form-inline" method="post" action="<?php echo site_url('contact/search_by_sanmar_ticket');?>">
					<!-- <div class="form-group">
						<label for="email">Project Name</label>
						<?php 

						$names=$this->prime_model->getByQuery('select distinct project_name from project');
						?>


						<select class="form-control input-sm" type="text" name="project_name" id="project_name">
							<option value='' >--</option>
							<?php
							foreach($names as $item)
							{
								$selected='';
								if(isset($params['project']) && $item['project_name']==$params['project'])
									$selected='selected';
								$text=str_replace('_',' ',$item['project_name']);
								$value=$item['project_name'];
								echo "<option value='$value' $selected>$text</option>";
							}

							?>
						</select>
					</div> -->
					<br><br/>

					<div class="form-group">
						<label for="email">Status</label>
						<?php

						$names=$this->prime_model->getByQuery('select distinct status from ticket');
						//print_r($names);
						?>


					<select class="form-control input-sm" type="text" name="status" id="status">
							<option value='' >-Select Status-</option>
							<?php
							foreach($names as $item)
							{
								$selected='';
								if(isset($params['ticket']) && $item['status']==$params['ticket'])
									$selected='selected';
								$text=str_replace('_',' ',$item['status']);
								$value=$item['status'];
								echo "<option value='$value' $selected>$text</option>";
							}

							?>
						</select>
					</div>
					<br><br/>
					<div class="form-group">
						<label for="email">Start Date</label>
						
						<input type="text" autocomplete="off" class="form-control input-sm" id="start_date" placeholder="Select Start Date" name="start_date">
					</div>
					<div class="form-group">
						<label for="email">End Date</label>
						
						<input type="text" autocomplete="off" class="form-control input-sm" id="end_date" placeholder="Select End Date" name="end_date">
					</div> 
					<button type="submit" class="btn btn-sm btn-success">Search</button>
				</form>
				<br>


				
			</div>
			<div class="table-responsive">
				<!--<input type="checkbox" class="selectAll" /> <span role='button' data-toggle='tooltip' data-placement='top' title='Delete selected' class='glyphicon glyphicon-trash linksDeleteSelected ' controllerMethod="<?php echo site_url('group_manager/deleteSelected') ?>" controllerReloadMethod="<?php echo site_url('group_manager/group_list') ?>" confirmationMsg='are you sure to delete selected group(s)' aria-hidden='true' onclick=''></span>-->

				<table class="table table-bordered table-hover table-striped" id="my_datatable">
					<thead>
						<tr>
							<!--<th style="width:10%;"></th>-->
							<th>Sl</th>
							<th>Customer Name</th>
							<th>Project Name</th>
							<th>Dept.</th>
							<th>Created By</th>
							<th>Status</th>
							<!-- <th>Shop No</th>
							<th>Level No</th>
							<th>Car Park</th>
							<th>Birth Date</th>
							<th>Occupation</th>
							<th>Purpose Call</th>
							<th>Deadline</th>
							<th>Permanent Address</th>
							<th>Present Address</th>
							<th>Category</th>
							<th>Details</th>
							<th>Respective Department</th>
							<th>Query Details</th>
							<th>Required Time</th>
							<th>Service Duration</th> -->
							
							<th class="col-md-3" align="center">Action</th>

						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

</body>
<!--<script src='<?php //echo base_url("assets/js/$paging_js_name?v3");?>'></script>-->
<?php //$this->load->view('common/footer.php'); ?>

<script type="text/javascript">
	<?php 
	if(isset($search_contact)&&$search_contact==true){
		?>
		$(document).ready(function() {
			var primary_number="<?php echo $primary_number;?>";
			$.ajax({        
				type: "POST" 
				,url: "<?php echo site_url('contact/search');?>" 
				,data:{primary_number:'<?php echo $primary_number;?>'}
				,async: false
                ,success: function(response) { // on success..
                	var obj=JSON.parse(response);
                	if(obj.record_count==0){
                		$('#search_info').html("Number "+primary_number+" is not in your contact list. <a role='button' class='btn btn-info' href='<?php echo site_url("contact/create/$primary_number");?>'> Add to Contact</a>");
                	}
                	else {
                		$('#search_info').html("Search result of primary number: "+primary_number);
                	}
                	var query_id=obj.query_id;
                	console.log(response);
                	$('#my_datatable').dataTable({
                        destroy: true, //use this to reinitiate the table, other wise problem will occur
                        processing: true,
                        serverSide: true,
                        ajax: {
                        	url: "<?php echo site_url('contact/process_paging_lifestyle');?>"
                        	,type: 'POST'
                        	,data:{query_id: query_id}
                        }
                    });
                }
            });
		} );

		<?  
	}
	else{
		?>
		$(document).ready(function() {
			$('#my_datatable').dataTable({
                destroy: true, //use this to reinitiate the table, other wise problem will occur
                processing: true,
                serverSide: true,
                ajax: {
                	url: "<?php echo site_url('contact/process_paging_lifestyle');?>"
                	,type: 'POST'
                	,data:{query_id: '0'}
                }
            });
		} );

		<?
	}
	?>
	var table = $('#my_datatable').DataTable({}) ;

	table.on('page.dt', function() {
		$('html, body').animate({
			scrollTop: $(".dataTables_wrapper").offset().top
		}, 'slow');
	});
	$( "form" ).submit(function( e ) {
		e.preventDefault();
		e.stopImmediatePropagation(); 
        //alert('form submitted');
        $.ajax({ // create an AJAX call...
            data: $(this).serialize(), // get the form data
            type: $(this).attr('method'), // GET or POST
            url: $(this).attr('action'), // the file to call
            success: function(response) { // on success..
            	var obj=JSON.parse(response);
            	var query_id=obj.query_id;
            	console.log(response);
            	$('#my_datatable').dataTable({
                    destroy: true, //use this to reinitiate the table, other wise problem will occur
                    processing: true,
                    serverSide: true,
                    ajax: {
                    	url: "<?php echo site_url('contact/process_paging_lifestyle');?>"
                    	,type: 'POST'
                    	,data:{query_id: query_id}
                    }
                });

            }
        });
        return false;
    });
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
        e.preventDefault();
		var delete_url= $(this).attr('href');
		

        bootbox.confirm("Are you sure you want to delete this entry?", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
                            $(this_holder).closest('td').closest('tr').hide(1000);
                        } else {

                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            }
        });
    });


</script>