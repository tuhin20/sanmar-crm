<?php  $this->load->view ('common/page_header.php');	?>	
<?php 
	$is_user_admin=$this->user_manager_model->is_user_admin();
	$user_has_permission=$this->user_manager_model->has_permission_for_role($this->role_manager_model->Deactive_clients);
	$current_user=$this->user_manager_model->get_current_user();
	
	
 //print_r($user_list->result())
 ?>
	<div class="row">
    	<div class="col-lg-12">
    		<h1 class="page-header"> Client Manager / <small>client List</small> </h1>
    	</div>
	</div>
<div class="row">
    <div class="col-sm-3 col-md-3 col-lg-3">
                <!--<a class="downloadCSV" STYLE="max-width:160px; max-height:25px;" id="downloadCSV" href="<?php echo site_url('/group_manager/downloadCSV');?>">Download csv</a>-->
				<a id="downloadExcel" href="<?php echo site_url('client_manager/download_client_excel');?>"  title="Download  Excel"></a>
            
    </div>
</div>
<div class="row">
            <div class="col-md-12">

            <div class="table-responsive">
                <!--<input type="checkbox" class="selectAll" /> <span role='button' data-toggle='tooltip' data-placement='top' title='Delete selected' class='glyphicon glyphicon-trash linksDeleteSelected ' controllerMethod="<?php echo site_url('group_manager/deleteSelected')?>" controllerReloadMethod="<?php echo site_url('group_manager/group_list')?>" confirmationMsg='are you sure to delete selected group(s)' aria-hidden='true' onclick=''></span>-->
            <table class="table table-bordered table-hover table-striped" id="user_list">
                <thead>
                    <tr>
                        <!--<th style="width:10%;"></th>-->
                        <th  class="col-md-1">Sl</th>
						<th >New Code</th>
                        <th >Name</th>
                        <!--<th >Type</th>-->
                        <!--<th >Address</th>-->
                        <th >Contact Name</th>
                        <th >Contacts</th>
                        <th >Local Call Rate</th>
                        <th >Local Pulse</th>
                        <th >ISD Pulse</th>
                        <th >Status</th>
                        <th class="col-md-2">Action</th>                        
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $i = 1;
                        //echo 'user_id='.$user_id;
                        //print_r($group_list);
                        foreach ($client_list->result() as $row)
                        {
                            $row_class='';
							$client_status='active';
                            if($row->is_active!=1){
                                $row_class='danger';
                                $client_status='deactive';
                            }
                    ?>
                            <tr class="<?php echo $row_class;?>">
                                <!--<td><input type='checkbox' value='<?php echo $row->id;?>' name='selectedIDs[]' class='resultRow' /></td>-->
                                <td><?php echo $i++; ?></td>
								<td><?php echo $row->new_code; ?></td>
                                <td><?php echo $row->name; ?></td>
                                <!--<td><?php echo $row->client_type_name; ?></td>-->
                                <!--<td><?php echo $row->address; ?></td>-->
                                <td><?php echo $row->contact_name; ?></td>
                                <td><?php echo $row->contact_no; ?></td>
                                <td><?php echo $row->call_rate_local; ?></td>
                                <td><?php echo $row->pulse_local; ?></td>
                                <td><?php echo $row->pulse_isd; ?></td>
                                <td><?php echo ($row->is_active?'Active':'Deactive'); ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="<?php echo base_url(); ?>index.php/client_manager/view_details/<?php echo $row->id; ?>" class="btn btn-success glyphicon glyphicon-eye-open" title="View Details"></a>
                                        <?php 
                                            if($this->user_manager_model->has_permission_for_role($this->role_manager_model->Manage_clients)){
                                                  echo '<a href='. site_url().'/client_manager/edit_client/'.$row->id.' class="btn btn-warning glyphicon glyphicon-pencil" title="Edit client"></a>';

												if( $is_user_admin || (user_has_permission && ( $row->created_by==$current_user['id']  )) ){
													 echo '<a client_status="'.$client_status.'" client_name="'.$row->name.'"  href='. site_url().'/client_manager/delete_client/'.$row->id.' class="btn btn-danger glyphicon glyphicon-exclamation-sign delete" title="Change status"></a>';
												}
                                            }
                                        ?>
                                                                            
                                        <!--<a href="<?php echo base_url(); ?>index.php/client_manager/edit_client/<?php echo $row->id; ?>" class="btn btn-warning glyphicon glyphicon-pencil" title="Edit Client"></a>                                    
                                        
                                        <a  href="<?php echo base_url(); ?>index.php/client_manager/delete_client/<?php echo $row->id; ?>" class="btn btn-danger glyphicon glyphicon-trash delete" client_status="<?php echo $client_status;?>" title="Delete Client"></a>-->
                                    </div>
                                </td>
                            </tr>
                    <?php
                        }
                    ?>
                </tbody>
            </table>
            </div>
    </div>
</div>
<?php $this->load->view ('common/page_footer.php');	?>
<style>
    	.bg_red{background-color:#ff0000 !important;}
 	.bg_green{background-color:#00ff00 !important;}
</style>
<script type="text/javascript">	
	jQuery(document.body).on('click', '.delete', function (e) {
		//this.prop('disabled', true);
		var click=0;
		var this_holder = this;
		var delete_url = $(this).attr('href');
		e.preventDefault();
		var client_status=$(this).attr('client_status');
		var client_name=$(this).attr('client_name');
		//alert(client_status);
		var msg='deactivate client: '+client_name;
		if(client_status=='deactive'){
			msg='activate client: '+client_name;   
		}
		var the_clicked_link=$(this);
		bootbox.confirm("Are you sure you want to "+msg+" ?", function (response) { 
			//$(this).prop('disabled', true);
			click++;
			console.log(click);
			if(response && (click<2)) {
					$.ajax({ 
					url: delete_url,
					dataType: 'text', 
					type: 'post', 
					contentType: 'application/x-www-form-urlencoded',  
					async:false,
					beforeSend: function(){
					   //bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Please wait...</div>' });
					},
					success: function( data, textStatus, jQxhr ){ 	
						console.log('in inside: '+click);
						if(data == true){
							//$(this_holder).closest('td').closest('tr').hide(1000);
							if(client_status=='active'){
								the_clicked_link.attr('client_status','deactive');
                                                                $(this_holder).closest('td').closest('tr').removeClass('bg_red bg_green');
                                                        	$(this_holder).closest('td').closest('tr').addClass('bg_red');
								bootbox.alert('Client: '+client_name+ ' deactivated successfully');
							}
							else {   
								 the_clicked_link.attr('client_status','active');
								 $(this_holder).closest('td').closest('tr').removeClass('danger bg_red bg_green');
								 $(this_holder).closest('td').closest('tr').addClass('bg_green');
								 bootbox.alert('Client: '+client_name+' activated successfully');
							}
                                                        
                                                        //location.reload();
						}else{
							bootbox.alert('Unable to change status this entry....');
						}						
					}, 
					error: function( jqXhr, textStatus, errorThrown ){ 
						alert(errorThrown); 
					} 
				});

			}
		});
	});
        $(document).on('click','.linksDeleteSelected',deleteSelected);
        $(document).on('change','.selectAll',function(){
            if($(this).is(":checked")) {
                $('.resultRow').prop('checked', true);
            }
            else
                $('.resultRow').prop('checked', false);
        });
        function deleteSelected(e) {
            if($('.resultRow:checked').size()<1)
            {
                alert('please select at least one record');
                return false;
            }
            var r = confirm(e.target.getAttribute("confirmationMsg"));
            if(r==false)
                return false;
            var controllerMethod = e.target.getAttribute("controllerMethod");
            var url=controllerMethod; 
            /*var obj = {
                "flammable": "inflammable",
                "duh": "no duh"
            };
            $.each( obj, function( key, value ) {
                alert( key + ": " + value );
            });*/

            var selectedIDs = [];
            $('.resultRow:checked').each(function() {
                selectedIDs.push($(this).val());
            });
            console.log(selectedIDs);
            $.post(
                url,
                { selectedIDs: selectedIDs.join(", ")},
                function(data) {
                    /*var obj = $.parseJSON(data);
                    if(obj.success){
                        showStatusModal('Success','modal-info',obj.content);
                        reloadResult(e.target.getAttribute("controllerReloadMethod"));
                    }
                    else showStatusModal('Error','modal-warning',obj.content);*/
                    window.location.replace(e.target.getAttribute('controllerReloadMethod'));
                }
            );
            return false;
        }

</script>
