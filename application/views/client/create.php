<!--input[required], select[required] {
    background-image: url('/img/star.png');
    background-repeat: no-repeat;
    background-position-x: right;
}-->
<?php $this->load->view ('common/page_header.php'); //print_r($params);print_r($group_info);//print_r($return_value);//print_r($avilable_contacts);	
 /*if(isset($params['name']))
     echo 'name='.$params['name'];*/
?>	

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Client Manager / <small><?php echo $ui['title']?> </small> </h1>
    </div>
</div>
<div class="row">
	<form role="form"  method='post' id="create_user_form" action="<?php echo $ui['action']; ?>" >
	<div class=" col-md-12"> 	<!-- this is alert size-->
		<?php 
			$return_value=$this->session->flashdata('return_value');
			if(isset($return_value)) 
			{
				//print_r($return_value);
				$msg=$return_value['msg'];
				if($return_value['success'] == true)
				{

					echo "<div class='alert alert-success' role='alert'>$msg</div>";

				}
				else if($msg!='') //if not success and msg not empty
				{
					echo "<div class='alert alert-danger' role='alert'>$msg</div>";
				}
			}
		?>
	</div>
    <div class="col-md-6"> <!-- This is the main form size-->
        <div class="row">
            
            
            <div class=" col-md-12" > <!-- this is form input fields -->	
            <!--<form role="form"  method='post' id="create_user_form" action="<?php echo $ui['action']; ?>" >-->
            <?php //print_r($params);?>

            <input type="hidden" class="form-control" id="id" name="id" value="<?php echo isset($client_info['id'])?$client_info['id']:-1 ; ?>"  required>

			<div class="form-group required">
                <label class='control-label'>Billing</label>
                <select class="form-control "  name="billing" id="billing" required>
					<option value='' >--</option>
					
					<option value='Prepaid' <?php echo (isset($client_info['billing'])&& $client_info['billing']=='Prepaid') ||(isset($params['billing'])&&$params['billing']=='Prepaid') ?'selected': '' ;?> >Prepaid</option>
					<option value='Postpaid' <?php echo (isset($client_info['billing'])&& $client_info['billing']=='Postpaid') ||(isset($params['billing'])&&$params['billing']=='Postpaid') ?'selected': '' ;?> >Postpaid</option>
				</select>
                <?php echo form_error('billing','<label class="error">','</label>');?>
            </div>	
			
			<div class="form-group required">
                <label class='control-label required'>New Code</label>
                <input class="form-control " type="text" id="new_code" name="new_code" value="<?php echo isset($client_info['new_code'])?$client_info['new_code']: set_value('new_code') ; ?>"  required>
                <?php echo form_error('new_code','<label class="error">','</label>');?>
            </div>
			
            
            
            <div class="form-group required">
                <label class='control-label'>Name</label>
                <input class="form-control" type="text" id="name" name="name" value="<?php echo isset($client_info['name'])?$client_info['name']: set_value('name') ; ?>"  >
                <?php echo form_error('name','<label class="error">','</label>');?>
            </div>
            <div class="form-group required">
                <label class='control-label'>Type</label>
                <?php
                    $this->load->view('controls/ctrl_select_client_type');
                ?>
            </div>
            
			
		
			<div class="form-group required">
                <label class='control-label'>Local Call Rate</label>
                <input type="number" min="0" max="60" step=".0001"  class="form-control" id="call_rate_local" name="call_rate_local" value="<?php echo isset($client_info['call_rate_local'])?$client_info['call_rate_local']: set_value('call_rate_local'); ?>" required>
                <?php echo form_error('call_rate_local','<label class="error">','</label>');?>
            </div>
            <div class="form-group required ">
                <label class='control-label'>Local Pulse</label> <!--<label class="error" style=" padding-left: 12px; "><b>*</b></label>-->
                <input type="number" min="0" max="60" step="1"  class="form-control" id="pulse_local" name="pulse_local" value="<?php echo isset($client_info['pulse_local'])?$client_info['pulse_local']: set_value('pulse_local'); ?>" required>
                <?php echo form_error('pulse_local','<label class="error">','</label>');?>
            </div>
            <div class="form-group required">
                <label class='control-label'>ISD Pulse</label>
                <input type="number" min="0" max="60" step="1"  class="form-control" id="pulse_isd" name="pulse_isd" value="<?php echo isset($client_info['pulse_isd'])?$client_info['pulse_isd']:set_value('pulse_isd'); ?>" required>
                <?php echo form_error('pulse_isd','<label class="error">','</label>');?>
            </div>
            <div class="form-group ">
                <label >Monthly Line Rent</label>
                <input type="number" min="0"  step="1"  class="form-control" id="monthly_line_rent" name="monthly_line_rent" value="<?php echo isset($client_info['monthly_line_rent'])?$client_info['monthly_line_rent']:$params['monthly_line_rent']; ?>" >
                <?php echo form_error('monthly_line_rent','<label class="error">','</label>');?>
            </div>
			
			

            
            <input class="form-control" type="hidden" id="is_active" name="is_active" value="<?php echo isset($client_info['is_active'])?$client_info['is_active']: '1' ; ?>"   >
            <!--short code-->
                <label>Short code(s)</label>
                <?php
                if(isset($short_code_list)){
                    foreach ($short_code_list as $row)
                    {
                    ?>
                        <div class="form-inline" style="margin-top:5px;">
                            <input type="text" class="form-control" name="short_code[]" value="<?php echo $row['short_code']; ?>" placeholder="short code" ><button type="button" class="btn btn-default delete_short_code" style=""><i class="glyphicon glyphicon-minus-sign"></i></button>
                        </div>      
                    <?php
                    }
                }
                ?>
                <?php //when form validation failed
                if(isset($params['short_code'])){
                    foreach ($params['short_code'] as $item){
                        if($item==''){
                            continue;
                        }
                        ?>
                        <div class="form-inline" style="margin-top:5px;">
                            <input type="text" class="form-control" name="short_code[]" value="<?php echo $item; ?>" placeholder="short code" >
                            <button type="button" class="btn btn-default delete_short_code" style=""><i class="glyphicon glyphicon-minus-sign"></i></button>
                        </div>
                <?php
                    }
                }
                ?>
                <div class="form-inline" style="margin-top:5px;">
                    
                    <input type="text" class="form-control" name="short_code[]" placeholder="short code" >
                    <button type="button" class="btn btn-default add_new_short_code" style=""><i class="glyphicon glyphicon-plus-sign"></i></button>
                </div>
                <div id="end_of_short_code"></div>
                <!--end of short code-->
                <!--toll free-->
                <br>
                <label>Toll free number(s)</label>
                <?php
                if(isset($toll_free_number_list)){
                    foreach ($toll_free_number_list as $row)
                    {
                    ?>
                        <div class="form-inline" style="margin-top:5px;">
                            <input type="text" class="form-control" name="toll_free_number[]" value="<?php echo $row['toll_free_number']; ?>" placeholder="Toll free number" ><button type="button" class="btn btn-default delete_toll_free_number" style=""><i class="glyphicon glyphicon-minus-sign"></i></button>
                        </div>      
                    <?php
                    }
                }
                ?>
                <?php //when form validation failed
                if(isset($params['toll_free_number'])){
                    foreach ($params['toll_free_number'] as $item){
                        if($item==''){
                            continue;
                        }
                        ?>
                        <div class="form-inline" style="margin-top:5px;">
                            <input type="text" class="form-control" name="toll_free_number[]" value="<?php echo $item; ?>" placeholder="Toll free number" >
                            <button type="button" class="btn btn-default delete_toll_free_number" style=""><i class="glyphicon glyphicon-minus-sign"></i></button>
                        </div>
                <?php
                    }
                }
                ?>
                <div class="form-inline" style="margin-top:5px;">
                    
                    <input type="text" class="form-control" name="toll_free_number[]" placeholder="Toll free number" >
                    <button type="button" class="btn btn-default add_new_toll_free_number" style=""><i class="glyphicon glyphicon-plus-sign"></i></button>
                </div>
                
                <!--end of toll free-->
               
            <div class="form-group">
                <button type="submit" id="save_client" name="save_client" class="btn btn-success btn-lg" style="margin-top: 20px; width: 150px;"><?php echo $ui['okButton'];?></button>
            </div>
            <!--</form>-->
        </div>  
        </div>
    </div>
    <div class="col-md-6">		
		<div class="form-group required">
			<label class='control-label'>Zone</label>
			<select class="form-control "  name="zone" id="zone" required>
				<option value='' >--</option>
				
				<option value='central' <?php echo (isset($client_info['zone'])&& $client_info['zone']=='central') ||(isset($params['zone'])&&$params['zone']=='central') ?'selected': '' ;?> >Central</option>
				<option value='north-east' <?php echo (isset($client_info['zone'])&& $client_info['zone']=='north-east') ||(isset($params['zone'])&&$params['zone']=='north-east') ?'selected': '' ;?> >North-East</option>
				<option value='north-west' <?php echo (isset($client_info['zone'])&& $client_info['zone']=='north-west') ||(isset($params['zone'])&&$params['zone']=='north-west') ?'selected': '' ;?> >North-West</option>
				<option value='south-east' <?php echo (isset($client_info['zone'])&& $client_info['zone']=='south-east') ||(isset($params['zone'])&&$params['zone']=='south-east') ?'selected': '' ;?> >South-East</option>
				<option value='south-west' <?php echo (isset($client_info['zone'])&& $client_info['zone']=='south-west') ||(isset($params['zone'])&&$params['zone']=='south-west') ?'selected': '' ;?> >South-West</option>
				
			</select>
			<?php echo form_error('zone','<label class="error">','</label>');?>
		</div>
		<div class="form-group required">
			<label class='control-label required'>Billing Row</label>
			<input type="number" min="0"  step="1"  class="form-control" id="billing_row" name="billing_row" value="<?php echo isset($client_info['billing_row'])?$client_info['billing_row']:$params['billing_row']; ?>" >
		
			<?php echo form_error('billing_row','<label class="error">','</label>');?>
		</div>
		
		<div class="form-group ">
			<label class='control-label required'>Total channel allocated</label>
			<input type="number" min="0"  step="1"  class="form-control" id="total_channel" name="total_channel" value="<?php echo isset($client_info['total_channel'])?$client_info['total_channel']:$params['total_channel']; ?>" >
		
			<?php echo form_error('total_channel','<label class="error">','</label>');?>
		</div>
		
		<div class="form-group">
			<label>Contact Name</label>
			<input class="form-control" type="text" id="contact_name" name="contact_name" value="<?php echo isset($client_info['contact_name'])?$client_info['contact_name']: $params['contact_name'] ; ?>"   >
			<?php echo form_error('contact_name','<label class="error">','</label>');?>
		</div>
		<div class="form-group">
			<label>Contact Number</label>
			<input class="form-control" type="text" id="contact_no" name="contact_no" value="<?php echo isset($client_info['contact_no'])?$client_info['contact_no']: $params['contact_no'] ; ?>"   >
			<?php echo form_error('contact_no','<label class="error">','</label>');?>
		</div>
		<div class="form-group">
			<label>E-Mail</label>
			<input class="form-control" type="text" id="mail" name="mail" value="<?php echo isset($client_info['mail'])?$client_info['mail']: $params['mail'] ; ?>"   >
			<?php echo form_error('mail','<label class="error">','</label>');?>
		</div>
		<div class="form-group required">
			<label class='control-label'>Web Name</label>      
			<input class="form-control" type="text" id="web_name" name="web_name" value="<?php echo isset($client_info['web_name'])?$client_info['web_name']: set_value('web_name') ; ?>"   required>
			<input class="form-control" type="text" style="display: none;" id="fake_web_name" name="fake_web_name" value="<?php echo isset($client_info['web_name'])?$client_info['web_name']: set_value('web_name') ; ?>"   >
			<?php echo form_error('web_name','<label class="error">','</label>');?>
		</div>
		<div class="form-group required">
			<label class='control-label'>Password</label>
			<input class="form-control" type="text" style="display: none;" id="fake_password" name="fake_password" value="<?php echo isset($client_info['password'])?$client_info['password']: set_value('password') ; ?>"   >
			<input class="form-control" type="text" id="password" name="password" value="<?php echo isset($client_info['password'])?$client_info['password']: set_value('password') ; ?>"   required>
			<?php echo form_error('password','<label class="error">','</label>');?>
		</div>
		<div class="form-group required">
			<label class='control-label '>OLD Code</label>
			<input class="form-control " type="text" id="code" name="code" value="<?php echo isset($client_info['code'])?$client_info['code']: set_value('code') ; ?>"  readonly>
			<?php //echo form_error('code','<label class="error">','</label>');?>
		</div>
        <div class="form-group">
			<label>Address</label>
			<textarea class="form-control" rows="5" name="address"><?php echo isset($client_info['address'])?$client_info['address']:$params['address']; ?></textarea>
			<?php echo form_error('address','<label class="error">','</label>');?>			
		</div>
		
	</div>
	</form>
</div>
<?php $this->load->view ('common/page_footer.php');	?>
<script type="text/javascript">
/*$(document).ready(function() {
	$('#create_user_form').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			
                        name: {
                            validators: {
                                notEmpty: {
                                                message: 'name is empty...'
                                        }
                            }
                        },
                        email: {
                            validators: {
                                email:  {
                                                message: 'provide valid email'
                                        }
                            }
                        }
		}
	});
        
});*/
    $(document).on('click', '.add_new_short_code', function(){ 
        $('#end_of_short_code').before('<div class="form-inline" style="margin-top:5px;"><input type="text" class="form-control" name="short_code[]" placeholder="short code">&nbsp;<button type="button" class="btn btn-default add_new_short_code" style=""><i class="glyphicon glyphicon-plus-sign"></i></button>');
        $(this).removeClass('add_new_short_code').addClass('delete_short_code').html('<i class="glyphicon glyphicon-minus-sign"></i>');					
        return false;	
    });

    $(document).on('click', '.delete_short_code', function(){ 
        $(this).parent('.form-inline').hide().find('.form-control').attr("disabled", "disabled");					
        return false;	
    });
    
    $(document).on('click', '.add_new_toll_free_number', function(){ 
        $('#save_client').before('<div class="form-inline" style="margin-top:5px;"><input type="text" class="form-control" name="toll_free_number[]" placeholder="Toll free number">&nbsp;<button type="button" class="btn btn-default add_new_toll_free_number" style=""><i class="glyphicon glyphicon-plus-sign"></i></button>');
        $(this).removeClass('add_new_toll_free_number').addClass('delete_toll_free_number').html('<i class="glyphicon glyphicon-minus-sign"></i>');					
        return false;	
    });

    $(document).on('click', '.delete_toll_free_number', function(){ 
        $(this).parent('.form-inline').hide().find('.form-control').attr("disabled", "disabled");					
        return false;	
    });
</script>