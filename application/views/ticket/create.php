<?php 
	$this->load->view('common/header.php'); //print_r($params);print_r($group_info);//print_r($return_value);//print_r($avilable_contacts);	
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Ticket / <small><?php echo $ui['title']?> </small> </h1>
    </div>
</div>
<form role="form" class="form-horizontal"  method='post' id="create_user_form" action="<?php echo $ui['action']; ?>" >
<div class="row">	
		<div class=" col-md-12"> 	<!-- this is alert size-->
			<?php 
				$return_value=$this->session->flashdata('return_value');
				if(isset($return_value)) 
				{
					//print_r($return_value);
					$msg=$return_value['msg'];
					if($return_value['success'] == true){
						echo "<div class='alert alert-success' role='alert'>$msg</div>";
					}
					else if($msg!=''){
						//if not success and msg not empty
						echo "<div class='alert alert-danger' role='alert'>$msg</div>";
					}
				}
			?>
		</div>
		<div class="col-md-6"> <!-- This is the main form size-->
				<input type="hidden" class="form-control" id="id" name="id" value="<?php echo isset($ticket['id'])?$ticket['id']:-1 ; ?>"  required>
				
				<div class="form-group">
					<label class="required control-label col-sm-5">Title</label>
					<div class="col-sm-7">
						<input class="form-control input-sm" type="text" id="title" name="title" value="<?php echo isset($ticket['title'])?$ticket['title']: $params['title'] ; ?>"  required>
						<?php echo form_error('title','<label class="error">','</label>');?>
					</div>
				</div> 
				<div class="form-group required">
					<label class='control-label col-sm-5 required'>Assigned To</label>
					<div class="col-sm-7">
						<?php $this->load->view('control/ctrl_assigned_to');?>
					</div>
				</div>
				<div class="form-group required">
					<label class='control-label col-sm-5 required'>Priority</label>
					<div class="col-sm-7">
						<?php
							$collection=$this->prime_model->getByQuery("select * from ticket_priority order by sequence");
						?>
						<select  class="form-control input-sm"  name="priority" id="priority" >
							<option value='' >--</option>
							<?php	
							foreach($collection as $item){
								$selected='';
								if(isset($params['priority']) && (trim($item['name'])==trim($params['priority'])))
									$selected='selected';
								$text=$item['name'];//str_replace('_',' ',$item['name']);
								echo "<option value='$text' $selected>$text</option>";
							}
							?>
						</select>						
					</div>
				</div>
				<div class="form-group required">
					<label class='control-label col-sm-5 required'>Category</label>
					<div class="col-sm-7">
						<?php
							$collection=$this->prime_model->getByQuery("select * from ticket_category order by sequence");
						?>
						<select  class="form-control input-sm"  name="category" id="category" >
							<option value='' >--</option>
							<?php	
							foreach($collection as $item){
								$selected='';
								if(isset($params['category']) && (trim($item['name'])==trim($params['category'])))
									$selected='selected';
								$text=$item['name'];//str_replace('_',' ',$item['name']);
								echo "<option value='$text' $selected>$text</option>";
							}
							?>
						</select>						
					</div>
				</div>
				<div class="form-group">
					<button type="submit" id="save_ticket" name="save_ticket" class="btn btn-success btn-lg" style="margin-top: 20px; width: 150px;"><?php echo $ui['okButton'];?></button>
				</div>
				<!--</form>-->
			 
		</div>
		<div class="col-md-6">		
			
			<div class="form-group">
				<label class=" control-label col-sm-5 required">Contacts</label>
				<div class="col-sm-7">
					<?php $this->load->view('control/ctrl_contact');?>
				</div>
			</div> 
			<div class="form-group required">
				<label class='control-label col-sm-5 required'>Product Category</label>
				<div class="col-sm-7">
					<?php $this->load->view('control/ctrl_product_category');?>
				</div>
			</div>
			<div class="form-group">
				<label class=" control-label col-sm-5 required">Products</label>
				<div class="col-sm-7">
					<?php $this->load->view('control/ctrl_product');?>
				</div>
			</div>
			<div class="form-group">
				<label class=" control-label col-sm-5 required">Status</label>
				<div class="col-sm-7">
					<?php
						$collection=$this->prime_model->getByQuery("select * from ticket_status order by sequence");
					?>
					<select  class="form-control input-sm"  name="status" id="status" >
						<option value='' >--</option>
						<?php	
						foreach($collection as $item){
							$selected='';
							if(isset($params['status']) && (trim($item['name'])==trim($params['status'])))
								$selected='selected';
							$text=$item['name'];//str_replace('_',' ',$item['name']);
							echo "<option value='$text' $selected>$text</option>";
						}
						?>
					</select>						
				</div>
			</div>
			<div class="form-group">
				<label class="required control-label col-sm-5">Due date</label>
				<div class="col-sm-7">
					<input class="form-control input-sm" type="text" id="due_date" name="due_date" value="<?php echo isset($ticket['due_date'])?$ticket['due_date']: $params['due_date'] ; ?>"  required>
					<?php echo form_error('due_date','<label class="error">','</label>');?>
				</div>
			</div>
			
			
		</div>
	
</div>
</form>
<?php $this->load->view ('common/footer.php');	?>
<script type="text/javascript">
/*$(document).ready(function() {
	$('#create_user_form').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			
                        name: {
                            validators: {
                                notEmpty: {
                                                message: 'name is empty...'
                                        }
                            }
                        },
                        email: {
                            validators: {
                                email:  {
                                                message: 'provide valid email'
                                        }
                            }
                        }
		}
	});
        
});*/
    $(document).on('click', '.add_new_short_code', function(){ 
        $('#end_of_short_code').before('<div class="form-inline" style="margin-top:5px;"><input type="text" class="form-control" name="short_code[]" placeholder="short code">&nbsp;<button type="button" class="btn btn-default add_new_short_code" style=""><i class="glyphicon glyphicon-plus-sign"></i></button>');
        $(this).removeClass('add_new_short_code').addClass('delete_short_code').html('<i class="glyphicon glyphicon-minus-sign"></i>');					
        return false;	
    });

    $(document).on('click', '.delete_short_code', function(){ 
        $(this).parent('.form-inline').hide().find('.form-control').attr("disabled", "disabled");					
        return false;	
    });
    
    $(document).on('click', '.add_new_toll_free_number', function(){ 
        $('#save_ticket').before('<div class="form-inline" style="margin-top:5px;"><input type="text" class="form-control" name="toll_free_number[]" placeholder="Toll free number">&nbsp;<button type="button" class="btn btn-default add_new_toll_free_number" style=""><i class="glyphicon glyphicon-plus-sign"></i></button>');
        $(this).removeClass('add_new_toll_free_number').addClass('delete_toll_free_number').html('<i class="glyphicon glyphicon-minus-sign"></i>');					
        return false;	
    });

    $(document).on('click', '.delete_toll_free_number', function(){ 
        $(this).parent('.form-inline').hide().find('.form-control').attr("disabled", "disabled");					
        return false;	
    });
</script>