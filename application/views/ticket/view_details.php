<?php  $this->load->view ('common/page_header.php');	?>	
<?php //print_r($user_list->result())?>
	<div class="row">
    	<div class="col-lg-12">
    		<h1 class="page-header"> Client Manager / <small>View Details</small> </h1>
			<?php  //phpinfo();?>
    	</div>
	</div>

<div class="row">
            <div class="col-md-12">

            <div class="table-responsive">
                <!--<input type="checkbox" class="selectAll" /> <span role='button' data-toggle='tooltip' data-placement='top' title='Delete selected' class='glyphicon glyphicon-trash linksDeleteSelected ' controllerMethod="<?php echo site_url('group_manager/deleteSelected')?>" controllerReloadMethod="<?php echo site_url('group_manager/group_list')?>" confirmationMsg='are you sure to delete selected group(s)' aria-hidden='true' onclick=''></span>-->

            <table class="table table-bordered table-hover table-striped" >
                
                <tbody>
                    <?php 
                        $i = 1;
                        //echo 'user_id='.$user_id;
                        //print_r($client_info);
                        $th_class="col-md-2";
                        if ( isset($client_info))
                        {
							echo "<tr>";
                            echo "<th class='$th_class'>Billing Type</th>";
                            echo "<td>".$client_info['billing']."</td>";
                            echo "</tr>";
							
							echo "<tr>";
                            echo "<th class='$th_class'>New Code</th>";
                            echo "<td>".$client_info['new_code']."</td>";
                            echo "</tr>";
							
							echo "<tr>";
                            echo "<th class='$th_class'>Billing Row</th>";
                            echo "<td>".$client_info['billing_row']."</td>";
                            echo "</tr>";
							
                            echo "<tr>";
                            echo "<th class='$th_class'>Name</th>";
                            echo "<td>".$client_info['name']."</td>";
                            echo "</tr>";
                            
                            echo "<tr>";
                            echo "<th class='$th_class'>Type</th>";
                            echo "<td>".$client_info['client_type_name']."</td>";
                            echo "</tr>";
                            
							echo "<tr>";
                            echo "<th class='$th_class'>Local Call Rate</th>";
                            echo "<td>".$client_info['call_rate_local']."</td>";
                            echo "</tr>";
                            
                            echo "<tr>";
                            echo "<th class='$th_class'>Local Pulse</th>";
                            echo "<td>".$client_info['pulse_local']."</td>";
                            echo "</tr>";
                            
                            echo "<tr>";
                            echo "<th class='$th_class'>ISD Pulse</th>";
                            echo "<td>".$client_info['pulse_isd']."</td>";
                            echo "</tr>";
							
							echo "<tr>";
                            echo "<th class='$th_class'>Channel Allocated</th>";
                            echo "<td>".$client_info['total_channel']."</td>";
                            echo "</tr>";
							
							echo "<tr>";
                            echo "<th class='$th_class'>Monthly Line Rent</th>";
                            echo "<td>".$client_info['monthly_line_rent']."</td>";
                            echo "</tr>";
							
							echo "<tr>";
                            echo "<th class='$th_class'>Zone</th>";
                            echo "<td>".$client_info['zone']."</td>";
                            echo "</tr>";
							
                           
                            
                            echo "<tr>";
                            echo "<th class='$th_class'>Contact Name</th>";
                            echo "<td>".$client_info['contact_name']."</td>";
                            echo "</tr>";
                            
                            echo "<tr>";
                            echo "<th class='$th_class'>Contact No</th>";
                            echo "<td>".$client_info['contact_no']."</td>";
                            echo "</tr>";
                            
							echo "<tr>";
                            echo "<th class='$th_class'>E-mail</th>";
                            echo "<td>".$client_info['mail']."</td>";
                            echo "</tr>";
							
                            echo "<tr>";
                            echo "<th class='$th_class'>Web Name</th>";
                            echo "<td>".$client_info['web_name']."</td>";
                            echo "</tr>";
                            
                            echo "<tr>";
                            echo "<th class='$th_class'>Password</th>";
                            echo "<td>".$client_info['password']."</td>";
                            echo "</tr>";
                            
							echo "<tr>";
                            echo "<th class='$th_class'>Address</th>";
                            echo "<td>".$client_info['address']."</td>";
                            echo "</tr>";
							
							echo "<tr>";
                            echo "<th class='$th_class'>Old Code</th>";
                            echo "<td>".$client_info['code']."</td>";
                            echo "</tr>";
							
                            echo "<tr>";
                            echo "<th class='$th_class'>Status</th>";
                            echo "<td>".($client_info['is_active']?'Active':'Deactive')."</td>";
                            echo "</tr>";
                            echo "<tr>";
                            echo "<th class='$th_class'>Short Codes</th>";
                            echo "<td>".  implode(',',$short_codes)."</td>";
                            echo "</tr>";
                            echo "<tr>";
                            echo "<th class='$th_class'>Toll free numbers</th>";
                            echo "<td>".  implode(',',$toll_free_numbers)."</td>";
                            echo "</tr>";
                            echo "<tr>";
                            echo "<th class='$th_class'>Created By</th>";
                            echo "<td>".  $client_info['created_by_name']."</td>";
                            echo "</tr>";
                            
                        }
                    ?>
                </tbody>
            </table>
            </div>
    </div>
</div>
<?php $this->load->view ('common/page_footer.php');	?>
