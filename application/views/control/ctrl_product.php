<?php 	
	$product_code='';
	$product_name="";
	if(isset ($ctrl_data['product_code']) && $ctrl_data['product_code']!=''){
		$product_code=$ctrl_data['product_code'];
		$temp=$this->prime_model->getByID('product','code',$product_code);
		$product_name=$temp['name'];
	}
?>
<div class="row">
	<div class="col-md-8">
		<input class="form-control input-sm" type="hidden" id="product_code" name="product_code"  value="<?php echo $product_code ; ?>"  >
		<input class="form-control input-sm" type="text" id="product_name"  value="<?php echo $product_name; ?>"  readonly>
	</div>
	<div class="col-md-2">
		<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#product_modal">&nbsp;+&nbsp;</button>
	</div>
	<div class="col-md-2">
		<button id='erase_product' type="button" class="btn btn-warning btn-sm " aria-label="Left Align">
		  <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
		</button>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="product_modal" tabindex="-1" role="dialog" aria-labelledby="product_modal_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Products</h4>
      </div>
      <div class="modal-body">
			<table class="table table-bordered table-hover table-striped" id="product_datatable">
                <thead>
                <tr>
                    <!--<th style="width:10%;"></th>-->
                    <th>Sl</th>
					<th>Name</th>
                    <th>Category</th>					
                    <th>Product ID</th>
					<th>Unit Price</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
      </div>
      
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
	$('#product_datatable').dataTable({
		destroy: true, //use this to reinitiate the table, other wise problem will occur
		processing: true,
		serverSide: true,
		ajax: {
			url: "<?php echo site_url('product/process_ctrl_paging');?>"
			,type: 'POST'
			,data:{query_id: '0'}
		}
	});
} );
$(document).on('click', '.select_product', function (e) {
	$('#product_code').val(e.target.getAttribute("product_code"));
	$('#product_name').val(e.target.getAttribute("product_name"));
	$('#product_modal').modal('hide');
	return false;
	
} );
$(document).on('click', '#erase_product', function (e) {
	$('#product_code').val('');
	$('#product_name').val('');
} );
</script>