<?php 	
	if(isset ($ctrl_data['checked_radio_button'])){
		if($ctrl_data['checked_radio_button']=='user'){
			$radio_user_checked="checked";
			$radio_team_checked="";			
		}
		else{
			$radio_user_checked="";
			$radio_team_checked="checked";			
		}
	}
	else{
		$radio_user_checked="checked";
	}
?>
<label class="radio-inline">
	&nbsp;&nbsp;&nbsp;<input type="radio" name="assign_type" value="user" <?php echo $radio_user_checked;?> > User
</label>
<label class="radio-inline">
	<input type="radio" name="assign_type" value="team" <?php echo $radio_team_checked;?>> Group
</label>
<?php
//prime_model is required for this control
$customers=$this->prime_model->getByQuery('select * from team order by name');
//print_r($vehicles);
?>
<!--<div id="divClient" class="forReloadSelect" >-->

<select style="display:<?php echo $user_dropdown_visible;?>;" class="form-control input-sm" type="client" name="assigned_to" id="assigned_to" >
    <option value='' >--</option>
    <?php
	$collection=array();
	if ($radio_user_checked=='checked')
		$collection=$this->prime_model->getByQuery('select * from user order by name');
	else $collection=$this->prime_model->getByQuery('select * from team order by name');
	
    foreach($collection as $item)
    {
        $selected='';
        if(isset($params['assigned_to']) && $item['id']==$params['assigned_to'])
            $selected='selected';
        $text=str_replace('_',' ',$item['name']);
        $value=$item['id'];
        echo "<option value='$value' $selected>$text</option>";
    }

    ?>
</select>
<script>
	$('input[type=radio][name=assign_type]').change(function() {
		var radio_value=this.value;
		console.log(radio_value); 
		$('#assigned_to').empty(); //remove all child nodes
		$('#assigned_to').append($('<option value="">--</option>'));
		$.ajax({
		 type: "POST",
		 async: false,
		 url: "<?php echo site_url("control/get_data_assigned_to");?>",
		 data: {assign_type: radio_value},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var data = $.parseJSON(data);
			console.log(data);
			$.each(data, function(i, item) {
				//console.log(item);
				$('#assigned_to').append($('<option value="'+item.id+'">'+item.name+'</option>'));
			});
			$('#assigned_to').trigger("chosen:updated");
			//console.log(obj);
			//$('#div_complainants_thana_permanent').html(obj.content);
			
		
	   });
		
        
        
	});
</script>
