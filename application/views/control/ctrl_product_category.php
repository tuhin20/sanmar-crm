<?php
//prime_model is required for this control
$collection=$this->prime_model->getByQuery("select * from product_category order by name");
//print_r ($ctrl_data);
//echo $ctrl_data['product_category'];
?>
<select  class="form-control input-sm"  name="product_category" id="product_category" >
    <option value='' >--</option>
    <?php	
    foreach($collection as $item){
        $selected='';
        if(isset($ctrl_data['product_category']) && (trim($item['name'])==trim($ctrl_data['product_category'])))
            $selected='selected';
		$text=$item['name'];//str_replace('_',' ',$item['name']);
        echo "<option value='$text' $selected>$text</option>";
    }
    ?>
</select>