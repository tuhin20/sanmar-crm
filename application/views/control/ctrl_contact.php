<?php 	
	$contact_id=-1;
	$contact_name="";
	if(isset ($ctrl_data['contact_id']) && $ctrl_data['contact_id']!=-1){
		$contact_id=$ctrl_data['contact_id'];
		$temp=$this->prime_model->getByID('contact','id',$contact_id);
		$contact_name=$temp['first_name']." ".$temp['middle_name']." ".$temp['last_name'];
	}
?>
<div class="row">
	<div class="col-md-8">
		<input class="form-control input-sm" type="hidden" id="contact_id" name="contact_id"  value="<?php echo $contact_id ; ?>"  >
		<input class="form-control input-sm" type="text" id="contact_name"  value="<?php echo $contact_name; ?>"  readonly>
	</div>
	<div class="col-md-2">
		<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#contact_modal">&nbsp;+&nbsp;</button>
	</div>
	<div class="col-md-2">
		<button id='erase_contact' type="button" class="btn btn-warning btn-sm " aria-label="Left Align">
		  <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
		</button>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="contact_modal" tabindex="-1" role="dialog" aria-labelledby="contact_modal_label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Contacts</h4>
      </div>
      <div class="modal-body">
			<table class="table table-bordered table-hover table-striped" id="contact_datatable">
                <thead>
                <tr>
                    <!--<th style="width:10%;"></th>-->
                    <th>Sl</th>
                    <th>First Name</th>
					<th>Last Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Department</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
      </div>
      
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
	$('#contact_datatable').dataTable({
		destroy: true, //use this to reinitiate the table, other wise problem will occur
		processing: true,
		serverSide: true,
		ajax: {
			url: "<?php echo site_url('contact/process_ctrl_paging');?>"
			,type: 'POST'
			,data:{query_id: '0'}
		}
	});
} );
$(document).on('click', '.select_contact', function (e) {
	$('#contact_id').val(e.target.getAttribute("contact_id"));
	$('#contact_name').val(e.target.getAttribute("contact_name"));
	$('#contact_modal').modal('hide');
	
} );
$(document).on('click', '#erase_contact', function (e) {
	$('#contact_id').val('-1');
	$('#contact_name').val('');
} );
</script>