<?php $this->load->view('common/header.php'); 
$this->load->view('common/navbar.php');


?>
<style>
    body {
        font-size: 12px;
		margin-left:30px;
    }
	
</style>
	<div class="row">
    	<div class="col-lg-12">
    		<h1 class="page-header"> Add Contact / <small>Import Contact</small> </h1>
    	</div>
	</div>
        <div class="row">
            <div class="col-lg-6"> 		
                <form role="form" method='post' action='<?php echo base_url(); ?>index.php/import_contact_manager/save_imported_contact' id="import_isd_rate_chart" enctype="multipart/form-data">
                    <?php 
                        if(isset($return_value)) 
                        {
                            //print_r($return_value);
                            $msg=$return_value['msg'];
                            if($return_value['success'] == true)
                            {
                                $params=array(); //empting input field values
                                echo "<div class='alert alert-success' role='alert'>$msg</div>";

                            }
                            else
                            {
                                echo "<div class='alert alert-danger' role='alert'>$msg</div>";
                            }
                        }
                    ?>
                    <div class="col-md-12" style="text-align:left;">						
                        <div class="form-group">
                            <label>Product Sale Information (*csv):</label>
                            <input type="file" id="isd_file" name="isd_file"  required>
                        </div>
                        <div id="msisdn_count"></div>
                        
                        <button type="submit" id="import" name="import" class="btn btn-default" style="width:200px; margin-top:20px;">Import</button>
                    </div>        				
                </form>
                


            </div>
        </div>

<?php $this->load->view ('common/footer.php');	?>
<script type="text/javascript">
	$('#import_isd_rate_chart').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			isd_file: {
				validators: {
					file: {
						extension: 'csv',
						type: 'text/x-comma-separated-values,text/comma-separated-values,application/octet-stream,application/vnd.ms-excel,text/csv,application/csv,application/excel,application/vnd.msexcel,text/csv,text/tsv',
						maxSize: 9999999999, 
						message: 'The selected file is not valid'
					},
					notEmpty: {
						message: 'CSV file is required...'
					}
				}
			}
		}
	});
        
</script>