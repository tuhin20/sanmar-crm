<?php $this->load->view('common/header.php'); 



?>
<style>
    body {
        font-size: 12px;
		margin-left:30px;
    }
	.modal-backdrop.fade.in {
    z-index: 0;
	
	</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Duplicate Contact Details /
            <small>List</small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-sm-3 col-md-3 col-lg-3">
        <!--<a class="downloadCSV" STYLE="max-width:160px; max-height:25px;" id="downloadCSV" href="<?php echo site_url('/group_manager/downloadCSV'); ?>">Download csv</a>-->
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
		<div class="table-responsive">
            <!--<input type="checkbox" class="selectAll" /> <span role='button' data-toggle='tooltip' data-placement='top' title='Delete selected' class='glyphicon glyphicon-trash linksDeleteSelected ' controllerMethod="<?php echo site_url('group_manager/deleteSelected') ?>" controllerReloadMethod="<?php echo site_url('group_manager/group_list') ?>" confirmationMsg='are you sure to delete selected group(s)' aria-hidden='true' onclick=''></span>-->

            <table class="table table-bordered table-hover table-striped" id="user_list">
                <thead>
                <tr>
                    <!--<th style="width:10%;"></th>-->
                    <th>Sl</th>
                    <th>Name</th>
					<th>Group</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Department</th>
                    <th>Lead Source</th>
                    <th>Passport Issue Date</th>
					<th>Passport Expiry Date</th>
                    <th class="col-md-2">Action</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $i = 1;

                foreach ($raw as $row) {
                    //print_r( $row);
                    ?>
                    <tr>
                        <!--<td><input type='checkbox' value='<?php echo $row->id; ?>' name='selectedIDs[]' class='resultRow' /></td>-->
                        <td><?php echo $i++; ?></td>
                        <td><?php echo $row['name'] ?></td>
						<td><?php echo $row['group_name'] ?></td>
                        <td><?php echo $row['email'] ?></td>
                        <td><?php echo $row['mobile'] ?></td>
                        <td><?php echo $row['department'] ?></td>
                        <td><?php echo $row['lead_source'] ?></td>
                        <td><?php echo $row['issue_date'] ?></td>
                        <td><?php echo $row['expiry_date'] ?></td>
						<td>
                            <div class="btn-group">

                                <?php

                                    echo '<a href='. site_url().'/contact/view_contact_details/'.$row['id'].' class="btn btn-success glyphicon glyphicon-eye-open" title="Details"></a>';

                                    echo '<a href='. site_url().'/contact/edit/'.$row['id'].' class="btn btn-warning glyphicon glyphicon-pencil" title="Edit"></a>';


                                    echo '<a href='. site_url().'/contact/delete/'.$row['id'].' class="btn btn-danger glyphicon glyphicon-trash delete" title="Delete Info"></a>';


                                ?>


                                <!--<a href="<?php echo base_url(); ?>index.php/client_manager/edit_client/<?php echo $row->id; ?>" class="btn btn-warning glyphicon glyphicon-pencil" title="Edit Client"></a>

                                        <a  href="<?php echo base_url(); ?>index.php/client_manager/delete_client/<?php echo $row->id; ?>" class="btn btn-danger glyphicon glyphicon-trash delete" client_status="<?php echo $client_status;?>" title="Delete Client"></a>-->
                            </div>
                        </td>




                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view('common/footer.php'); ?>
<script type="text/javascript">
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
        e.preventDefault();
		var delete_url= $(this).attr('href');
		

        bootbox.confirm("Are you sure you want to delete this entry?", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
                            $(this_holder).closest('td').closest('tr').hide(1000);
                        } else {

                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            }
        });
    });
    $(document).on('click', '.linksDeleteSelected', deleteSelected);
    $(document).on('change', '.selectAll', function () {
        if ($(this).is(":checked")) {
            $('.resultRow').prop('checked', true);
        }
        else
            $('.resultRow').prop('checked', false);
    });

    function deleteSelected(e) {
        if ($('.resultRow:checked').size() < 1) {
            alert('please select at least one record');
            return false;
        }
        var r = confirm(e.target.getAttribute("confirmationMsg"));
        if (r == false)
            return false;
        var controllerMethod = e.target.getAttribute("controllerMethod");
        var url = controllerMethod;
        /*var obj = {
            "flammable": "inflammable",
            "duh": "no duh"
        };
        $.each( obj, function( key, value ) {
            alert( key + ": " + value );
        });*/

        var selectedIDs = [];
        $('.resultRow:checked').each(function () {
            selectedIDs.push($(this).val());
        });
        console.log(selectedIDs);
        $.post(
            url,
            {selectedIDs: selectedIDs.join(", ")},
            function (data) {
                /*var obj = $.parseJSON(data);
                if(obj.success){
                    showStatusModal('Success','modal-info',obj.content);
                    reloadResult(e.target.getAttribute("controllerReloadMethod"));
                }
                else showStatusModal('Error','modal-warning',obj.content);*/
                window.location.replace(e.target.getAttribute('controllerReloadMethod'));
            }
        );
        return false;
    }

</script>