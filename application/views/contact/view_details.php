<?php $this->load->view('common/header.php');



?>
<style>
    body {
        font-size: 12px;
		margin-left:30px;
    }
	
	</style>


<div class="row">

    <div class="col-lg-12">
        <h1 class="page-header"> Contact /
            <small>View Details</small>
        </h1>


    </div>
</div>
<div class="row">
		<div class="col-md-12">
		    <a class="btn btn-warning" href="<?php echo site_url("contact/edit/".$contact_info['id']);?>" role="button">Edit Contact</a>
			
			
		</div>
		
	<div class="col-md-7">			
            <!-- Table -->
            <table class="table table-bordered table-hover">
                <div>
                    <h4>Contact Information</h4>
					
                </div>

                <?php
                        $i = 1;
                        //echo 'user_id='.$user_id;
                        //print_r($client_info);

                        if ( isset($contact_info)) {
                       echo "<tr>";
                       echo "<th> Name </th>";
                       echo "<td> ".$contact_info['name']." </td>";
                       echo "</tr>";
                       echo "<th> Email </th>";
                       echo "<td>".$contact_info['email']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Company Name </th>";
                       echo "<td>".$contact_info['company_name']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Department </th>";
                       echo "<td>".$contact_info['department']."</td>";
                       echo "</tr>";
					   echo "<tr>";
					   echo "<th> Designation </th>";
                       echo "<td>".$contact_info['designation']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Birth Date </th>";
                       echo "<td>".$contact_info['birth_date']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Passport Number </th>";
                       echo "<td>".$contact_info['passport_number']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Passport Issue Date </th>";
                       echo "<td>".$contact_info['issue_date']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Passport Expiry Date </th>";
                       echo "<td>".$contact_info['expiry_date']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Passport Validity </th>";
					   $date1= date("Y-m-d");
					   $date2= $contact_info['expiry_date'];
					   $diff = strtotime($date2) - strtotime($date1);
					   $years = floor($diff / (365*60*60*24));
                       $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                       $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
					   if($years>=0){
                       echo "<td>$years Years $months Months $days Days remaining</td>";
					   }
					   else 
					   echo "<td>Passport has expired</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Concerned Employee </th>";
                       echo "<td>".$contact_info['concerned_employee']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Lead Source </th>";
                       echo "<td>".$contact_info['lead_source']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Group </th>";
                       echo "<td>".$contact_info['group_name']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Mobile </th>";
                       echo "<td>".$contact_info['mobile']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Office Phone </th>";
                       echo "<td>".$contact_info['office_phone']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Home Phone </th>";
                       echo "<td>".$contact_info['home_phone']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Mailing Street </th>";
                       echo "<td>".$contact_info['mailing_street']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Mailing Police Station </th>";
                       echo "<td>".$contact_info['mailing_ps']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Mailing City </th>";
                       echo "<td>".$contact_info['mailing_city']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Mailing Postal Code </th>";
                       echo "<td>".$contact_info['mailing_postal']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Mailing Country </th>";
                       echo "<td>".$contact_info['mailing_country']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Comments </th>";
                       echo "<td>".$contact_info['comments']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Created By </th>";
                       echo "<td>".$contact_info['created_by']."</td>";
                       echo "</tr>";
					   echo "<tr>";
                       echo "<th> Created Time </th>";
                       echo "<td>".$contact_info['created_date']."</td>";
                       echo "</tr>";
					   
						}
					   
					   ?>
                       


            </table>
	</div>
	<div class="col-md-5">
		<div class="row" style="min-height:300px;">
			<h4>Contact images</h4> <hr>
			<?php
			
		
			$contact_images=$this->prime_model->getByQuery('select * from contact_files where contact_id='. $contact_info['id']);
			//$length_contact=sizeof($contact_images);
			foreach($contact_images as $item)
			  {
				$file_name=$item['name'];
				$source=base_url("files/$file_name");
							if($file_name!=''){
								echo "<div class=\"col-md-4\" ><img style=\" widht:100px;height:100px;\" class=\"img-rounded\"  src=\"$source\" alt=\"No Image Found\"></div>";
								//echo "&nbsp;<img style=\" widht:100px;height:100px;\" class=\"img-rounded\"  src=\"$source\" alt=\"No Image Found\">&nbsp;";
							}
              }
			
			?>
		</div>
		<div class="row" style="min-height:300px;">
			<h4>Passport images</h4><hr>
			<?php
			
		
			$passport_images=$this->prime_model->getByQuery('select * from passport_files where contact_id='. $contact_info['id']);
			//$length_passport=sizeof($passport_images);
			foreach($passport_images as $item)
			  {
				$file_name=$item['name'];
				$source=base_url("files/$file_name");
							if($file_name!=''){
								echo "<img style=\"max-height:200px;\" src=\"$source\" alt=\"No Image Found\">";
							}
              }
			
			?>
		</div>
	</div>



            
</div>
         
            
			
                 



<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">


    /*$(document).ready(function() {
        $('#create_user_form').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

                            name: {
                                validators: {
                                    notEmpty: {
                                                    message: 'name is empty...'
                                            }
                                }
                            },
                            email: {
                                validators: {
                                    email:  {
                                                    message: 'provide valid email'
                                            }
                                }
                            }
            }
        });

    });*/
</script>