<?php $this->load->view('common/header.php'); 
$this->load->view('common/navbar.php');


?>
<style>
    body {
        font-size: 12px;
		margin-left:30px;
    }
	
	.rating {
  display: inline-block;
  position: relative;
  height: 50px;
  line-height: 50px;
  font-size: 50px;
}

.rating label {
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  cursor: pointer;
}

.rating label:last-child {
  position: static;
}

.rating label:nth-child(1) {
  z-index: 5;
}

.rating label:nth-child(2) {
  z-index: 4;
}

.rating label:nth-child(3) {
  z-index: 3;
}

.rating label:nth-child(4) {
  z-index: 2;
}

.rating label:nth-child(5) {
  z-index: 1;
}

.rating label input {
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0;
}

.rating label .icon {
  float: left;
  color: transparent;
}

.rating label:last-child .icon {
  color: #000;
}

.rating:not(:hover) label input:checked ~ .icon,
.rating:hover label:hover input ~ .icon {
  color: #fcf52f;
}

.rating label input:focus:not(:checked) ~ .icon:last-child {
  color: #000;
  text-shadow: 0 0 5px #09f;
}
</style>
<script>

$( "input" ).on( "click", function() {
  $( "#log" ).html( $( "input:checked" ).val() + " is checked!" );
});
$(function() {
    $("#datepicker1").datepicker();
    $("#datepicker2").datepicker();
	$("#datepicker3").datepicker();
    $("#datepicker4").datepicker();
	$("#datepicker5").datepicker();
    $("#datepicker6").datepicker();
	$("#datepicker7").datepicker();
	$("#datepicker8").datepicker();
	$("#datepicker9").datepicker();
	
	

	
});
//alert('script working');
$(document).on('change', '#customer_id', function(){
        //alert($('#customer_id').val());
        //var value= $('#customer_id').val();
        var my_value=$('#customer_id').val();
        console.log(my_value);
        var url='<?php echo site_url('sale_details/get_customer_by_id/'.my_value); ?>';
        console.log(url);
        $.ajax({
            url: '<?php echo site_url('sale_details/get_customer_by_id/'.my_value); ?>',
            type: 'POST',
            data: {customer_id:my_value},
            async:false,
            success: function(data) {
                var obj=JSON.parse(data);
                console.log(obj);
                refresh_form_data(obj);
            }

        });
    });
 
function refresh_form_data(obj){
    $('#customer_name').val(obj.customer_name);
    $('#group_name').val(obj.group_name);
    $('#branch_name').val(obj.branch_name);
    $('#user_type').val(obj.user_type);
    $('#industry_type').val(obj.industry_type);
    $('#unit_user').val(obj.unit_user);
    
	$('#address_site_office').val(obj.address_site_office);
   
    
    $('#address_head_office').val(obj.address_head_office);
   
    
    $('#address_invoicing_office').val(obj.address_invoicing_office);
    
    
    $('#contact_name_primary').val(obj.contact_name_primary);
    $('#designation_primary').val(obj.designation_primary);
    $('#land_phone_primary').val(obj.land_phone_primary);
    $('#fax_number_primary').val(obj.fax_number_primary);
    $('#mobile_phone_primary').val(obj.mobile_phone_primary);
    $('#contact_email_primary').val(obj.contact_email_primary);
    $('#available_hour_primary').val(obj.available_hour_primary);
    
    $('#contact_name_secondary').val(obj.contact_name_secondary);
    $('#designation_secondary').val(obj.designation_secondary);
    $('#land_phone_secondary').val(obj.land_phone_secondary);
    $('#fax_number_secondary').val(obj.fax_number_secondary);
    $('#mobile_phone_secondary').val(obj.mobile_phone_secondary);
    $('#contact_email_secondary').val(obj.contact_email_secondary);
    $('#available_hour_secondary').val(obj.available_hour_secondary);
    
    $('#contact_name_management').val(obj.contact_name_management);
    $('#designation_management').val(obj.designation_management);
    $('#land_phone_management').val(obj.land_phone_management);
    $('#fax_number_management').val(obj.fax_number_management);
    $('#mobile_phone_management').val(obj.mobile_phone_management);
    $('#contact_email_management').val(obj.contact_email_management);
    $('#available_hour_management').val(obj.available_hour_management);
    
    
    
    
}   


</script>
    <div class="row">

        <div class="col-lg-12">
            <h1 class="page-header"> Product Sale Add /
               <small>Fill </small>
                    </h1>
					

        </div>
    </div>
<div class="row">
<?php
echo validation_errors();

$return_value=$this->session->flashdata('return_value');
//var_dump($return_value);
if(isset($return_value['msg'])){
    $msg=$return_value['msg'];
    echo "<div class=' col-sm-12'><div class='alert alert-success  role='alert'>$msg</div></div>";
}
?>


<div>
  <form action="contact/sendsms" method="post">
<div class="container">
  <h2>Send Message</h2>
  <p id="message"></p>
  <div class="form-group">
       <label for="senderid">Sender ID:</label>
       <input type="number" class="form-control" id="senderid" placeholder="Enter Sender ID" name="senderid">
    </div>
    <div class="form-group">
    <label for="pwd">Mobile No.:</label>
    <input type="text" class="form-control" id="contacts" placeholder="Enter Mobile No" name="contacts">
    </div>
    <div class="form-group">
    <label for="comment">Message:</label>
    <textarea class="form-control" rows="5" id="msg" name="msg" placeholder="Write Your Message Here...."></textarea>
  </div>
    <button type="submit" class="btn btn-primary" id="send">Send Message</button>
 
</div>

<form role="form" method='post' id="create_user_form" action="<?php echo $ui['action']; ?>">
<input type="hidden" name="id" value="<?php echo isset ($params['id'])? $params['id']:-1;  ?>">

	<div class="col-md-7">
	
            
  
  <!-- Table -->
  <table class="table table-bordered table-striped table-hover">
  <div>
  <h4>Customer Information</h4>
</div>
  
  <tr>
  <th>Customer/Company Name</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="customer_name" id="customer_name" title="Please Insert Customer Name" value="<?php echo isset($params['customer_name'])?$params['customer_name']:''; ?>"></td>
  </tr>
  <tr>
  <th>Customer Email ID</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="customer_email" id="customer_email" title="Please Insert Customer Email ID" value="<?php echo isset($params['customer_email'])?$params['customer_email']:''; ?>"></td>
  </tr>
  <tr>
  <th>Address(Head Office)</th>
  <td><textarea class="form-control" data-toggle="tooltip" type="text" name="address_head_office" id="address_head_office" title="Please Insert Address Head Office"><?php echo isset($params['address_head_office'])?$params['address_head_office']:''; ?></textarea></td>
  
  </tr>
  <tr>
  <th>Address(Site Office)</th>
  <td><textarea class="form-control" data-toggle="tooltip" type="text" name="address_site_office" id="address_site_office" title="Please Insert Address Site Office"><?php echo isset($params['address_site_office'])?$params['address_site_office']:''; ?></textarea></td>
  
  </tr>
 <!-- <tr>
  <th>Customer ID</th>
  <td></td> 
  </tr>
  <tr>
  <th>Group Company Name</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="group_name" id="group_name" title="Please Insert Group Company Name" value="<?php echo isset($params['group_name'])?$params['group_name']:''; ?>"></td>
  </tr>
  <tr>
  <th>Branch Name</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="branch_name" id="branch_name" title="Please Insert Branch Name" value="<?php echo isset($params['branch_name'])?$params['branch_name']:''; ?>"></td>
  </tr>
  <tr>
  <th>User Type</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="user_type" id="user_type" title="Please Insert User Type" value="<?php echo isset($params['user_type'])?$params['user_type']:''; ?>"></td>
  </tr>
  <tr>
  <th>Industry Type</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="industry_type" id="industry_type" title="Please Insert Industry Type" value="<?php echo isset($params['industry_type'])?$params['industry_type']:''; ?>"></td>
  </tr>
  <tr>
  <th>Unit User</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="unit_user" id="unit_user" title="Please Insert Unit User" value="<?php echo isset($params['unit_user'])?$params['unit_user']:''; ?>"></td>
  </tr>-->
    
  </table>
  </div>
  <div class="container col-md-9">

  
  <!-- Table -->
  <table class="table table-bordered table-striped table-hover">
  <div>
  <h4>Product Information</h4>
</div>
  
  <tr>
  <th>Division Name</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="division_name" id="division_name" title="Please Insert Division Name" value="<?php echo isset($params['division_name'])?$params['division_name']:''; ?>"></td>
  <th>Product Name</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="product_name" id="product_name" title="Please Insert Product Name" value="<?php echo isset($params['product_name'])?$params['product_name']:''; ?>"></td>
  <th>Product Delivery/Commissioning Date</th>
  <td><input class="form-control input-sm" id="datepicker1" data-toggle="tooltip" type="text" name="delivery_date" title="Please Insert Product Delivery/Commissioning Date" value="<?php echo isset($params['delivery_date'])?$params['delivery_date']:''; ?>"></td>
  </tr>
 
 
  <tr>
  <th>Brand Name</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="brand_name" id="brand_name" title="Please Insert Brand Name" value="<?php echo isset($params['brand_name'])?$params['brand_name']:''; ?>"></td>
  <th>Product/Engine Serial Number</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="engine_serial_number" id="engine_serial_number" title="Please Insert Engine Serial Number" value="<?php echo isset($params['engine_serial_number'])?$params['engine_serial_number']:''; ?>"></td>
  <th>Warranty Period</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="warranty_period" id="warranty_period" title="Please Insert Warranty Period" value="<?php echo isset($params['warranty_period'])?$params['warranty_period']:''; ?>"></td>
  </tr>
  <tr>
  <th>Model Number</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="product_model" id="product_model" title="Please Insert Model" value="<?php echo isset($params['product_model'])?$params['product_model']:''; ?>"></td>
  <th>Capacity</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="product_capacity" id="product_capacity" title="Please Insert Capacity" value="<?php echo isset($params['product_capacity'])?$params['product_capacity']:''; ?>"></td>
  <th>Chassis Number</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="chassis_number" id="chassis_number" title="Please Insert Chassis Number" value="<?php echo isset($params['chassis_number'])?$params['chassis_number']:''; ?>"></td>
  </tr>
 
    
  </table>
</div>


<div class="container col-md-9">

  
  <!-- Table -->
  <table class="table table-bordered table-striped table-hover">
  <div>
  <h4>Contact Information</h4>
</div>
  
  <tr>
  <th>Type of Contact</th>
  <th>Contact Name</th>
  <th>Mobile Phone</th>
  </tr>
  <tr>
  <th>Management/Owner</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="contact_name_management" id="contact_name_management" title="Please Insert Management/Owner Contact Name" value="<?php echo isset($params['contact_name_management'])?$params['contact_name_management']:''; ?>"></td>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="mobile_phone_management" id="mobile_phone_management" title="Please Insert Management/Owner Mobile Number" value="<?php echo isset($params['mobile_phone_management'])?$params['mobile_phone_management']:''; ?>"></td>
  </tr>
 
  <tr>
  <th>Site/Driver</th>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="contact_name_site" id="contact_name_site" title="Please Insert Site/Driver Contact Name" value="<?php echo isset($params['contact_name_site'])?$params['contact_name_site']:''; ?>"></td>
  <td><input class="form-control input-sm" data-toggle="tooltip" type="text" name="mobile_phone_site" id="mobile_phone_site" title="Please Insert Site/Driver Mobile Number" value="<?php echo isset($params['mobile_phone_site'])?$params['mobile_phone_site']:''; ?>"></td>
  
  </tr>
  </table>
</div>
<div class="col-md-6">

  
  <!-- Table -->
  <table class="table table-bordered table-striped table-hover">
  <div>
  <h4>Warranty/MC Information</h4>
</div>
  
  <tr>
  <th>Warranty/MC Start Date</th>
  <td><input class="form-control input-sm" id="datepicker2" data-toggle="tooltip" type="text" name="warranty_start_date" title="Please Insert Warranty/MC Start Date" value="<?php echo isset($params['warranty_start_date'])?$params['warranty_start_date']:''; ?>"></td>
  </tr>
  <tr>
  <th>Warranty/MC End Date</th>
  <td><input class="form-control input-sm" id="datepicker3" data-toggle="tooltip" type="text" name="warranty_end_date" title="Please Insert Warranty/MC End Date" value="<?php echo isset($params['warranty_end_date'])?$params['warranty_end_date']:''; ?>"></td>
  </tr>
   
    
  </table>
  </div>
  <div class="col-md-12">
  
   <table class="table table-bordered table-striped table-hover">
  <div>
  <h4>Service Information</h4>
</div>
  
  
  <tr>
  <th>Warranty Service Reminder Date</th>
  <td><input class="form-control input-sm" id="datepicker4" data-toggle="tooltip" type="text" name="reminder_date_first" placeholder="first date" title="Please Insert Warranty Service Reminder Date" value="<?php echo isset($params['warranty_end_date'])?$params['warranty_end_date']:''; ?>"></td>
  <td><input class="form-control input-sm" id="datepicker5" data-toggle="tooltip" type="text" name="reminder_date_second" placeholder="second date" title="Please Insert Warranty Service Reminder Date" value="<?php echo isset($params['warranty_end_date'])?$params['warranty_end_date']:''; ?>"></td>
  <td><input class="form-control input-sm" id="datepicker6" data-toggle="tooltip" type="text" name="reminder_date_third" placeholder="third date" title="Please Insert Warranty Service Reminder Date" value="<?php echo isset($params['warranty_end_date'])?$params['warranty_end_date']:''; ?>"></td>
  <td><input class="form-control input-sm" id="datepicker7" data-toggle="tooltip" type="text" name="reminder_date_fourth" placeholder="fourth date" title="Please Insert Warranty Service Reminder Date" value="<?php echo isset($params['warranty_end_date'])?$params['warranty_end_date']:''; ?>"></td>
  <td><input class="form-control input-sm" id="datepicker8" data-toggle="tooltip" type="text" name="reminder_date_fifth" placeholder="fifth date" title="Please Insert Warranty Service Reminder Date" value="<?php echo isset($params['warranty_end_date'])?$params['warranty_end_date']:''; ?>"></td>
  <td><input class="form-control input-sm" id="datepicker9" data-toggle="tooltip" type="text" name="reminder_date_sixth" placeholder="sixth date" title="Please Insert Warranty Service Reminder Date" value="<?php echo isset($params['warranty_end_date'])?$params['warranty_end_date']:''; ?>"></td>
  </tr>
 
 
  </table>
  </div>
  </div>
  </div>
  <div class="form-group">
  <button type="submit" id="save_product_sale_details" name="save_product_sale_details" class="btn btn-success" style="width: 150px;"><?php echo $ui['okButton'];?></button>
  </div>
  
  </form>

    
           
                
 <?php $this->load->view('common/footer.php'); ?>

<script>
    
    //alert('hi');
    
</script>