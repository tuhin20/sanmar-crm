<?php $this->load->view('common/header.php'); 



?>
<style>
    body {
        font-size: 12px;
		/*margin-left:30px;*/
    }
	/*.modal-backdrop.fade.in {
    z-index: 0;*/
	
	</style>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Contact Details /
            <small>List</small>
        </h1>
    </div>
</div>
<div class="row">
    <div class="col-sm-3 col-md-3 col-lg-3">
	<?php
	//if(isset($it_user)&&$it_user==true){
		?>
   
  <!--<a STYLE="max-width:160px; max-height:70px;" id="downloadCSV" class="btn btn-success glyphicon glyphicon-download-alt" href="<?php echo site_url('/contact/download_contact_chart'); ?>"> Download CSV</a>-->
<?php 
  // }
   ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
		<?php if(isset($search_info)) echo "<h3>$search_info</h3>"?>
		<div > <h3 id="search_info"></h3>
		<form class="form-inline" method="post" action="<?php echo site_url('contact/search_by_group');?>">
  <div class="form-group">
    <label for="email">Group</label>
    <?php
		
			$names=$this->prime_model->getByQuery('select distinct name from group_name');
			?>
			
		  
			<select class="form-control input-sm" type="text" name="group_name" id="group_name">
				<option value='' >--</option>
			<?php
				foreach($names as $item)
			  {
				$selected='';
				if(isset($params['group_name']) && $item['name']==$params['group_name'])
					$selected='selected';
				$text=str_replace('_',' ',$item['name']);
				$value=$item['name'];
				echo "<option value='$value' $selected>$text</option>";
              }

             ?>
			</select>
  </div>
  
  <button type="submit" class="btn btn-sm btn-success">Search</button>
</form>
<br>


		
		</div>
        <div class="table-responsive">
            <!--<input type="checkbox" class="selectAll" /> <span role='button' data-toggle='tooltip' data-placement='top' title='Delete selected' class='glyphicon glyphicon-trash linksDeleteSelected ' controllerMethod="<?php echo site_url('group_manager/deleteSelected') ?>" controllerReloadMethod="<?php echo site_url('group_manager/group_list') ?>" confirmationMsg='are you sure to delete selected group(s)' aria-hidden='true' onclick=''></span>-->

            <table class="table table-bordered table-hover table-striped" id="my_datatable">
                <thead>
                <tr>
                    <!--<th style="width:10%;"></th>-->
                    <th>Sl</th>
                    <th>First Name</th>
					<th>Middle Name</th>
					<th>Last Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Department</th>
                    <th>Lead Source</th>
					<th>Group</th>
                    <th>Passport Issue Date</th>
					<th>Passport Expiry Date</th>
					<th>Created Time</th>
                    <th class="col-md-2">Action</th>

                </tr>
                </thead>
                <tbody>
                
                </tbody>
            </table>
        </div>
    </div>
</div>
<!--<script src='<?php echo base_url("assets/js/$paging_js_name?v3");?>'></script>-->
<?php $this->load->view('common/footer.php'); ?>

<script type="text/javascript">
	<?php 
	if(isset($search_contact)&&$search_contact==true){
	?>
		$(document).ready(function() {
			var primary_number="<?php echo $primary_number;?>";
			$.ajax({ 		
				type: "POST" 
				,url: "<?php echo site_url('contact/search');?>" 
				,data:{primary_number:'<?php echo $primary_number;?>'}
				,async: false
				,success: function(response) { // on success..
					var obj=JSON.parse(response);
					if(obj.record_count==0){
						$('#search_info').html("Number "+primary_number+" is not in your contact list. <a role='button' class='btn btn-info' href='<?php echo site_url("contact/create/$primary_number");?>'> Add to Contact</a>");
					}
					else {
						$('#search_info').html("Search result of primary number: "+primary_number);
					}
					var query_id=obj.query_id;
					console.log(response);
					$('#my_datatable').dataTable({
						destroy: true, //use this to reinitiate the table, other wise problem will occur
						processing: true,
						serverSide: true,
						ajax: {
							url: "<?php echo site_url('contact/process_paging');?>"
							,type: 'POST'
							,data:{query_id: query_id}
						}
					});
				}
			});
		} );
	
	<?	
	}
	else{
	?>
		$(document).ready(function() {
			$('#my_datatable').dataTable({
				destroy: true, //use this to reinitiate the table, other wise problem will occur
				processing: true,
				serverSide: true,
				ajax: {
					url: "<?php echo site_url('contact/process_paging');?>"
					,type: 'POST'
					,data:{query_id: '0'}
				}
			});
		} );
		
	<?
	}
	?>
	$( "form" ).submit(function( e ) {
		  e.preventDefault();
        e.stopImmediatePropagation();
		$.ajax({
                data: $(this).serialize(), // get the form data
				type: $(this).attr('method'), // GET or POST
				url: $(this).attr('action'), // the file to call
                beforeSend: function(  ) {
                    $('#loading_modal').modal('toggle');
                }
            })
            .done(function( response ) {
                var obj=JSON.parse(response);
                var query_id=obj.query_id;
                var records_total=obj.records_total;
                console.log(response);
                $('#my_datatable').dataTable({
                    destroy: true, //use this to reinitiate the table, other wise problem will occur
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "<?php echo site_url('contact/process_paging')?>"
                        ,type: 'POST'
                        ,data:{query_id: query_id,'records_total': records_total}
                    }
                });
                $('#loading_modal').modal('toggle');
            });
        //alert('form submitted');
        
        return false;
	});
	 $(document).on('change','.selectAll',function(){
        //$(this).attr("disabled", true);
        $('#loading_modal').modal('toggle');
        if($(this).is(":checked")) {

            $('.resultRow').prop('checked', true);
        }
        else{
            $('.resultRow').prop('checked', false);
        }
        $('#loading_modal').modal('toggle');
        //$(this).removeAttr("disabled");
    });
    jQuery(document.body).on('click', '.delete', function (e) {
        var this_holder = this;
        e.preventDefault();
		var delete_url= $(this).attr('href');
		

        bootbox.confirm("Are you sure you want to delete this entry?", function (response) {
            if (response) {
                $.ajax({
                    url: delete_url,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    success: function (data, textStatus, jQxhr) {
                        if (data == 1) {
                            $(this_holder).closest('td').closest('tr').hide(1000);
                        } else {

                        }
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });

            }
        });
    });
	
		
    $(document).on('click', '.linksDeleteSelected', deleteSelected);
    $(document).on('change', '.selectAll', function () {
        if ($(this).is(":checked")) {
            $('.resultRow').prop('checked', true);
        }
        else
            $('.resultRow').prop('checked', false);
    });

    function deleteSelected(e) {
        if ($('.resultRow:checked').size() < 1) {
            alert('please select at least one record');
            return false;
        }
        var r = confirm(e.target.getAttribute("confirmationMsg"));
        if (r == false)
            return false;
        var controllerMethod = e.target.getAttribute("controllerMethod");
        var url = controllerMethod;
        /*var obj = {
            "flammable": "inflammable",
            "duh": "no duh"
        };
        $.each( obj, function( key, value ) {
            alert( key + ": " + value );
        });*/

        var selectedIDs = [];
        $('.resultRow:checked').each(function () {
            selectedIDs.push($(this).val());
        });
        console.log(selectedIDs);
        $.post(
            url,
            {selectedIDs: selectedIDs.join(", ")},
            function (data) {
                /*var obj = $.parseJSON(data);
                if(obj.success){
                    showStatusModal('Success','modal-info',obj.content);
                    reloadResult(e.target.getAttribute("controllerReloadMethod"));
                }
                else showStatusModal('Error','modal-warning',obj.content);*/
                window.location.replace(e.target.getAttribute('controllerReloadMethod'));
            }
        );
        return false;
    }

</script>