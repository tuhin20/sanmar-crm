<?php $this->load->view('common/header.php');
//print_r($params);
?>

<script>
	
	$( function() {
    $( "#issue_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: "-100:+0",
	  dateFormat: "yy-mm-dd"
    });
  } );
  
  $( function() {
    $( "#expiry_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: "-100:+10",
	  dateFormat: "yy-mm-dd"
    });
  } );

	


$( function() {
    $( "#birth_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  yearRange: "-100:+0",
	  dateFormat: "yy-mm-dd"
	  
    });
  } );



</script>
<div class=" col-md-12"> 	<!-- this is alert size-->
	<?php 
		$return_value=$this->session->flashdata('return_value');
		if(isset($return_value)) 
		{
			//print_r($return_value);
			$msg=$return_value['msg'];
			if($return_value['success'] == true)
			{

				echo "<div class='alert alert-success' role='alert'>$msg</div>";

			}
			else if($msg!='') //if not success and msg not empty
			{
				echo "<div class='alert alert-danger' role='alert'>$msg</div>";
			}
		}
	?>
</div>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> Contact / <small>Create<?php //echo $ui['title']?> </small> </h1>
    </div>
</div>

<div class="row">
  
	<form class="form-horizontal" method="post" action="<?php echo $ui['action']; ?>" accept-charset="utf-8">
	<div class="col-md-6">
		<input type="hidden" name="id" value="<?php echo isset ($params['id'])? $params['id']:-1;  ?>">
  
		<!--  <h4 class="text-center">Information About the Contact</h4>-->
		  
		  
		<div class="form-group">
				<label class="control-label col-sm-5">Name</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="name" placeholder="Enter Name" name="name" value="<?php echo isset($params['name'])?$params['name']:'';?>">
				<?php echo form_error('name','<label class="error">','</label>');?>
			</div>
		</div>
		
		<div class="form-group">
				<label class="control-label col-sm-5">Email</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="email" placeholder="Enter Email" name="email" value="<?php echo isset($params['email'])?$params['email']:'';?>">
			</div>
		</div>
		<div class="form-group">
				<label class="control-label col-sm-5">Company Name</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="company_name" placeholder="Company Name" name="company_name" value="<?php echo isset($params['company_name'])?$params['company_name']:'';?>">
			</div>
		</div>
		
		<div class="form-group">
				<label class="control-label col-sm-5">Department</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="department" placeholder="Enter Department" name="department" value="<?php echo isset($params['department'])?$params['department']:'';?>">
			</div>
		</div>
		<div class="form-group">
				<label class="control-label col-sm-5">Designation</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="designation" placeholder="Enter Designation" name="designation" value="<?php echo isset($params['designation'])?$params['designation']:'';?>">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-5">Birth Date</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="birth_date" placeholder="Select Birth Date" name="birth_date" value="<?php echo isset($params['birth_date'])?$params['birth_date']:'';?>">
		</div>
	    </div>
		<div class="form-group">
			<label class="control-label col-sm-5">Passport Number</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="passport_number" placeholder="Enter Passport Number" name="passport_number" value="<?php echo isset($params['passport_number'])?$params['passport_number']:'';?>">
		</div>
	</div>
	
	<div class="form-group">
			<label class="control-label col-sm-5">Passport Issue Date</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="issue_date" placeholder="Select Issue Date" name="issue_date" value="<?php echo isset($params['issue_date'])?$params['issue_date']:'';?>">
		</div>
	</div>
	
	
	<div class="form-group">
	
			<label class="control-label col-sm-5">Passport Expiry Date</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="expiry_date" placeholder="Select Expiry Date" name="expiry_date" value="<?php echo isset($params['expiry_date'])?$params['expiry_date']:'';?>">
			<?php 
			$now = time();
			$your_date = strtotime($NewDate);
			//	$your_date = strtotime($NewDate);
			$datediff = $now - $your_date;
	        ?>
		</div>
		
		<div class="col-md-offset-5 col-md-7"><label class='control-label error' id="remaining_time"> </label></div>
	</div>
	<div class="form-group">
			<label class="control-label col-sm-5">Concerned Employee</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="concerned_employee" placeholder="Enter Concerned Employee" name="concerned_employee" value="<?php echo isset($params['concerned_employee'])?$params['concerned_employee']:'';?>">
		</div>
	</div>
	<!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php //$current_user=$this->user_model->get_current_user(); echo $current_user['name'];?> <span class="caret"></span></a> -->
		 <div class="form-group">        
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default btn-success"><?php echo $ui['okButton'];?></button>
			</div>
		</div>
	</div>
	<div class="col-md-6">
	
	<div class="form-group">
      <label class="control-label col-sm-5">Lead Source</label>
      <div class="col-sm-7">
        <?php
		
			$leads=$this->prime_model->getByQuery('select distinct lead from lead_source');
			?>
			
		  


			<select class="form-control input-sm" type="text" name="lead_source" id="lead_source">
				<option value='' >--</option>
			<?php
				foreach($leads as $item)
			  {
				$selected='';
				if(isset($params['lead_source']) && $item['lead']==$params['lead_source'])
					$selected='selected';
				$text=str_replace('_',' ',$item['lead']);
				$value=$item['lead'];
				echo "<option value='$value' $selected>$text</option>";
              }

             ?>
			</select>
      </div>
    </div>
	
	<div class="form-group">
      <label class="control-label col-sm-5">Group</label>
      <div class="col-sm-7">
        <?php
		
			$names=$this->prime_model->getByQuery('select distinct name from group_name');
			?>
			
		  


			<select class="form-control input-sm" type="text" name="group_name" id="group_name">
				<option value='' >--</option>
			<?php
				foreach($names as $item)
			  {
				$selected='';
				if(isset($params['group_name']) && $item['name']==$params['group_name'])
					$selected='selected';
				$text=str_replace('_',' ',$item['name']);
				$value=$item['name'];
				echo "<option value=\"$value\" $selected>$text</option>";
              }

             ?>
			</select>
      </div>
    </div>
	
	<div class="form-group">
				<label class="control-label col-sm-5">Mobile</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="mobile" placeholder="Enter Mobile" name="mobile" value="<?php echo isset($params['mobile'])?$params['mobile']:'';?>">
				<?php echo form_error('mobile','<label class="error">','</label>');?>
			</div>
		</div>
		<div class="form-group">
				<label class="control-label col-sm-5">Office Phone</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="office_phone" placeholder="Enter Name" name="office_phone" value="<?php echo isset($params['office_phone'])?$params['office_phone']:'';?>">
			</div>
		</div>
		<div class="form-group">
				<label class="control-label col-sm-5">Home Phone</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="home_phone" placeholder="Enter Name" name="home_phone" value="<?php echo isset($params['home_phone'])?$params['home_phone']:'';?>">
			</div>
		</div>
	
	
	
	<div class="form-group">
			<label class="control-label col-sm-5">Mailing Street</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="mailing_street" placeholder="Enter Mailing Street" name="mailing_street" value="<?php echo isset($params['mailing_street'])?$params['mailing_street']:'';?>">
		</div>
	</div>
	<div class="form-group">
			<label class="control-label col-sm-5">Mailing Police Station</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="mailing_ps" placeholder="Enter Mailing Police Station" name="mailing_ps" value="<?php echo isset($params['mailing_ps'])?$params['mailing_ps']:'';?>">
		</div>
	</div>
	<div class="form-group">
			<label class="control-label col-sm-5">Mailing City</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="mailing_city" placeholder="Enter Mailing City" name="mailing_city" value="<?php echo isset($params['mailing_city'])?$params['mailing_city']:'Dhaka';?>">
		</div>
	</div>
	
	<div class="form-group">
			<label class="control-label col-sm-5">Mailing Postal Code</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="mailing_postal" placeholder="Enter Mailing Postal Code" name="mailing_postal" value="<?php echo isset($params['mailing_postal'])?$params['mailing_postal']:'';?>">
		</div>
	</div>
	
	<div class="form-group">
			<label class="control-label col-sm-5">Mailing Country</label>
		<div class="col-sm-7">
			<input type="text" autocomplete="off" class="form-control input-sm" id="mailing_country" placeholder="Enter Mailing Country" name="mailing_country" value="<?php echo isset($params['mailing_country'])?$params['mailing_country']:'Bangladesh';?>">
		</div>
	</div>
	
	<div class="form-group">
			<label class="control-label col-sm-5">Comments</label>
		<div class="col-sm-7">
			<textarea type="text" autocomplete="off" class="form-control input-sm" id="comments" placeholder="Enter Comments" name="comments"><?php echo isset($params['comments'])?$params['comments']:'';?></textarea>
		</div>
	</div>
	
	

	
	
		
	<div class="form-group">
      <label class="control-label col-sm-5">Contact Images</label>
      <div class="col-sm-7">
        <input id="contact_fileupload" type="file" name="files[]" data-url="<?php echo site_url('fileupload');?>" multiple>  <!--name is the folder name, controller method fileupload/index -->
		<div  id="contact_file_names" > </div>
		<?php
						if(isset($params['id'])&& $params['id']!=-1){
							$victims_uploaded_file=$params['victims_uploaded_file'];
							$href=base_url("uploads/$victims_uploaded_file");
							if($victims_uploaded_file!=''){
								echo "<a href=\"$href\">Download</a>"  ;
							}
							
						}
						?>
								
      </div>
    </div>
	
	<div class="form-group">
      <label class="control-label col-sm-5">Passport Images</label>
      <div class="col-sm-7">
        <input id="passport_fileupload" type="file" name="files[]" data-url="<?php echo site_url('fileupload');?>" multiple>  <!--name is the folder name, controller method fileupload/index -->
		<div  id="passport_file_names" > </div>
		<?php
						if(isset($params['id'])&& $params['id']!=-1){
							$victims_uploaded_file=$params['victims_uploaded_file'];
							$href=base_url("uploads/$victims_uploaded_file");
							if($victims_uploaded_file!=''){
								echo "<a href=\"$href\">Download</a>"  ;
							}
							
						}
						?>
								
      </div>
    </div>
	
	<!--  <div style="position:relative;">
	  <label class="control-label col-sm-5">Passport Image</label>
	  
		<a class='btn btn-primary' href='javascript:;'>
			Choose File...
			<input type="file" style='position:absolute;z-index:2;top:0;left:30;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
		</a>
		&nbsp;
		<span class='label label-info' id="upload-file-info"></span>
	</div>
	<br>
	
	
	<div style="position:relative;">
	  <label class="control-label col-sm-5">Contact Image</label>
	  
		<a class='btn btn-primary' href='javascript:;'>
			Choose File...
			<input type="file" style='position:absolute;z-index:2;top:0;left:30;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
		</a>
		&nbsp;
		<span class='label label-info' id="upload-file-info"></span>
	</div>-->
  
		
    
	
	
	
    
   
	
	
	</div>
	
	
	
  </form>
  
  
  
</div>  <!-- end of menu2 tab -->
    
      


                <?php $this->load->view('common/footer.php'); ?>
                     <script type="text/javascript">
					 $( "#expiry_date" ).change(function () {
						 var start = new Date($('#expiry_date').val());
						//var start = new Date("2018-09-29"),
						end   = new Date(); 
						diff  = new Date(start - end);
						days  = diff/1000/60/60/24;
						var remaining = Math.floor(days);
						var months = remaining/30;
						var month = Math.floor(months);
						var day = remaining%30;
						console.log(remaining);
						if(month>=0){
						$('#remaining_time').html("Passport will expire in "+month+" month "+day+" days");
						}
						else
						$('#remaining_time').html("Passport has expired");
							
						
						
					
					 });
					 
					 
					 
					 $(function () {						 
						$('#contact_fileupload').fileupload({
							dataType: 'json',
							done: function (e, data) {
								$.each(data.result.files, function (index, file) {
									console.log(file.name);
									$('<p/>').append(file.name).appendTo($('#contact_file_names'));
									$('#contact_file_names').append("<input type='hidden' name='contact_file_uploads[]' value='"+file.name+"'>");
									//$('#file_names').append(file.name).appendTo(document.body);
								});
							}
						});
					});
					
					$(function () {
						$('#passport_fileupload').fileupload({
							dataType: 'json',
							done: function (e, data) {
								$.each(data.result.files, function (index, file) {
									console.log(file.name);
									$('<p/>').append(file.name).appendTo($('#passport_file_names'));
									$('#passport_file_names').append("<input type='hidden' name='passport_file_uploads[]' value='"+file.name+"'>");
									//$('#file_names').append(file.name).appendTo(document.body);
								});
							}
						});
					});
					 
					 
					 function load_victims_thana_permanent(val){
						 $.ajax({
							 type: "POST",
							 url: "<?php echo site_url("incident_info/get_ctrl_victims_thana_permanent");?>",
							 data: {victims_district_permanent: val},
							 beforeSend: function(  ) {
								//$('#loading_modal').modal('toggle');
							 }
						   })
						   .done(function( data ) {
							   //console.log(data);
								var obj = $.parseJSON(data);
								//console.log(obj);
								$('#div_victims_thana_permanent').html(obj.content);
								
							
						   });
					 }
					 
					  function load_victims_thana_present(val){
						 $.ajax({
								 type: "POST",
								 url: "<?php echo site_url("incident_info/get_ctrl_victims_thana_present");?>",
								 data: {victims_district_present: val},
								 beforeSend: function(  ) {
									//$('#loading_modal').modal('toggle');
								 }
							   })
						   .done(function( data ) {
							   //console.log(data);
								var obj = $.parseJSON(data);
								//console.log(obj);
								$('#div_victims_thana_present').html(obj.content);
			
		
							});
					 }
					 
	$("#victims_district_permanent").change(function() {
	var val = $(this).val();
	console.log(val);
	load_victims_thana_permanent(val);
		
		
});

$("#victims_district_present").change(function() {
	var val = $(this).val();
	console.log(val);
	load_victims_thana_present(val);
		
		
});

$("#respondents_district_permanent").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_respondents_thana_permanent");?>",
		 data: {respondents_district_permanent: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_respondents_thana_permanent').html(obj.content);
			
		
	   });
		
		
});

$("#respondents_district_present").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_respondents_thana_present");?>",
		 data: {respondents_district_present: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_respondents_thana_present').html(obj.content);
			
		
	   });
		
		
});

 $("#complainants_district_permanent").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_complainants_thana_permanent");?>",
		 data: {complainants_district_permanent: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_complainants_thana_permanent').html(obj.content);
			
		
	   });
		
		
});

$("#complainants_district_present").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_complainants_thana_present");?>",
		 data: {complainants_district_present: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_complainants_thana_present').html(obj.content);
			
		
	   });
		
		
});
$(document).ready(function() {
    //set initial state.
    //$('#textbox1').val($(this).is(':checked'));

    $('#victims_self').change(function() {
        if($(this).is(":checked")) {
             $('#victims_name').val($('#complainants_name').val());
			 $('#victims_fathers_name').val($('#complainants_fathers_name').val());
			 $('#victims_mothers_name').val($('#complainants_mothers_name').val());
			 $('#victims_spouses_name').val($('#complainants_spouses_name').val());
			 $('#victims_sex').val($('#complainants_sex').val());
			 $('#victims_age').val($('#complainants_age').val());
			 $('#victims_religion').val($('#complainants_religion').val());
			 $('#victims_village_permanent').val($('#complainants_village_permanent').val());
			 $('#victims_district_permanent').val($('#complainants_district_permanent').val());
			 load_victims_thana_permanent($('#complainants_district_permanent').val());
			 $("#victims_thana_permanent").selectmenu();
			 $("#victims_thana_permanent").selectmenu('refresh', true);
			 console.log($('#complainants_thana_permanent').val());
	         $('#victims_thana_permanent').val($('#complainants_thana_permanent').val());
	         $('#victims_phone_permanent').val($('#complainants_phone_permanent').val());
	         $('#victims_fax_permanent').val($('#complainants_fax_permanent').val());
	         $('#victims_email_permanent').val($('#complainants_email_permanent').val());
	         $('#victims_village_present').val($('#complainants_village_present').val());
	         $('#victims_district_present').val($('#complainants_district_present').val());
			 load_victims_thana_present($('#complainants_district_present').val());
			 console.log($('#complainants_thana_present').val());
			 $('#victims_thana_present').val($('#complainants_thana_present').val());
	         $('#victims_phone_present').val($('#complainants_phone_present').val());
	         $('#victims_fax_present').val($('#complainants_fax_present').val());
	         $('#victims_email_present').val($('#complainants_email_present').val());
			 
			 
			 
			 
			 
			 
			 
        }
            
    });
});

$("#victims_selected").click(function(){
	
	
	
	
});

        /*$(document).ready(function() {
            $('#create_user_form').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {

                                name: {
                                    validators: {
                                        notEmpty: {
                                                        message: 'name is empty...'
                                                }
                                    }
                                },
                                email: {
                                    validators: {
                                        email:  {
                                                        message: 'provide valid email'
                                                }
                                    }
                                }
                }
            });

        });*/
    </script>