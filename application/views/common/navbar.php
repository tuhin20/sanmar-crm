<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					  <span class="sr-only">Toggle navigation</span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
				</button>
		     <a class="navbar-brand" href="#">CRM</a>
			  
			  
			</div>
		  
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
				
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Contact <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<?php 
							   //if($this->user_model->has_permission_for_role($this->role_model->Add_product_sale)){
							  /* echo "<li><a href=".site_url('contact/create').">Create for Heritage Air Express </a></li>";
							   echo "<li><a href=".site_url('contact/index').">Heritage Air Express View List</a></li>";
							   echo "<li><a href=".site_url('contact/create_sultan').">Create for Sultan Tea</a></li>";
							   echo "<li><a href=".site_url('contact/view_sultan').">Sultan Tea View List</a></li>";
							   echo "<li><a href=".site_url('contact/duplicate_mobile_numbers')."> Duplicate Number List</a></li>";
							   echo "<li><a href=".site_url('contact/group_sms_send').">Group SMS</a></li>";
							   //if($this->user_model->has_permission_for_role($this->role_model->Import_contact)){
							   echo "<li role=\"separator\" class=\"divider\"></li>";
							   
							   echo "<li><a href=".site_url('import_contact_manager/import_contact').">Import Contact</a></li>";
							    */
							   // }
						  // }
						    ?>
							
						</ul>
					  
					</li>
					
					
					<?php
					if($this->user_model->has_permission_for_role($this->role_model->Download_contact)){
					echo "<li><a href=".site_url('contact/download_contact_chart').">Download</a></li>";
					}
					?>
			<!--		<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ticket <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<?php 
							   if($this->user_model->has_permission_for_role($this->role_model->View_ticket_list)){
								   echo "<li><a href=".site_url('ticket/create').">Create</a></li>";
								   echo "<li><a href=".site_url('ticket/index').">View List</a></li>";
							   }
							?>
							
						</ul>
					  
					</li>-->
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php $current_user=$this->user_model->get_current_user(); echo $current_user['name'];?> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="<?php echo site_url('login/logout');?>">Logout</a></li>
						<li><a href="<?php echo site_url('login/change_password'); ?>" >Change credentials</a></li>
					</ul>
					</li>
				</ul>
      
      
			</div><!-- /.navbar-collapse -->
          
    </div><!-- /.container-fluid -->
</nav>