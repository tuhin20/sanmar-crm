<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <script src="<?php echo base_url('assets/js/vendor/bootbox.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/vendor/jquery.dataTables.min.js')?>"></script>
  <link href="<?php echo base_url('assets/css/jquery.dataTables.min.css')?>" rel="stylesheet">




  <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/bootstrap-theme.min.css')?>" rel="stylesheet">
  <script src="<?php echo base_url('assets/js/vendor/bootstrap.min.js')?>"></script>

  <style>
    body {margin:0;font-family:Arial}

    .topnav {
      overflow: hidden;
      background-color: #6F3218;
    }

    .topnav {
      overflow: hidden;
      background-color: #6F3218;
    }

    .topnav a {
      float: left;
      display: block;
      color: #f2f2f2;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
      font-size: 17px;
    }

    .active {
      background-color: #734e32;
      color: white;
    }

    .topnav .icon {
      display: none;
    }

    .dropdown {
      float: left;
      overflow: hidden;
    }

    .dropdown .dropbtn {
      font-size: 17px;    
      border: none;
      outline: none;
      color: white;
      padding: 14px 16px;
      background-color: inherit;
      font-family: inherit;
      margin: 0;
    }

    .dropdown-content {
      display: none;
      position: absolute;
      background-color: #f9f9f9;
      min-width: 160px;
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
      z-index: 1;
    }

    .dropdown-content a {
      float: none;
      color: black;
      padding: 12px 16px;
      text-decoration: none;
      display: block;
      text-align: left;
    }

    .topnav a:hover, .dropdown:hover .dropbtn {
      background-color: #555;
      color: white;
    }

    .dropdown-content a:hover {
      background-color: #ddd;
      color: black;
    }

    .dropdown:hover .dropdown-content {
      display: block;
    }

    @media screen and (max-width: 600px) {
      .topnav a:not(:first-child), .dropdown .dropbtn {
        display: none;
      }
      .topnav a.icon {
        float: right;
        display: block;
      }
    }

    @media screen and (max-width: 600px) {
      .topnav.responsive {position: relative;}
      .topnav.responsive .icon {
        position: absolute;
        right: 0;
        top: 0;
      }
      .topnav.responsive a {
        float: none;
        display: block;
        text-align: left;
      }
      .topnav.responsive .dropdown {float: none;}
      .topnav.responsive .dropdown-content {position: relative;}
      .topnav.responsive .dropdown .dropbtn {
        display: block;
        width: 100%;
        text-align: left;
      }
    }


  </style>
</head>
<body>

  <div class="topnav">


    <?php  $current_user=$this->user_model->get_current_user(); 
    if($current_user['group_id']==1){
      ?>
      <div class="col-md-12" style="margin: 0px;">


        <a href="<?php echo site_url('contact/create_customer');?>">Customer</a>
        <a href="<?php echo site_url('contact/customerlist');?>"> Customer List</a> 
        <a href="<?php echo site_url('contact/create_project');?>">Project</a>
        <a href="<?php echo site_url('contact/projectlist');?>">Project List</a>
        <a href="<?php echo site_url('contact/ticket');?>">Ticket</a>
        <a href="<?php echo site_url('contact/ticketlist');?>">Ticket List</a>
        <a href="<?php echo site_url('contact/landowner');?>">Landowner</a>
        <a href="<?php echo site_url('contact/landownerlist');?>">Landowner List</a>
        <a href="<?php echo site_url('contact/career');?>">Career</a>
        <a href="<?php echo site_url('contact/careerlist');?>">Career List</a>
        <a href="<?php echo site_url('contact/investor');?>">Investor</a>
        <a href="<?php echo site_url('contact/investorlist');?>">Investor List</a> 
      </div>
    <?php }  ?> 
    <?php  $current_user=$this->user_model->get_current_user(); 
    if($current_user['group_id']==3){
      ?>
      <div class="col-md-12" style="margin: 0px;">
        
       
        <a href="<?php echo site_url('contact/customer_service');?>">Customer Service Ticket</a>
        <!-- <a href="<?php echo site_url('contact/edit_customer_service');?>">Ticket List</a> -->
      
      </div>
    <?php }  ?>

    <?php  $current_user=$this->user_model->get_current_user(); 
    if($current_user['group_id']==0){
      ?>
      <div class="col-md-12" style="align:center;">

       <a href="<?php echo site_url('contact/ticket');?>">Ticket</a>
       <a href="<?php echo site_url('contact/ticketlist');?>">Ticket List</a>
     </div>
   <?php }  ?>

<!-- Maintenance user -->
    <?php  $current_user=$this->user_model->get_current_user(); 
    if($current_user['group_id']==5){
      ?>
      <div class="col-md-12" style="align:center;">

       <a href="<?php echo site_url('contact/lifestyle');?>">LifeStyle Details</a>
      
     </div>
   <?php }  ?>
   <!-- Maintenance user -->
    <?php  $current_user=$this->user_model->get_current_user(); 
    if($current_user['group_id']==4){
      ?>
      <div class="col-md-12" style="align:center;">

       <a href="<?php echo site_url('contact/maintanance');?>">Maintenancelist</a>
       <!-- <a href="<?php echo site_url('contact/ticketlist');?>">Ticket List</a> -->
     </div>
   <?php }  ?>

   <?php  $current_user=$this->user_model->get_current_user(); 
   if($current_user['group_id']==2){
    ?>
    <div class="col-md-12" style="margin: 0px;">

      <a href="<?php echo site_url('contact/create_customer');?>" class="dropdown-item">Customer</a>
      <a href="<?php echo site_url('contact/customerlist');?>" class="dropdown-item"> Customer List</a> 
      <a href="<?php echo site_url('contact/create_project');?>">Project</a>
      <a href="<?php echo site_url('contact/projectlist');?>">Project List</a>
      <a href="<?php echo site_url('contact/ticket');?>">Ticket</a>
      <a href="<?php echo site_url('contact/ticketlist');?>">Ticket List</a>
      <a href="<?php echo site_url('contact/landowner');?>">Landowner</a>
      <a href="<?php echo site_url('contact/landownerlist');?>">Landowner List</a>
      <a href="<?php echo site_url('contact/career');?>">Career</a>
      <a href="<?php echo site_url('contact/careerlist');?>">Career List</a>
      <a href="<?php echo site_url('contact/investor');?>">Investor</a>
      <a href="<?php echo site_url('contact/investorlist');?>">Investor List</a> 
    </div>
  <?php }  ?>



  
  <!-- <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a> -->

  <div class="pull-right col-md-0" style="margin: 2px;">
    <!-- 	<ul class="nav navbar-nav"> -->

     <a href="<?php echo site_url('login/logout');?>" class="btn navbar-btn btn-danger" style="padding: 0px;">Logout</a>
     <a href="#" class=""  >
      <?php $current_user=$this->user_model->get_current_user(); echo $current_user['name'];?> <span ></span></a>
      <!-- <button type="submit" class="btn navbar-btn btn-danger" name="logout" id="logout"  value="Log Out" >Log Out</button> -->
      <!-- 	</ul>  -->

		  <!-- <ul class="nav navbar-nav navbar-right">
			  <li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php //$current_user=$this->user_model->get_current_user(); echo $current_user['name'];?> <span class="caret"></span></a>
			  <ul>
			   
				<li><a href="<?php// echo site_url('login/change_password'); ?>" >Change credentials</a></li>
			  </ul>
			  <ul> <li><a href="<?php //echo site_url('login/logout');?>">Logout</a></li></ul>
			  </li>
			</ul>  -->   
   </div>

 </div>



 <script>
  function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }
</script>

</body>
</html>
