<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Admin Panel(Login)</title>
		<link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>css/sb-admin.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	</head>
	
	<body>
		<div class="container">    
		
			<div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
				<div class="panel panel-info" >
					<div class="panel-heading">
						<div class="panel-title">Sign In</div>
					</div>     
					<form method="post" action="<?php echo base_url(); ?>index.php/login/validate_user">
						<div style="padding-top:30px" class="panel-body" >
							<?php	if(isset($msg)){	?>
							<div id="login-alert" class="alert alert-danger col-sm-12">
								<?php echo $msg; ?>
							</div>
							<?php	}	?>
							<form id="loginform" class="form-horizontal" role="form">
								<div style="margin-bottom: 25px" class="input-group">
								<?php //echo $password_encoded;?>
									<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									<input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username" required>                                        
								</div>
									
								<div style="margin-bottom: 25px" class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
									<input id="login-password" type="password" class="form-control" name="password" placeholder="password" required>
								</div>							

								<div style="margin-top:10px" class="form-group">
									<div class="col-sm-12 controls">
										<input type="submit" id="btn-login" href="#" class="btn btn-success" value="Login" />									
									</div>
								</div>									
							</form>                  
						</div>
					</form>
					
				</div>
			</div> 
		</div>
	</body>
</html>
