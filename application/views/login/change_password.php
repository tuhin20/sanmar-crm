<?php $this->load->view('common/header.php'); 
$this->load->view('common/navbar.php');
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> User Manager / <small>Change Password</small> </h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12"> 	
        <form role="form" method='post' id="create_user_form" action='<?php echo base_url(); ?>index.php/login/save_changed_password' enctype="multipart/form-data">
            <?php 
                if(isset($type) && strlen($type) > 1) 
                {
                    if($type == 'success')
                    {
            ?>
                        <div class="alert alert-success" role="alert"><?php echo $msg; ?></div>
            <?php
                    }
                    else
                    {
            ?>
                        <div class="alert alert-danger" role="alert"><?php echo $msg; ?></div>
            <?php
                    }
                }
            ?>
            <div class="col-md-6" style="text-align:left;">						
                <div class="form-group">
                    <label>Current Password:</label>
                    <input type="password" class="form-control" id="old_password" name="old_password" required>
                </div>
				<div class="form-group">
                    <label>New Password:</label>
                    <input type="password" class="form-control" id="new_password" name="new_password" required>
                </div>
                <div class="form-group">
                    <label>Confirm New Password:</label>
                    <input type="password" class="form-control" id="confirm_new_password" name="confirm_new_password" required>
                </div>           
                <button type="submit" id="send_password_change_info" name="send_password_change_info" class="btn btn-default" style="width:200px; margin-top:20px;">Change Password</button>
            </div>        				
        </form>
	</div>
</div>
<?php $this->load->view ('common/footer.php');	?>
<script type="text/javascript">
$(document).ready(function() {
	$('#create_user_form').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			old_password: {
                validators: {                    
					notEmpty: {
						message: 'Prefix is required...'
					}
                }
            },
			new_password: {
                validators: {
                    notEmpty: {
						message: 'Operator name is required...'
					},
					identical: {
						field: 'confirm_new_password',
						message: 'The password and its confirm are not the same'
					}
                }
            },
			confirm_new_password: {
                validators: {
                    notEmpty: {
						message: 'Charge per sms is required...'
					},
					identical: {
						field: 'new_password',
						message: 'The password and its confirm are not the same'
					}
                }
            }	
		}
	});	
});
</script>