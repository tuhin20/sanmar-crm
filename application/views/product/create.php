<?php $this->load->view('common/header.php');
//print_r($params);
?>

<script>


$(function() {
    //$("#issue_date").datepicker();
	
    //$("#expiry_date").datepicker();
	//$("#birth_date").datepicker();
   /* $("#datepicker4").datepicker();
	$("#datepicker5").datepicker();
    $("#datepicker6").datepicker();
	$("#datepicker7").datepicker();
	$("#datepicker8").datepicker();
	$("#datepicker9").datepicker();*/
	
	$( function() {
    $( "#issue_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  dateFormat: "yy-mm-dd"
    });
  } );
  
  $( function() {
    $( "#expiry_date" ).datepicker({
      changeMonth: true,
      changeYear: true,
	  dateFormat: "yy-mm-dd"
    });
  } );

	
});

$( function() {
    $( "#birth_date" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
  } );



</script>
<div class=" col-md-12"> 	<!-- this is alert size-->
	<?php 
		$return_value=$this->session->flashdata('return_value');
		if(isset($return_value)) 
		{
			//print_r($return_value);
			$msg=$return_value['msg'];
			if($return_value['success'] == true)
			{

				echo "<div class='alert alert-success' role='alert'>$msg</div>";

			}
			else if($msg!='') //if not success and msg not empty
			{
				echo "<div class='alert alert-danger' role='alert'>$msg</div>";
			}
		}
	?>
</div>
<h2 class="">Product/<small>Create</small></h2>
<hr>
<div class="row">

  
	<form class="form-horizontal" method="post" action="<?php echo $ui['action']; ?>" accept-charset="utf-8">
	<div class="col-md-6">
		<input type="hidden" name="id" value="<?php echo isset ($params['id'])? $params['id']:-1;  ?>">
  
		<!--  <h4 class="text-center">Information About the Contact</h4>-->
		  
		  
		<div class="form-group">
				<label class="control-label col-sm-5">Product Name</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="name" placeholder="Enter Product Name" name="name" value="<?php echo isset($params['name'])?$params['name']:'';?>">
			</div>
		</div>
		<div class="form-group">
				<label class="control-label col-sm-5">Product ID</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="code" placeholder="Enter Product ID" name="code" value="<?php echo isset($params['code'])?$params['code']:'';?>">
			</div>
		</div>
		<div class="form-group">
				<label class="control-label col-sm-5">Description</label>
			<div class="col-sm-7">
				<textarea type="text" autocomplete="off" class="form-control input-sm" id="description" placeholder="Enter Description" name="description"><?php echo isset($params['description'])?$params['description']:'';?></textarea>
			</div>
		</div>
		
		 <div class="form-group">        
			<div class="col-md-offset-5 col-sm-7">
				<button type="submit" class="btn btn-default btn-success"><?php echo $ui['okButton'];?></button>
			</div>
		</div>
	</div>
	<div class="col-md-6">
	
	<div class="form-group">
      <label class="control-label col-sm-5">Product Category</label>
      <div class="col-sm-7">
        <?php
		
			$categories=$this->prime_model->getByQuery('select distinct name from product_category order by name ');
			?>
			
		  


			<select class="form-control input-sm" type="text" name="category" id="category">
				<option value='' >--</option>
			<?php
				foreach($categories as $item)
			  {
				$selected='';
				if(isset($params['category']) && $item['name']==$params['category'])
					$selected='selected';
				$text=str_replace('_',' ',$item['name']);
				$value=$item['name'];
				echo "<option value='$value' $selected>$text</option>";
              }

             ?>
			</select>
      </div>
    </div>
	
	<div class="form-group">
				<label class="control-label col-sm-5">Unit Price</label>
			<div class="col-sm-7">
				<input type="text" autocomplete="off" class="form-control input-sm" id="unit_price" placeholder="Enter Unit Price" name="unit_price" value="<?php echo isset($params['unit_price'])?$params['unit_price']:'';?>">
			</div>
		</div>
		
	
	<!--  <div style="position:relative;">
	  <label class="control-label col-sm-5">Passport Image</label>
	  
		<a class='btn btn-primary' href='javascript:;'>
			Choose File...
			<input type="file" style='position:absolute;z-index:2;top:0;left:30;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
		</a>
		&nbsp;
		<span class='label label-info' id="upload-file-info"></span>
	</div>
	<br>
	
	
	<div style="position:relative;">
	  <label class="control-label col-sm-5">Contact Image</label>
	  
		<a class='btn btn-primary' href='javascript:;'>
			Choose File...
			<input type="file" style='position:absolute;z-index:2;top:0;left:30;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
		</a>
		&nbsp;
		<span class='label label-info' id="upload-file-info"></span>
	</div>-->
  
		
    
	
	
	
    
   
	
	
	</div>
	
	
	
  </form>
  
  
  
</div>  <!-- end of menu2 tab -->
    
      


                <?php $this->load->view('common/footer.php'); ?>
                     <script type="text/javascript">
					 $( "#expiry_date" ).change(function () {
						 var start = new Date($('#expiry_date').val());
						//var start = new Date("2018-09-29"),
						end   = new Date(); 
						diff  = new Date(start - end);
						days  = diff/1000/60/60/24;
						var remaining = Math.floor(days);
						var months = remaining/30;
						var month = Math.floor(months);
						var day = remaining%30;
						console.log(remaining);
						if(month>=0){
						$('#remaining_time').html("Passport will expire in "+month+" month "+day+" days");
						}
						else
						$('#remaining_time').html("Passport has expired");
							
						
						
					
					 });
					 
					 
					 
					 $(function () {						 
						$('#contact_fileupload').fileupload({
							dataType: 'json',
							done: function (e, data) {
								$.each(data.result.files, function (index, file) {
									console.log(file.name);
									$('<p/>').append(file.name).appendTo($('#contact_file_names'));
									$('#contact_file_names').append("<input type='hidden' name='contact_file_uploads[]' value='"+file.name+"'>");
									//$('#file_names').append(file.name).appendTo(document.body);
								});
							}
						});
					});
					
					$(function () {
						$('#passport_fileupload').fileupload({
							dataType: 'json',
							done: function (e, data) {
								$.each(data.result.files, function (index, file) {
									console.log(file.name);
									$('<p/>').append(file.name).appendTo($('#passport_file_names'));
									$('#passport_file_names').append("<input type='hidden' name='passport_file_uploads[]' value='"+file.name+"'>");
									//$('#file_names').append(file.name).appendTo(document.body);
								});
							}
						});
					});
					 
					 
					 function load_victims_thana_permanent(val){
						 $.ajax({
							 type: "POST",
							 url: "<?php echo site_url("incident_info/get_ctrl_victims_thana_permanent");?>",
							 data: {victims_district_permanent: val},
							 beforeSend: function(  ) {
								//$('#loading_modal').modal('toggle');
							 }
						   })
						   .done(function( data ) {
							   //console.log(data);
								var obj = $.parseJSON(data);
								//console.log(obj);
								$('#div_victims_thana_permanent').html(obj.content);
								
							
						   });
					 }
					 
					  function load_victims_thana_present(val){
						 $.ajax({
								 type: "POST",
								 url: "<?php echo site_url("incident_info/get_ctrl_victims_thana_present");?>",
								 data: {victims_district_present: val},
								 beforeSend: function(  ) {
									//$('#loading_modal').modal('toggle');
								 }
							   })
						   .done(function( data ) {
							   //console.log(data);
								var obj = $.parseJSON(data);
								//console.log(obj);
								$('#div_victims_thana_present').html(obj.content);
			
		
							});
					 }
					 
	$("#victims_district_permanent").change(function() {
	var val = $(this).val();
	console.log(val);
	load_victims_thana_permanent(val);
		
		
});

$("#victims_district_present").change(function() {
	var val = $(this).val();
	console.log(val);
	load_victims_thana_present(val);
		
		
});

$("#respondents_district_permanent").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_respondents_thana_permanent");?>",
		 data: {respondents_district_permanent: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_respondents_thana_permanent').html(obj.content);
			
		
	   });
		
		
});

$("#respondents_district_present").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_respondents_thana_present");?>",
		 data: {respondents_district_present: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_respondents_thana_present').html(obj.content);
			
		
	   });
		
		
});

 $("#complainants_district_permanent").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_complainants_thana_permanent");?>",
		 data: {complainants_district_permanent: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_complainants_thana_permanent').html(obj.content);
			
		
	   });
		
		
});

$("#complainants_district_present").change(function() {
	var val = $(this).val();
	console.log(val);
	$.ajax({
		 type: "POST",
		 url: "<?php echo site_url("incident_info/get_ctrl_complainants_thana_present");?>",
		 data: {complainants_district_present: val},
		 beforeSend: function(  ) {
			//$('#loading_modal').modal('toggle');
		 }
	   })
	   .done(function( data ) {
		   //console.log(data);
			var obj = $.parseJSON(data);
			//console.log(obj);
			$('#div_complainants_thana_present').html(obj.content);
			
		
	   });
		
		
});
$(document).ready(function() {
    //set initial state.
    //$('#textbox1').val($(this).is(':checked'));

    $('#victims_self').change(function() {
        if($(this).is(":checked")) {
             $('#victims_name').val($('#complainants_name').val());
			 $('#victims_fathers_name').val($('#complainants_fathers_name').val());
			 $('#victims_mothers_name').val($('#complainants_mothers_name').val());
			 $('#victims_spouses_name').val($('#complainants_spouses_name').val());
			 $('#victims_sex').val($('#complainants_sex').val());
			 $('#victims_age').val($('#complainants_age').val());
			 $('#victims_religion').val($('#complainants_religion').val());
			 $('#victims_village_permanent').val($('#complainants_village_permanent').val());
			 $('#victims_district_permanent').val($('#complainants_district_permanent').val());
			 load_victims_thana_permanent($('#complainants_district_permanent').val());
			 $("#victims_thana_permanent").selectmenu();
			 $("#victims_thana_permanent").selectmenu('refresh', true);
			 console.log($('#complainants_thana_permanent').val());
	         $('#victims_thana_permanent').val($('#complainants_thana_permanent').val());
	         $('#victims_phone_permanent').val($('#complainants_phone_permanent').val());
	         $('#victims_fax_permanent').val($('#complainants_fax_permanent').val());
	         $('#victims_email_permanent').val($('#complainants_email_permanent').val());
	         $('#victims_village_present').val($('#complainants_village_present').val());
	         $('#victims_district_present').val($('#complainants_district_present').val());
			 load_victims_thana_present($('#complainants_district_present').val());
			 console.log($('#complainants_thana_present').val());
			 $('#victims_thana_present').val($('#complainants_thana_present').val());
	         $('#victims_phone_present').val($('#complainants_phone_present').val());
	         $('#victims_fax_present').val($('#complainants_fax_present').val());
	         $('#victims_email_present').val($('#complainants_email_present').val());
			 
			 
			 
			 
			 
			 
			 
        }
            
    });
});

$("#victims_selected").click(function(){
	
	
	
	
});

        /*$(document).ready(function() {
            $('#create_user_form').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {

                                name: {
                                    validators: {
                                        notEmpty: {
                                                        message: 'name is empty...'
                                                }
                                    }
                                },
                                email: {
                                    validators: {
                                        email:  {
                                                        message: 'provide valid email'
                                                }
                                    }
                                }
                }
            });

        });*/
    </script>