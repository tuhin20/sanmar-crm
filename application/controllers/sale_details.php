<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class sale_details extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('sale_details_model');
        $this->load->model('client_sip_info_model');
        $this->load->model('prime_model');
        $this->load->model('user_manager_model');
        $this->load->helper(array('form', 'url'));
		//$this->checkAuthorization();
		
    }
	
	public function checkAuthorization()
        {
			
			
            //$current_user=$this->user_model->get_current_user();
            if(!$this->user_manager_model->is_logged_in()){
				//$this->session->set_userdata(array('redirect_after_login'=>site_url("ticket/search/$caller_number")));
                redirect('/login/index');
                return;
            }
            /*$hasPermission=$this->user_manager_model->has_permission_for_role($this->role_manager_model->See_isd_rate_chart);
            if(!$hasPermission){
                redirect('/login/index');
                return;
            }*/
        }

    public function create_sale(){
		if(!$this->user_manager_model->is_logged_in()){
				//$this->session->set_userdata(array('redirect_after_login'=>site_url("ticket/search/$caller_number")));
                redirect('/login/index');
                return;
            }
        $data['ui']=array('title'=>'Create','action'=>site_url('sale_details/save_sale'),'okButton'=>'Save');
        //$data['sale_info']=array();
        $data['child_view']=$this->load->view('controls/ctrl_select_customer', $data, TRUE);

        $this->load->view('sale_details/sale_details_form', $data );
        
    }
	public function CallAPI($method, $url, $data = false)
	{
		$curl = curl_init();

		switch ($method)
		{
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);

				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			case "PUT":
				curl_setopt($curl, CURLOPT_PUT, 1);
				break;
			default:
				if ($data)
					$url = sprintf("%s?%s", $url, http_build_query($data));
		}
		 
		// Optional Authentication:
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_USERPWD, "username:password");

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($curl);
		//print_r($result);
		curl_close($curl);

		return $result;
	}
	public function get_power_user(){
		$json=$this->CallAPI('GET', "http://192.168.0.8:96/api/EpglEmployee/GetBasicInfo", $data = false);
		//print_r($json);
		return json_decode($json);
	}
    public function import_power_user(){
		
		$json=$this->CallAPI('GET', "http://192.168.0.8:96/api/EpglEmployee/GetBasicInfo", $data = false);
		//print_r($json);
		$result= json_decode($json);
		$employees=array();
		foreach($result as $row){
			//EmployeeID":"0007","FullName":"Aftab Uddin Mondul","Designation":"Executive","Department":"Company Secretariat & Corporate Affairs --- Company Secretariat","Division":"Company Secretariat & Corporate Affairs","Branch":"DHK","Mobile":"01713335808","Email":"aftab.admin@energypac.com
			$employees[]=array('EmployeeID'=>$row->EmployeeID,'FullName'=>$row->FullName,'Designation'=>$row->Designation,'Department'=>$row->Department,'Division'=>$row->Division,'Branch'=>$row->Branch,'Mobile'=>$row->Mobile,'Email'=>$row->Email);
		}
		//print_r($employees);
		$this->prime_model->bulkInsert('employee',$employees);
	}
    public function sale_list(){
		$data['icx_list'] = $this->sale_details_model->get_sale_list();
		$this->load->view('sale_details/sale_details_form', $data);
    }
    
    
    public function create_customer(){
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        $data['ui']=array('title'=>'Create','action'=>site_url('sale_details/save_customer'),'okButton'=>'Save');
        $this->load->view('customer_details/customer_add_form', $data);

    }
    

    public function create_ticket(){
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
		$data['ui']= array('title'=>'Create','action'=> site_url('sale_details/save_ticket'),'okButton'=>'Save');	
		$data['child_view_division']=$this->load->view('controls/ctrl_select_division',$data,true);
		$data['child_view_product_category']=$this->load->view('controls/ctrl_select_product_category',$data,true);
		$sale_id=$this->input->get('sale_id');
        
		if($sale_id!=''){
			//$sale_id=$sale_id;
			$sale_info=$this->prime_model->getByID('sale','id',$sale_id);
			//print_r($sale_info);
			$data['params']=array('customer_name'=>$sale_info['customer_name'],
			'sale_id'=>$sale_info['id'],
			'customer_id'=>$sale_info['customer_id'],
			'contact_name'=>$sale_info['contact_name_primary'],
			'contact_number'=>$sale_info['mobile_phone_primary'],
			'address'=>$sale_info['address_head_office'],
			'brand_name'=>$sale_info['brand_name'],
			'product_model'=>$sale_info['product_model'],
			'product_capacity'=>$sale_info['product_capacity'],
			'equipment_serial_number'=>$sale_info['equipment_serial_number'],
			'engine_serial_number'=>$sale_info['engine_serial_number'],
			'delivery_date'=>$sale_info['delivery_date'],
			'commissioning_date'=>$sale_info['commissioning_date'],
			'warranty_start_date'=>$sale_info['warranty_start_date'],
			'warranty_end_date'=>$sale_info['warranty_end_date'],
			'mc_start_date'=>$sale_info['mc_start_date'],
			'mc_end_date'=>$sale_info['mc_end_date']
			
			);
			
		}
		
		$data['child_view_select_power_user']=$this->load->view('controls/ctrl_client',$data,true);
		//print_r($data);
        $this->load->view('ticket_details/ticket_add_form', $data);

    }
	
	
	public function create_service(){
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
		$data['ui']= array('title'=>'Create','action'=> site_url('sale_details/save_service'),'okButton'=>'Save');	
		$data['child_view_division']=$this->load->view('controls/ctrl_select_division',$data,true);
		$data['child_view_product_category']=$this->load->view('controls/ctrl_select_product_category',$data,true);
		$sale_id=$this->input->get('sale_id');
        
		if($sale_id!=''){
			//$sale_id=$sale_id;
			$sale_info=$this->prime_model->getByID('sale','id',$sale_id);
			//print_r($sale_info);
			$data['params']=array('customer_name'=>$sale_info['customer_name'],
			'sale_id'=>$sale_info['id'],
			'customer_id'=>$sale_info['customer_id'],
			'contact_name'=>$sale_info['contact_name_primary'],
			'contact_number'=>$sale_info['mobile_phone_site'],
			'address'=>$sale_info['address_site_office'],
			'brand_name'=>$sale_info['brand_name'],
			'product_model'=>$sale_info['product_model'],
			'product_capacity'=>$sale_info['product_capacity'],
			'chassis_number'=>$sale_info['chassis_number'],
			'engine_serial_number'=>$sale_info['engine_serial_number'],
			'delivery_date'=>$sale_info['delivery_date'],
			'warranty_start_date'=>$sale_info['warranty_start_date'],
			'warranty_end_date'=>$sale_info['warranty_end_date']
			
			
			);
			
		}
		
		$data['child_view_select_power_user']=$this->load->view('controls/ctrl_client',$data,true);
		//print_r($data);
        $this->load->view('service_details/service_add_form', $data);

    }
	
	public function save_service(){
		  if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $this->form_validation->set_rules('title','Ticket Title','trim|required');
        $data=array('ui'=>array('title'=>'Create','action'=>site_url('sale_details/save_service'),'okButton'=>'Save')
                    ,'params'=>$params
            );
        if($this->form_validation->run()){
            $return_value= $this->sale_details_model->save_ticket($params); 
                        
            $this->session->set_flashdata('return_value', $return_value);
            redirect('sale_details/create_ticket'); //for clearing input fields [stackoverflow :) ] 
        }        
        $this->load->view('ticket_details/ticket_add_form', $data);
        
          
          }
    
    public function create_faq(){
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        $data['ui']=array('title'=>'Create','action'=>site_url('sale_details/save_faq'),'okButton'=>'Save');
        $this->load->view('faq_details/faq_add_form', $data);

    }
	
	
	public function create_sales_feedback(){
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        $data['ui']=array('title'=>'Create','action'=>site_url('sale_details/save_sales_feedback'),'okButton'=>'Save');
		
		$sale_id=$this->input->get('sale_id');
        
		if($sale_id!=''){
			//$sale_id=$sale_id;
			$sale_info=$this->prime_model->getByID('sale','id',$sale_id);
			//print_r($sale_info);
			$data['params']=array('customer_name'=>$sale_info['customer_name'],
			'sale_id'=>$sale_info['id'],
			'customer_name'=>$sale_info['customer_name'],
			'product_name'=>$sale_info['product_name'],
			'contact_site_mobile'=>$sale_info['mobile_phone_site']
			
			);
			
		}
        $this->load->view('feedback_details/feedback_add_form', $data);

    }
	
	public function save_sales_feedback(){
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        
         $data=array('ui'=>array('title'=>'Create','action'=>site_url('sale_details/save_sales_feedback'),'okButton'=>'Save')
                    ,'params'=>$params
            );
         
             $return_value= $this->sale_details_model->save_sales_feedback($params);
             $this->session->set_flashdata('return_value', $return_value);
             redirect('sale_details/create_sales_feedback');
           
         $this->load->view('feedback_details/feedback_add_form', $data);
    }
	
	 public function show_sales_feedback()
    {
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        $data['raw'] = $this->sale_details_model->get_all_sales_feedback_details();

        $this->load->view('feedback_details/feedback_details_show', $data);
    }
	
	
	public function view_sales_feedback_details($id){
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');
        $data['sales_feedback_info']=$this->sale_details_model->get_sales_feedback_by_id($id);
        $this->load->view('feedback_details/feedback_view_details',$data);
		


    }
	
	
	
	public function edit_sales_feedback_info($id){


        $data['ui']=array('title'=>'Edit','action'=>site_url("sale_details/update_sales_feedback_info/$id"),'okButton'=>'Update');

        $data['params']=$this->sale_details_model->get_sales_feedback_by_id($id);

        $this->load->view('feedback_details/feedback_add_form',$data);
        
    }
    
    public function update_sales_feedback_info($id){
        //$this->form_validation->set_rules();
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
		print_r($params);
        if($this->input->post('submit')){
            //$client=$this->client_manager_model->refreshObject($params);
            $return_value = $this->sale_details_model->save_sales_feedback($params);

            $this->session->set_flashdata('return_value', $return_value);
            redirect('sale_details/edit_sales_feedback_info/'.$id); //for clearing input fields [stackoverflow :) ]
			
        }
        else{
            $data['ui']=array('title'=>'Edit','action'=>site_url("sale_details/update_sales_feedback_info/$id"),'okButton'=>'Update');
            
            $data['params']=$params;
            $this->load->view('feedback_details/feedback_add_form', $data);

        }
    }
	
	public function show_query_report()
    {
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
		$params = $this->security->xss_clean($this->input->post(NULL, TRUE));
		$data=array('ui'=>array('title'=>'Create','action'=>site_url('sale_details/show_query_report'))
                    ,'params'=>$params
            );
		$result=	$this->sale_details_model->get_all_query_report_details($params);
        $data['raw'] = $result['raw'];
        $data['query_id'] = $result['query_id'];
		

        $this->load->view('query_report/query_report_details_show', $data);
    }
	
	public function download_query_report(){
			$query_id=$this->uri->segment(3);	
			echo "query id:".$query_id;
            $this->sale_details_model->download_query_report($query_id);
        }
    
    public function save_sale(){
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        //print_r($params);
        
        $data=array('ui'=>array('title'=>'Create','action'=>site_url('sale_details/save_sale'),'okButton'=>'Save')
                    ,'params'=>$params
            );
        
            $return_value= $this->sale_details_model->save_sale($params); 
                        
            $this->session->set_flashdata('return_value', $return_value);
            redirect('sale_details/create_sale'); //for clearing input fields [stackoverflow :) ] 
         
        $this->load->view('sale_details/sale_details_form', $data);
    }
    
    public function save_faq(){
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $this->form_validation->set_rules('question','Question','trim|required');
         $data=array('ui'=>array('title'=>'Create','action'=>site_url('sale_details/save_faq'),'okButton'=>'Save')
                    ,'params'=>$params
            );
         if($this->form_validation->run()){
             $return_value= $this->sale_details_model->save_faq($params);
             $this->session->set_flashdata('return_value', $return_value);
             redirect('sale_details/create_faq');
             }
         $this->load->view('faq_details/faq_add_form', $data);
    }
    
    public function show_faq()
    {
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        $data['raw'] = $this->sale_details_model->get_all_faq_details();

        $this->load->view('faq_details/faq_details_show', $data);
    }
    
    public function view_faq_details($id){
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');
        $data['faq_info']=$this->sale_details_model->get_faq_by_id($id);
        $this->load->view('faq_details/faq_view_details',$data);


    }
    
    public function edit_faq_info($id){


        $data['ui']=array('title'=>'Edit','action'=>site_url("sale_details/update_faq_info/$id"),'okButton'=>'Update');

        $data['params']=$this->sale_details_model->get_faq_by_id($id);

        $this->load->view('faq_details/faq_add_form',$data);
        
    }
    
    public function update_faq_info($id){
        $this->form_validation->set_rules('question','Question','trim|required');
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
        if($this->form_validation->run()){
            //$client=$this->client_manager_model->refreshObject($params);
            $return_value = $this->sale_details_model->save_faq($params);

            $this->session->set_flashdata('return_value', $return_value);
            redirect('sale_details/edit_faq_info/'.$id); //for clearing input fields [stackoverflow :) ]
        }
        else{
            $data['ui']=array('title'=>'Edit','action'=>site_url("sale_details/update_faq_info/$id"),'okButton'=>'Update');
            
            $data['params']=$params;
            $this->load->view('faq_details/faq_add_form', $data);

        }
    }
	
	public function edit_ticket_info($id){


        $data['ui']=array('title'=>'Edit','action'=>site_url("sale_details/update_ticket_info/$id"),'okButton'=>'Update');

        $params=$this->sale_details_model->get_ticket_by_id($id);
		$params['selected_employees']=explode(",",$params['assigned_user_id']);
		$data['params']=$params;
		$data['child_view_select_power_user']=$this->load->view('controls/ctrl_client',$data,true);
		$data['child_view_division']=$this->load->view('controls/ctrl_select_division',$data,true);
		$data['child_view_product_category']=$this->load->view('controls/ctrl_select_product_category',$data,true);

        $this->load->view('ticket_details/ticket_add_form',$data);
        
    }
	
	public function update_ticket_info($id){
        $this->form_validation->set_rules('title','Ticket Title','trim|required');
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
        if($this->form_validation->run()){
            //$client=$this->client_manager_model->refreshObject($params);
            $return_value = $this->sale_details_model->save_ticket($params);

            $this->session->set_flashdata('return_value', $return_value);
            redirect('sale_details/edit_ticket_info/'.$id); //for clearing input fields [stackoverflow :) ]
        }
        else{
            $data['ui']=array('title'=>'Edit','action'=>site_url("sale_details/update_ticket_info/$id"),'okButton'=>'Update');
            
            $data['params']=$params;
            $this->load->view('ticket_details/ticket_add_form', $data);

        }
    }

    
    
    public function show_sale_info()
    {
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
		$primary_number=$this->uri->segment(3);
		if($primary_number!=''){
			$data['search_info']="Search result of sales against phone: $primary_number";
			$data['code'] = $this->sale_details_model->get_sale_by_primary_number($primary_number);
		}
		else
			$data['code'] = $this->sale_details_model->get_all_sale_details();
		
        $this->load->view('sale_details/sale_details_show', $data);
    }
      public function save_customer(){
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $this->form_validation->set_rules('customer_name','Customer Name','trim|required');
        $data=array('ui'=>array('title'=>'Create','action'=>site_url('sale_details/save_customer'),'okButton'=>'Save')
                    ,'params'=>$params
            );
        if($this->form_validation->run()){
            $return_value= $this->sale_details_model->save_customer($params); 
                        
            $this->session->set_flashdata('return_value', $return_value);
            redirect('sale_details/create_customer'); //for clearing input fields [stackoverflow :) ] 
        }        
        $this->load->view('customer_details/customer_add_form', $data);
        
        
        }

    public function show_customer_info()
    {
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        $data['raw'] = $this->sale_details_model->get_all_customer_details();

        $this->load->view('customer_details/customer_show', $data);
    }
    
    public function show_ticket_info()
    {
		if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
		if(!$this->user_manager_model->has_permission_for_role($this->role_manager_model->View_ticket_list)){
			echo "You don't have permission to view all tickets";
			return;
		}
        $data['raw'] = $this->sale_details_model->get_all_ticket_details();
        
        $this->load->view('ticket_details/ticket_details_show', $data);
    }
    
      public function save_ticket(){
		  if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $this->form_validation->set_rules('title','Ticket Title','trim|required');
        $data=array('ui'=>array('title'=>'Create','action'=>site_url('sale_details/save_ticket'),'okButton'=>'Save')
                    ,'params'=>$params
            );
        if($this->form_validation->run()){
            $return_value= $this->sale_details_model->save_ticket($params); 
                        
            $this->session->set_flashdata('return_value', $return_value);
            redirect('sale_details/create_ticket'); //for clearing input fields [stackoverflow :) ] 
        }        
        $this->load->view('ticket_details/ticket_add_form', $data);
        
          
          }
		  
		  public function save_ticket_feedback(){
			  if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
			  $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
			  $ticket_id=$params['ticket_id'];
			  $this->form_validation->set_rules('feedback','Feedback','trim|required');
			  $data=array('ui'=>array('action'=>site_url('sale_details/save_ticket_feedback'))
                    ,'params'=>$params
            );
             if($this->form_validation->run()){
             $return_value= $this->sale_details_model->save_ticket_feedback($params);
             $this->session->set_flashdata('return_value', $return_value); 
			 
             redirect("sale_details/view_ticket_details/$ticket_id");
             }
		 $this->load->view("ticket_details/ticket_view_details/$ticket_id", $data);
		  }

    public function view_sale_details($id){
        //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');
		
        $data['client_info']=$this->sale_details_model->get_sale_by_id($id);
        $this->load->view('sale_details/view_details',$data);


    }

    public function view_customer_details($customer_id){
        //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');
        $data['customer_info']=$this->sale_details_model->get_customer_by_id($customer_id);
        $this->load->view('customer_details/customer_view_details',$data);
        

    }
	
	 public function view_ticket_details($id){
		 
		if(!$this->user_manager_model->is_logged_in()){
				$this->session->set_userdata(array('redirect_after_login'=>site_url("sale_details/view_ticket_details/$id")));
                redirect('/login/index');
                return;
           }
		$current_user=$this->user_manager_model->get_current_user();
		
			
		$data['params']=array('ticket_id'=>$id);
        $data['ticket_info']=$this->sale_details_model->get_ticket_by_id($id);
		if($current_user['group_id']==23){
			$selected_employees=explode(",",$data['ticket_info']['assigned_user_id']);
			if(! in_array($current_user['email'], $selected_employees)){
				echo "This ticket is not assigned to you";
				return;
			}
		}
		$data['feedback_info']=$this->sale_details_model->get_feedback_by_id($id);
        $this->load->view('ticket_details/ticket_view_details',$data);


    }
    public function edit_sale_info($id){


        $data['ui']=array('title'=>'Edit','action'=>site_url("sale_details/update_sale_info/$id"),'okButton'=>'Update');

        $data['params']=$this->sale_details_model->get_sale_by_id($id);

        $this->load->view('sale_details/sale_details_form',$data);
        
       }
    public function update_sale_info($id){
        $this->form_validation->set_rules('customer_name','Customer Name','trim|required');
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
        if($this->form_validation->run()){
            //$client=$this->client_manager_model->refreshObject($params);
            $return_value = $this->sale_details_model->save_sale($params);

            $this->session->set_flashdata('return_value', $return_value);
            redirect('sale_details/edit_sale_info/'.$id); //for clearing input fields [stackoverflow :) ]
        }
        else{
            $data['ui']=array('title'=>'Edit','action'=>site_url("sale_details/update_sale_info/$id"),'okButton'=>'Update');
            
            $data['params']=$params;
            $this->load->view('sale_details/sale_details_form', $data);

        }
    }
    
    public function edit_customer_info($customer_id){


        $data['ui']=array('title'=>'Edit','action'=>site_url("sale_details/update_customer_info/$customer_id"),'okButton'=>'Update');

        $data['params']=$this->sale_details_model->get_customer_by_id($customer_id);

        $this->load->view('customer_details/customer_add_form',$data);
        
    }
    public function get_customer_by_id(){
        $customer_id= $this->input->post('customer_id');
        $customer=$this->sale_details_model->get_customer_by_id($customer_id);         
        print_r(json_encode($customer, JSON_HEX_QUOT | JSON_HEX_TAG)) ;        
    }
	public function get_ctrl_product_category(){
		$params=array();
		//echo "division: ".$this->input->post('division');
		$params['division']=$this->input->post('division');
        $data['params']=$params;
		
		ob_start();
        $this->load->view('controls/ctrl_select_product_category', $data);
        $content=ob_get_clean();
        $arr_data = array('success' => true,'content' => $content);
        print_r(json_encode($arr_data, JSON_HEX_QUOT | JSON_HEX_TAG)) ;  
    }
       
       
    public function update_customer_info($customer_id){
        $this->form_validation->set_rules('customer_name','Customer Name','trim|required');
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
        if($this->form_validation->run()){
            //$client=$this->client_manager_model->refreshObject($params);
            $return_value = $this->sale_details_model->save_customer($params);

            $this->session->set_flashdata('return_value', $return_value);
            redirect('sale_details/edit_customer_info/'.$customer_id); //for clearing input fields [stackoverflow :) ]
        }
        else{
            $data['ui']=array('title'=>'Edit','action'=>site_url("sale_details/update_customer_info/$customer_id"),'okButton'=>'Update');
            
            $data['params']=$params;
            $this->load->view('customer_details/customer_add_form', $data);

        }
    }


/*public function setValidation($type){
        //without this set_value('field_name') will not work [stackoverflow 100% right]
        if($type=='create'){
                $this->form_validation->set_rules('code','Code','trim|required|is_unique[client.code]');
                $this->form_validation->set_rules('web_name','Web Name','trim|required|is_unique[client.web_name]');
        }
        else{
                $this->form_validation->set_rules('code','Code','trim|required|callback_is_unique_for_edit');
                $this->form_validation->set_message('is_unique_for_edit','Code must be unique.');

                $this->form_validation->set_rules('web_name','Web Name','trim|required|callback_is_unique_web_name_for_edit');
                $this->form_validation->set_message('is_unique_web_name_for_edit','Web name must be unique.');
        }
        $this->form_validation->set_rules('name','Name','required');

        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_rules('pulse_local','Local Pulse','required');
        $this->form_validation->set_rules('call_rate_local','Local Call Rate','required');
        $this->form_validation->set_rules('pulse_isd','ISD Pulse','required');
    }*/

    
    public function ajax_edit($id)
    {
        $data = $this->sale_details_model->get_by_id($id);


        echo json_encode($data);

    }

    public function short_code_info_update()
    {
        echo "Updated";
    }

    public function short_code_info_delete($id)
    {
        $this->sale_details_model->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	 public function faq_info_delete($id)
    {
        $this->sale_details_model->delete_by_id_faq($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function customer_info_delete($customer_id)
    {
        $this->sale_details_model->delete_by_id_customer($customer_id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function sales_feedback_info_delete($id)
    {
        $this->sale_details_model->delete_by_id_sales_feedback($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function sale_info_delete($id)
    {
        $this->sale_details_model->delete_by_id_sale($id);
        echo json_encode(array("status" => TRUE));
    }











}
