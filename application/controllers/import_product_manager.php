<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class import_product_manager extends CI_Controller {
	
	public function __construct() {
		parent::__construct ();
                $this->load->library('form_validation');
                $this->load->model('prime_model');
		        $this->load->model ( 'import_product_model' );
                $this->load->model('user_model');
                $this->load->model('role_model');
		//$this->checkAuthorization();	
	}	
        public function checkAuthorization()
        {
            //$current_user=$this->user_model->get_current_user();
            if(!$this->user_manager_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
            $hasPermission=$this->user_manager_model->has_permission_for_role($this->role_manager_model->See_isd_rate_chart);
            if(!$hasPermission){
                redirect('/login/index');
                return;
            }
        }
	public function setValidation(){
            $this->form_validation->set_rules('name','Name','required');
            $this->form_validation->set_rules('prefix','Prefix','required');
            $this->form_validation->set_rules('rate','Rate','required');

        }
	public function create_isd_rate_chart(){
            $data['ui']=array('title'=>'Create','action'=>site_url('isd_rate_chart_manager/save_isd_rate_chart'),'okButton'=>'Save');
            $data['isd_rate_chart_info']=array();
            $this->load->view('isd_rate_chart/create_isd_rate_chart',$data);
	}
        public function save_isd_rate_chart(){
            $this->setValidation();
            $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
            if($this->form_validation->run()){
		$return_value= $this->import_product_model->save_isd_rate_chart($params); 
                $this->session->set_flashdata('return_value', $return_value);
                redirect(current_url()); //for clearing input fields [stackoverflow :) ]
            }
            $data['ui']=array('title'=>'Create','action'=>site_url('isd_rate_chart_manager/save_isd_rate_chart'),'okButton'=>'Save');
            $this->load->view('isd_rate_chart/create_isd_rate_chart', $data);
	}
	
	public function isd_rate_chart_list(){
		$data['isd_rate_chart_list'] = $this->import_product_model->get_isd_rate_chart_list();
		$this->load->view('isd_rate_chart/isd_rate_chart_list', $data);
	}
    public function download_isd_rate_chart(){
            $this->import_product_model->download_isd_rate_chart();
        }
    public function import_product(){
		if(!$this->user_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
		$this->load->view('product/import_product');
	}
	
	public function save_imported_product(){
		$params = $this->security->xss_clean($this->input->post(NULL, TRUE));
		/*$params['username'] = $this->session->userdata('user_name');
		$params['password'] = $this->session->userdata('password');*/
		
		
		$config['upload_path'] = './csv_files';
		$config['allowed_types'] = 'csv';
		$new_name = time().'_'.str_replace(' ', '_', $_FILES["isd_file"]['name']) ;
		$config['file_name'] = $new_name;
		$config['max_size']	= '30000';
		$config['max_width']  = '50000';
		$config['max_height']  = '50000';
		$config['max_filename'] = '200';
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
                //$data['params']=$params; //for giving back user input data
		if($this->upload->do_upload('isd_file')){
			$this->load->library('CSVReader');
			$file = fopen($config['upload_path'].'/'.$new_name,"r");
			$result=array();
			while(! feof($file)){
				$result[]=fgetcsv($file);
			}

			//$result=fgetcsv($file);
			fclose($file);
			//$result = $this->csvreader->parse_file($config['upload_path'].'/'.$new_name);			
			$data['return_value'] = $this->import_product_model->save_imported_product( $result);	
		}
		else{
			$data['return_value'] = array('success'=>false,'msg'=>$this->upload->display_errors());
		}
		//print_r($result);
		$this->load->view('product/import_product',$data);
	}
	
	public function edit_isd_rate_chart($id){
            $data['ui']=array('title'=>'Edit','action'=>site_url("isd_rate_chart_manager/update_isd_rate_chart/$id"),'okButton'=>'Update');
            $data['isd_rate_chart_info']=$this->import_product_model->get_isd_rate_chart_by_id($id);
            $data['ctrl_select_datas']['selected_isd_rate_chart_type']=$data['isd_rate_chart_info']['type'];
            $this->load->view('isd_rate_chart/create_isd_rate_chart',$data);
            
            
	}
	
	public function update_isd_rate_chart($id){
            $this->setValidation();
            $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
            if($this->form_validation->run()){
		
                $return_value = $this->import_product_model->save_isd_rate_chart($params);
                $this->session->set_flashdata('return_value', $return_value);
                redirect('isd_rate_chart_manager/edit_isd_rate_chart/'.$id); 
                
            }
            else{
                $data['ui']=array('title'=>'Edit','action'=>site_url("isd_rate_chart_manager/update_isd_rate_chart/$id"),'okButton'=>'Update');
                $data['isd_rate_chart_info']['id']=$params['id'];
                $data['ctrl_select_datas']['selected_isd_rate_chart_type']=$params['isd_rate_chart_type'];
                $this->load->view('isd_rate_chart/create_isd_rate_chart', $data);
            }
	}
        
	public function delete_isd_rate_chart($id){
		echo $this->import_product_model->delete_isd_rate_chart($id);
	}
        /*public function deleteSelected(){
            $selectedIDs=$this->input->post('selectedIDs');
            $this->prime_model->executeQuery("delete from groups where id in($selectedIDs)");
        }*/
}