<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class product extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('product_model');
        $this->load->model('prime_model');
        $this->load->model('user_model');
        $this->load->helper(array('form', 'url'));
		$this->checkAuthorization();
		
    }
	
	public function checkAuthorization()
        {
			
			
            //$current_user=$this->user_model->get_current_user();
            if(!$this->user_model->is_logged_in()){
				//$this->session->set_userdata(array('redirect_after_login'=>site_url("ticket/search/$caller_number")));
                redirect('/login/index');
                return;
            }
            /*$hasPermission=$this->user_model->has_permission_for_role($this->role_manager_model->See_isd_rate_chart);
            if(!$hasPermission){
                redirect('/login/index');
                return;
            }*/
        }

    public function create(){
		if(!$this->user_model->is_logged_in()){
				//$this->session->set_userdata(array('redirect_after_login'=>site_url("ticket/search/$caller_number")));
                redirect('/login/index');
                return;
            }
			
		//$primary_number=$this->uri->segment(3);
        $data['ui']=array('title'=>'Create','action'=>site_url('product/save'),'okButton'=>'Save');
		//$data['params']=array('mobile'=>$primary_number);
        //$data['sale_info']=array();
        //$data['child_view']=$this->load->view('controls/ctrl_select_customer', $data, TRUE);

        $this->load->view('product/create', $data );
        
    }
	
	public function save(){
		if(!$this->user_model->is_logged_in()){
			redirect('/login/index');
			return;
		}
		
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $this->form_validation->set_rules('name','Name','trim|required');
        //$this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_add');
		//$this->form_validation->set_message('is_unique_mobile_for_add','Mobile Number must be unique.');
		
        $data=array('ui'=>array('title'=>'Create','action'=>site_url('product/save'),'okButton'=>'Save')
                    ,'params'=>$params
            );
        if($this->form_validation->run()){
            $return_value= $this->product_model->save($params); 
                        
            $this->session->set_flashdata('return_value', $return_value);
            redirect('product/create'); //for clearing input fields [stackoverflow :) ] 
        }        
        $this->load->view('product/create', $data);        
          
	}
	function is_unique_mobile_for_add($str){
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->contact_model->is_mobile_already_exist('add',$id=(int)$this->input->post('id'),$str)){
            return false;
        }
        else return true;
    }
	
	/*public function search(){
		$primary_number=$this->input->post('primary_number');
		$conditions='';
		$query_id=0;
		
		$sql="select * from product where 1 ";
		if($primary_number!=''){
			$conditions .=" and ( mobile like '%$primary_number%' or office_phone like '%$primary_number%' or home_phone like '%$primary_number%' ) ";
		}
		if(strlen($conditions)> 0){
			$sql .=$conditions;
			$query_id=$this->prime_model->insert("query",array('value'=>$conditions));
		}
		$record_count=$this->get_count($sql);
		echo json_encode(array('record_count'=>$record_count,'query_id'=>$query_id,'conditions'=>$primary_number));
    }*/
	
	public function get_count($sql){
		$query = $this->db->query($sql);
		return $query->num_rows($query);
	}
	
	public function process_paging(){
		$sql="select * from product where 1 ";
		
		$conditions='';
		$query_id=$this->input->post('query_id');
		if($query_id>0){
			$temp=$this->prime_model->getByID('query','id',$query_id);
			$conditions=$temp['value'];
			$sql .=$conditions;
		}
		$recordsTotal=$this->get_count($sql);
		$recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
		
		$draw=$this->input->post('draw');
		$search=$this->input->post('search');
		$start=$this->input->post('start');
		$length=$this->input->post('length');
		
		if($search['value']!=''){
			$value=$search['value'];
			$sql .=" and ( name like '%$value%' or category like '%$value%' or code like '%$value%' or unit_price like '%$value%' or description like '%$value%')";
			$recordsFiltered=$this->get_count($sql);
		}
		
		//for getting data with limit
		$sql .=" order by id limit $start,$length";
		
		$contacts=$this->prime_model->getByQuery($sql) ;
		$output=array();
		$i=$start+1;
		foreach($contacts as $item){
			//buttons
			$btn_details="<a href='". site_url('product/view_product_details/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;
			
			$btn_edit="<a href='". site_url('product/edit/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;
			
			$btn_delete="<a href='". site_url('product/delete/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Edit\"></a>" ;

                                    //echo '<a href='. site_url().'/contact/edit/'.$row->id.' class="btn btn-warning glyphicon glyphicon-pencil" title="Edit"></a>';


                                    //echo '<a href='. site_url().'/contact/delete/'.$row->id.' class="btn btn-danger glyphicon glyphicon-trash delete" title="Delete Info"></a>';
			//end of buttons
			
			$output[]=array($i,$item['name'],$item['category'],$item['code'],$item['unit_price']," $btn_details $btn_edit $btn_delete");
			$i++;
		}
		$json_data = array(
					 "draw"            => $draw,   
					 "recordsTotal"    => $recordsTotal ,  
					 "recordsFiltered" => $recordsFiltered,
					 "data"            => $output   // total data array
					 );
		echo json_encode($json_data);
    }
	public function process_ctrl_paging(){
		$sql="select * from product where 1 ";
		
		$conditions='';
		$query_id=$this->input->post('query_id');
		if($query_id>0){
			$temp=$this->prime_model->getByID('query','id',$query_id);
			$conditions=$temp['value'];
			$sql .=$conditions;
		}
		$recordsTotal=$this->get_count($sql);
		$recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
		
		$draw=$this->input->post('draw');
		$search=$this->input->post('search');
		$start=$this->input->post('start');
		$length=$this->input->post('length');
		
		if($search['value']!=''){
			$value=$search['value'];
			$sql .=" and ( name like '%$value%' or category like '%$value%' or code like '%$value%' or unit_price like '%$value%' or description like '%$value%')";
			$recordsFiltered=$this->get_count($sql);
		}
		
		//for getting data with limit
		$sql .=" order by id  limit $start,$length";
		
		$contacts=$this->prime_model->getByQuery($sql) ;
		$output=array();
		$i=$start+1;
		foreach($contacts as $item){
			$code=$item['code'];
			$name=$item['name'];
			$output[]=array($i,"<a href='#' class='select_product' product_code='$code' product_name='$name' title='select'>$name</a>",$item['category'],$item['code'],$item['unit_price']);
			$i++;
		}
		$json_data = array(
					 "draw"            => $draw,   
					 "recordsTotal"    => $recordsTotal ,  
					 "recordsFiltered" => $recordsFiltered,
					 "data"            => $output   // total data array
					 );
		echo json_encode($json_data);
    }
	
	public function index(){
		
		$this->load->view('product/product_list');
	}
	
	public function duplicate(){
		
		$data['raw'] = $this->contact_model->get_duplicate_contact_details();
		
		$this->load->view('contact/duplicate_list',$data);
		
	}
	
	 public function view_product_details($id){
        //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');
		
        $data['product_info']=$this->product_model->get_product_by_id($id);
        $this->load->view('product/view_details',$data);


    }	
    
  
	
	public function edit($id){

        $data['ui']=array('title'=>'Edit','action'=>site_url("product/update/$id"),'okButton'=>'Update');

        $data['params']=$this->product_model->get_product_by_id($id);

        $this->load->view('product/create',$data);
        
    }
    
    public function update($id){
        $this->form_validation->set_rules('product_name','Product Name','trim|required');
		//$this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_edit');
		//$this->form_validation->set_message('is_unique_mobile_for_edit','Mobile Number must be unique.');
		
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
        if($this->form_validation->run()){
            //$client=$this->client_manager_model->refreshObject($params);
            $return_value = $this->product_model->save($params);

            $this->session->set_flashdata('return_value', $return_value);
            redirect('product/edit/'.$id); //for clearing input fields [stackoverflow :) ]
        }
        else{
            $data['ui']=array('title'=>'Edit','action'=>site_url("product/update/$id"),'okButton'=>'Update');
            
            $data['params']=$params;
            $this->load->view('product/create', $data);

        }
    }
	
	function is_unique_mobile_for_edit($str)
    {
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->contact_model->is_mobile_already_exist('edit',$id=(int)$this->input->post('id'),$str)){
            return false;
        }
        else return true;
    }
	
	
	public function delete($id)
    {
        $this->product_model->delete_by_id($id);
        echo "1";
    }
	
	public function import_product(){
		if(!$this->user_model->is_logged_in()){
                redirect('/login/index');
                return;
            }
		$this->load->view('product/import_product');
	}
	
	public function save_imported_product(){
		$params = $this->security->xss_clean($this->input->post(NULL, TRUE));
		/*$params['username'] = $this->session->userdata('user_name');
		$params['password'] = $this->session->userdata('password');*/
		
		
		$config['upload_path'] = './csv_files';
		$config['allowed_types'] = 'csv';
		$new_name = time().'_'.str_replace(' ', '_', $_FILES["isd_file"]['name']) ;
		$config['file_name'] = $new_name;
		$config['max_size']	= '30000';
		$config['max_width']  = '50000';
		$config['max_height']  = '50000';
		$config['max_filename'] = '200';
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		
                //$data['params']=$params; //for giving back user input data
		if($this->upload->do_upload('isd_file')){
			$this->load->library('CSVReader');
			$file = fopen($config['upload_path'].'/'.$new_name,"r");
			$result=array();
			while(! feof($file)){
				$result[]=fgetcsv($file);
			}

			//$result=fgetcsv($file);
			fclose($file);
			//$result = $this->csvreader->parse_file($config['upload_path'].'/'.$new_name);			
			$data['return_value'] = $this->product_model->save_imported_product( $result);	
		}
		else{
			$data['return_value'] = array('success'=>false,'msg'=>$this->upload->display_errors());
		}
		//print_r($result);
		$this->load->view('product/import_product',$data);
	}
	
	 public function download_product_chart(){
            $this->product_model->download_product_chart();
        }

}
