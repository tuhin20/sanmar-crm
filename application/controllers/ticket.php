<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ticket extends CI_Controller {
	
    public function __construct() {
        parent::__construct ();
        $this->load->library('form_validation');
        $this->load->model('prime_model');
        $this->load->model ('ticket_model');
        $this->load->model('user_model');
        $this->load->model('role_model');
        $this->checkAuthorization();	
        $this->load->helper(array('form', 'url'));
        $this->load->library("session");
        
    }	
    public function checkAuthorization()
    {
        //$current_user=$this->user_model->get_current_user();
        if(!$this->user_model->is_logged_in()){
            redirect('/login/index');
            return;
        }
        $hasPermission=$this->user_model->has_permission_for_role($this->role_model->View_ticket_list);
        if(!$hasPermission){
            redirect('/login/index');
            return;
        }
    }
	
    public function success()
    {
      $this->session->set_flashdata('success', 'customer add successfully');
      return $this->load->view('sanmar.customer');
    }

  public function error()
  {
      $this->session->set_flashdata('error', 'Something is wrong.');
      return $this->load->view('myPages');
  }

  public function warning()
  {
      $this->session->set_flashdata('warning', 'Something is wrong.');
      return $this->load->view('myPages');
  }

  public function info()
  {
      $this->session->set_flashdata('info', 'User listed bellow');
      return $this->load->view('myPages');
  }

    public function setValidation($type){
        //without this set_value('field_name') will not work [stackoverflow 100% right]
        if($type=='create'){
                $this->form_validation->set_rules('code','Code','trim|required|is_unique[client.code]');
                $this->form_validation->set_rules('web_name','Web Name','trim|required|is_unique[client.web_name]');
        }
        else{
                $this->form_validation->set_rules('code','Code','trim|required|callback_is_unique_for_edit');
                $this->form_validation->set_message('is_unique_for_edit','Code must be unique.');

                $this->form_validation->set_rules('web_name','Web Name','trim|required|callback_is_unique_web_name_for_edit');
                $this->form_validation->set_message('is_unique_web_name_for_edit','Web name must be unique.');
        }
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_rules('pulse_local','Local Pulse','required');
        $this->form_validation->set_rules('call_rate_local','Local Call Rate','required');
        $this->form_validation->set_rules('pulse_isd','ISD Pulse','required');
    }
    function is_unique_for_edit($str)
    {
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->ticket_model->is_code_already_exist($id=(int)$this->input->post('id'),$str))
        {
            return false;
        }
        else return true;
    }
    function is_unique_web_name_for_edit($str)
    {
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->ticket_model->is_web_name_already_exist($id=(int)$this->input->post('id'),$str))
        {
            return false;
        }
        else return true;
    }
	public function is_client_created_by_current_user($client_id){
		$client_info=$this->ticket_model->get_client_by_id($client_id);
        if(sizeof($client_info)==0)
            return false;
		$current_user=$this->user_manager_model->get_current_user();
		if($client_info['created_by']==$current_user['id']){
			return true;
		}
		return false;
	}
    
    public function create(){
        $data['ui']=array('title'=>'Create','action'=>site_url('ticket/save'),'okButton'=>'Save');
        $data['ctrl_data']=array('contact_id'=>3804,'product_category'=>'EG 2 White Switches & Sockets');
		
        $this->load->view('ticket/create',$data);
        
    }
    public function save(){       
        $this->setValidation('create');
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
        if($this->form_validation->run()){
            $return_value = $this->ticket_model->save($params);
            $this->session->set_flashdata('return_value', $return_value);
            redirect(current_url()); //for clearing input fields [stackoverflow :) ]
        }
        if(isset($params['client_type'])){
            $data['ctrl_select_datas'] =array('selected_client_type'=>$params['client_type']); //when form validation failed dropdown input is given back 
        }
        
        $data['ui']=array('title'=>'Create','action'=>site_url('ticket/save_client'),'okButton'=>'Save');
        $this->load->view('client/create_client', $data);
    }

    public function index(){
            $data['client_list'] = $this->ticket_model->get_client_list();
            $this->load->view('client/client_list', $data);
    }
	public function download_client_excel(){
            $this->ticket_model->download_client_excel();
    }
    public function view_details($id){
        $data['ui']=array('title'=>'Edit','action'=>site_url("ticket/update_client/$id"),'okButton'=>'Update');
        $data['sale_details']=$this->ticket_model->get_client_by_id($id);
        $created_by=$this->user_manager_model->get_by_id($data['sale_details']['created_by']);
        $created_by_name='';
        if(sizeof($created_by)>0){
            $data['sale_details']['created_by_name']=$created_by['name'];
        }

        $data['short_codes']=  $this->ticket_model->get_short_codes_in_single_array_by_client_id($id);
        $data['toll_free_numbers']=$this->ticket_model->get_toll_free_numbers_in_single_array_by_client_id($id);
        $this->load->view('client/view_details',$data);


    }

    public function edit($id){
        $client_info=$this->ticket_model->get_client_by_id($id);
        if(sizeof($client_info)==0)
            return;
        if(! $this->user_manager_model->is_user_admin()){
            $current_user=$this->user_manager_model->get_current_user();
            if($client_info['created_by']!=$current_user['id']){
                $created_by_whom=$this->user_manager_model->get_by_id($client_info['created_by']);
                $created_by_name='';
                if(sizeof($created_by_whom)>0){
                    $created_by_name=$created_by_whom['name'];
                }
                $data['error_msg']="This client has been created by user: [ $created_by_name ]. You can't update client of another user.";
                $this->load->view('error_custom/error_viewer',$data);
                return;
            }
        }

        $data['ui']=array('title'=>'Edit','action'=>site_url("ticket/update_client/$id"),'okButton'=>'Update');
        $data['sale_details']=$client_info;
        $data['short_code_list']=$this->ticket_model->get_short_codes_by_client_id($id);
        $data['toll_free_number_list']=$this->ticket_model->get_toll_free_numbers_by_client_id($id);
        if(isset($data['sale_details']['type'])){
            $data['ctrl_select_datas'] =array('selected_client_type'=>$data['sale_details']['type']);
        }
        $this->load->view('client/create_client',$data);




    }

    public function update($id){
        $this->setValidation('edit');
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
        if($this->form_validation->run()){            
            //$client=$this->ticket_model->refreshObject($params);
            $return_value = $this->ticket_model->save_client($params);
            $this->session->set_flashdata('return_value', $return_value);
            redirect('ticket/edit_client/'.$id); //for clearing input fields [stackoverflow :) ]
        }
        else{
            $data['ui']=array('title'=>'Edit','action'=>site_url("ticket/update_client/$id"),'okButton'=>'Update');
            if(isset($params['client_type'])){
                $data['ctrl_select_datas'] =array('selected_client_type'=>$params['client_type']); //when form validation failed dropdown input is given back 
            }
            $data['sale_details']['id']=$params['id'];
            $this->load->view('client/create_client', $data);
        
        }
    }

    public function delete($id){
        if( $this->user_manager_model->is_user_admin()){ 
			echo $this->ticket_model->delete_client($id);
        }
		else if(($this->user_manager_model->has_permission_for_role($this->role_model->Deactive_clients))&& ($this->ticket_model->is_client_created_by_current_user($id))  ){
			echo $this->ticket_model->delete_client($id);
		}
        else{
            echo false;
        }
    }
}
