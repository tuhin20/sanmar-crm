<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {

	public function __construct() {
		parent::__construct ();

			$this->load->model ( 'user_model' );
			$this->load->model ( 'role_model' );
			$this->load->model ( 'prime_model' );
			
	}	
	
	public function index()
	{
			/*$test=  $this->prime_model->getByQuery("select * from user");
            print_r($test);
			$users=$this->user_model->get_user_list();
			print_r($users->result_array());*/
            //$this->load->view('login/login');
			/*echo 'x';
			$encrypted=$this->utility_model->encrypt('x');
			echo $encrypted;
			*/
            if($this->user_model->is_logged_in()){
                redirect('contact');
                return;
            }
            else
            {
                $this->load->view('login/login');
            }


	}
	
	public function get_pie_chart_query_type(){
		//$result= $this->sale_details_model->get_pie_chart_query_type_information();
		echo json_encode($result);
		
    }
	
	public function get_pie_chart_division_type(){
		//$result= $this->sale_details_model->get_pie_chart_division_type_information();
		echo json_encode($result);
		
    }
	
	public function view_dashboard()
	{


        if(!$this->user_model->is_logged_in()){
               $this->load->view('login/login');
            }
            else{
				//$data['dashboard_info'] = $this->sale_details_model->get_dashboard_information();
				//$data['pie_chart_info'] = $this->sale_details_model->get_pie_chart_query_type_information();
				$this->load->view('dashboard/dashboard', $data);
            }

		/*$data['rate_chart_details'] = $this->rate_chart_manager_model->get_rate_chart_details($this->session->userdata ( 'rate_chart_id'));
		$data['refill_history'] = $this->user_model->get_refill_history($this->session->userdata ( 'user_id'), 90);
		$user_details = $this->user_model->get_user_info($this->session->userdata ( 'user_id'));
		
		foreach($user_details->result() as $row)
		{
			$data['avail_balance'] = $row->avail_balance;
		}
		
		$this->load->view('dashboard/dashboard', $data);*/
             
	}
	
	
	public function validate_user()
	{
                if($this->user_model->is_logged_in()){
                    //$this->view_dashboard();
                    redirect('contact');
                }
		else
		{
		
                    $user_name = trim($this->security->xss_clean($this->input->post('username', TRUE)));		
                    $password = trim($this->security->xss_clean($this->input->post('password', TRUE)));

                    $isValid=$this->user_model->is_valid($user_name,$password);
                    if($isValid)
                    {		
                            $this->user_model->log_user_in($user_name,$password);
							//redirect code							
                            $redirect_after_login=$this->session->userdata('redirect_after_login');
							if(isset($redirect_after_login) && $redirect_after_login!=''){
								$this->session->unset_userdata('redirect_after_login');
								redirect ($redirect_after_login);
							} 
							//end of redirect code 
                            redirect('contact');
                    }
                    else
                    {
                            $data['msg'] = "Invalid username or password.";
                            //$data['password_encoded']=$password_encoded;
                            $this->load->view('login/login', $data);
                    }
		}
	}
	
	public function change_password($type = "", $msg = "")
	{
		$data['type'] = $type;
		$data['msg'] = $msg;
		
		$this->load->view('login/change_password', $data);
	}
	
	public function save_changed_password()
	{
		$old_password = $this->security->xss_clean($this->input->post('old_password', TRUE));		
		$new_password = $this->security->xss_clean($this->input->post('new_password', TRUE));
		$confirm_new_password = $this->security->xss_clean($this->input->post('confirm_new_password', TRUE));
		
		if($this->user_model->is_old_password_valid($old_password))
		{
			if($new_password == $confirm_new_password)
			{
				if($this->user_model->change_password($new_password))
				{
					//$this->session->set_userdata ( 'password', $new_password );
					$this->change_password("success", "Password changed successfully...");
				}
				else
				{
					$this->change_password("danger", "Unable to change password. Please try after sometime...");
				}
			}
			else
			{
				$this->change_password("danger", "The password and its confirmation are not the same...");
			}
		}
		else
		{
			$this->change_password("danger", "Provided old password is not valid...");
		}
	}
	
	function logout()
	{
		$this->user_model->log_user_out();
		$this->index();
	}
}