            <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

            class contact extends CI_Controller
            {

              public function __construct()
              {
                parent::__construct();
                $this->load->database();
                $this->load->library('form_validation');
                $this->load->model('contact_model');
                $this->load->model('prime_model');
                $this->load->model('user_model');
                $this->load->model('product_model');
                $this->load->model('ticket_model');
                $this->load->library('session');
                   // $this->load->model('sultanModel');
                $this->load->helper(array('form', 'url'));
                $this->checkAuthorization();

            }
            public function send_mail(){
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'smtp.gmail.com',
                    'smtp_port' => 25,
                    'smtp_user' => 'sanmarhelpdesk@gmail.com',
                    'smtp_pass' => 'spl@helpdesk',
                    'mailtype'  => 'html', 
                    'charset'   => 'iso-8859-1'
                );

                $this->load->library('email', $config); 
                $this->email->set_newline("\r\n");
                $this->email->from('sanmarhelpdesk@gmail.com', 'Sanmar Helpdesk');
                $this->email->to('mainuddin.tuhin@metro.net.bd');
                $this->email->subject(' This is test mail'); 
                $this->email->message('Hello World…');
                if (!$this->email->send()) {
                    show_error($this->email->print_debugger()); }
                    else {
                        echo 'Your e-mail has been sent!';
                    }
                }
                public function dashboard(){
                    $this->load->view('sanmar/dashboard');
                } 
                public function page(){
                    $this->load->view('sanmar/test');
                }

                public function sms_send_test(){
                    $send_sms=file_get_contents("http://portal.metrotel.com.bd/smsapi?api_key=MetronetAdmin5d89ad84587338.49681431&type=text&contacts=8801720861366&senderid=8809612520000&msg=test") ;
                    echo $send_sms;
                }

                public function checkAuthorization()
                {


                        //$current_user=$this->user_model->get_current_user();
                    if(!$this->user_model->is_logged_in()){
                    //$this->session->set_userdata(array('redirect_after_login'=>site_url("ticket/search/$caller_number")));
                      redirect('/login/index');
                            //redirect('/contact/sanmar');
                      return;
                  }
                        /*$hasPermission=$this->user_model->has_permission_for_role($this->role_manager_model->See_isd_rate_chart);
                        if(!$hasPermission){
                            redirect('/login/index');
                            return;
                        }*/
                    }
                    /********************Customer start**********************/

                    public function edit_customer($id){
                      $data['customerinfo']=$this->product_model->edit_customer($id);
                          //$result=$this->product_model->edit_customer($id);
                          //$data['customerinfo']=$result[0];
                      $data['project_info'] = $this->product_model->get_all_project_details();
                   /* echo "<pre>";
                    print_r($data);
                    die();*/
                // $this->load->view('editproduct',$data);
                    $this->load->view('sanmar/edit_customer1',$data);
                    
                }
                public function delete_customer($id)
                {
                   $this->contact_model->delete_customer($id);
                   return  redirect('contact/customerlist');
               }
               public function customerlist(){
                  $data['customer_info']=$this->product_model->get_all_customer_details();
                 /* echo "<pre>";
                  print_r($data);
                  die();*/
                  $this->load->view('sanmar/customer_list',$data);
              }
              public function input_mobile(){
            // $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
                $phone_search = $this->input->post('phone');
                $data=$this->prime_model->get_data_model($phone_search);

            //echo "<pre>";print_r($data);die();
                if ($data) {
                    echo  json_encode($data);
                }else{
                    echo  json_encode($data);
                }

            }
            public function res()
            {

              $data['customer_info'] = $this->product_model->get_all_customer_details();
                   /* echo "<pre>";
                    print_r($data);
                    die();*/
                    //I take here a sample view, you can put more view pages here
                    $this->load->view('sanmar/customer',$data);
                }
                public function create_customer(){


                    $data['project_info'] = $this->product_model->get_all_project_details();
                /*    echo "<pre>";
                    print_r($data);
                    die();*/
                    $this->load->view('sanmar/customer',$data);
                }
                public function customer(){

                    $data['name']=$this->input->post('name');
                    $data['phone']=$this->input->post('phone');
                    $data['phone2']=$this->input->post('phone2');
                    $data['phone_int']=$this->input->post('phone_int');
                    $data['email']=$this->input->post('email');
                    $data['type']=$this->input->post('type');
                    $data['mothers_name']=  $this->input->post('mothers_name');
                    $data['location']=$this->input->post('location');
                   //   $data['source']=  $this->input->post('source');
                    $source=  $this->input->post('source');
                    
                    
                    $data['interested_project_type']=$this->input->post('interested_project_type');
                    $data['note']=$this->input->post('note');
                    $data['birthdate']=$this->input->post('birthdate');
                    $occupation=  $this->input->post('occupation');
                    $data['address']= $this->input->post('address');
                    $data['religion']=  $this->input->post('religion');
                    $type= $this->input->post('type');
                    $data['call_type']= $this->input->post('call_type');
                    $data['preferd']= $this->input->post('preferd');
                    $data['purpose']= $this->input->post('purpose');
                    $data['interested_project']=$this->input->post('interested_project');
                    $neighborhood=$this->input->post('neighborhood');
                    $lead_forword=$this->input->post('lead_forword');
                    if ($lead_forword=='Dhaka') {
                     $data['lead_forword']= $this->input->post('ld_dhk');
                 }
                 elseif($lead_forword=='Chattogram'){
                     $data['lead_forword']= $this->input->post('ld_ctg');
                 }

                 $others_ctg=$this->input->post('ctg_city');
                 $others_dhk=$this->input->post('dhk_city');
                 if ($neighborhood=='ctg') {
                    $data['neighborhood']=$this->input->post('ctg_city');
                    
                }
                    //$ctg_city=$this->input->post('others_ctg');
                if ($others_ctg=='others_ctg') {
                    $data['neighborhood']=$this->input->post('others_ctg_in');
                }
                if ($others_dhk=='others_dhk') {
                    $data['neighborhood']=$this->input->post('others_dhk_in');
                }
                
                elseif($neighborhood=='dhk'){
                 $data['neighborhood']=$this->input->post('dhk_city');
             }
             elseif ($neighborhood=='rest') {

                $data['neighborhood']=$this->input->post('rest');
            }
           /* if ($interested_project=='GreenPark'||$interested_project=='OrchardPreen'||$interested_project=='EdaniaAyub') {
               $data['interested_project']= $this->input->post('interested_project');
           }else{
               $data['interested_project']= $this->input->post('others_interested');
           }*/
           if ( $occupation=='Business'||$occupation=='Service_Holders') {
             $data['occupation']= $this->input->post('occupation');
         }else{
             $data['occupation']= $this->input->post('occ_others');
         }
         if ( $type=='New') {
             $data['type']= $this->input->post('type');
         }elseif($type=='Existing'){
             $data['type']= $this->input->post('ext');
         }
         if($source=='Facebook'||$source=='Instagram'||$source=='Tweeter'||$source=='Tiktok'||$source=='News'||$source=='TV'||$source=='Paper_Insertion'||$source=='Caravan'||$source=='Bill_Board'||$source=='Linkdin'||$source=='Sociamedia'||$source=='Website'||$source=='Staff'||$source=='Employee_Referal'||$source=='Lifestyle_executive'||$source=='SMS_or_email'||$source=='Telemarketing'||$source=='Hoarding'||$source=='Others' ) {
             $data['source']= $this->input->post('source');
         }else{
             $data['source']= $this->input->post('scmedia');
         }

         if($source=='Employee_Referal'){
            $data['source']= $this->input->post('Referal');
        } 
                    /*else{
                     $data['source']= $this->input->post('scmedia');
                 }*/
                   /*if ($source=='Sociamedia'||$source=='Website'||$source=='Lifestyle_executive'||$source=='Invitation'||$source=='Telemarketing'||$source=='Hoarding' ) {
                     $data['source']= $this->input->post('source');
                   }else{
                    $data['source']= $this->input->post('abc');
                }*/

                /*  echo "<pre>";
               print_r($data);
               die();*/
               $id=$this->contact_model->add_customer($data);
                  /*echo "<pre>";
               print_r($id);
               die();*/
               $this->session->set_flashdata('success', 'Customer create successfully');
               redirect('contact/create_customer');

                     // $this->load->view('sanmar/customer',$data);
           }
           public function update_customer(){
            $id=$this->input->post('id');
            $data['name']=$this->input->post('name');
            $data['phone']=$this->input->post('phone');
            $data['phone2']=$this->input->post('phone2');
            $data['phone_int']=$this->input->post('phone_int');
            $data['email']=$this->input->post('email');
            $data['type']=$this->input->post('type');
            $data['mothers_name']=  $this->input->post('mothers_name');
            $data['location']=$this->input->post('location');
            $data['source']=  $this->input->post('source');
                    //$source=  $this->input->post('source');
            $data['lead_forword']=$this->input->post('lead_forword');
            
            $data['interested_project_type']=$this->input->post('interested_project_type');
            $data['note']=$this->input->post('note');
            $data['birthdate']=$this->input->post('birthdate');
            $data['occupation']= $this->input->post('occupation');
            $data['address']= $this->input->post('address');
            $data['religion']=  $this->input->post('religion');
            $type= $this->input->post('type');
            $data['call_type']= $this->input->post('call_type');
            $data['preferd']= $this->input->post('preferd');
            $data['purpose']= $this->input->post('purpose');
            $data['interested_project']=$this->input->post('interested_project');
            $data['neighborhood']=$this->input->post('neighborhood');
                    /*if ($neighborhood=='ctg') {
                       $data['neighborhood']=$this->input->post('ctg_city');
                    }
                    elseif($neighborhood=='dhk'){
                         $data['neighborhood']=$this->input->post('dhk_city');
                    }elseif ($neighborhood=='rest') {
                        $data['neighborhood']=$this->input->post('rest');
                    }
                    if ($interested_project=='GreenPark'||$interested_project=='OrchardPreen'||$interested_project=='EdaniaAyub') {
                        $data['interested_project']= $this->input->post('interested_project');
                    }else{
                        $data['interested_project']= $this->input->post('others_interested');
                    }
                    if ( $occupation=='Business'||$occupation=='Service_Holders') {
                     $data['occupation']= $this->input->post('occupation');
                   }else{
                     $data['occupation']= $this->input->post('occ_others');
                 }*/
                 if ( $type=='New') {
                     $data['type']= $this->input->post('type');
                 }elseif($type=='Existing'){
                     $data['type']= $this->input->post('ext');
                 }
                 /*  if($source=='Facebook'||$source=='Instagram'||$source=='Tweeter'||$source=='Tiktok'||$source=='News'||$source=='TV'||$source=='Paper_Insertion'||$source=='Caravan'||$source=='Bill_Board'||$source=='Linkdin'||$source=='Sociamedia'||$source=='Website'||$source=='Staff'||$source=='Employee_Referal'||$source=='Lifestyle_executive'||$source=='SMS_or_email'||$source=='Telemarketing'||$source=='Hoarding'||$source=='Others' ) {
                     $data['source']= $this->input->post('source');
                   }else{
                     $data['source']= $this->input->post('scmedia');
                 }*/
             /* echo "<pre>";
                  print_r($data);
                  die();*/
                  $this->product_model->update_customer($data,$id);
                  $this->session->set_flashdata('success', 'Customer Update successfully');
                  return  redirect('contact/customerlist');
                      //$this->load->view('sanmar/project',$data);
              }
              public function view_customer_details($id){
               $data['customer_info']=$this->contact_model->get_customer_details_by_id($id);
                  /* echo "<pre>";
                  print_r($data);
                  die();*/
                  $this->load->view('sanmar/view_customer_details',$data);
              }
              public function process_paging_customer(){
                  $sql="select * from customer where 1 ";

                  $conditions='';
                  $query_id=$this->input->post('query_id');
                  if($query_id>0){
                    $temp=$this->prime_model->getByID('query','id',$query_id);
                    $conditions=$temp['value'];
                    $sql .=$conditions;
                }
                $recordsTotal=$this->get_count($sql);
                $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
                
                $draw=$this->input->post('draw');
                $search=$this->input->post('search');
                $start=$this->input->post('start');
                $length=$this->input->post('length');
                
                
                if($search['value']!=''){
                  $value=$search['value'];
                  $sql .=" and (name like '%$value%' or phone like '%$value%' or email like '%$value%'  or type like '%$value%' or mothers_name like '%$value%' or location like '%$value%' or source like '%$value%' or lead_forword like '%$value%' or interested_project like '%$value%'or interested_project_type like '%$value%' or note like '%$value%' or birthdate like '%$value%' or occupation like '%$value%' or address like '%$value%' or religion like '%$value%')";
                  $recordsFiltered=$this->get_count($sql);
              }
              
                //for getting data with limit
              $sql .=" order by id desc limit $start,$length";
              
              $contacts=$this->prime_model->getByQuery($sql) ;

              $output=array();
              $i=$start+1;
              $current_user=$this->user_model->get_current_user();
              foreach($contacts as $item){
                  //buttons
               $btn_details="<a href='". site_url('contact/view_customer_details/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;
               $btn_edit="";
               $btn_delete="";

               if($current_user['group_id']==1){


                   $btn_edit="<a href='". site_url('contact/edit_customer/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;

                   $btn_delete="<a href='". site_url('contact/delete_customer/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Edit\"></a>" ;
               }

                                                //echo '<a href='. site_url().'/contact/edit/'.$row->id.' class="btn btn-warning glyphicon glyphicon-pencil" title="Edit"></a>';


                                                //echo '<a href='. site_url().'/contact/delete/'.$row->id.' class="btn btn-danger glyphicon glyphicon-trash delete" title="Delete Info"></a>';


               $output[]=array($i,$item['name'],$item['phone'],$item['email'],$item['type'],/*$item['mothers_name'],$item['location'],$item['source'],$item['lead_forword'],$item['interested_project'],$item['note'],date('d-m-Y', strtotime($item['birthdate'])),$item['occupation'],$item['address'],$item['religion'],*/" $btn_delete    $btn_edit $btn_details ");
               $i++;
           }
           $json_data = array(
            "draw"            => $draw,   
            "recordsTotal"    => $recordsTotal ,  
            "recordsFiltered" => $recordsFiltered,
                       "data"            => $output   // total data array
                   );
           echo json_encode($json_data);
       }


       /*****************Customer end**************************/

       /******************Project Start*************************/
       public function project(){
          $this->load->view('sanmar/project');
      }
      public function create_project(){
          $this->load->view('sanmar/add_project');
      }


      public function addproject(){
          $data['project_name']=$this->input->post('project_name');
          $data['project_type']=$this->input->post('project_type');
          $data['location']=$this->input->post('location');
          $data['storied']= $this->input->post('storied');
          $data['project_facing']=$this->input->post('project_facing');
          $data['total_units']= $this->input->post('total_units');
          $data['total_saleable_unit']=$this->input->post('total_saleable_unit');
          $data['apartment']=$this->input->post('apartment');
          $data['sold_unit']=$this->input->post('sold_unit');
          $data['unsold']=$this->input->post('unsold');
          $data['handover_date']= $this->input->post('handover_date');
          $data['total_land_area']= $this->input->post('total_land_area');
          $data['project_usp']= $this->input->post('project_usp');
          $data['landmark']=  $this->input->post('landmark');
          $data['parking_per_unit']=  $this->input->post('parking_per_unit');
          $data['parking_space']= $this->input->post('parking_space');
          $image=$_FILES['image']['name'];
          $data['image']=$image;
              //$data['image']=$image;
          
          if ($image) {
            $destination_path = getcwd().DIRECTORY_SEPARATOR;
            $target_path = $destination_path . 'uploads/images/'. basename( $_FILES["image"]["name"]);
            move_uploaded_file($_FILES['image']['tmp_name'], $target_path);

               /*	$destination_path = getcwd().DIRECTORY_SEPARATOR;
               $target_path = $destination_path .($_FILES['image']['tmp_name'], 'uploads/images/' .$image);*/
                  /*echo "<pre>";
               print_r($target_path);
               die();*/
           }
                 /*$config['upload_path'] = './asset/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = 2000;
                    $config['max_width'] = 1500;
                    $config['max_height'] = 1500;*/

                   // $this->load->library('upload', $config);

                    /*if (!$this->upload->do_upload('profile_pic')) 
                {
                        //$error = array('error' => $this->upload->display_errors());
                        echo "image uploaded failed";
                       
                    } 


                      /*  $data['image']= $this->input->post('image');

                      if($data['image']){
                        
                        //Check whether user upload picture
                        if(!empty($_FILES['file_name']['name'])){
                         
                            $config['upload_path'] = 'uploads/images/';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                            $config['file_name'] = $_FILES['picture']['name'];
                            
                            //Load upload library and initialize configuration
                            $this->load->library('upload',$config);
                            $this->upload->initialize($config);
                            
                            if($this->upload->do_upload('picture')){
                                $uploadData = $this->upload->data();
                                $picture = $uploadData['file_name'];
                            }else{
                                $picture = '';
                          
                       }
                      }
                  }*/
                 /* echo "<pre>";
               print_r($data);
               die();*/
               $id=$this->contact_model->add_project($data);
                /*  echo "<pre>";
               print_r($id);
               die();*/
               redirect('contact/projectlist');
           }

           public function projectlist(){
              $data['project_info']=$this->product_model->get_all_project_details();
                  /*echo "<pre>";
                  print_r($data);
                  die();*/
                  $this->load->view('sanmar/project',$data);
              }
              public function view_project_dateils($id){
                 $data['project_info']=$this->product_model->get_project_details_by_id($id);
                  /* echo "<pre>";
                  print_r($data);
                  die();*/
                  $this->load->view('sanmar/project_dt',$data);
                  //$this->load->view('sanmar/view_project_details',$data);
              }
              public function view_ticket_dateils($id){
                 $data['ticket_info']=$this->ticket_model->get_ticket_details_by_id($id);
                  /* echo "<pre>";
                  print_r($data);
                  die();*/
                  $this->load->view('sanmar/view_ticket_details',$data);
              }

              public function edit_project($id){
                  $data['projectinfo']=$this->product_model->edit_project($id);
                  /*  echo "<pre>";
                    print_r($data);
                    die();*/
                // $this->load->view('editproduct',$data);
                    $this->load->view('sanmar/edit_project',$data);
                    
                }
                public function delete_project($id)
                {
                   $this->product_model->delete_project($id);
                   return  redirect('contact/projectlist');
               }

               public function update_project(){
                  $id=$this->input->post('id');
                  $data['project_name']=$this->input->post('project_name');
                  $data['project_type']=$this->input->post('project_type');
                  $data['location']=$this->input->post('location');
                  $data['storied']= $this->input->post('storied');
                  $data['project_facing']=$this->input->post('project_facing');
                  $data['total_units']= $this->input->post('total_units');
                  $data['total_saleable_unit']=$this->input->post('total_saleable_unit');
                  $data['apartment']=$this->input->post('apartment');
                  $data['sold_unit']=$this->input->post('sold_unit');
                  $data['unsold']=$this->input->post('unsold');
                  $data['handover_date']= $this->input->post('handover_date');
                  $data['total_land_area']= $this->input->post('total_land_area');
                  $data['project_usp']= $this->input->post('project_usp');
                  $data['landmark']=  $this->input->post('landmark');
                  $data['parking_per_unit']=  $this->input->post('parking_per_unit');
                  $data['parking_space']= $this->input->post('parking_space');
                  $image=$_FILES['image']['name'];
                  $data['image']=$image;
              //$data['image']=$image;

                  if ($image) {
                    $destination_path = getcwd().DIRECTORY_SEPARATOR;
                    $target_path = $destination_path . 'uploads/images/'. basename( $_FILES["image"]["name"]);
                    move_uploaded_file($_FILES['image']['tmp_name'], $target_path);

               /* $destination_path = getcwd().DIRECTORY_SEPARATOR;
               $target_path = $destination_path .($_FILES['image']['tmp_name'], 'uploads/images/' .$image);*/
                  /*echo "<pre>";
               print_r($target_path);
               die();*/
           }
            /*echo "<pre>";
            print_r($data);
            die();
            */
            $this->product_model->update_project($data,$id);
            return  redirect('contact/projectlist');
                      //$this->load->view('sanmar/project',$data);
        }
        public function process_paging_project(){
          $sql="select * from project where 1 ";

          $conditions='';
          $query_id=$this->input->post('query_id');
          if($query_id>0){
           $temp=$this->prime_model->getByID('query','id',$query_id);
           $conditions=$temp['value'];
           $sql .=$conditions;
       }
       $recordsTotal=$this->get_count($sql);
                $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
                
                $draw=$this->input->post('draw');
                $search=$this->input->post('search');
                $start=$this->input->post('start');
                $length=$this->input->post('length');
                
                
                if($search['value']!=''){
                  $value=$search['value'];
                  $sql .=" and ( project_name like '%$value%' or project_type like '%$value%' or location like '%$value%' or storied like '%$value%' or project_facing like '%$value%' or total_units like '%$value%' or total_saleable_unit like '%$value%' or apartment like '%$value%' or sold_unit like '%$value%' or unsold like '%$value%' or handover_date like '%$value%' or total_land_area like '%$value%' or project_usp like '%$value%' or landmark like '%$value%'  or parking_per_unit like '%$value%'  or parking_space like '%$value%'or image like '%$value%')";
                  $recordsFiltered=$this->get_count($sql);
              }
              
                //for getting data with limit
              $sql .=" order by id desc limit $start,$length";
              
              $contacts=$this->prime_model->getByQuery($sql) ;

              $output=array();
              $i=$start+1;
              $current_user=$this->user_model->get_current_user();
              foreach($contacts as $item){



                  $btn_details="<a href='". site_url('contact/view_project_dateils/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;

                  $btn_edit="";
                  $btn_delete="";

                  if($current_user['group_id']==1){
                    $btn_edit="<a href='". site_url('contact/edit_project/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;

                    $btn_delete="<a href='". site_url('contact/delete_project/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Edit\"></a>" ;

                }


                $output[]=array($i,$item['project_name'],$item['project_type'],$item['location'],$item['total_units']," $btn_details $btn_edit $btn_delete");
                $i++;
            }
            $json_data = array(
              "draw"            => $draw,   
              "recordsTotal"    => $recordsTotal ,  
              "recordsFiltered" => $recordsFiltered,
                       "data"            => $output   // total data array
                   );
            echo json_encode($json_data);
        }
        /******************Project end*************************/
        /******************Ticket Start*************************/

        public function ticket(){
                    //echo "string";
            $this->load->view('sanmar/add_ticket');
        }
        public function create_ticket(){
            $current_user=$this->user_model->get_current_user();
            $email=$this->input->post('email');
            $cc=$this->input->post('cc');

            /*mail*/

            /*$config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'smtp.gmail.com',
                'smtp_port' => 25,
                'smtp_user' => 'sanmarhelpdesk@gmail.com',
                'smtp_pass' => 'spl@helpdesk',
                'mailtype'  => 'html', 
                'charset'   => 'iso-8859-1'
            );

            $this->load->library('email', $config); 
            $this->email->set_newline("\r\n");
            $this->email->from('sanmarhelpdesk@gmail.com', 'Sanmar Helpdesk');
            $this->email->to($email);
            $this->email->cc($cc);
            $this->email->subject(' This is test mail'); 
            $this->email->message('Hello World…');
            if (!$this->email->send()) {
                show_error($this->email->print_debugger()); }
                else {
                    echo 'Your e-mail has been sent!';
                }*/
                /*mail*/


                $data['user']=$current_user['name'];
                $data['status']=$this->input->post('status');
                $data['email']=$this->input->post('email');
                $data['project']=$this->input->post('project');
                $data['name']=$this->input->post('name');
                $data['date']=$this->input->post('date');
                $data['phone']=$this->input->post('phone');
                $data['shop_no']=  $this->input->post('shop_no');
                $data['level_no']=$this->input->post('level_no');
                $data['car_park']= $this->input->post('car_park');
                $data['birthdate']=$this->input->post('birthdate');
                $data['feedback1']=$this->input->post('feedback1');
                $data['feedback2']=$this->input->post('feedback2');
                $data['feedback3']=$this->input->post('feedback3');
               //$data['occupation']=$this->input->post('occupation');
                

            //$data['deadline']=$this->input->post('deadline');
                $data['permanent_address']=  $this->input->post('permanent_address');
                $data['present_address']=  $this->input->post('present_address');
                
                $data['details']=  $this->input->post('details');
                $data['respective_dept']=  $this->input->post('respective_dept');
                $data['query_details']=  $this->input->post('query_details');
                $data['required_time']=  $this->input->post('required_time');
                $data['ticket_no']=mt_rand(1,100000);
                $duration= $this->input->post('duration');
                $occupation=$this->input->post('occupation');
                
                $pd_category= $this->input->post('pd_category');
                $details=  $this->input->post('details');
                $purpose=$this->input->post('purpose_call');
                if ($details=='Tiles'||$details=='Painting'||$details=='Sanitary'||$details=='Plumbing'||$details=='Aluminium'||$details=='Carpentry'||$details=='Electrical') {
                  $data['details']=  $this->input->post('details');
              }else{
               $data['details']=  $this->input->post('others_details');
           }
           if ($pd_category=='BeforeHandover'||$pd_category=='WarrentyPeriods'||$pd_category=='AfterHandover') {
             $data['pd_category']=  $this->input->post('pd_category');
         }else {
           $data['pd_category']=  $this->input->post('others_category');
       }

       if ($purpose=='SL' || $purpose=='CS' ||$purpose=='BD') {
          $ticket = mt_rand(1,1000);
          $currentDate = date('d-m-Y');
          $data['purpose_call']=$this->input->post('purpose_call').''.$currentDate.'-'.''.$ticket;
               //$result=$this->input->post('purpose_call');
               // $data['purpose_call']=$result;
                //$data['purpose_call']=$result."".$ticket;
          
      }elseif ($purpose=='others1') {
        $data['purpose_call']=$this->input->post('others');
    }{

    }
    if ($occupation=='Business'||$occupation=='Service_Holders') {
     $data['occupation']=$this->input->post('occupation');
 }elseif ($occupation=='others') {
     $data['occupation']=$this->input->post('occu');
 }



 if($duration=='ExistingProblem'||$duration=='PossibleProblem' ) {
     $data['duration']= $this->input->post('duration');
 }else{
     $data['duration']= $this->input->post('date');
 }

 $msg=urldecode("we had conveyed information accurately.");
 $contact=$data['phone'];
 $send_sms=file_get_contents("http://portal.metrotel.com.bd/smsapi?api_key=C2001648626512f54de267.45460717&type=text&contacts=$contact&senderid=8809612666888&msg=$msg");

              /*echo "<pre>";
               print_r($data);
               die(); */   

               $id=$this->ticket_model->add_ticket($data);
                /*  echo "<pre>";
               print_r($id);
               die();*/
               redirect('contact/ticket');
           }
           public function download_cdr_chart(){
               $data['status']=$this->input->post('status');
               $data['respective_dept']=$this->input->post('respective_dept');
               $data['start_date']=$this->input->post('start_date');
               $data['end_date']=$this->input->post('end_date');
                  /*  echo "<pre>";
                   print_r( $data);
                   die();*/
                   $this->product_model->download_cdr_chart($data);
               }
               public function download_customer_chart(){
                   $data['source']=$this->input->post('source');
                   $data['location']=$this->input->post('location');
                   $data['interested_project']=$this->input->post('interested_project');
              /* $data['start_date']=$this->input->post('date');
              $data['end_date']=$this->input->post('date');*/
                   /* echo "<pre>";
                   print_r( $data);
                   die();*/
                   $this->contact_model->download_customer_chart($data);
               }
               public function download_project_chart(){
                $data['location']=$this->input->post('location');
                $data['project_name']=$this->input->post('project_name');
               /*echo "<pre>";
                   print_r( $data);
                   die();*/
                   $this->product_model->download_project_chart($data);
               }
               public function edit_ticket($id){
                  $data['ticket_info']=$this->ticket_model->edit_ticket($id);
                  $data['ticket_info1']=$this->ticket_model->get_ticket_details_by_id($id);
                  /*  echo "<pre>";
                    print_r($data);
                    die();*/
                // $this->load->view('editproduct',$data);
                    $this->load->view('sanmar/edit_ticket',$data);
                    
                }
                public function update_ticket(){
                    $id=$this->input->post('id');
                    $email=$this->input->post('email');
                    $cc=$this->input->post('cc');

                    /*mail*/

                    $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'smtp.gmail.com',
                        'smtp_port' => 25,
                        'smtp_user' => 'sanmarhelpdesk@gmail.com',
                        'smtp_pass' => 'spl@helpdesk',
                        'mailtype'  => 'html', 
                        'charset'   => 'iso-8859-1'
                    );

                    $this->load->library('email', $config); 
                    $this->email->set_newline("\r\n");
                    $this->email->from('sanmarhelpdesk@gmail.com', 'Sanmar Helpdesk');
                    $this->email->to($email);
                    $this->email->cc($cc);
                    $this->email->subject(' This is test mail'); 
                    $this->email->message('Hello World…');
                    if (!$this->email->send()) {
                        show_error($this->email->print_debugger()); }
                        else {
                            echo 'Your e-mail has been sent!';
                        }
                        /*mail*/

                        $data['project']=$this->input->post('project');
                        $data['name']=$this->input->post('name');
                        $data['date']=$this->input->post('date');
                        $data['phone']=$this->input->post('phone');
                        $data['shop_no']=  $this->input->post('shop_no');
                        $data['level_no']=$this->input->post('level_no');
                        $data['car_park']= $this->input->post('car_park');
                        $data['birthdate']=$this->input->post('birthdate');
                        $data['status']=$this->input->post('status');
                        $data['feedback1']=$this->input->post('feedback1');
                        $data['feedback2']=$this->input->post('feedback2');
                        $data['feedback3']=$this->input->post('feedback3');
               //$data['occupation']=$this->input->post('occupation');
                        

                        $data['deadline']=$this->input->post('deadline');
                        $data['permanent_address']=  $this->input->post('permanent_address');
                        $data['present_address']=  $this->input->post('present_address');
                        
                        $data['details']=  $this->input->post('details');
                        $data['respective_dept']=  $this->input->post('respective_dept');
                        $data['query_details']=  $this->input->post('query_details');
                        $data['required_time']=  $this->input->post('required_time');
                        $duration= $this->input->post('duration');
                        $occupation=$this->input->post('occupation');
                        $pd_category= $this->input->post('pd_category');
                        $details=  $this->input->post('details');
                        $purpose=$this->input->post('purpose_call');
                        if ($details=='Tiles'||$details=='Painting'||$details=='Sanitary'||$details=='Plumbing'||$details=='Aluminium'||$details=='Carpentry'||$details=='Electrical') {
                          $data['details']=  $this->input->post('details');
                      }else{
                       $data['details']=  $this->input->post('others_details');
                   }
                   if ($pd_category=='BeforeHandover'||$pd_category=='WarrentyPeriods'||$pd_category=='AfterHandover') {
                     $data['pd_category']=  $this->input->post('pd_category');
                 }else {
                   $data['pd_category']=  $this->input->post('others_category');
               }

               if ($purpose=='SL' || $purpose=='CS001' ||$purpose=='BD') {
               	 // $ticket = mt_rand(100000, 999999);
                $currentDate = date('d-m-Y');
                $data['purpose_call']=$this->input->post('purpose_call').''.$currentDate;
               //$result=$this->input->post('purpose_call');
               // $data['purpose_call']=$result;
                //$data['purpose_call']=$result."".$ticket;

            }elseif ($purpose=='others1') {
                $data['purpose_call']=$this->input->post('others');
            }{

            }
            if ($occupation=='Business'||$occupation=='Service_Holders') {
             $data['occupation']=$this->input->post('occupation');
         }elseif ($occupation=='others') {
             $data['occupation']=$this->input->post('occu');
         }



         if($duration=='ExistingProblem'||$duration=='PossibleProblem' ) {
             $data['duration']= $this->input->post('duration');
         }else{
             $data['duration']= $this->input->post('date');
         }

            /*echo "<pre>";
            print_r($data);
            die();*/

            $this->ticket_model->update_ticket($data,$id);
            return  redirect('contact/index');
                      //$this->load->view('sanmar/project',$data);
        }
        public function delete_ticket($id)
        {
         $this->ticket_model->delete_ticket($id);
         return  redirect('contact/ticketlist');
     }





     public function ticketlist(){
         $this->load->view('sanmar/ticket_list');
     }
     public function maintanance(){
         $this->load->view('sanmar/maintanance_list');
     }
     public function lifestyle(){
         $this->load->view('sanmar/lifestylelist');
     }
     public function update_maintanance($id){
        $data['ticket_info']=$this->ticket_model->edit_ticket($id);
        $data['ticket_info1']=$this->ticket_model->get_ticket_details_by_id($id);
                    /*echo "<pre>";
                    print_r($data);
                    die();*/
                // $this->load->view('editproduct',$data);
                  //  $this->load->view('sanmar/edit_ticket',$data);
                    $this->load->view('sanmar/update_maintanance',$data);
                }
                public function update_lifestyle($id){
                    $data['ticket_info']=$this->ticket_model->edit_ticket($id);
                    
                    /*echo "<pre>";
                    print_r($data);
                    die();*/
                // $this->load->view('editproduct',$data);
                  //  $this->load->view('sanmar/edit_ticket',$data);
                    $this->load->view('sanmar/update_lifestyle',$data);
                }
                public function customer_service(){

                 $this->load->view('sanmar/customerservice_list',$data);
             }
             public function edit_customer_service($id){
                 $data['ticket_info']=$this->ticket_model->edit_ticket($id);
                 $data['ticket_info1']=$this->ticket_model->get_ticket_details_by_id($id);
                 $this->load->view('sanmar/update_customerservice',$data);
             }

             /*********************Ticket End*****************************/
             /*********************Landowner Start*****************************/

             public function landowner(){
              $this->load->view('sanmar/add_land_owner');

          } 

          public function create_landowner(){
              $data['name']=$this->input->post('name');
              $data['contact_name']=$this->input->post('contact_name');
              $data['contact_number']=$this->input->post('contact_number');
              $data['registered_address']=$this->input->post('registered_address');
              $data['measurement']= $this->input->post('measurement');
              $data['front_road_width']=$this->input->post('front_road_width');
              $data['side_road_width']=  $this->input->post('side_road_width');
              $data['land_status']=$this->input->post('land_status');
              $data['types_of_land']=$this->input->post('types_of_land');



              $id=$this->ticket_model->add_landowner($data);
                /*  echo "<pre>";
               print_r($id);
               die();*/
               redirect('contact/landownerlist');
           }  
           public function landownerlist(){
               $data['landowner_info']=$this->product_model->get_all_landowner_details();
                  /*echo "<pre>";
                  print_r($data);
                  die();*/
                  $this->load->view('sanmar/landownerlist',$data);
              }

              public function delete_landowner($id){

               $this->product_model->delete_landowner($id);
               return  redirect('contact/landownerlist');

           }
           public function download_landowner_chart(){
            $this->product_model->download_landowner_chart($data);
        }
        public function download_career_chart(){
            $this->product_model->download_career_chart($data);
        }
        public function download_investor_chart(){
            $this->product_model->download_investor_chart($data);
        }
        public function process_paging_landowner(){
          $sql="select * from landowner where 1 ";

          $conditions='';
          $query_id=$this->input->post('query_id');
          if($query_id>0){
            $temp=$this->prime_model->getByID('query','id',$query_id);
            $conditions=$temp['value'];
            $sql .=$conditions;
        }
        $recordsTotal=$this->get_count($sql);
                $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
                
                $draw=$this->input->post('draw');
                $search=$this->input->post('search');
                $start=$this->input->post('start');
                $length=$this->input->post('length');
                
                
                if($search['value']!=''){
                  $value=$search['value'];
                  $sql .=" and (name like '%$value%' or contact_name like '%$value%'or contact_number like '%$value%' or registered_address like '%$value%'  or measurement like '%$value%' or front_road_width like '%$value%' or side_road_width like '%$value%' or land_status like '%$value%' or types_of_land like '%$value%')";
                  $recordsFiltered=$this->get_count($sql);
              }
              
                //for getting data with limit
              $sql .=" order by id desc limit $start,$length";
              
              $contacts=$this->prime_model->getByQuery($sql) ;

              $output=array();
              $i=$start+1;
              $current_user=$this->user_model->get_current_user();
              foreach($contacts as $item){
                  //buttons
                  $btn_details="<a href='". site_url('contact/view_project_dateils/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;
                  
                  $btn_edit="<a href='". site_url('contact/edit_project/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;
                  
                  $btn_delete="<a href='". site_url('contact/delete_landowner/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Delete\"></a>" ;
               // }


                  $output[]=array($i,$item['name'],$item['contact_name'],$item['contact_number'],$item['registered_address'],$item['measurement'],$item['front_road_width'],$item['side_road_width'],$item['land_status'],$item['types_of_land'],"$btn_delete");
                  $i++;
              }
              $json_data = array(
                 "draw"            => $draw,   
                 "recordsTotal"    => $recordsTotal ,  
                 "recordsFiltered" => $recordsFiltered,
                       "data"            => $output   // total data array
                   );
              echo json_encode($json_data);
          }


          /*****************Landowner  End***************************/
          /*****************Career  Start***************************/
          public function career(){
            $this->load->view('sanmar/add_career');
        } 
        public function add_career(){
            $data['name']=$this->input->post('name');
            $data['contact_no']=$this->input->post('contact_no');
            $data['email']=$this->input->post('email');
            $data['location']= $this->input->post('location');
            $data['position']=$this->input->post('position');
             /*   echo "<pre>";
               print_r($data);
               die();*/
               $id=$this->ticket_model->add_career($data);

               redirect('contact/careerlist');
           } 
           public function delete_career($id){

               $this->product_model->delete_career($id);
               return  redirect('contact/careerlist');

           }
           public function careerlist(){
               $data['career_info']=$this->product_model->get_all_career_details();
                  /*echo "<pre>";
                  print_r($data);
                  die();*/
                  $this->load->view('sanmar/careerlist',$data);    

              }
              public function process_paging_career(){
                  $sql="select * from career where 1 ";

                  $conditions='';
                  $query_id=$this->input->post('query_id');
                  if($query_id>0){
                    $temp=$this->prime_model->getByID('query','id',$query_id);
                    $conditions=$temp['value'];
                    $sql .=$conditions;
                }
                $recordsTotal=$this->get_count($sql);
                $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
                
                $draw=$this->input->post('draw');
                $search=$this->input->post('search');
                $start=$this->input->post('start');
                $length=$this->input->post('length');
                
                
                if($search['value']!=''){
                  $value=$search['value'];
                  $sql .=" and (name like '%$value%' or contact_no like '%$value%' or email like '%$value%'  or location like '%$value%' or position like '%$value%')";
                  $recordsFiltered=$this->get_count($sql);
              }
              
                //for getting data with limit
              $sql .=" order by id desc limit $start,$length";
              
              $contacts=$this->prime_model->getByQuery($sql) ;

              $output=array();
              $i=$start+1;
              $current_user=$this->user_model->get_current_user();
              foreach($contacts as $item){
                  //buttons
                  $btn_details="<a href='". site_url('contact/view_project_dateils/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;
                  
                  $btn_edit="<a href='". site_url('contact/edit_project/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;
                  
                  $btn_delete="<a href='". site_url('contact/delete_career/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Delete\"></a>" ;
               // }


                  $output[]=array($i,$item['name'],$item['contact_no'],$item['email'],$item['location'],$item['position']," $btn_delete ");
                  $i++;
              }
              $json_data = array(
                 "draw"            => $draw,   
                 "recordsTotal"    => $recordsTotal ,  
                 "recordsFiltered" => $recordsFiltered,
                       "data"            => $output   // total data array
                   );
              echo json_encode($json_data);
          }

          /************************Career End***********************/
          /************************Investor Start***********************/
          public function investor(){
            $this->load->view('sanmar/add_investor');
        } 
        public function add_investor(){
           $data['name']=$this->input->post('name');
           $data['contact_no']=$this->input->post('contact_no');
           $data['email']=$this->input->post('email');
           $data['city']= $this->input->post('city');
           $id=$this->ticket_model->add_investor($data);


           redirect('contact/investorlist');
       } 

       public function investorlist(){

          $data['investor_info']=$this->product_model->get_all_investor_details();

          $this->load->view('sanmar/investorlist',$data);    

      }
      public function delete_investor($id){
        $this->product_model->delete_investor($id);
        return redirect('contact/investorlist');
    }
    public function process_paging_investor(){
      $sql="select * from investor where 1 ";

      $conditions='';
      $query_id=$this->input->post('query_id');
      if($query_id>0){
        $temp=$this->prime_model->getByID('query','id',$query_id);
        $conditions=$temp['value'];
        $sql .=$conditions;
    }
    $recordsTotal=$this->get_count($sql);
                $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
                
                $draw=$this->input->post('draw');
                $search=$this->input->post('search');
                $start=$this->input->post('start');
                $length=$this->input->post('length');
                
                
                if($search['value']!=''){
                  $value=$search['value'];
                  $sql .=" and (name like '%$value%' or contact_no like '%$value%' or email like '%$value%'  or email like '%$value%')";
                  $recordsFiltered=$this->get_count($sql);
              }
              
                //for getting data with limit
              $sql .=" order by id desc limit $start,$length";
              
              $contacts=$this->prime_model->getByQuery($sql) ;

              $output=array();
              $i=$start+1;
              $current_user=$this->user_model->get_current_user();
              foreach($contacts as $item){
                  //buttons
                  $btn_details="<a href='". site_url('contact/view_project_dateils/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;
                  
                  $btn_edit="<a href='". site_url('contact/edit_project/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;
                  
                  $btn_delete="<a href='". site_url('contact/delete_investor/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Delete\"></a>" ;
               // }


                  $output[]=array($i,$item['name'],$item['contact_no'],$item['email'],$item['city'],"$btn_delete ");
                  $i++;
              }
              $json_data = array(
                 "draw"            => $draw,   
                 "recordsTotal"    => $recordsTotal ,  
                 "recordsFiltered" => $recordsFiltered,
                       "data"            => $output   // total data array
                   );
              echo json_encode($json_data);
          }
          /****************************Investor End**********************/



          public function checkmobile_duplicate()
          {

            $mobile=$this->input->post('mobile');

                //$this->load->model('sultanModel');

            $result=$this->contact_model->checkmobile($mobile);
            if($result)
            {
              echo  1;  
          }
          else
          {
              echo  0;  
          }
      } 
      public function create(){


        $primary_number=$this->uri->segment(3);
        $data['ui']=array('title'=>'Create','action'=>site_url('contact/save'),'okButton'=>'Save');
        $data['params']=array('mobile'=>$primary_number);
                    //$data['sale_info']=array();
                    //$data['child_view']=$this->load->view('controls/ctrl_select_customer', $data, TRUE);

        $this->load->view('contact/create', $data );

    }


    public function smssend(){


      $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
      $this->form_validation->set_rules('name','Name','trim|required');
      $this->form_validation->set_rules('telephone','Telephone','trim|required');
                    //$this->form_validation->set_message('is_unique_mobile_for_add','Mobile Number must be unique.');

      $data=array('ui'=>array('title'=>'Create','action'=>site_url('contact/sendsms'),'okButton'=>'Save')
        ,'params'=>$params
    );
      if($this->form_validation->run()){
        $return_value= $this->contact_model->sendsmsmodel($params);

        $this->session->set_flashdata('return_value', $return_value);
                        redirect('contact/smssend'); //for clearing input fields [stackoverflow :) ]
                    }
                    $this->load->view('contact/sendsms'); 
                }
                public function group_sms_send(){

                  $receiver=$this->input->post('group_name');
                  $msg=$this->input->post('msg');
                  $this->form_validation->set_rules('group_name','Group Name','trim|required');
                  $this->form_validation->set_rules('msg','Message','trim|required');

                  if($this->form_validation->run()){
                   $query="select mobile from  contact where group_name='$receiver' and mobile!='null'";
                   $results=$this->prime_model->getByQuery($query);
                    //echo $query;
                    // print_r($results);

                   foreach($results as $result){
                    $group_name =  $result['mobile'];
                     //$this->input->post('mobile');
                    $url = "http://portal.metrotel.com.bd/smsapi";
                    $data = array(
                      "api_key" => "C20000275d67bde7e5f5d1.77213309",
                      "type" => "text",
                      "contacts" => $group_name,
                      "senderid" => "8809612112233",
                      "msg" => $msg,
                  );
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $response = curl_exec($ch);
                    curl_close($ch);
                //return $response;
                }


            }
                   //$msg=urlencode($msg); //even you have already sent urlencoded message, you  must have to encode here again, becuase not getting encoded message through GET
                  //$result=file_get_contents("http://103.36.103.64/smsapi?api_key=$api_key&type=text&contacts=$receiver&senderid=$senderid&msg=$msg");
                  //echo file_get_contents("http://172.16.252.49/smsapi/send_sms.php/smsapi?api_key=$api_key&type=text&contacts=$receiver&senderid=$senderid&msg=$msg");
            $this->load->view('contact/sendsms',results); 

        }



        function send_sms() {
            $this->input->post('mobile');
            $url = "http://portal.metrotel.com.bd/smsapi";
            $data = array(
              "api_key" => "C20000275d67bde7e5f5d1.77213309",
              "type" => "text",
              "contacts" => "8801720861366",
              "senderid" => "8809612112233",
              "msg" => "test",
          );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
        } 

        public function save(){


            $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
            $this->form_validation->set_rules('name','Name','trim|required');
            $this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_add');
            $this->form_validation->set_message('is_unique_mobile_for_add','Mobile Number must be unique.');

            $data=array('ui'=>array('title'=>'Create','action'=>site_url('contact/save'),'okButton'=>'Save')
              ,'params'=>$params
          );
            if($this->form_validation->run()){
              $return_value= $this->contact_model->save($params); 

              $this->session->set_flashdata('return_value', $return_value);
                        redirect('contact/create'); //for clearing input fields [stackoverflow :) ] 
                    }        
                    $this->load->view('contact/create', $data);        
                    
                }
                function is_unique_mobile_for_add($str){
                    $field_value = $str; //this is redundant, but it's to show you how
                    //the content of the fields gets automatically passed to the method
                    if($this->contact_model->is_mobile_already_exist('add',$id=(int)$this->input->post('id'),$str)){
                      return false;
                  }
                  else return true;
              }

              public function CallAPI($method, $url, $data = false, $compCode)
              {

                $curl = curl_init();

                switch ($method)
                {
                 case "POST":
                 curl_setopt($curl, CURLOPT_POST, 1);

                 if ($data)
                   curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
               break;
               case "PUT":
               curl_setopt($curl, CURLOPT_PUT, 1);
               break;
               default:
               if ($data)
                   $url = sprintf("%s?%s", $url, http_build_query($data));
           }
           curl_setopt( $curl,CURLOPT_HTTPHEADER, array(
             "Comp-Code:$compCode"
             ,"Default-Comp:$compCode"
             ,'Content-Type: application/json'
         )
       ); 
                // Optional Authentication:
           curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
           curl_setopt($curl, CURLOPT_USERPWD, "username:password");

           curl_setopt($curl, CURLOPT_URL, $url);
           curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

           $result = curl_exec($curl);
           if(curl_errno($curl)){
            echo 'Request Error:' . curl_error($curl);
        }
                //print_r($result);
        curl_close($curl);

        return $result;
    }

    public function get_api_contact_info(){
                //$tracking_no=$this->uri->segment(3);

     $json=$this->CallAPI('GET', "http://192.168.1.106:8080/smartapi/customerlist", $data = false,'01');
              //$json=$this->CallAPI('GET', "http://192.168.1.147/smartapi/customerlist", $data = false,'01');
     print_r($json);
                //$response = file_get_contents('http://172.20.16.10/nhrc/index.php/incident_info/get_tracking_api_info?tracking_no=081800ID4TN');

     $json_array=json_decode($json,true);
     print_r($json_array);


                //192.168.1.106:8080/smartapi/customerlist?Comp-Code=01&Default-Comp=01

                    //return $json_array;             

 }

 public function import_attendance_data(){
    $compCode=array('01','02','03','06','07','08');
    $records=array();
    foreach($compCode as $value){
                  $json=$this->CallAPI('GET', "http://192.168.1.106:8080/smartapi/customerlist", $data = false,$value); //disable 14November 21
                  /*$json=$this->CallAPI('GET', "http://192.168.1.147:8080/smartapi/customerlist", $data = false,$value);*/// change only 106 to 147
                  $result= json_decode($json,true);
                  
                  foreach($result as $row){
                    $group_name='';
                    if($row['companyCode']=='01')
                      $group_name='Motijheel';
                  else if($row['companyCode']=='02')
                      $group_name='Banani';
                  else if($row['companyCode']=='03')
                      $group_name='Chawkbazar';
                  else if($row['companyCode']=='06')
                      $group_name='Helpline';
                  else if($row['companyCode']=='07')
                      $group_name='UK';
                  else if($row['companyCode']=='08')
                      $group_name='Patuatuli';
                  $records[]=array(
                      'customerCode'=>$row['customerCode']
                      ,'name'=>$row['customerName']
                      ,'group_name'=>$group_name
                      ,'mailing_street'=>$row['address']
                      ,'created_date'=>$row['createdDate']
                      ,'mobile'=>$row['telephone']
                      ,'email'=>$row['email']
                      ,'concerned_employee'=>$row['contactperson']
                  );
              }
          }


                //print_r($json);

                //print_r($records);
          $this->prime_model->bulkInsert('contact',$records);
      }

      public function search(){
        $primary_number=$this->input->post('primary_number');
        $conditions='';
        $query_id=0;
        
        $sql="select * from contact where 1 ";
        if($primary_number!=''){
          $conditions .=" and ( mobile like '%$primary_number%' or office_phone like '%$primary_number%' or home_phone like '%$primary_number%' ) ";
      }
      if(strlen($conditions)> 0){
          $sql .=$conditions;
          $query_id=$this->prime_model->insert("query",array('value'=>$conditions));
      }
      $record_count=$this->get_count($sql);
      echo json_encode(array('record_count'=>$record_count,'query_id'=>$query_id,'conditions'=>$primary_number));
  }


  public function search_by_group(){
    $params=$this->input->post(null);
    $group_name=addslashes(trim($params['group_name']));
    $created_by=addslashes(trim($params['created_by']));
    $start_date=addslashes(trim($params['start_date']));
    $end_date=addslashes(trim($params['end_date']));

    $conditions='';
    $query_id=0;

    $sql="select * from ticket where 1 ";
    if($group_name!=''){
     $conditions .=" and group_name like '%$group_name%'";
 }
 if($created_by!=''){
     $conditions .=" and created_by like '%$created_by%'";
 }
 if($start_date!=''){
     $conditions .=" and created_date >='$start_date 00:00:00'";
 }
 if($end_date!=''){
     $conditions .=" and created_date <='$end_date 23:59:59'";
 }

 if(strlen($conditions)> 0){
     $sql .=$conditions;
     $query_id=$this->prime_model->insert("query",array('value'=>$conditions));
 }
 $records_total=$this->get_count($sql);
 echo json_encode(array('records_total'=>$records_total,'query_id'=>$query_id));
}

public function search_by_sanmar_project(){
  $params=$this->input->post(null);
  $project_name=addslashes(trim($params['project_name']));
  $location=addslashes(trim($params['location']));
  $start_date=addslashes(trim($params['start_date']));
  $end_date=addslashes(trim($params['end_date']));

  $conditions='';
  $query_id=0;

  $sql="select * from project where 1 ";
  if($project_name!=''){
   $conditions .=" and project_name like '%$project_name%'";
}
if($location!=''){
   $conditions .=" and location like '%$location%'";
}
                /*if($start_date!=''){
                  $conditions .=" and start_date >='$start_date 00:00:00'";
                }
                if($end_date!=''){
                  $conditions .=" and start_date <='$end_date 23:59:59'";
              }*/
              
              if(strlen($conditions)> 0){
                  $sql .=$conditions;
                  $query_id=$this->prime_model->insert("query",array('value'=>$conditions));
                  /*echo "<pre>";
                  print_r($query_id);
                  die();*/
              }
              $records_total=$this->get_count($sql);
              echo json_encode(array('records_total'=>$records_total,'query_id'=>$query_id));
          }


          public function search_by_sanmar_ticket(){
              $params=$this->input->post(null);
              $status=addslashes(trim($params['status']));
              $respective_dept=addslashes(trim($params['respective_dept']));
              $start_date=addslashes(trim($params['start_date']));
              $end_date=addslashes(trim($params['end_date']));

              $conditions='';
              $query_id=0;

              $sql="select * from ticket where 1 ";
              if($status!=''){
               $conditions .=" and status like '%$status%'";
           }
           if($respective_dept!=''){
               $conditions .=" and respective_dept like '%$respective_dept%'";
           }
           if($start_date!=''){
              $conditions .=" and date >='$start_date'";
          }
          if($end_date!=''){
              $conditions .=" and date <='$end_date'";
          }

          if(strlen($conditions)> 0){
              $sql .=$conditions;
              $query_id=$this->prime_model->insert("query",array('value'=>$conditions));
                  /*echo "<pre>";
                  print_r($query_id);
                  die();*/
              }
              $records_total=$this->get_count($sql);
              echo json_encode(array('records_total'=>$records_total,'query_id'=>$query_id));
              
               // $this->product_model->download_cdr_chart($records_total);
          }public function search_by_sanmar_customer(){
              $params=$this->input->post(null);
              $location=addslashes(trim($params['location']));
              $source=addslashes(trim($params['source']));
              $interested_project=addslashes(trim($params['interested_project']));
              /*$start_date=addslashes(trim($params['start_date']));
              $end_date=addslashes(trim($params['end_date']));*/

              $conditions='';
              $query_id=0;

              $sql="select * from customer where 1 ";
              if($source!=''){
               $conditions .=" and source like '%$source%'";
           }
           if($location!=''){
               $conditions .=" and location like '%$location%'";
           }
           if($interested_project!=''){
               $conditions .=" and interested_project like '%$interested_project%'";
           }
       /*    if($start_date!=''){
              $conditions .=" and date >='$start_date'";
          }
          if($end_date!=''){
              $conditions .=" and date <='$end_date'";
          }*/

          if(strlen($conditions)> 0){
              $sql .=$conditions;
              $query_id=$this->prime_model->insert("query",array('value'=>$conditions));
                  /*echo "<pre>";
                  print_r($query_id);
                  die();*/
              }
              $records_total=$this->get_count($sql);
              echo json_encode(array('records_total'=>$records_total,'query_id'=>$query_id));
              
               // $this->product_model->download_cdr_chart($records_total);
          }
          public function search_ticket1(){
            $params=$this->input->post(null);

            $received_by=addslashes(trim($params['received_by']));
            
            $status=addslashes(trim($params['status']));
            $start_date=addslashes(trim($params['start_date']));
            $end_date=addslashes(trim($params['end_date']));
            

            $conditions='';
            $query_id=0;


            
        $sql=" from ticket where 1 ";  //select *
        
        if($received_by!=''){
            $sql .=" and (created_by='$received_by') ";
        }
        if($status!=''){
            $sql .=" and (status='$status') ";
        }
        if($start_date!=''){
            $sql .=" and (date >='$start_date 00:00:00') ";
        }

        if($end_date!=''){
            $sql .=" and (date <='$end_date 23:59:59') ";
        }
        
        
        //$sql .=" order by calldate desc";

        
        $query_id=$this->prime_model->insert("query",array('value'=>$sql));
        
        $this->session->set_userdata(array('query_id'=>$query_id));
        //$txt = "sohan writes the file";
        /*$txt = print_r($this->session->userdata('query_id'), true);//;
        file_put_contents('/var/www/html/cdr/query.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);*/
        //echo "Current User: ". $this->session->userdata('current_user');
        $records_total=$this->get_count("select count(*)as total $sql");
        //echo $sql;
        echo json_encode(array('query_id'=>$query_id,'records_total'=>$records_total));
    }
    


    public function get_count($sql){
        $query = $this->db->query($sql);
        return $query->num_rows($query);
    }
              /*public function get_initial_parameters(){
                $recordsTotal=$this->get_count("select * from contact where 1");
                echo json_encode(array('recordsTotal'=>$recordsTotal,'query_id'=>0));
            }*/



            public function process_paging_ticket(){
                $sql="select * from ticket where 1 ";

                $conditions='';
                $query_id=$this->input->post('query_id');
                if($query_id>0){
                 $temp=$this->prime_model->getByID('query','id',$query_id);
                 $conditions=$temp['value'];
                 $sql .=$conditions;
             }
             $recordsTotal=$this->get_count($sql);
                $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
                
                $draw=$this->input->post('draw');
                $search=$this->input->post('search');
                $start=$this->input->post('start');
                $length=$this->input->post('length');
                
                
                if($search['value']!=''){
                  $value=$search['value'];
                  $sql .=" and ( name like '%$value%' or project like '%$value%' or date like '%$value%' or phone like '%$value%' or shop_no like '%$value%' or level_no like '%$value%' or car_park like '%$value%' or birthdate like '%$value%' or occupation like '%$value%' or purpose_call like '%$value%' or deadline like '%$value%' or permanent_address like '%$value%' or present_address like '%$value%' or pd_category like '%$value%' or details like '%$value%'  or respective_dept like '%$value%'  or query_details like '%$value%' or required_time like '%$value%'or duration like '%$value%' or user like '%$value%')";
                  $recordsFiltered=$this->get_count($sql);
              }
              
                //for getting data with limit
              $sql .=" order by id desc limit $start,$length";
              
              $contacts=$this->prime_model->getByQuery($sql) ;

              $output=array();
              $i=$start+1;
              $current_user=$this->user_model->get_current_user();
              foreach($contacts as $item){



                  $btn_details="<a href='". site_url('contact/view_ticket_dateils/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;
                  $btn_edit="<a href='". site_url('contact/edit_ticket/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;

                  
                  $btn_delete="";

                  if($current_user['group_id']==1){


                    $btn_delete="<a href='". site_url('contact/delete_ticket/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Edit\"></a>" ;

                }



                $output[]=array($i,$item['name'],$item['project'],$item['date'],$item['user'],$item['status']," $btn_edit $btn_delete $btn_details");
                $i++;
            }
            $json_data = array(
              "draw"            => $draw,   
              "recordsTotal"    => $recordsTotal ,  
              "recordsFiltered" => $recordsFiltered,
                       "data"            => $output   // total data array
                   );
            echo json_encode($json_data);
        }
        public function process_paging_maintanance(){
            $sql="select * from ticket where respective_dept='Maintenance'";

            $conditions='';
            $query_id=$this->input->post('query_id');
            if($query_id>0){
             $temp=$this->prime_model->getByID('query','id',$query_id);
             $conditions=$temp['value'];
             $sql .=$conditions;
         }
         $recordsTotal=$this->get_count($sql);
                $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
                
                $draw=$this->input->post('draw');
                $search=$this->input->post('search');
                $start=$this->input->post('start');
                $length=$this->input->post('length');
                
                
                if($search['value']!=''){
                  $value=$search['value'];
                  $sql .=" and ( name like '%$value%' or project like '%$value%' or date like '%$value%' or phone like '%$value%' or shop_no like '%$value%' or level_no like '%$value%' or car_park like '%$value%' or birthdate like '%$value%' or occupation like '%$value%' or purpose_call like '%$value%' or deadline like '%$value%' or permanent_address like '%$value%' or present_address like '%$value%' or pd_category like '%$value%' or details like '%$value%'  or respective_dept like '%$value%'  or query_details like '%$value%' or required_time like '%$value%'or duration like '%$value%' or user like '%$value%')";
                  $recordsFiltered=$this->get_count($sql);
              }
              
                //for getting data with limit
              $sql .=" order by id desc limit $start,$length";
              
              $contacts=$this->prime_model->getByQuery($sql) ;

              $output=array();
              $i=$start+1;
              $current_user=$this->user_model->get_current_user();
              foreach($contacts as $item){



                  $btn_details="<a href='". site_url('contact/view_ticket_dateils/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;
                  $btn_edit="<a href='". site_url('contact/update_maintanance/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;

                  
                  $btn_delete="";

                  if($current_user['group_id']==1){


                    $btn_delete="<a href='". site_url('contact/delete_ticket/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Edit\"></a>" ;

                }



                $output[]=array($i,$item['name'],$item['project'],$item['respective_dept'],$item['user'],$item['status']," $btn_edit $btn_delete $btn_details");
                $i++;
            }
            $json_data = array(
              "draw"            => $draw,   
              "recordsTotal"    => $recordsTotal ,  
              "recordsFiltered" => $recordsFiltered,
                       "data"            => $output   // total data array
                   );
            echo json_encode($json_data);
        }
        public function process_paging_Customer_Service(){
            $sql="select * from ticket where respective_dept='Customer_Service'";

            $conditions='';
            $query_id=$this->input->post('query_id');
            if($query_id>0){
             $temp=$this->prime_model->getByID('query','id',$query_id);
             $conditions=$temp['value'];
             $sql .=$conditions;
         }
         $recordsTotal=$this->get_count($sql);
                $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
                
                $draw=$this->input->post('draw');
                $search=$this->input->post('search');
                $start=$this->input->post('start');
                $length=$this->input->post('length');
                
                
                if($search['value']!=''){
                  $value=$search['value'];
                  $sql .=" and ( name like '%$value%' or project like '%$value%' or date like '%$value%' or phone like '%$value%' or shop_no like '%$value%' or level_no like '%$value%' or car_park like '%$value%' or birthdate like '%$value%' or occupation like '%$value%' or purpose_call like '%$value%' or deadline like '%$value%' or permanent_address like '%$value%' or present_address like '%$value%' or pd_category like '%$value%' or details like '%$value%'  or respective_dept like '%$value%'  or query_details like '%$value%' or required_time like '%$value%'or duration like '%$value%' or user like '%$value%')";
                  $recordsFiltered=$this->get_count($sql);
              }
              
                //for getting data with limit
              $sql .=" order by id desc limit $start,$length";
              
              $contacts=$this->prime_model->getByQuery($sql) ;

              $output=array();
              $i=$start+1;
              $current_user=$this->user_model->get_current_user();
              foreach($contacts as $item){



                  $btn_details="<a href='". site_url('contact/view_ticket_dateils/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;
                  $btn_edit="<a href='". site_url('contact/edit_customer_service/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;

                  
                  $btn_delete="";

                  if($current_user['group_id']==1){


                    $btn_delete="<a href='". site_url('contact/delete_ticket/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Edit\"></a>" ;

                }



                $output[]=array($i,$item['name'],$item['project'],$item['respective_dept'],$item['user'],$item['status']," $btn_edit $btn_delete $btn_details");
                $i++;
            }
            $json_data = array(
              "draw"            => $draw,   
              "recordsTotal"    => $recordsTotal ,  
              "recordsFiltered" => $recordsFiltered,
                       "data"            => $output   // total data array
                   );
            echo json_encode($json_data);
        }

        public function process_paging_lifestyle(){
            $sql="select * from ticket where respective_dept='Lifestyle'";

            $conditions='';
            $query_id=$this->input->post('query_id');
            if($query_id>0){
             $temp=$this->prime_model->getByID('query','id',$query_id);
             $conditions=$temp['value'];
             $sql .=$conditions;
         }
         $recordsTotal=$this->get_count($sql);
                $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
                
                $draw=$this->input->post('draw');
                $search=$this->input->post('search');
                $start=$this->input->post('start');
                $length=$this->input->post('length');
                
                
                if($search['value']!=''){
                  $value=$search['value'];
                  $sql .=" and ( name like '%$value%' or project like '%$value%' or date like '%$value%' or phone like '%$value%' or shop_no like '%$value%' or level_no like '%$value%' or car_park like '%$value%' or birthdate like '%$value%' or occupation like '%$value%' or purpose_call like '%$value%' or deadline like '%$value%' or permanent_address like '%$value%' or present_address like '%$value%' or pd_category like '%$value%' or details like '%$value%'  or respective_dept like '%$value%'  or query_details like '%$value%' or required_time like '%$value%'or duration like '%$value%' or user like '%$value%')";
                  $recordsFiltered=$this->get_count($sql);
              }
              
                //for getting data with limit
              $sql .=" order by id desc limit $start,$length";
              
              $contacts=$this->prime_model->getByQuery($sql) ;

              $output=array();
              $i=$start+1;
              $current_user=$this->user_model->get_current_user();
              foreach($contacts as $item){



                  $btn_details="<a href='". site_url('contact/view_ticket_dateils/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;
                  $btn_edit="<a href='". site_url('contact/update_lifestyle/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;

                  
                  $btn_delete="";

                  if($current_user['group_id']==1){


                    $btn_delete="<a href='". site_url('contact/delete_ticket/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Edit\"></a>" ;

                }



                $output[]=array($i,$item['name'],$item['project'],$item['respective_dept'],$item['user'],$item['status']," $btn_edit $btn_delete $btn_details");
                $i++;
            }
            $json_data = array(
              "draw"            => $draw,   
              "recordsTotal"    => $recordsTotal ,  
              "recordsFiltered" => $recordsFiltered,
                       "data"            => $output   // total data array
                   );
            echo json_encode($json_data);
        }
        public function process_ctrl_paging(){
            $sql="select * from contact where 1 ";

            $conditions='';
            $query_id=$this->input->post('query_id');
                /*if($query_id>0){
                  $temp=$this->prime_model->getByID('query','id',$query_id);
                  $conditions=$temp['value'];
                  $sql .=$conditions;
              }*/
              $recordsTotal=$this->get_count($sql);
                $recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
                
                $draw=false;
                $search=$this->input->post('search');
                $start=0; 
                $length=10;
                
                if($search['value']!=''){
                  $value=$search['value'];
                  $sql .=" and ( number like '%$value%' or name like '%$value%' or address like '%$value%' or sales like '%$value%' or target like '%$value%' or rate like '%$value%' or category like '%$value%' or district like '%$value%'or start_date like '%$value%' or remarks like '%$value%')";
                  $recordsFiltered=$this->get_count($sql);
              }
              
                //for getting data with limit
              $sql .=" limit 0,10";
              
              $contacts=$this->prime_model->getByQuery($sql) ;
              $output=array();
              $i=$start+1;
              foreach($contacts as $item){
                  $id=$item['id'];
                  $name=$item['first_name']." ".$item['middle_name']." ".$item['last_name'];
                  $output[]=array($i,$item['first_name'],$item['last_name'],$item['email'],"<a href='#' class='select_contact' contact_id='$id' contact_name='$name' title='select'>".$item['mobile']."</a>",$item['department']);
                  $i++;
              }
              $json_data = array(
                  "draw"            => $draw,   
                  "recordsTotal"    => $recordsTotal ,  
                  "recordsFiltered" => $recordsFiltered,
                       "data"            => $output   // total data array
                   );
              echo json_encode($json_data);
          }


          public function index(){


                //$data['ui']=array('title'=>'Open','statusButton'=>'TRUE');
                //$primary_number=$this->uri->segment(3);
                /*if($primary_number!=''){
                  $data['search_info']="Search result of case against phone: $primary_number";
                  $data['raw'] = $this->incident_info_model->get_case_by_complainants_number($primary_number);
              }*/
                //else
              $it_user=$this->user_model->is_user_it();

              $primary_number=$this->uri->segment(3);
              /*  if($it_user=='true'){
                  $data=array('it_user'=>true);
              }*/
              /*  if($primary_number!=''){      
                  $data=array('search_contact'=>true,'primary_number'=>$primary_number);
                  
                
                }
                else{
                  $data['raw'] = array();
                  // $this->contact_model->get_all_contact_details();
              }*/
              redirect('contact/dashboard');
                 //$this->load->view('sanmar/customer');
                //$this->load->view('contact/contact_list',$data);
          }
          public function duplicate_mobile_numbers(){
            $sql = "select name,group_name,email,mobile,department,lead_source,issue_date,expiry_date, count(*) AS duplicates from contact group by mobile HAVING COUNT(*)>1 order by duplicates  desc";

            /*
            $sql = "select mobile,count(*) AS duplicates from contact group by mobile HAVING COUNT(*)>1 order by duplicates  desc";*/
            /**/
            $query = $this->prime_model->getByQuery($sql);
            $data['raw']=$query;
                /*echo "<pre>";
            print_r($data);
            die();*/
                //$data['raw'] = $this->prime_model->getByQuery($sql);
            $this->load->view('contact/duplicate',$data);
            //$result = $conn->query($sql);
            /*echo "<pre>";
            print_r($result);
            die();
            if ($result->num_rows > 0) 
            {
              // output data of each row
              while($row = $result->fetch_assoc()) 
              {
                echo $row["mobile"]. "<br>";
              }
            } 
            else 
            {
              echo "string";
          }*/
      }
      public function duplicate($id){

                    //echo "string";

          $data['raw'] = $this->contact_model->get_duplicate_contact_details($id);
          echo "<pre>";
          print_r($data);
          die();
          $this->load->view('contact/duplicate_list',$data);

      }

      public function view_contact_details($id){
                    //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');

          $data['contact_info']=$this->contact_model->get_contact_by_id($id);
          $this->load->view('contact/view_details',$data);


      }
      public function view_sultan_dateils($id){
          $data['sultan_info']=$this->contact_model->get_sultan_details_by_id($id);
          $this->load->view('sultan/view_details_sultan',$data);
      }
      public function view_sultan(){
                    //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');
                //$data['productlist']=$this->ProductModel->get_all_product();
                /*echo '<pre>';p
                print_r($data);*/
                //$this->load->view('product',$data);
                $data['sultan_info']=$this->contact_model->get_sultan_by_id();
                $this->load->view('sultan/view',$data);


            } 




            public function edit($id){

                $data['ui']=array('title'=>'Edit','action'=>site_url("contact/update/$id"),'okButton'=>'Update');

                $data['params']=$this->contact_model->get_contact_by_id($id);

                $this->load->view('contact/create',$data);

            }

            public function update($id){
                $this->form_validation->set_rules('name','Name','trim|required');
                $this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_edit');
                $this->form_validation->set_message('is_unique_mobile_for_edit','Mobile Number must be unique.');

                $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
                $data['params']=$params;
                if($this->form_validation->run()){
                        //$client=$this->client_manager_model->refreshObject($params);
                  $return_value = $this->contact_model->save($params);

                  $this->session->set_flashdata('return_value', $return_value);
                        redirect('contact/edit/'.$id); //for clearing input fields [stackoverflow :) ]
                    }
                    else{
                        $data['ui']=array('title'=>'Edit','action'=>site_url("contact/update/$id"),'okButton'=>'Update');
                        
                        $data['params']=$params;
                        $this->load->view('contact/create', $data);

                    }
                }

                function is_unique_mobile_for_edit($str)
                {
                    $field_value = $str; //this is redundant, but it's to show you how
                    //the content of the fields gets automatically passed to the method
                    if($this->contact_model->is_mobile_already_exist('edit',$id=(int)$this->input->post('id'),$str)){
                      return false;
                  }
                  else return true;
              }

              public function download_contact_chart(){
                $this->contact_model->download_contact_chart();
            }
            



            public function delete($id)
            {
                $this->contact_model->delete_by_id($id);
                echo "1";
            }
            public function delete_sultan($id)
            {
               $this->contact_model->delete_sultan($id);
               echo "1";
           }
           public function testpr(){
              $this->load->view('sanmar/project_dt');
          }

          public function jsondata()
          {
                 //return $this->response->setJson(['msg'=>'update-success']);
             return $this->response->setJson(['msg'=>'update-success']);

         }

     }
