<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class control extends CI_Controller {
	
    public function __construct() {
        parent::__construct ();
        $this->load->model('prime_model');
        
    }	
    /*public function checkAuthorization()
    {
        //$current_user=$this->user_model->get_current_user();
        if(!$this->user_manager_model->is_logged_in()){
            redirect('/login/index');
            return;
        }
        $hasPermission=$this->user_manager_model->has_permission_for_role($this->role_manager_model->See_clients);
        if(!$hasPermission){
            redirect('/login/index');
            return;
        }
    }
    */
    public function get_data_assigned_to(){
		$assign_type=$this->input->post('assign_type');
		if ($assign_type=='user')
			$collection=$this->prime_model->getByQuery('select * from user order by name');
		else $collection=$this->prime_model->getByQuery('select * from team order by name');
		
		print_r(json_encode($collection, JSON_HEX_QUOT | JSON_HEX_TAG)) ;
	}
    public function printJSONofView($partialViewName,$data,$boolSuccess,$extra=''){
        ob_start();
        $this->load->view($partialViewName, $data);
        $content=ob_get_clean();
        $arr_data = array('success' => $boolSuccess,'content' => $content,'extra'=>$extra);
        print_r(json_encode($arr_data, JSON_HEX_QUOT | JSON_HEX_TAG)) ;
        
    }
    
    public function reload_ctrl_select_phone_numbers($client_id=false){
        //$client_id=$this->input->post('client_id');
        $data['params']=array('client_id'=>$client_id);
        //echo $client_id;
        //$this->load->view('controls/ctrl_select_Phone_numbers', $data);
        $this->printJSONofView('controls/ctrl_select_phone_numbers',$data,true);
        return;
    }
    public function reload_ctrl_select_phone_numbers_tech($client_id=false){
        //$client_id=$this->input->post('client_id');
        $data['params']=array('client_id'=>$client_id);
        //echo $client_id;
        //$this->load->view('controls/ctrl_select_Phone_numbers', $data);
        $this->printJSONofView('controls/ctrl_select_phone_numbers_tech',$data,true);
        return;
    }
}