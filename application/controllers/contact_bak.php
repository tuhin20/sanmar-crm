<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class contact extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->model('contact_model');
        $this->load->model('prime_model');
        $this->load->model('user_model');
        $this->load->helper(array('form', 'url'));
		$this->checkAuthorization();
		
    }
	
	public function checkAuthorization()
        {
			
			
            //$current_user=$this->user_model->get_current_user();
            if(!$this->user_model->is_logged_in()){
				//$this->session->set_userdata(array('redirect_after_login'=>site_url("ticket/search/$caller_number")));
                redirect('/login/index');
                return;
            }
            /*$hasPermission=$this->user_model->has_permission_for_role($this->role_manager_model->See_isd_rate_chart);
            if(!$hasPermission){
                redirect('/login/index');
                return;
            }*/
        }

    public function create(){
		
			
		$primary_number=$this->uri->segment(3);
        $data['ui']=array('title'=>'Create','action'=>site_url('contact/save'),'okButton'=>'Save');
		$data['params']=array('mobile'=>$primary_number);
        //$data['sale_info']=array();
        //$data['child_view']=$this->load->view('controls/ctrl_select_customer', $data, TRUE);

        $this->load->view('contact/create', $data );
        
    }
	
	public function save(){
		
		
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $this->form_validation->set_rules('first_name','First Name','trim|required');
        $this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_add');
		$this->form_validation->set_message('is_unique_mobile_for_add','Mobile Number must be unique.');
		
        $data=array('ui'=>array('title'=>'Create','action'=>site_url('contact/save'),'okButton'=>'Save')
                    ,'params'=>$params
            );
        if($this->form_validation->run()){
            $return_value= $this->contact_model->save($params); 
                        
            $this->session->set_flashdata('return_value', $return_value);
            redirect('contact/create'); //for clearing input fields [stackoverflow :) ] 
        }        
        $this->load->view('contact/create', $data);        
          
	}
	function is_unique_mobile_for_add($str){
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->contact_model->is_mobile_already_exist('add',$id=(int)$this->input->post('id'),$str)){
            return false;
        }
        else return true;
    }
	
	public function search(){
		$primary_number=$this->input->post('primary_number');
		$conditions='';
		$query_id=0;
		
		$sql="select * from contact where 1 ";
		if($primary_number!=''){
			$conditions .=" and ( mobile like '%$primary_number%' or office_phone like '%$primary_number%' or home_phone like '%$primary_number%' ) ";
		}
		if(strlen($conditions)> 0){
			$sql .=$conditions;
			$query_id=$this->prime_model->insert("query",array('value'=>$conditions));
		}
		$record_count=$this->get_count($sql);
		echo json_encode(array('record_count'=>$record_count,'query_id'=>$query_id,'conditions'=>$primary_number));
    }
	
	public function search_by_group(){
		$params=$this->input->post(null);
		$group_name=addslashes(trim($params['group_name']));
		
		$conditions='';
		$query_id=0;
		
		$sql="select * from contact where 1 ";
		if($group_name!=''){
			$conditions .=" and group_name like '%$group_name%'";
		}
		
		if(strlen($conditions)> 0){
			$sql .=$conditions;
			$query_id=$this->prime_model->insert("query",array('value'=>$conditions));
		}
		//$records_total=$this->get_count("select count(*)as total $sql");
		$records_total=$this->get_count($sql);
		echo json_encode(array('query_id'=>$query_id,'records_total'=>$records_total));
    }
	
	public function get_count($sql){
		/*$query = $this->db->query($sql);
		return $query->num_rows($query);*/
		$result=$this->prime_model->getByQuery($sql); //select count(*)as total from ($sql)as mytable
		return $result[0]['total'];
	}
	/*public function get_initial_parameters(){
		$recordsTotal=$this->get_count("select * from contact where 1");
		echo json_encode(array('recordsTotal'=>$recordsTotal,'query_id'=>0));
	}*/
	
	
	public function process_paging(){//$this->session->set_userdata(array('query_id'=>$query_id));
		$sql=" from contact where 1 ";
		//$conditions='';
		$query_id=$this->input->post('query_id');
		if($query_id>0){
			$temp=$this->prime_model->getByID('query','id',$query_id);
			$sql=$temp['value'];
		}
		
		$count_sql="select count(*)as total $sql";		
		$sql="select * $sql";
		//$recordsTotal=$this->input->post('recordsTotal');
		if(!isset($_POST['records_total'])){  //if($recordsTotal== NULL)			
			$records_total=$this->get_count($count_sql);
		}
		else{
			$records_total=$_POST['records_total'];
		}
		$recordsFiltered=$records_total; //by default its equal to total record when no search applied
		
		$draw=$this->input->post('draw');
		$search=$this->input->post('search');
		$start=$this->input->post('start');
		$length=$this->input->post('length');
		
		if($search['value']!=''){
			$value=$search['value'];
			$sql .=" and ( first_name like '%$value%' or middle_name like '%$value%' or last_name like '%$value%' or email like '%$value%' or mobile like '%$value%' or department like '%$value%' or lead_source like '%$value%' or group_name like '%$value%' or issue_date like '%$value%' or expiry_date like '%$value%' or created_date like '%$value%')";
			$recordsFiltered=$this->get_count($sql);
		}
		
		//for getting data with limit
		$sql .=" order by id desc limit $start,$length";
		
		$contacts=$this->prime_model->getByQuery($sql) ;
		$output=array();
		$i=$start+1;
		foreach($contacts as $item){
			//buttons
			$btn_details="<a href='". site_url('contact/view_contact_details/'.$item['id'])."' class=\"btn btn-success glyphicon glyphicon-eye-open\" title=\"Details\"></a>" ;
			
			$btn_edit="<a href='". site_url('contact/edit/'.$item['id'])."' class=\"btn btn-warning glyphicon glyphicon-pencil\" title=\"Edit\"></a>" ;
			
			$btn_delete="<a href='". site_url('contact/delete/'.$item['id'])."' class=\"btn btn-danger glyphicon glyphicon glyphicon-trash delete\" title=\"Edit\"></a>" ;

                                    //echo '<a href='. site_url().'/contact/edit/'.$row->id.' class="btn btn-warning glyphicon glyphicon-pencil" title="Edit"></a>';


                                    //echo '<a href='. site_url().'/contact/delete/'.$row->id.' class="btn btn-danger glyphicon glyphicon-trash delete" title="Delete Info"></a>';
			//end of buttons
			
			$output[]=array($i,$item['first_name'],$item['middle_name'],$item['last_name'],$item['email'],$item['mobile'],$item['department'],$item['lead_source'],$item['group_name'],$item['issue_date'],$item['expiry_date'],$item['created_date']," $btn_details $btn_edit $btn_delete");
			$i++;
		}
		$json_data = array(
					 "draw"            => $draw,   
					 "records_total"    => $records_total ,  
					 "recordsFiltered" => $recordsFiltered,
					 "data"            => $output   // total data array
					 );
		echo json_encode($json_data);
    }
	public function process_ctrl_paging(){
		$sql="select * from contact where 1 ";
		
		$conditions='';
		$query_id=$this->input->post('query_id');
		/*if($query_id>0){
			$temp=$this->prime_model->getByID('query','id',$query_id);
			$conditions=$temp['value'];
			$sql .=$conditions;
		}*/
		$recordsTotal=$this->get_count($sql);
		$recordsFiltered=$recordsTotal; //by default its equal to total record when no search applied
		
		$draw=false;
		$search=$this->input->post('search');
		$start=0; 
		$length=10;
		
		if($search['value']!=''){
			$value=$search['value'];
			$sql .=" and ( first_name like '%$value%' or last_name like '%$value%' or email like '%$value%' or mobile like '%$value%' or department like '%$value%' or lead_source like '%$value%' or issue_date like '%$value%' or expiry_date like '%$value%')";
			$recordsFiltered=$this->get_count($sql);
		}
		
		//for getting data with limit
		$sql .=" limit 0,10";
		
		$contacts=$this->prime_model->getByQuery($sql) ;
		$output=array();
		$i=$start+1;
		foreach($contacts as $item){
			$id=$item['id'];
			$name=$item['first_name']." ".$item['middle_name']." ".$item['last_name'];
			$output[]=array($i,$item['first_name'],$item['last_name'],$item['email'],"<a href='#' class='select_contact' contact_id='$id' contact_name='$name' title='select'>".$item['mobile']."</a>",$item['department']);
			$i++;
		}
		$json_data = array(
					 "draw"            => $draw,   
					 "recordsTotal"    => $recordsTotal ,  
					 "recordsFiltered" => $recordsFiltered,
					 "data"            => $output   // total data array
					 );
		echo json_encode($json_data);
    }
	
	
	public function index(){
		
		
		//$data['ui']=array('title'=>'Open','statusButton'=>'TRUE');
		//$primary_number=$this->uri->segment(3);
		/*if($primary_number!=''){
			$data['search_info']="Search result of case against phone: $primary_number";
			$data['raw'] = $this->incident_info_model->get_case_by_complainants_number($primary_number);
		}*/
		//else
		$it_user=$this->user_model->is_user_it();
	    
		$primary_number=$this->uri->segment(3);
	/*	if($it_user=='true'){
			$data=array('it_user'=>true);
		}*/
		if($primary_number!=''){			
			$data=array('search_contact'=>true,'primary_number'=>$primary_number);
			
			/*$extra="";
			if(sizeof($data['raw'])==0){
				$url=site_url("contact/create/$primary_number");
				$extra=" <a role='button' class='btn btn-info' href='$url'> Add to Contact</a>";
			}
			$data['search_info']="Search result of contacts against phone: $primary_number . $extra";*/
		}
		else{
			$data['raw'] = array();// $this->contact_model->get_all_contact_details();
		}
		
		$this->load->view('contact/contact_list',$data);
	}
	
	public function duplicate($id){
		
		$data['raw'] = $this->contact_model->get_duplicate_contact_details($id);
		
		$this->load->view('contact/duplicate_list',$data);
		
	}
	
	 public function view_contact_details($id){
        //$data['ui']=array('title'=>'Edit','action'=>site_url("client_manager/update_client/$id"),'okButton'=>'Update');
		
        $data['contact_info']=$this->contact_model->get_contact_by_id($id);
        $this->load->view('contact/view_details',$data);


    }	
    
  
	
	public function edit($id){

        $data['ui']=array('title'=>'Edit','action'=>site_url("contact/update/$id"),'okButton'=>'Update');

        $data['params']=$this->contact_model->get_contact_by_id($id);

        $this->load->view('contact/create',$data);
        
    }
    
    public function update($id){
        $this->form_validation->set_rules('first_name','First Name','trim|required');
		$this->form_validation->set_rules('mobile','Mobile','trim|required|callback_is_unique_mobile_for_edit');
		$this->form_validation->set_message('is_unique_mobile_for_edit','Mobile Number must be unique.');
		
        $params = $this->security->xss_clean($this->input->post(NULL, TRUE));
        $data['params']=$params;
        if($this->form_validation->run()){
            //$client=$this->client_manager_model->refreshObject($params);
            $return_value = $this->contact_model->save($params);

            $this->session->set_flashdata('return_value', $return_value);
            redirect('contact/edit/'.$id); //for clearing input fields [stackoverflow :) ]
        }
        else{
            $data['ui']=array('title'=>'Edit','action'=>site_url("contact/update/$id"),'okButton'=>'Update');
            
            $data['params']=$params;
            $this->load->view('contact/create', $data);

        }
    }
	
	function is_unique_mobile_for_edit($str)
    {
        $field_value = $str; //this is redundant, but it's to show you how
        //the content of the fields gets automatically passed to the method
        if($this->contact_model->is_mobile_already_exist('edit',$id=(int)$this->input->post('id'),$str)){
            return false;
        }
        else return true;
    }
	
	public function download_contact_chart(){
            $this->contact_model->download_contact_chart();
        }
	
	
	
	public function delete($id)
    {
        $this->contact_model->delete_by_id($id);
        echo "1";
    }

}
