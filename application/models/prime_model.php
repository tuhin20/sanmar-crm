<?php
class prime_model extends CI_Model {

    public function get($table,$orderBy=false,$sortOrder='asc',$limit,$start) {

        $this->db->from($table);
        if($orderBy){

          $this->db->order_by($orderBy, $sortOrder);
        }
        if($limit){
          $this->db->limit($limit, $start);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
  public function get_data_model($id){
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('phone',$id);
        $query=$this->db->get();
        $result=$query->row_array();
        return $result;
    }
    public function getByID($table,$idField,$id) {
        $this->db->from($table);
        $this->db->where($idField,$id);
        $query = $this->db->get();
        $results=$query->result_array();
        return $results[0];
    }

    public function getByQuery($sql) {
        $query=$this->db->query($sql);
        return $query->result_array();
    }
	
	public function getFirstRow($sql) {
        $query=$this->db->query($sql);
		return $query->row_array();
		
    }
	
    public function executeQuery($sql) {
        $this->db->query($sql);
    }
    public function insert($table,$data){
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }
    public function insert_details($table,$data){
        $this->load->helper('date');
        $datestring = "%Y-%m-%d  %H:%i:%s";
        $time = time();
        $data['created_date']=mdate($datestring, $time);
        $current_user=$this->user_model->get_current_user();
        $data['created_by']=$current_user['name'];
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }
	
	  public function insert_ticket_details($table,$data){
        $this->load->helper('date');
        $datestring = "%Y-%m-%d  %H:%i:%s";
        $time = time();
        $data['created_date']=mdate($datestring, $time);
        $current_user=$this->user_model->get_current_user();
        $data['created_by']=$current_user['email'];
        
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }
	
	public function insert_feedback_details($table,$data){
        $this->load->helper('date');
        $datestring = "%Y-%m-%d  %H:%i:%s";
        $time = time();
        $data['created_date']=mdate($datestring, $time);
        $current_user=$this->user_model->get_current_user();
        $data['created_by']=$current_user['email'];
        
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }
	
    public function bulkInsert($table,$data){
        $this->db->insert_batch($table, $data);
    }
    public function update($table,$data){
        $this->db->where('id',$data['id'])->update($table,$data);
    }
    public function update_details($table,$data){
        $this->load->helper('date');
        $datestring = "%Y-%m-%d  %H:%i:%s";
        $time = time();
        $data['modified_date']=mdate($datestring, $time);
        $current_user=$this->user_model->get_current_user();
        $data['modified_by']=$current_user['id'];
        $datestring = "%Y-%m-%d %h:%i:s";
        $data['created_date']=mdate($datestring);
        //print_r($data);
        $this->db->where('id',$data['id'])->update($table,$data);
    }
    
    
    public function send_mail($subject,$msg_txt){
        $to = "muzahid@metro.net.bd";//"meher@metro.net.bd,shohel@metro.net.bd";
        $subject = $subject;
        //$msgTxt='test msg';
        $msg_txt="Dear concern, "."\r\n\r\n" .$msg_txt."\r\n\r\n"."Thanks\r\n\r\nMetroTel Billing";
        //$headers = "CC: smstomeher@gmail.com";
        $headers = "";
        $isSuccess=mail($to,$subject,$msg_txt,$headers,"-f 'billing@metrotel.com.bd'");
        //$isSuccess=true;
        return $isSuccess;
    }

}