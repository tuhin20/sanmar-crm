<?php
class product_model extends CI_Model
{
    var $table = 'project';
    var $table1 = 'ticket';
    
    public $queryProject="select * from project ";
    public $queryTicket="select * from ticket ";
    


	//public $orderPart=" order by client.name,client.id";
    public $orderPart="order by project.id desc";
    public $orderTicket="order by ticket.id desc";


    public function __construct()
    {
        parent::__construct();
        //$this->load->model('utility_model');
        $this->load->helper('date');
        $this->load->database();

    }
    public function download_landowner_chart($data){

     $this->db->select('*');
     $this->db->from('landowner');

     $query=$this->db->get();

     $isd_data=$query->result_array();
     header('Content-Type: text/csv; charset=utf-8');
     header('Content-Disposition: attachment; filename=Landowner.csv');

            // create a file pointer connected to the output stream
     $output = fopen('php://output', 'w');
     fputcsv($output, array('ID'
        ,'Landowner'
        , 'Number'
        , 'Name'
        ,'Address'
        ,'Measurement'
        ,'Front road width'
        ,'Side road width'
        ,'Land status'
        ,'Types of land'
       

    ));

     foreach($isd_data as $item)
     {

        fputcsv($output,array(
           $item['id']
           ,$item['name']
           ,$item['contact_number']
           ,$item['contact_name']
           ,$item['registered_address']
           ,$item['measurement']
           ,$item['front_road_width']
           ,$item['side_road_width']
           ,$item['land_status']
           ,$item['types_of_land']
           

       ));
    }

    fclose($output);                 

}
public function download_career_chart($data){

     $this->db->select('*');
     $this->db->from('career');

     $query=$this->db->get();

     $isd_data=$query->result_array();
     header('Content-Type: text/csv; charset=utf-8');
     header('Content-Disposition: attachment; filename=Career.csv');

            // create a file pointer connected to the output stream
     $output = fopen('php://output', 'w');
     fputcsv($output, array('ID'
        ,'name'
        , 'Number'
        , 'Email'
        ,'Location'
        ,'Position'
       

    ));

     foreach($isd_data as $item)
     {

        fputcsv($output,array(
           $item['id']
           ,$item['name']
           ,$item['contact_no']
           ,$item['email']
           ,$item['location']
           ,$item['position']
          
       ));
    }

    fclose($output);                 

}public function download_investor_chart($data){

     $this->db->select('*');
     $this->db->from('investor');

     $query=$this->db->get();

     $isd_data=$query->result_array();
     header('Content-Type: text/csv; charset=utf-8');
     header('Content-Disposition: attachment; filename=Investor.csv');

            // create a file pointer connected to the output stream
     $output = fopen('php://output', 'w');
     fputcsv($output, array('ID'
        ,'Name'
        , 'Contact No'
        , 'Email'
        ,'City'
        

    ));

     foreach($isd_data as $item)
     {

        fputcsv($output,array(
           $item['id']
           ,$item['name']
           ,$item['contact_no']
           ,$item['email']
           ,$item['city']
           
       ));
    }

    fclose($output);                 

}
public function download_project_chart($data){

 $this->db->select('*');
 $this->db->from('project');

 if ($data['location']) {
    $this->db->where('location',$data['location']);
}
if ($data['project_name']) {
    $this->db->where('project_name',$data['project_name']);
}
           /* if ($data['start_date']) {
                 $this->db->where('date >=', $data['start_date']); 
            } if ($data['end_date']) {
                 $this->db->where('date <=', $data['end_date']); 
             }*/
            /*$this->db->where('date >=', $data['start_date']); 
            $this->db->where('date <=',$data['end_date']); */
          //  $this->db->where('date <=',$end_date);
            /*$this->db->where('order_date >=', $first_date);
            $this->db->where('order_date <=', $second_date);*/

            $query=$this->db->get();

            $isd_data=$query->result_array();
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=Project.csv');

            // create a file pointer connected to the output stream
            $output = fopen('php://output', 'w');
            fputcsv($output, array('ID'
                ,'Project Name'
                , 'Project Type'
                , 'Location'
                ,'Storied'
                ,'Project Facing'
                ,'Total Units'
                ,'Total Salesable Unit'
                ,'Apartment'
                ,'Sold Unit'
                ,'Unsold'
                ,'Handover Date'
                ,'Total Land Area'
                ,'Project Usp'
                ,'Landmark'
                ,'Parking Per Unit'
                ,'Parking Space'


            ));

            foreach($isd_data as $item)
            {

                fputcsv($output,array(
                   $item['id']
                   ,$item['project_name']
                   ,$item['project_type']
                   ,$item['location']
                   ,$item['storied']
                   ,$item['project_facing']
                   ,$item['total_units']
                   ,$item['total_saleable_unit']
                   ,$item['apartment']
                   ,$item['sold_unit']
                   ,$item['unsold']
                   ,$item['handover_date']
                   ,$item['total_land_area']
                   ,$item['project_usp']
                   ,$item['landmark']
                   ,$item['parking_per_unit']
                   ,$item['parking_space'],



               ));
            }

            fclose($output);                 

        }
        public function get_project_ticket_by_id($id)
        {
            $query=  $this->queryProject1. "where ticket.id=$id";
            $results=  $this->prime_model->getByQuery($query);
            if(sizeof($results)>0)
                return $results[0];
            else return array();
        }
        public function edit_project($id){
           $this->db->select('*');
           $this->db->from('project');
           $this->db->where('id',$id);
           $query=$this->db->get();
           $result=$query->result_array();
           return $result;
       }  
       public function edit_customer($id){
           $this->db->select('*');
           $this->db->from('customer');
           $this->db->where('id',$id);
           $query=$this->db->get();
           $result=$query->result_array();
           return $result;
       }
       public function update_project($data,$id)
       {
         $this->db->where('id', $id);
         $this->db->update('project',$data);

     } 
     public function update_customer($data,$id)
     {
         $this->db->where('id', $id);
         $this->db->update('customer',$data);

     }
     public function delete_project($id){

        $this->db->from('project');
        $this->db->where('id',$id);
        $this->db->delete();

    }
    public function delete_landowner($id){

        $this->db->from('landowner');
        $this->db->where('id',$id);
        $this->db->delete();

    }
    public function delete_career($id){

        $this->db->from('career');
        $this->db->where('id',$id);
        $this->db->delete();

    }
    public function delete_investor($id){

        $this->db->from('investor');
        $this->db->where('id',$id);
        $this->db->delete();

    }

    public function is_mobile_already_exist($task,$id,$mobile) {
      if(substr($mobile,0,2)=='88')
         $mobile=substr($mobile,2);
     else if(substr($mobile,0,3)=='+88')
         $mobile=substr($mobile,3); 
        //$mobile=$this->db->escape(trim($mobile)); //It also automatically adds single quotes around the data so you don’t have to do that as well.
     if($task=='add')
         $sql="select * from contact where  mobile like '%$mobile%'";
     else 
         $sql="select * from contact where id<> $id and mobile like '%$mobile%'";
     $contacts=$this->prime_model->getByQuery($sql);
     if(sizeof($contacts)>0)
        return true;
    else return false;
}



public function save($params){
		//print_r($params);

    $data = array( 'id'=>$params['id'],
             //will unset during insert
        'name' => $params['name'],
        'category' => $params['category'],
        'code' => $params['code'],
        'unit_price' => $params['unit_price'],
        'description'=>$params['description']

    );

    if($params['id']==-1){
        unset($data['id']);
        $this->prime_model->insert_details('product', $data);
        $success=true;
    }

    else{
        $data=$this->prime_model->update_details('product', $data);
        $success=true;
    }

    if($success){
        return array('success'=>true,'msg'=>'Product saved successfully','test'=>$data);
    }
    else{
        return array('success'=>false,'msg'=>'Unable to save Product. Please try after sometime','test'=>$data);
    }
}

	/*   public function get_all_project_details()
    {
        $sql = "select * from project";

        $query = $this->db->query($sql);
        return $query->result();
    }  */
    public function get_all_project_details()
    {

        $this->db->select('*');
        $this->db->from('project');
        $query=$this->db->get();
        $result=$query->result_array();
        return $result;
    }
    public function get_all_landowner_details()
    {

        $this->db->select('*');
        $this->db->from('landowner');
        $query=$this->db->get();
        $result=$query->result_array();
        return $result;
    }
    public function get_all_investor_details()
    {

        $this->db->select('*');
        $this->db->from('investor');
        $query=$this->db->get();
        $result=$query->result_array();
        return $result;
    }
    public function test(){
       $sql="select name from career";
       $query = $this->db->query($sql);
       return $query->result();   
   }
   public function get_all_career_details()

   {


    $this->db->select('*');
    $this->db->from('career');
    $query=$this->db->get();
    $result=$query->result_array();
    return $result;
} 
public function get_all_customer_details()
{

    $this->db->select('*');
    $this->db->from('customer');
    $query=$this->db->get();
    $result=$query->result_array();
    return $result;
        /*$sql = "select * from customer";

        $query = $this->db->query($sql);
        echo "<pre>";
        print_r($query);
        die();
        return $query->result();*/
    }

    public function get_duplicate_contact_details()
    {
      $duplicates=array();
      $sql = "select t1.mobile,t2.mobile,count(*),group_concat(t2.id) as contact_ids from contact t1 inner join contact t2 on t2.mobile like concat('%',t1.mobile) where t1.mobile !='' group by t1.mobile having count(*) > 1 ";
      $result= $this->prime_model->getByQuery($sql);
      foreach($result as $row){
         $temp_result=$this->prime_model->getByQuery("select * from contact where id in (".$row['contact_ids'].")");
         foreach($temp_result as $item)
            $duplicates[]=$item;
    }
    return $duplicates;
}

public function get_project_details_by_id($id)
{
    $query=  $this->queryProject. "where project.id=$id";
    $results=  $this->prime_model->getByQuery($query);
    if(sizeof($results)>0)
        return $results[0];
    else return array();
}  


public function get_contact_by_primary_number($primary_number)
{
    $sql = "select * from contact where mobile like '%$primary_number%' or office_phone like '%$primary_number%' or home_phone like '%$primary_number%'";
    $query = $this->db->query($sql);
    return $query->result();
}



public function delete_by_id($id)
{
  $this->db->where('id', $id);
  $this->db->delete($this->table);
}

public function save_imported_product($result){
            //the first row 
    if(sizeof($result)<=1){
        return array('success'=>true,'msg'=>"Nothing to save.");
    }
    $first_row=true;
    $table_data=array();           
            //print_r($db_result);

    foreach($result as $value){
        if($first_row==TRUE){
            $first_row=false;
            continue;
        }
                /*$name = trim($value[0]);
                $prefix = trim($value[1]);
                $rate = trim($value[2]);*/


                $product_name = trim($value[0]);
                $product_category = trim($value[1]);
                $product_id = trim($value[2]);
                $unit_price = trim($value[3]);
                $description = trim($value[4]);


				//validation
                if( (strlen($product_id) == '')  ){
                    continue;
                }
                //end of validation


                $sale =  array(

                 'name' =>$product_name,
                 'category' =>$product_category,
                 'code' =>$product_id,
                 'unit_price' =>$unit_price,
                 'description' =>$description

             ); 



                $table_data[]=$sale;
			} //end of foreach
			
			//print_r($table_data);
			// end of foreach
			$this->prime_model->bulkInsert('product',$table_data);
			
            return array('success'=>true,'msg'=>'data imported successfully');
            //return array('success'=>false,'msg'=>"Unable to send SMS. Please try after sometime.");
            
        }

        public function download_cdr_chart($data){
/*$where_query='';
            if($query_id!=-1){
                $result=$this->prime_model->getByID('query','id',$query_id);
                $where_query=$result['where_condition'];
                //print_r( $where_query);
            }*/
            /*$isd_data=  $this->prime_model->getByQuery($this->queryTicket.$this->orderTicket);*/
            
            $this->db->select('*');
            $this->db->from('ticket');
              /*$data['status']='';
              $data['respective_dept']='';
              $data['start_date']='';
              $data['end_date']='';*/
            if ($data['status']) {
                $this->db->where('status',$data['status']);
            }
            if ($data['respective_dept']) {
                $this->db->where('respective_dept',$data['respective_dept']);
            }
            if ($data['start_date']) {
               $this->db->where('date >=', $data['start_date']); 
           } if ($data['end_date']) {
               $this->db->where('date <=', $data['end_date']); 
           }
            /*$this->db->where('date >=', $data['start_date']); 
            $this->db->where('date <=',$data['end_date']); */
          //  $this->db->where('date <=',$end_date);
            /*$this->db->where('order_date >=', $first_date);
            $this->db->where('order_date <=', $second_date);*/

            $query=$this->db->get();

            $isd_data=$query->result_array();


            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=Ticket.csv');

            // create a file pointer connected to the output stream
            $output = fopen('php://output', 'w');
            fputcsv($output, array('Customer Name', 'Project Name', 'Date','Phone','Shop no','level No','Car park','Birthdate','Occupation','Permanent Address','Present Address','Details','Respective Dept','Query Details','Required Time','User','Status','Ticket No'));

            foreach($isd_data as $item)
            {
                fputcsv($output,array($item['name'],$item['project'],$item['date'],$item['phone'],$item['shop_no'],$item['level_no'],$item['car_park'],$item['birthdate'],$item['occupation'],$item['permanent_address'],$item['present_address'],$item['details'],$item['respective_dept'],$item['query_details'],$item['required_time'],$item['user'],$item['status'],$item['ticket_no'],));
            }

            fclose($output);                 

        }


    }
