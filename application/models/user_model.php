<?php
class user_model extends CI_Model {
    public $generalQuery="select user.*,user_group.name as group_name from user  inner join user_group  on user.group_id=user_group.id  ";
    public $orderPart=" order by user.name,user.id";
    public function __construct() {
        parent::__construct();
        $this->load->model('role_model');

    }
    public function get_by_query($sql) {
        $query=$this->db->query($sql);
        $users= $query->result_array();
        for($i=0;$i<sizeof($users);$i++)
        {
            //$decrypted=$this->utility_model->decrypt($users[$i]['password']);
            //$users[$i]['password']=$decrypted;
        }
        return $users;
    }
    
    public function get_by_name_and_password($name,$password){
        //this function will be called after isValid so there be an entry
        //$encrypted=$this->utility_model->encrypt($password);
        $encrypted=sha1($password);
        $sql="select * from user where name='$name' and password='$encrypted'   ";
        $query=$this->db->query($sql);
        $users= $query->result_array();
        if(sizeof($users)>0)
            return $users[0];
        else return array();
    }
    public function is_name_already_exist($id,$name) {
		$name=$this->db->escape(trim($name)); //It also automatically adds single quotes around the data so you don’t have to do that as well. 
        $sql="select * from user where id<> $id and name=$name";
        //echo $sql;
        $users=$this->prime_model->getByQuery($sql);
        //$users= $query->result_array();
        if(sizeof($users)>0)
            return true;
        else return false;
    }
    public function is_email_already_exist($id,$email) {
		$email=$this->db->escape(trim($email)); //It also automatically adds single quotes around the data so you don’t have to do that as well. 
        $sql="select * from user where id<> $id and email=$email";
        //echo $sql;
        $users=$this->prime_model->getByQuery($sql);
        //$users= $query->result_array();
        if(sizeof($users)>0)
            return true;
        else return false;
    }
    public function is_old_password_valid($old_password){
        $is_valid=FALSE;
        $current_user=  $this->get_current_user();
        $temp=  $this->prime_model->getByQuery("select * from user where id=".$current_user['id']);
        //print_r($temp);
        if(sizeof($temp)>0){
            if((isset($temp[0]['password']))&&($temp[0]['password']==sha1($old_password))){
                $is_valid=TRUE;
            }
            //$is_valid=TRUE;
        }
        return $is_valid;
    }
    public function change_password($new_password){
        $current_user=  $this->get_current_user();
        $encrypted_new_password=  sha1($new_password);
        $this->prime_model->executeQuery("update user set password='$encrypted_new_password' where id=".$current_user['id']);
        return TRUE;
    }

    public function is_valid($name,$password){
		
        $encrypted=sha1($password);//$this->utility_model->encrypt($password);
        $sql="select * from user where name='$name' and password='$encrypted'   ";
		//echo $sql;
        $query=$this->db->query($sql);
        $users= $query->result_array();
		//print_r($users);
        if(sizeof($users)>0){
            return true;
        }
        else return false;
    }
    /*public function get_agent_groupID(){
        $sql=$sql="select * from user_group where name='agent'  ";
        $query=$this->db->query($sql);
        $results=$query->result_array();
        return $results[0]['id'];
    }*/
    public function log_user_in($name,$password){
        $user=$this->get_by_name_and_password($name,$password);

        $currentUser = array(
            'id'=>$user['id'],
            'name'  => $user['name'],
            'group_id'=>$user['group_id'],
            'email'     => $user['email'],
            'loggedIn' => TRUE
        );
        $this->session->set_userdata(array('currentUser_Billing'=>$currentUser));
		$id=$user['id'];
		$query="SELECT CONCAT( 'DROP TABLE IF EXISTS ', GROUP_CONCAT(table_name) , ';' ) ". 
				"AS statement FROM information_schema.tables ". 
				"WHERE table_schema = 'iptsp_billing' AND table_name LIKE 'my_temp_".$user['name']."_%'";
		//echo $query;
		$result=$this->prime_model->getByQuery($query);
		//print_r ($result);
		if(sizeof($result)>0){
			if($result[0]['statement']!=''){
				$this->prime_model->executeQuery($result[0]['statement']);
				
			}
		}
    }
    public function log_user_out(){
		$user=$this->session->userdata('currentUser_Billing');
		$id=$user['id'];
                $query="SELECT CONCAT( 'DROP TABLE IF EXISTS ', GROUP_CONCAT(table_name) , ';' ) ".
                                "AS statement FROM information_schema.tables ".
                                "WHERE table_schema = 'iptsp_billing' AND table_name LIKE 'my_temp_".$user['name']."_%'";
                //echo $query;
                $result=$this->prime_model->getByQuery($query);
                //print_r ($result);
                if(sizeof($result)>0){
                        if($result[0]['statement']!=''){
                                $this->prime_model->executeQuery($result[0]['statement']);

                        }
                }
        $this->session->unset_userdata('currentUser_Billing');
        //$this->session->sess_destroy();
    }
	
  /*  public function has_permission_for_role($strRole){
        $current_user=$this->get_current_user();
        if(!isset($current_user['id']))
            return false;
		if($current_user['name']=='super_admin')
			return true;
        $group_id=$current_user['group_id'];
        $sql="select * from permission p inner join role r on p.role_id=r.id where r.name='$strRole' and p.group_id=$group_id   ";
        //print_r($sql);
        $query=$this->db->query($sql);
        $permissions= $query->result_array();
        if(sizeof($permissions)>0){
            return true;
        }
        else return false;
    }*/
    public function get_current_user(){
         return $this->session->userdata('currentUser_Billing');//$this->session->all_userdata();
    }
    public function is_logged_in(){
        $current_user= $this->session->userdata('currentUser_Billing');//$this->session->all_userdata();
        if(!isset($current_user['id'])){
            return false;
        }
        else return true;
    }
	public function is_user_admin(){
		$current_user= $this->get_current_user();
		if($current_user['id']==$this->role_model->Admin_user_id)
			return true;
		else return false;
	}
	
	public function is_user_it(){
		$current_user= $this->get_current_user();
		if($current_user['id']=='549')
			return true;
		else return false;
	}

    public function refreshObject($params)
    {
        $id=(int)$params['id'];
        $name =trim($params['name']);
        $email = trim($params['email']);
        $password = $params['password'];
        $group_id=$params['user_group_id'];
        //$encryptedPassword=$this->utility_model->encrypt($password);
        $user = array(
            'id'=>$id,
            'name' => $name,
            'email' => $email,
            'password' =>$password,
            'group_id'=>$group_id
        );
        //print_r($user);
        return $user;
    }
    /*public function save($params){
        $user=$this->refreshObject($params);

        //validation
        $email=$user['email'];
        $extra_query="";
        if($params['id']!=-1){
            $extra_query=" and id<>".$params['id'];
        }
        $query="select * from user where email='$email' $extra_query";
        $already_exists=$this->prime_model->getByQuery($query);
        if(sizeof($already_exists)>0)
        {
            //$user['password']=$params['password'];
            return array('success'=>false,'msg'=>'this email already exists','user'=>$user);
        }
        if($params['password']!=$params['retypePassword']){
            //$user['password']=$params['password'];
            return array('success'=>false,'msg'=>'password and retype password is not same','user'=>$user);
        }
        //end of validation

        //print_r($params);
        if($params['id']==-1){
            unset($user['id']); // unset id
            //print_r($user);
            $user['password']=$this->utility_model->encrypt($user['password']);
            $this->prime_model->insert('user',$user);
            return array('success'=>true,'msg'=>'Added successfully','user'=>array());
        }
        else {
            //print_r($user);
            $user['password']=$this->utility_model->encrypt($user['password']);
            $this->prime_model->update('user',$user);
            $user['password']=$params['password'];
            return array('success'=>true,'msg'=>'updated successfully','user'=>$user);
        }
    }
    */

    public function save_user($params){
        $user=$this->refreshObject($params);
        //print_r($params);
        if($user['id']==-1){
            unset($user['id']); // unset id
            //print_r($user);
            $user['password']=sha1($user['password']);//$this->utility_model->encrypt($user['password']);
            $this->prime_model->insert_details('user',$user);
        }
        else {
            //print_r($user);
            unset($user['password']); 
            //$user['password']=$this->utility_model->encrypt($user['password']);
            $this->prime_model->update_details('user',$user);
        }
        $success=true;
        if($success)
        {
                return array('success'=>true,'msg'=>'User saved successfully.','test'=>$data);
        }
        else
        {
                return array('success'=>false,'msg'=>'Unable to save user. Please try after sometime','test'=>$data);
        }
    }
    public function get_user_list()
    {
        $admin_group_id=$this->role_model->Admin_group_id;
        //$sql="select * from user_group where id!=$admin_group_id order by name,id";
        
        $sql=  $this->generalQuery." where user.group_id!=$admin_group_id ".$this->orderPart;
        $query=$this->db->query($sql);
        return $query;
        //return $query->result_array();
        /*$this->db->where('user_id', $user_id)->order_by('id asc, name asc');
        return $this->db->get("groups");*/
    }
    public function get_by_id($id) {
        $admin_group_id=$this->role_model->Admin_group_id;
        $sql="select u.*,g.name as group_name from user u inner join user_group g on u.group_id=g.id  where u.id=$id and u.group_id!=$admin_group_id";
        $query=$this->db->query($sql);
        $users= $query->result_array();
        if(sizeof($users)>0){
            //$decrypted=$this->utility_model->decrypt($users[0]['password']);
            //$users[0]['password']=$decrypted;
            return $users[0];
        }
        else return array();
    }
    public function delete_user($id){
        $this->db->where('id', $id);
        $this->db->delete('user');
        return true;
    }

}
