<?php
class contact_model extends CI_Model
{
    var $table = 'customer';
    //var $table = 'project';
	public $queryProject="select * from customer ";
	public $queryContact="select * from customer ";
	//public $queryProject="select * from project ";
	//public $querySultan="select * from customer ";
	
	public $orderPart=" order by customer.id desc";
    

	
	//public $orderPart=" order by client.name,client.id";
    //public $orderPart=" order by client.id desc";


    public function __construct()
    {
        parent::__construct();
        //$this->load->model('utility_model');
        $this->load->helper('date');
        $this->load->database();

    }
	public function add_customer($data){
$result=$this->db->insert('customer',$data);

  $value= $this->db->insert_id(); 
   /*echo "<pre>";
	 print_r($value);
	 die();*/

}
   public function get_customer_details_by_id($id)
    {
        $query=  $this->queryProject. "where customer.id=$id";
        $results=  $this->prime_model->getByQuery($query);
        if(sizeof($results)>0)
            return $results[0];
        else return array();
    }	
public function test(){
	$query = $this->db->query('SELECT name FROM customer');
    return $this->db->query($query)->result();
}
public function add_project($data){
$result=$this->db->insert('project',$data);

   return $this->db->insert_id(); 

}

public function delete_customer($id){
	$this->db->from('customer');
$this->db->where('id',$id);
$this->db->delete();
}
public function edit_project($id){
	 $this->db->select('*');
$this->db->from('project');
$this->db->where('id',$id);
$query=$this->db->get();
$result=$query->result_array();
 return $result;
}
 public function update_project($data,$id)
{
   $this->db->where('id', $id);
   $this->db->update('project', $data);
    
}
	public function is_mobile_already_exist($task,$id,$mobile) {
		if(substr($mobile,0,2)=='88')
			$mobile=substr($mobile,2);
		else if(substr($mobile,0,3)=='+88')
			$mobile=substr($mobile,3); 
        //$mobile=$this->db->escape(trim($mobile)); //It also automatically adds single quotes around the data so you don’t have to do that as well.
		if($task=='add')
			$sql="select * from contact where  mobile like '%$mobile%'";
		else 
			$sql="select * from contact where id<> $id and mobile like '%$mobile%'";
        $contacts=$this->prime_model->getByQuery($sql);
        if(sizeof($contacts)>0)
            return true;
        else return false;
    }
	public function sendsmsmodel($params){
		//print_r($params);
        
        $data = array( 'id'=>$params['id'],
             //will unset during insert
            'contacts' => $params['contacts'],
            'msg' => $params['msg']
            
            
            );


		$contact_id=-1;
        if($params['id']==-1){
            unset($data['id']);
            //$contact_id=$this->prime_model->insert_details('ticket', $data);
			$api_key="";
			$prefix='88';
			$mobile=$data['contacts'];
			//$mobile='01786739367';
			$receiver=$prefix.$mobile;

			$senderid='8809612244244';
			$msg='Dear%20Colleague%0AYour%20Complain%20has%20been%20Received.%0A%0ARegards%2C%0AIT%20Department%0ASonyRangs';

			$sms_dlr=file_get_contents("http://202.164.208.226/smsapi?api_key=$api_key&type=text&contacts=$receiver&senderid=$senderid&msg=$msg");
			//print_r($url);
			$ticket_id=$this->prime_model->insert_ticket_details('ticket', $data);
			$ticket_no=date('My') . str_pad($ticket_id, 4, "0", STR_PAD_LEFT);
			$this->prime_model->update('ticket', array('id'=>$ticket_id,'ticket_no'=>$ticket_no,'sms_dlr'=>$sms_dlr));
			$success=true;
        }
        
        else{
            $contact_id=$params['id'];
			if($data['ticket_status']=='Closed')
			{
			$api_key="";
			$prefix='88';
			$mobile=$data['telephone'];
			//$mobile='01786739367';
			$receiver=$prefix.$mobile;

			$senderid='8809612244244';
			$msg='Dear%20Colleague%0AYour%20Problem%20has%20been%20Solved.%0A%0ARegards%2C%0AIT%20Department%0AUniGroup';

			$sms_dlr=file_get_contents("http://202.164.208.226/smsapi?api_key=$api_key&type=text&contacts=$receiver&senderid=$senderid&msg=$msg");
				
			}
            $data=$this->prime_model->update_details('ticket', $data);
			
            $success=true;
        }
		
		
        if($success){
            return array('success'=>true,'msg'=>'Ticket saved successfully','test'=>$data);
        }
        else{
            return array('success'=>false,'msg'=>'Unable to save Ticket. Please try after sometime','test'=>$data);
        }
    }

    public function save($params){
		//print_r($params);
        $prefix='88';
        $data = array( 'id'=>$params['id'],
             //will unset during insert
        	
            'name' => $params['name'],
            'email' => $params['email'],
			'company_name' => $params['company_name'],
			'department'=>$params['department'],
			'designation'=>$params['designation'],
            'birth_date'=>$params['birth_date'],
            'passport_number'=>$params['passport_number'],
			'issue_date'=>$params['issue_date'],
            'expiry_date'=>$params['expiry_date'],
			'concerned_employee'=>$params['concerned_employee'],
			'lead_source'=>$params['lead_source'],
			'group_name'=>($params['group_name']),
			'mobile'=>$prefix.$params['mobile'],
            'office_phone'=>$params['office_phone'],
            'home_phone'=>$params['home_phone'],
            'mailing_street'=>$params['mailing_street'],
            'mailing_ps'=>$params['mailing_ps'],
            'mailing_city'=>$params['mailing_city'],
            'mailing_postal'=>$params['mailing_postal'],
            'mailing_country'=>$params['mailing_country'],
			'comments'=>$params['comments']
            
            
            

        );
		print_r($data);
		$contact_id=-1;
        if($params['id']==-1){
            unset($data['id']);
            $contact_id=$this->prime_model->insert_details('contact', $data);
			$success=true;
        }
        
        else{
            $contact_id=$params['id'];
            $data=$this->prime_model->update_details('contact', $data);
            $success=true;
        }
		
		//regardless of insert or edit
		if(isset($params['contact_file_uploads'])){
			$file_data=array();
			foreach($params['contact_file_uploads'] as $file_name){
				$file_data[]=array('contact_id'=>$contact_id,'name'=>$file_name);
			}
			$this->prime_model->bulkInsert('contact_files', $file_data);
		}
		//////////////////////////////
		
		//regardless of insert or edit
		if(isset($params['passport_file_uploads'])){
			$file_data=array();
			foreach($params['passport_file_uploads'] as $file_name){
				$file_data[]=array('contact_id'=>$contact_id,'name'=>$file_name);
			}
			$this->prime_model->bulkInsert('passport_files', $file_data);
		}
			//////////////////////////////
      
		
        if($success){
            return array('success'=>true,'msg'=>'Contact saved successfully','test'=>$data);
        }
        else{
            return array('success'=>false,'msg'=>'Unable to save Contact. Please try after sometime','test'=>$data);
        }
    }
	
	   public function get_all_project_details()
    {
          $this->db->select('*');
$this->db->from('project');
$query=$this->db->get();
$result=$query->result_array();
 return $result;

       /* $sql = "select * from project";

        $query = $this->db->query($sql);
        return $query->result();*/
    }
	
	public function get_duplicate_contact_details($id)
    {
		$duplicates=array();
		$sql = "select t1.mobile,t2.mobile,count(*),group_concat(distinct t2.id) as contact_ids from contact t1 inner join contact t2 on t2.mobile like concat('%',t1.mobile) where t1.mobile !='' group by t1.mobile having count(*) > 1 ";
        $result= $this->prime_model->getByQuery($sql);
		foreach($result as $row){
			$temp_result=$this->prime_model->getByQuery("select * from contact where id in (".$row['contact_ids'].")");
			
			
			foreach($temp_result as $item)
				$duplicates[]=$item;
		}
		
		return $duplicates;
    }

    public function get_duplicate_contact_details_demo($id)
    {
		$duplicates=array();
		$sql = "select t1.mobile,t2.mobile,count(*),group_concat(distinct t2.id) as contact_ids from contact t1 inner join contact t2 on t2.mobile like concat('%',t1.mobile) where t1.mobile !='' group by t1.mobile having count(*) > 1 ";
        $result= $this->prime_model->getByQuery($sql);
		foreach($result as $row){
			$temp_result=$this->prime_model->getByQuery("select * from contact where id in (".$row['contact_ids'].")");
			
			
			foreach($temp_result as $item)
				$duplicates[]=$item;
		}
		
		return $duplicates;
    }
	
	   public function get_contact_by_id($id)
    {
        $query=  $this->queryContact. "where contact.id=$id";
        $results=  $this->prime_model->getByQuery($query);
        if(sizeof($results)>0)
            return $results[0];
        else return array();
    }
    public function get_sultan_details_by_id($id){
    	 $query=  $this->querySultan. "where sultan.id=$id";
        $results=  $this->prime_model->getByQuery($query);
        if(sizeof($results)>0)
            return $results[0];
        else return array();
    }

     public function get_project_details_by_id($id){
    	 $query=  $this->queryProjec. "where project.id=$id";
        $results=  $this->prime_model->getByQuery($query);
        if(sizeof($results)>0)
            return $results[0];
        else return array();
    }

       public function get_sultan_by_id()
    {
      $this->db->select('*');
$this->db->from('sultan');
$query=$this->db->get();
$result=$query->result_array();
 return $result;
    }
    
       public function get_contact_by_primary_number($primary_number)
    {
        $sql = "select * from contact where mobile like '%$primary_number%' or office_phone like '%$primary_number%' or home_phone like '%$primary_number%'";
        $query = $this->db->query($sql);
        return $query->result();
    }
	
	public function download_customer_chart($data){

           $this->db->select('*');
            $this->db->from('customer');
           
            if ($data['source']) {
                $this->db->where('source',$data['source']);
            }
            if ($data['location']) {
                $this->db->where('location',$data['location']);
            }
            if ($data['interested_project']) {
                $this->db->where('interested_project',$data['interested_project']);
            }
           /* if ($data['start_date']) {
                 $this->db->where('date >=', $data['start_date']); 
            } if ($data['end_date']) {
                 $this->db->where('date <=', $data['end_date']); 
            }*/
            /*$this->db->where('date >=', $data['start_date']); 
          $this->db->where('date <=',$data['end_date']); */
          //  $this->db->where('date <=',$end_date);
            /*$this->db->where('order_date >=', $first_date);
            $this->db->where('order_date <=', $second_date);*/

            $query=$this->db->get();

            $isd_data=$query->result_array();

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=Customer.csv');

            // create a file pointer connected to the output stream
            $output = fopen('php://output', 'w');
            fputcsv($output, array('ID'
			,'Customer Name'
			, 'Phone'
			, 'Email'
			,'Mothers Name'
			,'Location'
			,'Source'
			,'Lead Forward'
			,'Interested Project'
			,'Interested Project Type'
			,'Note'
			,'Birth Date'
			,'Occupation'
			,'Address'
			,'Religion'
			
			));

            foreach($isd_data as $item)
            {
			
			fputcsv($output,array(
			 $item['id']
			,$item['name']
			,$item['phone']
			,$item['email']
			,$item['mothers_name']
			,$item['location']
			,$item['source']
			,$item['lead_forword']
			,$item['interested_project']
			,$item['interested_project_type']
			,$item['note']
			,$item['birthdate']
			,$item['occupation']
			,$item['address']
			,$item['religion'],
			
			
			));
            }

            fclose($output);                 

        }
    
    	
	
	  public function delete_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->table);
	}

	   public function checkmobile($mobile) 
	{
		$this->db->where('mobile',$mobile);
		$query=$this->db->get('sultan');
		if($query->num_rows()>0)
		{
		return 1;	
		}
		else
		{
		return 0;	
		}
    }
	/*public function delete_sultan($id){
		$this->db->from('sultan');
$this->db->where('id',$id);
$this->db->delete();
	}*/

  
	
	


    


}
