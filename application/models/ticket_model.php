<?php
class ticket_model extends CI_Model {
    var $table = 'ticket';
    
    public $queryProject="select * from ticket ";
    

    
    //public $orderPart=" order by client.name,client.id";
    public $orderPart="order by ticket.id desc";
   /* public $generalQuery="select client.*,client_type.visible_name as client_type_name from client left join client_type on client.type=client_type.id  ";*/
    //public $orderPart=" order by client.name,client.id";
	//public $orderPart=" order by client.id desc";
    public function __construct() {
        parent::__construct();
        $this->load->model('utility_model');
        $this->load->helper('date');

    }
      public function get_ticket_details_by_id($id)
    {
        $query=  $this->queryProject. "where ticket.id=$id";
        $results=  $this->prime_model->getByQuery($query);
        if(sizeof($results)>0)
            return $results[0];
        else return array();
    }

        public function edit_ticket($id){
     $this->db->select('*');
$this->db->from('ticket');
$this->db->where('id',$id);
$query=$this->db->get();
$result=$query->result_array();
 return $result;
}
 public function update_ticket($data,$id)
{
   $this->db->where('id', $id);
   $this->db->update('ticket',$data);
    
}
public function delete_ticket($id){

$this->db->from('ticket');
$this->db->where('id',$id);
$this->db->delete();

}


    public function is_ticket_created_by_current_user($client_id){
	$client_info=$this->get_client_by_id($client_id);
        if(sizeof($client_info)==0)
            return false;
		$current_user=$this->user_manager_model->get_current_user();
		if($client_info['created_by']==$current_user['id']){
			return true;
		}
	return false;
    }

    public function refreshObject($params)
    {
        
        
        $id=(int)$params['id'];
        $name =$params['name'];
        $type=$params['client_type'];
        
        $client = array(
            'id'=>$id,
			'new_code'=>trim($params['new_code']),
			'billing_row'=>trim($params['billing_row']),
			'total_channel'=>trim($params['total_channel']),
			'code'=>trim($params['code']),
            'name' => $params['name'],
            'type' => $params['client_type'],
            'address'=>$params['address'],
            'contact_name'=>$params['contact_name'],
            'contact_no'=>$params['contact_no'],
            'web_name'=>$params['web_name'],
            'password'=>$params['password'],
            'call_rate_local'=>$params['call_rate_local'],
            'pulse_local'=>$params['pulse_local'],
            'pulse_isd'=>$params['pulse_isd'],
			'monthly_line_rent'=>$params['monthly_line_rent'],
            'is_active'=>$params['is_active'],
			'is_processed'=>0,
			'zone'=>$params['zone']
			,'billing'=>$params['billing']
			,'mail'=>$params['mail']
			
            
        );
        return $client;
    }

    public function save($params){
        $client=$this->refreshObject($params);
        
        $success=false;
        $client_id=$client['id'];


        $is_new=false;
        $client_old=$this->get_client_by_id($client_id);
        $previous_toll_free_numbers_of_this_client=$this->get_toll_free_numbers_in_single_array_by_client_id($client_id);
        $previous_short_codes_of_this_client=$this->get_short_codes_in_single_array_by_client_id($client_id);

        if($client['id']==-1){
            unset($client['id']); // unset id
            $this->prime_model->insert_details('client',$client); 
            $client_id=$this->db->insert_id();
            $this->save_short_code($params, $client_id);
            $this->save_toll_free($params, $client_id);
            $is_new=true;

        }
        else {            
            $this->prime_model->update_details('client',$client);
            $this->save_short_code($params, $client_id);
            $this->save_toll_free($params, $client_id);
        }
        
        $success=true;
        if($success){
            //mail
            if($is_new){
                $this->prime_model->send_mail('New Client',$this->get_mail_text($is_new,$client_id,$client_old,$client,$previous_short_codes_of_this_client,$previous_toll_free_numbers_of_this_client,$params));
            }
            else {
                $this->prime_model->send_mail('Client Modification',$this->get_mail_text($is_new,$client_id,$client_old,$client,$previous_short_codes_of_this_client,$previous_toll_free_numbers_of_this_client,array_filter($params)));
            }
            //end of mail
            return array('success'=>true,'msg'=>'Client saved successfully');
        }
        else{
            return array('success'=>false,'msg'=>'Unable to save Client. Please try after sometime');
        }
    }
    public function get_mail_text($is_new,$client_id,$client_old,$client_new,$previous_short_codes_of_this_client,$previous_toll_free_numbers_of_this_client,$params){
        $current_user=$this->user_manager_model->get_current_user();
        $datestring = "%d-%m-%Y  %H:%i:%a";
        $time = time();
        $date=mdate($datestring, $time);
        if($is_new){
            //$msg_txt=$current_user['name']." has created New client: ".$client_new['name']." at $date";
            $msg_txt=$current_user['name']." has created new client: [ ".$client_new['name']." ] at $date.";
        }
        else{
            $changes=array();
            //$client_old=$this->get_client_by_id($client_id);
            foreach ($client_new as $key => $value) {
                if(in_array($key,array('id','is_processed'))){
                    continue;
                }
                else if($client_new[$key]!=$client_old[$key]){
                    if($key=='type'){
                        $changes[]=" changed client type [ ".$this->get_client_type($client_old[$key])." ] to [ ".$this->get_client_type($client_new[$key])." ]";
                    }
                    else {
                        $changes[]=" changed $key [".$client_old[$key]."] to [".$client_new[$key]."]";
                    }

                }
            }
            //$previous_toll_free_numbers_of_this_client=$this->get_toll_free_numbers_in_single_array_by_client_id($client_id);

            $result_toll_1 = array_diff($previous_toll_free_numbers_of_this_client, $params['toll_free_number']);
            if(sizeof($result_toll_1)>0)
            {
                $changes[]=" deleted toll free number(s) (".implode($result_toll_1)." )";
            }
            $result_toll_2 = array_diff( array_filter($params['toll_free_number']),$previous_toll_free_numbers_of_this_client);
            if(sizeof($result_toll_2)>0)
            {
                $changes[]=" added toll free number(s) (".implode($result_toll_2)." )";
            }



            //$previous_short_codes_of_this_client=$this->get_short_codes_in_single_array_by_client_id($client_id);
            $result_short_1 = array_diff($previous_short_codes_of_this_client, $params['short_code']);
            if(sizeof($result_short_1)>0)
            {
                $changes[]=" deleted short code(s) (".implode($result_short_1)." )";
            }
            $result_short_2 = array_diff( array_filter($params['short_code']),$previous_short_codes_of_this_client);
            if(sizeof($result_short_2)>0)
            {
                $changes[]= " added short code(s) ( ".implode($result_short_2)." )";
            }


            if(sizeof($changes)>0){
                $msg_txt=$current_user['name']." has ".implode(' and ',$changes)." of client: [".$client_old['name']." ] at $date.";
            }
            else{
                $msg_txt=$current_user['name']." has edit client: [".$client_old['name']." ] at $date. No change made. ";
            }
        }
        return $msg_txt;


    }
        public function add_ticket($data){
         $result=$this->db->insert('ticket',$data);
/*echo "<pre>";
     print_r($result);
     die();*/
   return $this->db->insert_id(); 
    }
        public function add_landowner($data){
        $result=$this->db->insert('landowner',$data);
        /*echo "<pre>";
         print_r($result);
        die();*/
        return $this->db->insert_id(); 
    }
    public function add_investor($data){
        $result=$this->db->insert('investor',$data);
        /*echo "<pre>";
         print_r($result);
        die();*/
        return $this->db->insert_id(); 
    }
     public function add_career($data){
$result=$this->db->insert('career',$data);
/*echo "<pre>";
     print_r($result);
     die();*/
   return $this->db->insert_id(); 
    }
	public function download_client_excel(){
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Client List');
        //$query=$this->session->userdata ( 'query_smsc_report');
		$sql=  "select client.*,client_type.visible_name as client_type_name ".
				",(select group_concat(s.short_code) from  client_short_code s where s.client_id=client.id group by s.client_id ) as `short_codes` ".
				",(select group_concat(t.toll_free_number) from  client_toll_free t where t.client_id=client.id group by t.client_id ) as `toll_free_numbers` ".
				",(select u.name from user u where  u.id=client.created_by) as `created_by_name` ".
				" from client left join client_type on client.type=client_type.id ".  $this->orderPart;
        $report_data=  $this->prime_model->getByQuery($sql);
        
        $i=1;
        
		
        
		$data_array=array();
		$data_array[]=array("Name","Type","Address","Contact Name","Contact No","Web Name","Password","Local Call Rate","Local Pulse","ISD Pulse","Monthly Line Rent","Status","Short Codes","Toll free Numbers","Created By");
		$cell_to_start="A$i";
        foreach ($report_data as $item){
			
			$data_array[]=array($item['name']
								,$item['client_type_name']
								,$item['address']
								,$item['contact_name']
								,$item['contact_no']
								,$item['web_name']
								,$item['password']
								,$item['call_rate_local']
								,$item['pulse_local']
								,$item['pulse_isd']
								,$item['monthly_line_rent']
								,$item['is_active']?'Active':'Deactive'
								,$item['short_codes']
								,$item['toll_free_numbers']
								,$item['created_by_name']
								);
			
			
        }
        $this->excel->getActiveSheet()->fromArray($data_array, null, $cell_to_start);
        
        $filename='client_list.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        
    }
    public function get_list()
    {
        $sql=  $this->generalQuery.  $this->orderPart;
        return $this->prime_model->getByQuery($sql) ;
    }
    
    public function get_by_id($client_id)
    {
        $query=  $this->generalQuery. "where client.id=$client_id";
        $results=  $this->prime_model->getByQuery($query);
        if(sizeof($results)>0)
            return $results[0];
        else return array();
    }
    public function is_new_code_already_exist($id,$new_code) {
		$new_code=$this->db->escape(trim($new_code)); //It also automatically adds single quotes around the data so you don’t have to do that as well. 
        $sql="select * from client where id<> $id and new_code=$new_code";
        $clients=$this->prime_model->getByQuery($sql);
        if(sizeof($clients)>0)
            return true;
        else return false;
    }

    public function delete($id){
        //$this->db->where('id', $id);
        //$this->db->delete('client');
        //for mail
        $client_old=$this->get_client_by_id($id);
        if($client_old['is_active']==1){
            $subject="Client Deactivation";
            $new_state='Deactivated';
	    $value_to_set=0;
        }

        else {
            $subject="Client Activation";
            $new_state='Activated';
	    $value_to_set=1;
        }

        $current_user=$this->user_manager_model->get_current_user();
        $datestring = "%d-%m-%Y  %H:%i:%a";
        $time = time();
        $date=mdate($datestring, $time);
        $msg_txt=$current_user['name']." has $new_state client: [ ".$client_old['name']." ] at $date .";
        //$msg_txt="test sms";

        $this->prime_model->executeQuery("update client set is_active= $value_to_set,is_processed=0 where id=$id");

        //for mail
        $this->prime_model->send_mail($subject,$msg_txt);

        return true;		
    }

}
