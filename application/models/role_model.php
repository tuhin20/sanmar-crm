<?php
class role_model extends CI_Model {
	public $Manage_users="manage_users";
    public $Change_login_credentials="change_login_credentials";
    public $Manage_user_groups="manage_user_groups";
    public $Add_product_sale="add_product_sale";
	public $Import_product_sale="import_product_sale";
	public $Add_customer="add_customer";
	public $Download_contact="download_contact";
	public $Import_contact="import_contact";
	//Ticket
	public $Add_ticket="add_ticket";
	public $Edit_ticket="edit_ticket";
	public $Delete_ticket="delete_ticket";
	public $View_ticket_details="view_ticket_details";
	public $View_ticket_list= "view_ticket_list";
	////
    
    /*public $Admin_group_id=1;
    public $Admin_user_id=1;*/


    /*
     * public  $See_schedules="see_schedules";
    public  $Book_schedule="book_schedule";
    public  $View_single_schedule="view_single_schedule";
    public  $Approve_schedule="approve_schedule";
    public  $Cancel_schedule="cancel_schedule";
    public  $Manage_vehicles="manage_vehicles";
    public  $Manage_user_groups="manage_user_groups";
     */
     

    public function __construct() {
        parent::__construct();
        $this->load->model('utility_model');
        //$this->load->model('user_model');
        $this->load->model('prime_model');
    }
    public function get_all(){
        $query="select * from role";
        return $this->prime_model->getByQuery($query);
    }
    public function get_by_group($group_id){
        $query="select * from role where id in ( select role_id from permission where group_id=$group_id)";
        return $this->prime_model->getByQuery($query);
    }
    

}
