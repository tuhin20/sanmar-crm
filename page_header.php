<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Dhallywood24 Portal</title>
		<link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>css/sb-admin.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url(); ?>css/custom.css" rel="stylesheet" type="text/css">
		<link href="<?php echo base_url(); ?>css/bootstrapValidator.min.css" rel="stylesheet" type="text/css">	
	</head>
	
	<body>
		<div id="wrapper">
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Dhallywood24 Portal Admin Panel</a>
				</div>
				<ul class="nav navbar-right top-nav">							
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo strtoupper($this->session->userdata ( 'user_name')); ?> <b class="caret"></b></a>
						<ul class="dropdown-menu">							
							<li>
								<a href="<?php echo base_url(); ?>index.php/login/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
							</li>
						</ul>
					</li>
				</ul>
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav side-nav">
						<?php 
							foreach($CATEGORY as $CATEGORY_ID => $CATEGORY_NAME)
							{
						?>
						<li>
							<a href="javascript:;" data-toggle="collapse" data-target="#<?php echo str_replace(' ', '_', $CATEGORY_NAME); ?>"><i class="fa fa-fw fa-arrows-v"></i> <?php echo $CATEGORY_NAME; ?> <i class="fa fa-fw fa-caret-down"></i></a>							
							<ul id="<?php echo str_replace(' ', '_', $CATEGORY_NAME); ?>" class="collapse">
							<?php 
								foreach($SUB_CATEGORY[$CATEGORY_ID] as $SUB_CATEGORY_ID => $SUB_CATEGORY_NAME )
								{
								?>									
									<li><a href="<?php echo base_url(); ?>index.php/contents/item_list/<?php echo $CATEGORY_ID; ?>/<?php echo $SUB_CATEGORY_ID; ?>"><?php echo $SUB_CATEGORY_NAME; ?></a></li>														
								<?php
								}
							?>
							</ul>
						</li>						
						<?php
							}
						?>
					</ul>
				</div>
			</nav>

			<div id="page-wrapper">
				<div class="container-fluid" style="text-align:center; padding:20px 0px; width:80%;">		