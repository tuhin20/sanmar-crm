-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `team_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `id` (`id`),
  KEY `group_id` (`group_id`),
  KEY `group_id_2` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `group_id`, `name`, `email`, `password`, `phone`, `team_id`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(1,	1,	'admin',	'admin@metro.net.bd',	'8cb2237d0679ca88db6464eac60da96345513964',	'01672034372',	4,	1,	'2018-08-14 19:42:06',	NULL,	NULL),
(574,	0,	'test',	'test@gmail.com',	'8cb2237d0679ca88db6464eac60da96345513964',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(575,	2,	'tuhin',	'tuhin@gmail.com',	'8cb2237d0679ca88db6464eac60da96345513964',	'12345566',	1,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(577,	1,	'sanmar',	'sanmar@gmail.com',	'8cb2237d0679ca88db6464eac60da96345513964',	'12345566',	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(578,	2,	'user1',	'user1@gmail.com',	'032e372be76f5f635105aa8346c8db7c50e1ea79',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(579,	2,	'user2',	'user2@gmail.com',	'ff7013e00df1bdf24edf6ae9cb68f04c0401079b',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(580,	2,	'user3',	'user3@gmail.com',	'e22fa6d7a29d8176837a4e56e114547694d01ee4',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(581,	2,	'user4',	'user4@gmail.com',	'366d8aac3264061783933b2bbb05e0b22e330c88',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(582,	2,	'user5',	'user5@gmail.com',	'95fb99d8c03239ba7a78134502a5f07e5674e509',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(583,	2,	'user6',	'user6@gmail.com',	'5f600481179bcde4487467b751c4aaa568ce6e9f',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(584,	2,	'user7',	'user@gmail.com',	'539dedc177be582530a145b7a9c3aa9b8a25a16e',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(585,	2,	'user8',	'user8@gmail.com',	'7c5287dbc3fe870602b9cf89ad7ac662fe4f31bb',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(586,	2,	'user9',	'user9@gmail.com',	'85a4034ff789c0ff51be7aa4c5adc30e2277f7ae',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(587,	2,	'user10',	'user10@gmail.com',	'9a16762632d3c20c1d555262f24a88ffed980ee6',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(588,	0,	'user11',	'user11@gmail.com',	'e8537046e76806929e3c0b094f4c21733a7cd717',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(589,	0,	'user12',	'user12@gmail.com',	'9a5e94a57d5dcf9849242a4dce602ef77c0a1e40',	NULL,	0,	0,	'0000-00-00 00:00:00',	NULL,	NULL),
(590,	3,	'csuser',	'csuser@gmail.com',	'bafd405213d842b04a1686402b6ee6e399ca1eb2',	NULL,	0,	0,	'2023-01-04 12:56:40',	NULL,	NULL),
(591,	4,	'maintenance',	'maintenance@gmail.com',	'ffd9e95829925ff3a13ab198ba6b91690f19de33',	NULL,	0,	0,	'2023-01-05 18:09:55',	NULL,	NULL),
(592,	5,	'Lifestyle',	'lifestyle@gmail.com',	'752115fd61af07fea48ad220b1801856528c73b0',	'12345566',	0,	0,	'2023-01-17 18:39:03',	NULL,	NULL);

-- 2023-01-22 11:04:27
